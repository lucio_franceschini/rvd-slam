# Rendezvous and Docking Simultaneous Localization and Mapping (RvD SLAM)

## Build Environment

* Windows 10 x64
* Visual Studio 2012

## Software Dependencies / External Libraries

* cpu_tsdf
* Cuda 8.0
* Eigen 3.2.8
* fastfusion
* Kinect 1.8 
* Kinect 2.0
* MRPT 1.3.2
* OpenCV 3.0.0
* Point Cloud Library (PCL) 1.7.2
* Qt 5.2.1 
* VTK 6.3.0
* ZED 1.2.0

## Student Thesis

* Tim Wiese: \\\nas.ads.mwn.de\TUMW\lrt\Staff\01-Lehre\06-Studienarbeiten\01-Semesterarbeiten\Abgeschlossen\SA_2015_10_Wiese

## Installation

1. Checkout repository
   git clone https://gitlab.lrz.de/racoon-projects/rvdslam.git
2. Add external libaries for all dependencies to ..\\external from [server](\\nas.ads.mwn.de\tumw\lrt\Staff\04-Infrastruktur\09-RACOON-Lab\06_Code\external)
3. Install Qt 5.2.1 (qt-opensource-windows-x86-msvc2012_64_opengl-5.2.1)
4. Install the Qt Visual Studio integration (qt-vs-addin-1.2.5)
5. Edit the project's Qt options and select path for Qt version 5.2.1 (..\\external\Qt\5.2.1\msvc2012_64_opengl)


      
      