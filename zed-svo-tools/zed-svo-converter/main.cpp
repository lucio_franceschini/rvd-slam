#include <HPSLoader.h>
#include <RealsenseCameraLoader.h>
#include <ZedCameraLoader.h>
#include <date/tz.h>
#include <io_data.h>

#include <cxxopts.hpp>
#include <exception>
#include <vector>

constexpr auto sourceDirArgName = "source_dir";
constexpr auto outputDirArgName = "output_dir";
constexpr auto zedChannelsArgName = "ZED_channels";
constexpr auto d435ChannelsArgName = "D435_channels";
constexpr auto hps160ChannelsArgName = "HPS160_channels";
constexpr auto tzPath = ".\\tzdata";
constexpr auto recordingsBaseName = "recording";

const std::vector<converter::RealsenseStreamProfile> d435AvailableStreams = {
    converter::RealsenseStreamProfile{
        io::d435DepthGroupName,
        RS2_STREAM_DEPTH,
        -1},
    converter::RealsenseStreamProfile{"LEFT_INFRARED", RS2_STREAM_INFRARED, 1},
    converter::RealsenseStreamProfile{"RIGHT_INFRARED", RS2_STREAM_INFRARED, 2},
    converter::RealsenseStreamProfile{"COLOR", RS2_STREAM_COLOR, -1},
};

const std::vector<converter::ZedStreamProfile> zedAvailableStreams = {
    converter::ZedStreamProfile{"VIEW_LEFT", sl::VIEW::LEFT},
    converter::ZedStreamProfile{"VIEW_RIGHT", sl::VIEW::RIGHT},
    converter::ZedStreamProfile{io::zedDepthGroupName, sl::VIEW::DEPTH},
    converter::ZedStreamProfile{"VIEW_CONFIDENCE", sl::VIEW::CONFIDENCE},
};

auto splitString(const std::string_view source, const char delimiter = ',')
{
  std::vector<std::string> segments;

  auto from = std::cbegin(source);
  const auto end = std::cend(source);

  while (from != end)
  {
    const auto it = std::find_if(from, end, [delimiter](const char param) {
      return param == delimiter;
    });
    segments.emplace_back(from, it);
    from = it;
    if (from != end)
    {
      ++from;
    }
  }
  return segments;
}

template <typename T>
auto stringsToStreamProfiles(
    const std::vector<std::string>& streamNames,
    const std::vector<T>& streamsReference)
{
  std::vector<T> streams;
  const auto begin = std::cbegin(streamsReference);
  const auto end = std::cend(streamsReference);
  for (const auto& streamName : streamNames)
  {
    const auto it = std::find_if(begin, end, [&streamName](const T& source) {
      return source.name == streamName;
    });
    if (it != end)
    {
      streams.push_back(*it);
    }
    else
    {
      if (streamName == io::noneChannel)
      {
        return decltype(streams){};
      }
      else
      {
        throw std::runtime_error("Unknown channel: " + streamName);
      }
    }
  }
  return streams;
}

void exportFolderToNetCDF(
    const std::filesystem::path& sourceDir,
    const std::filesystem::path& destinationDir,
    const std::vector<converter::RealsenseStreamProfile>& d435Streams,
    const std::vector<converter::ZedStreamProfile>& zedStreams,
    const bool exportHps160)
{
  const auto sessionTimestamp =
      io::stringToTimestamp(sourceDir.filename().string());
  const auto outputPath =
      destinationDir /
      (io::timestampToString(sessionTimestamp) + io::outputFileExtension);

  const auto fileMode = [&outputPath] {
    if (std::filesystem::exists(outputPath))
    {
      return netCDF::NcFile::write;
    }
    else
    {
      return netCDF::NcFile::newFile;
    }
  }();

  auto session = netCDF::NcFile(outputPath.string(), fileMode);

  if (fileMode == netCDF::NcFile::newFile)
  {
    io::saveMetadata(
        session,
        {{io::timestampMetadata, io::timestampToString(sessionTimestamp)}});
  }

  const auto sessionMetadata = sourceDir / io::sessionMetadataFilename;
  if (std::filesystem::exists(sessionMetadata))
  {
    std::cout << "Exporting metadata" << std::endl;
    io::saveMetadata(session, YAML::LoadFile(sessionMetadata.string()));
  }

  const auto zedFilePath =
      sourceDir / (recordingsBaseName + std::string(".svo"));
  if (!zedStreams.empty())
  {
    std::cout << "Exporting ZED..." << std::endl;
    const auto zed = converter::loadZedCameraLoader(zedFilePath);
    const auto sensorZed = io::readOrCreateNode(session, io::zedName);
    exportSensorsRawDatas(zed, sensorZed, zedStreams, std::cout);
  }

  const auto d435FilePath =
      sourceDir / (recordingsBaseName + std::string(".bag"));
  if (!d435Streams.empty())
  {
    std::cout << "Exporting D435..." << std::endl;
    const auto realsenseData = converter::loadRealsenseCameraData(
        d435FilePath,
        sourceDir / (recordingsBaseName + std::string(".json")));
    const auto sensorD435 = io::readOrCreateNode(session, io::d435Name);
    exportSensorsRawDatas(realsenseData, sensorD435, d435Streams, std::cout);
  }

  const auto hps160DirPath = sourceDir / recordingsBaseName;
  if (exportHps160)
  {
    std::cout << "Exporting HPS-160..." << std::endl;
    const auto sensorHps160Name = io::readOrCreateNode(session, io::hps160Name);
    converter::exportHps160RawData(hps160DirPath, sensorHps160Name, std::cout);
  }
}

int main(int argc, char** argv)
{
  try
  {
    date::set_install(tzPath);
    cxxopts::Options options("Recorder", "Record data from the RACOON-Lab");
    options.add_options()(
        sourceDirArgName,
        "Where to find the source data",
        cxxopts::value<std::filesystem::path>())(
        outputDirArgName,
        "Where to save the resulting dataset",
        cxxopts::value<std::filesystem::path>())(
        zedChannelsArgName,
        "Which channels should be exported from the ZED",
        cxxopts::value<std::vector<std::string>>()->default_value({}))(
        d435ChannelsArgName,
        "Which channels should be exported from the D435",
        cxxopts::value<std::vector<std::string>>()->default_value({}))(
        hps160ChannelsArgName,
        "Which channels should be exported from the HPS-160",
        cxxopts::value<std::vector<std::string>>()->default_value({}));

    const auto args = options.parse(argc, argv);

    const auto zedStreams =
        stringsToStreamProfiles<converter::ZedStreamProfile>(
            args[zedChannelsArgName].as<std::vector<std::string>>(),
            zedAvailableStreams);

    const auto d435Streams =
        stringsToStreamProfiles<converter::RealsenseStreamProfile>(
            args[d435ChannelsArgName].as<std::vector<std::string>>(),
            d435AvailableStreams);

    const auto exportHps160 = [&args] {
      const auto channels =
          args[hps160ChannelsArgName].as<std::vector<std::string>>();
      if (channels.size() == 1)
      {
        if (channels[0] == "DEPTH")
        {
          return true;
        }
        else if (channels[0] == io::noneChannel)
        {
          return false;
        }
      }
      throw std::runtime_error("Unknown channel");
    }();
    exportFolderToNetCDF(
        args[sourceDirArgName].as<std::filesystem::path>(),
        args[outputDirArgName].as<std::filesystem::path>(),
        d435Streams,
        zedStreams,
        exportHps160);
  }
  catch (const std::exception& ex)
  {
    std::cout << "Exception: " << ex.what() << std::endl;
  }

  std::cout << "Exporting completed" << std::endl;
  return 0;
}
