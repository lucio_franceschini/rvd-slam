#include <array>

#include "units.h"

namespace io
{
// Groups
constexpr auto zedName = "ZED";
constexpr auto d435Name = "D435";
constexpr auto hps160Name = "HPS-160";
constexpr auto filtersGroupName = "FILTERS";
constexpr auto algorithmsGroupName = "ALGORITHMS";
constexpr auto sessionsGroupName = "SESSIONS";
constexpr auto pathsGroupName = "PATHS";
constexpr auto zedDepthGroupName = "VIEW_DEPTH";
constexpr auto d435DepthGroupName = "DEPTH";
constexpr auto hps160DepthGroupName = "DEPTH";
constexpr auto modulesDataGroupName = "MODULES_DATA";
constexpr auto sourcesGroupName = "SOURCES";

// Metadata
constexpr auto timestampMetadata = "timestamp";
constexpr auto filterIdMetadata = "filter_id";
constexpr auto filterAliasMetadata = "alias";
constexpr auto algorithmIdMetadata = "algorithm_id";
constexpr auto algorithmAliasMetadata = "alias";
constexpr auto sensorAttributeName = "sensor";
constexpr auto sessionTimestampAttributeName = "session_timestamp";

// Variables/Dimensions
constexpr auto dataFieldName = "data";
constexpr auto positionVarName = "position";
constexpr auto orientationVarName = "orientation";
constexpr auto confidenceVarName = "confidence";
constexpr auto orientationAxesDimName = "quaternion_axes";
constexpr auto positionAxesDimName = "position_axes";
constexpr auto timestampDimName = "timestamp";
constexpr auto color3ChannelsAxesVarName = "color_channels";
constexpr auto color4ChannelsAxesVarName = "color_alpha_channels";
constexpr auto uAxisVarName = "u";
constexpr auto vAxisVarName = "v";

// https://howardhinnant.github.io/date/date.html#from_stream_formatting
constexpr auto timestampFormat = "%Y%m%dT%H%M%S%z";

// Attributes of the variables
constexpr auto nameAttrName = "name";
constexpr auto descriptionAttrName = "description";
constexpr auto unitsAttrName = "units";

constexpr auto outputFileExtension = ".nc";
constexpr auto sessionMetadataFilename = "session_metadata.yaml";

constexpr auto positionAxesDimSize = 3;
constexpr std::array<const char*, positionAxesDimSize> positionAxesValues = {
    "x",
    "y",
    "z"};

constexpr auto orientationAxesDimSize = 4;
constexpr std::array<const char*, orientationAxesDimSize>
    orientationAxesValues = {"x", "y", "z", "r"};

constexpr auto unitsTimestampAttrPreamble = "seconds since ";

constexpr auto timestampDimLocation = 0;
constexpr auto vDimLocation = 1;
constexpr auto uDimLocation = 2;
constexpr auto wDimLocation = 3;
constexpr auto xyzDimLocation = 1;
constexpr auto xyzrDimLocation = 1;

constexpr auto dimensionUVSize = 2;

constexpr auto dimension3ChannelVarSize = 3;
constexpr auto dimension4ChannelVarSize = 4;
const std::array<const char*, 4> color4ChannelsAxesData = {"r", "g", "b", "a"};
const std::array<const char*, 3> color3ChannelsAxesData = {"r", "g", "b"};

// Formats
constexpr auto zedOutputFormat = "svo";
constexpr auto realsenseOutputFormat = "bag";
constexpr auto hps160OutputFormat = "pcl";

constexpr auto noneChannel = "NONE";

const auto pathUnit = UNIT::METER;
constexpr auto hps160HorizontalFieldOfView = Degree(76).toRadiant();
constexpr auto hps160VerticalFieldOfView = Degree(32).toRadiant();
}  // namespace io
