#include "sensors.h"

namespace sensors
{
namespace
{
const auto sensorsNames = [] {
  std::array<std::string, 3> names;
  names[static_cast<int>(Sensor::ZED)] = io::zedName;
  names[static_cast<int>(Sensor::D435)] = io::d435Name;
  names[static_cast<int>(Sensor::HPS160)] = io::hps160Name;
  return names;
}();

const auto depthGroupNames = [] {
  std::array<std::string, 3> names;
  names[static_cast<int>(Sensor::ZED)] = io::zedDepthGroupName;
  names[static_cast<int>(Sensor::D435)] = io::d435DepthGroupName;
  names[static_cast<int>(Sensor::HPS160)] = io::hps160DepthGroupName;
  return names;
}();
}  // namespace
std::string sensorName(const Sensor sensor)
{
  return sensorsNames[static_cast<int>(sensor)];
}

std::string depthGroupName(const Sensor sensor)
{
  return depthGroupNames[static_cast<int>(sensor)];
}

Sensor detectSensor(const std::string& sensorName)
{
  const auto it =
      std::find(sensorsNames.begin(), sensorsNames.end(), sensorName);
  if (it != sensorsNames.end())
  {
    const auto distance = std::distance(sensorsNames.begin(), it);
    return static_cast<Sensor>(distance);
  }
  else
  {
    throw std::logic_error("Wrong sensor name");
  }
}
}  // namespace sensors
