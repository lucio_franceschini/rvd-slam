#include "units.h"
#include <gsl/gsl>

auto& operator<<(std::ostream& out, const Pixel q)
{
  out << q.value() << " px";
  return out;
}

auto& operator<<(std::ostream& out, const FullPixel q)
{
  out << q.value() << " px";
  return out;
}

auto& operator<<(std::ostream& out, const Degree q)
{
  out << q.value() << "�";
  return out;
}

auto& operator<<(std::ostream& out, const Radiant q)
{
  out << q.value() << " rad";
  return out;
}

constexpr FullPixel operator"" _px(unsigned long long q)
{
  return FullPixel(q);
}

constexpr Radiant operator"" _rad(long double q)
{
  return Radiant(gsl::narrow_cast<double>(q));
}

constexpr Degree operator"" _deg(long double q)
{
  return Degree(gsl::narrow_cast<double>(q));
}

constexpr Pixel operator"" _px(long double q)
{
  return Pixel(gsl::narrow_cast<double>(q));
}

std::string unitToString(const UNIT unit)
{
  // https://www.unidata.ucar.edu/software/udunits/udunits-2-units.html
  switch (unit)
  {
    case UNIT::METER:
      return std::string("meter");
    case UNIT::MILLIMETER:
      return std::string("millimeter");
    case UNIT::RADIANT:
      return std::string("radian");
    case UNIT::DEGREE:
      return std::string("degree");
    case UNIT::PIXEL:
      return std::string("pixel");
    default:
      throw std::logic_error("Not implemented");
  };
}
