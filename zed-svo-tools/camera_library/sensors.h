#pragma once
#include <string>

#include "io_data.h"

namespace sensors
{
enum class Sensor
{
  ZED = 0,
  D435 = 1,
  HPS160 = 2,
};

std::string sensorName(const Sensor sensor);

std::string depthGroupName(const Sensor sensor);

Sensor detectSensor(const std::string& sensorName);

}  // namespace sensors