#pragma once
#ifdef CUDA
#include <iostream>

#include <filesystem>
#include <netcdf>

#pragma warning(push, 0)
#include <sl/Camera.hpp>
#pragma warning(pop)

#include "units.h"

namespace converter
{
struct ZedCameraData
{
  const sl::InitParameters initParameters;
  const sl::RuntimeParameters runtimeParameters;
  const sl::PositionalTrackingParameters trackingParameters;
};

struct ZedStreamProfile
{
  const std::string name;
  const sl::VIEW view;
};

ZedCameraData loadZedCameraLoader(const std::filesystem::path& file);
void exportSensorsRawDatas(
    const ZedCameraData& data,
    const netCDF::NcGroup& sensor,
    const std::vector<ZedStreamProfile>& streams,
    std::ostream& ostream);

std::vector<sl::Pose> exportSdkPath(
    const ZedCameraData& data,
    const int confidenceThreshold,
    const Meter depthMaxRange,
    std::ostream& output);

}  // namespace converter

#endif