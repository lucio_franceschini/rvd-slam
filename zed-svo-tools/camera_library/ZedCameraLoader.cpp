#include "ZedCameraLoader.h"

#include <chrono>
#include <exception>
#include <gsl/gsl>
#include <optional>
#ifdef CUDA
#include "io_data.h"

namespace converter
{
namespace
{
constexpr auto chunkTimeSize = 500;
constexpr auto timeReference = sl::TIME_REFERENCE::IMAGE;

auto slStringToStdString(const sl::String& base)
{
  return std::string(base.c_str());
}

auto setInitParameters(const std::filesystem::path& file)
{
  // First set initial configuration parameters according to API documentation
  // These parameters can't be changed during runtime without re-initialization
  sl::InitParameters initParameters;
  initParameters.input.setFromSVOFile(file.string().c_str());
  initParameters.svo_real_time_mode = false;

  initParameters.depth_mode = sl::DEPTH_MODE::ULTRA;
  initParameters.coordinate_units = sl::UNIT::METER;
  initParameters.coordinate_system = sl::COORDINATE_SYSTEM::IMAGE;

  initParameters.depth_minimum_distance = -1;
  initParameters.camera_disable_self_calib = true;
  initParameters.camera_image_flip = false;
  initParameters.enable_right_side_measure = false;
  initParameters.depth_stabilization = true;

  initParameters.sdk_verbose = true;
  return initParameters;
}

auto setRuntimeParameters()
{
  // Parameters that defines the behavior of the grab
  // These parameters are application specific and should not be changed

  return sl::RuntimeParameters(
      sl::SENSING_MODE::STANDARD,
      true,
      100,
      100,
      sl::REFERENCE_FRAME::CAMERA);
}

auto setTrackingParameters()
{
  // Parameters for position tracking
  return sl::PositionalTrackingParameters(
      sl::Transform(),
      true,
      false,
      sl::String(),
      false,
      true,
      false);
}
auto retrievePoses(
    sl::Camera& zedCamera,
    const sl::RuntimeParameters& runtimeParameters,
    std::ostream& output)
{
  std::vector<sl::Pose> cameraPoses;
  const auto totalFrames = zedCamera.getSVONumberOfFrames();
  while (zedCamera.grab(runtimeParameters) == sl::ERROR_CODE::SUCCESS)
  {
    sl::Pose pose;
    const auto trackingState =
        zedCamera.getPosition(pose, sl::REFERENCE_FRAME::WORLD);
    if (trackingState == sl::POSITIONAL_TRACKING_STATE::OK && pose.valid)
    {
      cameraPoses.push_back(pose);
      output << "Frame: " << cameraPoses.size() + 1 << "/" << totalFrames
             << '\n';
    }
  }
  return cameraPoses;
}

io::Metadata cameraParameters(sl::Camera& zedCamera, const sl::VIEW& view)
{
  const auto cameraInformation = zedCamera.getCameraInformation();
  const auto params = [&cameraInformation, view] {
    switch (view)
    {
      case sl::VIEW::LEFT:
        return cameraInformation.calibration_parameters.left_cam;
      case sl::VIEW::RIGHT:
        return cameraInformation.calibration_parameters.right_cam;
      default:
        throw std::logic_error("Not implemented");
    }
  }();

  return {
      {"fx", params.fx},
      {"fy", params.fy},
      {"cx", params.cx},
      {"cy", params.cy},
      {"h_fov", params.h_fov},
      {"d_fov", params.d_fov},
      {"v_fov", params.v_fov},
      {"distorsion_coeffs_0", params.disto[0]},
      {"distorsion_coeffs_1", params.disto[1]},
      {"distorsion_coeffs_2", params.disto[2]},
      {"distorsion_coeffs_3", params.disto[3]},
      {"distorsion_coeffs_4", params.disto[4]},
      {"fps", cameraInformation.camera_fps},
  };
}

void openCamera(sl::Camera* zedCamera, const sl::InitParameters& initParameters)
{
  // Attempt to open the camera
  if (const auto zedError = zedCamera->open(initParameters);
      zedError != sl::ERROR_CODE::SUCCESS)
  {
    throw std::runtime_error(toString(zedError));
  }
}

auto streamFormat(const ZedStreamProfile& stream)
{
  switch (stream.view)
  {
    case sl::VIEW::LEFT:
    case sl::VIEW::RIGHT:
      return sl::MAT_TYPE::U8_C4;
    case sl::VIEW::DEPTH:
    case sl::VIEW::CONFIDENCE:
      return sl::MAT_TYPE::F32_C1;
    default:
      throw std::logic_error("Not implemented");
  }
}

auto createVariableGroup(
    const sl::MAT_TYPE format,
    const netCDF::NcGroup& streamGroup,
    const netCDF::NcDim& timestampDim,
    const netCDF::NcDim& vDim,
    const netCDF::NcDim& uDim,
    const netCDF::NcDim& thirdDim,
    const size_t chunkSize)
{
  netCDF::NcVar var;
  mrpt::vector_size_t chunks;
  switch (format)
  {
    case sl::MAT_TYPE::U8_C4:
      chunks = {chunkSize, vDim.getSize(), uDim.getSize(), thirdDim.getSize()};
      var = streamGroup.addVar(
          io::dataFieldName,
          netCDF::ncUbyte,
          {timestampDim, vDim, uDim, thirdDim});
      break;
    case sl::MAT_TYPE::F32_C1:
      chunks = {chunkSize, vDim.getSize(), uDim.getSize()};
      var = streamGroup.addVar(
          io::dataFieldName,
          netCDF::ncFloat,
          {timestampDim, vDim, uDim});
      break;
    default:
      throw std::logic_error("Not implemented");
  }
  var.setChunking(netCDF::NcVar::ChunkMode::nc_CHUNKED, chunks);
  return var;
}

auto retrieveMat(sl::Camera& zedCamera, const sl::VIEW view)
{
  auto imageZed = std::make_unique<sl::Mat>();
  if (view == sl::VIEW::DEPTH)
  {
    zedCamera.retrieveMeasure(*imageZed, sl::MEASURE::DEPTH);
  }
  else if (view == sl::VIEW::CONFIDENCE)
  {
    zedCamera.retrieveMeasure(*imageZed, sl::MEASURE::CONFIDENCE);
  }
  else
  {
    zedCamera.retrieveImage(*imageZed, view);
  }
  return imageZed;
}

auto retrieveTimestamp(sl::Camera& zedCamera)
{
  const auto durationNs =
      std::chrono::nanoseconds(zedCamera.getTimestamp(timeReference));

  const auto t = date::sys_time<io::Timestamp::duration>(durationNs);
  return date::to_utc_time<io::Timestamp::duration>(t);
}

void saveCameraParametersAsMetadata(
    const converter::ZedStreamProfile& stream,
    netCDF::NcGroup& group,
    sl::Camera& zedCamera)
{
  switch (stream.view)
  {
    case sl::VIEW::LEFT:
    case sl::VIEW::DEPTH:
      io::saveMetadata(group, cameraParameters(zedCamera, sl::VIEW::LEFT));
      break;
    case sl::VIEW::RIGHT:
      io::saveMetadata(group, cameraParameters(zedCamera, sl::VIEW::RIGHT));
      break;
  }
}

void saveDeviceInformationAsMetadata(
    const netCDF::NcGroup& group,
    sl::Camera& zedCamera)
{
  const auto cameraInformation = zedCamera.getCameraInformation();
  const auto initParams = zedCamera.getInitParameters();
  const auto runtimeParams = zedCamera.getRuntimeParameters();
  const auto trackingParams = zedCamera.getPositionalTrackingParameters();

  io::saveMetadata(
      group,
      {
          {"serial_number", cameraInformation.serial_number},
          {"camera_firmware_version",
           cameraInformation.camera_firmware_version},
          {"camera_model",
           slStringToStdString(sl::toString(cameraInformation.camera_model))},
          {"zed_sdk_build_id", std::string(ZED_SDK_BUILD_ID)},
          {"zed_sdk_major_version", ZED_SDK_MAJOR_VERSION},
          {"zed_sdk_minor_version", ZED_SDK_MINOR_VERSION},
          {"zed_sdk_path_version", ZED_SDK_PATCH_VERSION},
          {"cuda_version", CUDA_VERSION},
          {"init_params_camera_disable_self_calib_bool",
           initParams.camera_disable_self_calib},
          {"init_params_camera_fps", initParams.camera_fps},
          {"init_params_camera_image_flip", initParams.camera_image_flip},
          {"init_params_coordinate_system",
           slStringToStdString(sl::toString(initParams.coordinate_system))},
          {"init_params_coordinate_units",
           slStringToStdString(sl::toString(initParams.coordinate_units))},
          {"init_params_depth_maximum_distance",
           initParams.depth_maximum_distance},
          {"init_params_depth_minimum_distance",
           initParams.depth_minimum_distance},
          {"init_params_depth_mode",
           slStringToStdString(sl::toString(initParams.depth_mode))},
          {"init_params_depth_stabilization", initParams.depth_stabilization},
          {"init_params_enable_image_enhancement_bool",
           initParams.enable_image_enhancement},
          {"init_params_enable_right_side_measure_bool",
           initParams.enable_right_side_measure},
          {"init_params_svo_real_time_mode", initParams.svo_real_time_mode},
          {"init_params_camera_disable_self_calib_bool",
           initParams.camera_disable_self_calib},

          {"runtime_params_confidence_threshold",
           runtimeParams.confidence_threshold},
          {"runtime_params_enable_depth_bool", runtimeParams.enable_depth},
          {"runtime_params_measure3D_reference_frame",
           slStringToStdString(
               sl::toString(runtimeParams.measure3D_reference_frame))},
          {"runtime_params_sensing_mode",
           slStringToStdString(sl::toString(runtimeParams.sensing_mode))},
          {"runtime_params_textureness_confidence_threshold",
           runtimeParams.textureness_confidence_threshold},

          {"positional_tracking_params_enable_area_memory_bool",
           trackingParams.enable_area_memory},
          {"positional_tracking_params_enable_pose_smoothing_bool",
           trackingParams.enable_pose_smoothing},
          {"positional_tracking_params_set_as_static_bool",
           trackingParams.set_as_static},
          {"positional_tracking_params_set_floor_as_origin_bool",
           trackingParams.set_floor_as_origin},

          {"camera_settings_AEC_AGC",
           zedCamera.getCameraSettings(sl::VIDEO_SETTINGS::AEC_AGC)},
          {"camera_settings_BRIGHTNESS",
           zedCamera.getCameraSettings(sl::VIDEO_SETTINGS::BRIGHTNESS)},
          {"camera_settings_CONTRAST",
           zedCamera.getCameraSettings(sl::VIDEO_SETTINGS::CONTRAST)},
          {"camera_settings_EXPOSURE",
           zedCamera.getCameraSettings(sl::VIDEO_SETTINGS::EXPOSURE)},
          {"camera_settings_GAIN",
           zedCamera.getCameraSettings(sl::VIDEO_SETTINGS::GAIN)},
          {"camera_settings_HUE",
           zedCamera.getCameraSettings(sl::VIDEO_SETTINGS::HUE)},
          {"camera_settings_LED_STATUS",
           zedCamera.getCameraSettings(sl::VIDEO_SETTINGS::LED_STATUS)},
          {"camera_settings_SATURATION",
           zedCamera.getCameraSettings(sl::VIDEO_SETTINGS::SATURATION)},
          {"camera_settings_SHARPNESS",
           zedCamera.getCameraSettings(sl::VIDEO_SETTINGS::SHARPNESS)},
          {"camera_settings_WHITEBALANCE_AUTO",
           zedCamera.getCameraSettings(sl::VIDEO_SETTINGS::WHITEBALANCE_AUTO)},
          {"camera_settings_WHITEBALANCE_TEMPERATURE",
           zedCamera.getCameraSettings(
               sl::VIDEO_SETTINGS::WHITEBALANCE_TEMPERATURE)},
      });

  const auto matrixSize = 4;
  for (int row = 0; row < matrixSize; row++)
  {
    for (int column = 0; column < matrixSize; column++)
    {
      io::saveMetadata(
          group,
          {
              {"positional_tracking_params_initial_world_transform_r" +
                   std::to_string(row) + std::to_string(column),
               trackingParams.initial_world_transform
                   .m[row * matrixSize + column]},
          });
    }
  }
}

void setDescriptiveAttributes(const netCDF::NcVar& var, const sl::VIEW view)
{
  switch (view)
  {
    case sl::VIEW::DEPTH:
      io::setUnitAttribute(var, UNIT::METER);
      [[gsl::suppress(es .84)]] var.putAtt(io::nameAttrName, "Depth View");
      [[gsl::suppress(es .84)]] var.putAtt(
          io::descriptionAttrName,
          "Distances from the ZED left camera of the scene");
      break;
    case sl::VIEW::RIGHT:
      [[gsl::suppress(es .84)]] var.putAtt(
          io::descriptionAttrName,
          "Frames from the right camera of the ZED");
      [[gsl::suppress(es .84)]] var.putAtt(io::nameAttrName, "Right View");
      break;
    case sl::VIEW::LEFT:
      [[gsl::suppress(es .84)]] var.putAtt(
          io::descriptionAttrName,
          "Frames from the left camera of the ZED");
      [[gsl::suppress(es .84)]] var.putAtt(io::nameAttrName, "Left View");
      break;
    case sl::VIEW::CONFIDENCE:
      [[gsl::suppress(es .84)]] var.putAtt(
          io::descriptionAttrName,
          "Confidence of the distance calculated by the ZED");
      [[gsl::suppress(es .84)]] var.putAtt(io::nameAttrName, "Confidence");
      break;
    default:
      throw std::logic_error("Not implemented");
  }
}

void exportStreams(
    sl::Camera& zedCamera,
    const sl::RuntimeParameters& runtimeParameters,
    const io::Sensor& sensor,
    const std::vector<ZedStreamProfile>& streams,
    std::ostream& ostream)
{
  saveDeviceInformationAsMetadata(sensor, zedCamera);

  const auto streamsType = [&streams] {
    std::vector<sl::MAT_TYPE> streamsType;
    std::transform(
        std::cbegin(streams),
        std::cend(streams),
        std::back_inserter(streamsType),
        streamFormat);
    return streamsType;
  }();
  const auto streamGroups = [&streams, &sensor, &zedCamera] {
    std::vector<netCDF::NcGroup> outputGroups;
    std::transform(
        std::cbegin(streams),
        std::cend(streams),
        std::back_inserter(outputGroups),
        [&sensor, &zedCamera](const ZedStreamProfile& stream) {
          auto group = sensor.addGroup(stream.name);
          saveCameraParametersAsMetadata(stream, group, zedCamera);
          return group;
        });
    return outputGroups;
  }();
  const auto totalFrames = zedCamera.getSVONumberOfFrames();
  const auto [timestampDims, timestampVars] = [&streamGroups] {
    std::vector<netCDF::NcDim> dimensions;
    std::vector<netCDF::NcVar> variables;
    for (const auto& streamGroup : streamGroups)
    {
      const auto [var, dim] = io::addTimestampsDimension(streamGroup);
      dimensions.push_back(dim);
      variables.push_back(var);
    }
    return std::make_tuple(dimensions, variables);
  }();

  const auto resolution = zedCamera.getCameraInformation().camera_resolution;
  const auto [uDims, vDims, thirdDims] =
      [&streamGroups, &streamsType, &resolution] {
        std::vector<netCDF::NcDim> uDims;
        std::vector<netCDF::NcDim> vDims;
        std::vector<netCDF::NcDim> thirdDims;
        for (auto idx = size_t{0}; idx < streamGroups.size(); idx++)
        {
          const auto& streamGroup = streamGroups[idx];
          const auto& streamType = streamsType[idx];
          uDims.push_back(io::createAndFillUVDim(
              streamGroup,
              io::uAxisVarName,
              gsl::narrow<int>(resolution.width)));
          vDims.push_back(io::createAndFillUVDim(
              streamGroup,
              io::vAxisVarName,
              gsl::narrow<int>(resolution.height)));
          switch (streamType)
          {
            case sl::MAT_TYPE::U8_C4:
              thirdDims.push_back(io::createThirdDimensionRGBA(streamGroup));
              break;
            case sl::MAT_TYPE::F32_C1:
              thirdDims.push_back(netCDF::NcDim());
              break;
            default:
              throw std::logic_error("Not implemented");
          }
        }
        return std::make_tuple(uDims, vDims, thirdDims);
      }();

  const auto dataVars = [&streams,
                         &timestampDims,
                         &streamGroups,
                         &streamsType,
                         &uDims,
                         &vDims,
                         &thirdDims] {
    std::vector<netCDF::NcVar> vars;
    for (auto idx = size_t{0}; idx < streamGroups.size(); idx++)
    {
      const auto& streamGroup = streamGroups[idx];
      const auto& timestampDim = timestampDims[idx];
      const auto& vDim = vDims[idx];
      const auto& uDim = uDims[idx];
      const auto& thirdDim = thirdDims[idx];
      const auto& streamType = streamsType[idx];

      const auto var = createVariableGroup(
          streamType,
          streamGroup,
          timestampDim,
          vDim,
          uDim,
          thirdDim,
          chunkTimeSize);
      setDescriptiveAttributes(var, streams[idx].view);
      vars.push_back(var);
    }
    return vars;
  }();
  std::vector<double> timestamps(
      zedCamera.getSVONumberOfFrames(),
      std::numeric_limits<double>::quiet_NaN());

  std::optional<io::Timestamp> startingDate;
  while (zedCamera.grab(runtimeParameters) == sl::ERROR_CODE::SUCCESS)
  {
    const auto timestamp = retrieveTimestamp(zedCamera);
    const auto frameNumber =
        gsl::narrow<size_t>(zedCamera.getSVOPosition() - 1);

    io::showPercentualCompleted(
        io::toPercentual(
            gsl::narrow_cast<double>(frameNumber),
            gsl::narrow_cast<double>(totalFrames)),
        ostream);

    if (!startingDate)
    {
      timestamps[frameNumber] = 0.0;
      startingDate = timestamp;
    }
    else
    {
      timestamps[frameNumber] =
          io::timestampDifference(timestamp, *startingDate).count();
    }
    for (auto idx = size_t{0}; idx < streamGroups.size(); idx++)
    {
      const auto& streamType = streamsType[idx];
      const auto& dataVar = dataVars[idx];
      const auto mat = retrieveMat(zedCamera, streams[idx].view);

      const auto [start, count] = [&streamType, &resolution, frameNumber] {
        switch (streamType)
        {
          case sl::MAT_TYPE::U8_C4:
            return std::make_tuple(
                std::vector<size_t>{frameNumber, 0, 0, 0},
                std::vector<size_t>{
                    1,
                    resolution.height,
                    resolution.width,
                    io::dimension4ChannelVarSize});
            break;
          case sl::MAT_TYPE::F32_C1:
            return std::make_tuple(
                std::vector<size_t>{frameNumber, 0, 0},
                std::vector<size_t>{1, resolution.height, resolution.width});
          default:
            throw std::logic_error("Not implemented");
        }
      }();
      switch (streamType)
      {
        case sl::MAT_TYPE::U8_C4:
          dataVar.putVar(start, count, mat->getPtr<sl::uchar1>(sl::MEM::CPU));
          break;
        case sl::MAT_TYPE::F32_C1:
          dataVar.putVar(start, count, mat->getPtr<sl::float1>(sl::MEM::CPU));
          break;
        default:
          throw std::logic_error("Not implemented");
      }
    }
  }

  std::for_each(
      std::cbegin(timestampVars),
      std::cend(timestampVars),
      [&startingDate, &timestamps](const netCDF::NcVar& var) {
        [[gsl::suppress(es .84)]] var.putAtt(
            io::unitsAttrName,
            io::timestampAttributeData(*startingDate));
        var.putVar(timestamps.data());
      });
}

}  // namespace

ZedCameraData loadZedCameraLoader(const std::filesystem::path& file)
{
  return {
      setInitParameters(file),
      setRuntimeParameters(),
      setTrackingParameters()};
}

void exportSensorsRawDatas(
    const ZedCameraData& data,
    const netCDF::NcGroup& sensor,
    const std::vector<ZedStreamProfile>& streams,
    std::ostream& ostream)
{
  sl::Camera zedCamera;
  openCamera(&zedCamera, data.initParameters);
  const auto leftCameraParams = cameraParameters(zedCamera, sl::VIEW::LEFT);
  exportStreams(zedCamera, data.runtimeParameters, sensor, streams, ostream);
  zedCamera.close();
}

std::vector<sl::Pose> exportSdkPath(
    const ZedCameraData& data,
    const int confidenceThreshold,
    const Meter depthMaxRange,
    std::ostream& output)
{
  sl::Camera zedCamera;
  auto initParameters = data.initParameters;
  auto runtimeParameters = data.runtimeParameters;
  runtimeParameters.confidence_threshold = confidenceThreshold;

  initParameters.depth_maximum_distance =
      gsl::narrow_cast<float>(depthMaxRange.value());

  openCamera(&zedCamera, initParameters);

  // Attempt to enable tracking
  if (const auto zedError =
          zedCamera.enablePositionalTracking(data.trackingParameters);
      zedError != sl::ERROR_CODE::SUCCESS)
  {
    throw std::runtime_error(toString(zedError));
  }

  return retrievePoses(zedCamera, runtimeParameters, output);
}
}  // namespace converter

#endif