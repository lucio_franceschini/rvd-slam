#pragma once

#include <H5Cpp.h>

#include <any>
#include <array>
#include <chrono>
#include <exception>
#include <filesystem>
#include <map>
#include <netcdf>
#pragma warning(push, 0)
#include <date/tz.h>
#include <mrpt/poses/CPose3DQuat.h>
#include <yaml-cpp/yaml.h>

#include <opencv2/opencv.hpp>
#pragma warning(pop)
#include <string>
#include <vector>

#include "RealsenseCameraLoader.h"
#include "ZedCameraLoader.h"
#include "constants.h"
#include "units.h"

namespace io
{
using Output = netCDF::NcFile;
using Sessions = netCDF::NcGroup;
using Session = netCDF::NcGroup;
using Sensor = netCDF::NcGroup;
using Algorithms = netCDF::NcGroup;
using Algorithm = netCDF::NcGroup;
using Filter = netCDF::NcGroup;

using Metadata = std::map<std::string, std::any>;
using Timestamp = date::utc_time<std::chrono::nanoseconds>;
struct Timestamps
{
  const std::vector<double> timestampsSec;
  const std::string since;
};

struct CameraParameters
{
  // Focal length along x axis
  const Pixel fx;

  // Focal length along y axis
  const Pixel fy;

  // Optical center along x axis
  const Pixel cx;

  // Optical center along y axis
  const Pixel cy;

  const float fps;
};

CameraParameters readCameraParameters(const netCDF::NcGroup& group);

cv::Mat readDepthFrameZed(
    const netCDF::NcGroup& depthGroup,
    const size_t timestampIdx);

cv::Mat readDepthFrameD435(
    const netCDF::NcGroup& depthGroup,
    const size_t timestampIdx);

cv::Mat readDepthFrameHPS160(
    const netCDF::NcGroup& depthGroup,
    const size_t timestampIdx);

template <typename T>
constexpr T extractAttribute(
    const netCDF::NcGroup& group,
    const std::string& attributeName)
{
  const auto attrInfo = group.getAtt(attributeName);
  if constexpr (std::is_same<T, std::string>::value)
  {
    std::string res;
    attrInfo.getValues(res);
    return res;
  }
  if (sizeof(T) != attrInfo.getType().getSize())
  {
    throw std::logic_error("Wrong type");
  }
  T value;
  attrInfo.getValues(&value);
  return value;
}

template <typename T>
constexpr T extractAttribute(
    const netCDF::NcVar& var,
    const std::string& attributeName)
{
  const auto attrInfo = var.getAtt(attributeName);
  if constexpr (std::is_same<T, std::string>::value)
  {
    std::string res;
    attrInfo.getValues(res);
    return res;
  }
  if (sizeof(T) != attrInfo.getType().getSize())
  {
    throw std::logic_error("Wrong type");
  }
  T value;
  attrInfo.getValues(&value);
  return value;
}

template <typename T>
constexpr T extractAttribute(
    const H5::Group& group,
    const std::string& attributeName)
{
  const auto attrInfo = group.openAttribute(attributeName);
  if constexpr (std::is_same<T, std::string>::value)
  {
    std::string result;
    attrInfo.read(attrInfo.getDataType(), result);
    return result;
  }
  else
  {
    T value;
    attrInfo.read(attrInfo.getDataType(), &value);
    return value;
  }
}

Timestamps readTimestamps(const netCDF::NcGroup& depthGroup);

Timestamp stringToTimestamp(
    const std::string source,
    const std::string& format = timestampFormat);

std::string timestampToString(const Timestamp timestamp);
std::string timestampAttributeData(const Timestamp startingDate);

constexpr std::chrono::duration<double> nanosecondsToSeconds(
    const std::chrono::nanoseconds value);

constexpr std::chrono::nanoseconds secondsToNanoseconds(
    const std::chrono::duration<double> seconds);

constexpr std::chrono::nanoseconds nanosecondsFromBeginning(
    const Timestamp instant,
    const Timestamp beginning);

std::chrono::duration<double> timestampDifference(
    const Timestamp instant,
    const Timestamp beginning);

void saveMetadata(const netCDF::NcGroup& output, const Metadata& options);
void saveMetadata(const netCDF::NcGroup& output, const YAML::Node& node);

std::string generatePathId();

Sensor readOrCreateNode(const Session& experiment, const std::string_view name);

std::tuple<netCDF::NcVar, netCDF::NcDim> addTimestampsDimension(
    const netCDF::NcGroup& stream);

netCDF::NcDim createAndFillUVDim(
    const netCDF::NcGroup& stream,
    const std::string& name,
    const int size);

netCDF::NcDim createThirdDimensionRGB(const netCDF::NcGroup& streamGroup);
netCDF::NcDim createThirdDimensionRGBA(const netCDF::NcGroup& streamGroup);

void saveSingleMetadata(
    const netCDF::NcGroup& output,
    const std::string& key,
    const std::any& value);

double toPercentual(double fraction, double total = 1.0);

void setUnitAttribute(const netCDF::NcVar& group, const UNIT unit);

Timestamp readStartingDate(const netCDF::NcVar& var);

void showPercentualCompleted(const double percentual, std::ostream& ostream);
}  // namespace io