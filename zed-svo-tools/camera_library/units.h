#pragma once
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>

#include <exception>

// http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html
enum class UNIT
{
  METER = 0,
  MILLIMETER = 4,
  RADIANT = 1,
  DEGREE = 2,
  PIXEL = 3,
};

std::string unitToString(const UNIT unit);

template <typename T>
class Quantity
{
 private:
  const T quantity;

 public:
  constexpr Quantity(const T value) : quantity{value} {}
  constexpr T value() const { return quantity; }
};

template <typename T>
auto& operator<<(std::ostream& out, const Quantity<T> q)
{
  out << q.value();
  return out;
}

template <typename T>
constexpr Quantity<T> operator-(const Quantity<T>& first)
{
  return Quantity<T>{-first.value()};
}

template <typename T>
constexpr Quantity<T> operator+(
    const Quantity<T>& first,
    const Quantity<T>& second)
{
  return Quantity<T>{first.value() + second.value()};
}

template <typename T>
constexpr Quantity<T> operator-(
    const Quantity<T>& first,
    const Quantity<T>& second)
{
  return Quantity<T>{first.value() + (-second.value())};
}

template <typename T, typename Q>
constexpr Quantity<T> operator*(const Q first, const Quantity<T>& second)
{
  return Quantity<T>{first * second.value()};
}

template <typename T, typename Q>
constexpr Quantity<T> operator*(const Quantity<T>& first, const Q second)
{
  return second * first;
}

template <typename T, typename Q>
constexpr Quantity<T> operator/(const Quantity<T>& first, const Q second)
{
  return Quantity<T>{first.value() / second};
}

class Pixel : public Quantity<double>
{
 public:
  constexpr Pixel(const double v) : Quantity{v} {};
};

class FullPixel : public Quantity<unsigned long long>
{
 public:
  constexpr FullPixel(const unsigned long long v) : Quantity{v} {};
};

class Radiant;

class Degree : public Quantity<double>
{
 public:
  constexpr Degree(const double v) : Quantity{v} {};
  constexpr Radiant toRadiant() const;
};

class Radiant : public Quantity<double>
{
 public:
  constexpr Radiant(const double v) : Quantity{v} {};
  constexpr Degree toDegree() const;
};

class MilliWatt : public Quantity<double>
{
 public:
  constexpr MilliWatt(const double v) : Quantity{v} {};
};

constexpr Radiant Degree::toRadiant() const { return {value() * M_PI / 180.0}; }

constexpr Degree Radiant::toDegree() const { return {value() * 180.0 / M_PI}; }

class Meter : public Quantity<double>
{
 public:
  constexpr Meter(const double v) : Quantity{v} {};
};
