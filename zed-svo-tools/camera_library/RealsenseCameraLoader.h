#pragma once
#include <algorithm>
#include <filesystem>
#include <netcdf>
#include <set>
#include <iostream>

#include "units.h"

#pragma warning(push, 0)
#include <yaml-cpp/yaml.h>
#include <librealsense2/rs.hpp>
#include <opencv2/opencv.hpp>
#pragma warning(pop)

namespace converter
{
struct RealsenseStreamProfile
{
  const std::string name;
  const rs2_stream type;
  const int stream_idx;
};

struct RealsenseCameraData
{
  const rs2::config cfg;
  const YAML::Node advanceSettings;
};

RealsenseCameraData loadRealsenseCameraData(
    const std::filesystem::path& file,
    const std::filesystem::path& advanceSettingsPath);

void exportSensorsRawDatas(
    const RealsenseCameraData& data,
    const netCDF::NcGroup& sensor,
    const std::vector<RealsenseStreamProfile>& streams,
    std::ostream& ostream);
}  // namespace converter