#include "HPSLoader.h"

#define BOOST_USE_WINDOWS_H
#pragma warning(push, 0)
#include <mrpt/utils.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#pragma warning(pop)
#undef BOOST_USE_WINDOWS_H

#include <gsl/gsl>
#include <numeric>

namespace
{
constexpr auto chunkTimeSize = 500;
auto createVariableGroup(
    const netCDF::NcGroup& streamGroup,
    const netCDF::NcDim& timestampDim,
    const netCDF::NcDim& vDim,
    const netCDF::NcDim& uDim,
    const netCDF::NcDim& wDim,
    const size_t chunkSize)
{
  const netCDF::NcVar var = streamGroup.addVar(
      io::dataFieldName,
      netCDF::ncFloat,
      {timestampDim, vDim, uDim, wDim});
  var.setFill(true, std::numeric_limits<float>::quiet_NaN());
  auto chunk = std::vector<size_t>{
      chunkSize,
      vDim.getSize(),
      uDim.getSize(),
      wDim.getSize()};
  var.setChunking(netCDF::NcVar::ChunkMode::nc_CHUNKED, chunk);
  return var;
}
}  // namespace

void converter::exportHps160RawData(
    const std::filesystem::path& rootFolder,
    const netCDF::NcGroup& sensor,
    std::ostream& ostream)
{
  const auto files = [&rootFolder] {
    std::vector<std::filesystem::path> files;
    for (const auto& p : std::filesystem::directory_iterator(rootFolder))
    {
      if (p.path().extension() == ".pcl")
      {
        files.push_back(p);
      }
    }
    std::sort(std::begin(files), std::end(files));
    return files;
  }();
  const auto depthGroup = sensor.addGroup(io::hps160DepthGroupName);
  io::saveMetadata(depthGroup, YAML::LoadFile((rootFolder / "recording.yaml").string()));
  const auto startingDate = [&files] {
    const auto file = files[0].stem();
    return io::stringToTimestamp(file.string());
  }();
  const auto timestamps = [&files, &startingDate] {
    std::vector<double> timestamps;
    std::transform(
        std::cbegin(files),
        std::cend(files),
        std::back_inserter(timestamps),
        [&startingDate](const auto& file) {
          return io::timestampDifference(
                     io::stringToTimestamp(file.stem().string()),
                     startingDate)
              .count();
        });
    return timestamps;
  }();

  const auto [timestampVar, timestampDim] =
      io::addTimestampsDimension(depthGroup);
  [[gsl::suppress(es .84)]] timestampVar.putAtt(
      io::unitsAttrName,
      io::timestampAttributeData(startingDate));
  [[gsl::suppress(es .84)]] timestampVar.putVar(
      {0},
      {timestamps.size()},
      timestamps.data());

  const auto [width, height] = [&files, &depthGroup] {
    const auto file = files[0];
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::io::loadPCDFile(file.string(), cloud);
    return std::make_tuple(cloud.width, cloud.height);
  }();

  const auto [uDim, vDim, wDim] = [width, height, &depthGroup] {
    const netCDF::NcDim uDim = io::createAndFillUVDim(
        depthGroup,
        io::uAxisVarName,
        gsl::narrow<int>(width));
    const netCDF::NcDim vDim = io::createAndFillUVDim(
        depthGroup,
        io::vAxisVarName,
        gsl::narrow<int>(height));

    
    const auto wDim = depthGroup.addDim(io::positionAxesDimName, io::positionAxesDimSize);

    depthGroup.addVar(io::positionAxesDimName, netCDF::ncUint, {wDim})
        .putVar(io::positionAxesValues.data());
    return std::make_tuple(uDim, vDim, wDim);
  }();

  const auto var =
      createVariableGroup(depthGroup, timestampDim, vDim, uDim, wDim, chunkTimeSize);

  io::setUnitAttribute(var, UNIT::MILLIMETER);

  const auto totalFiles = files.size();
  for (size_t idxFile = 0; idxFile < totalFiles; idxFile++)
  {
    io::showPercentualCompleted(
        io::toPercentual(
            gsl::narrow_cast<double>(idxFile),
            gsl::narrow_cast<double>(totalFiles)),
        ostream);

    const auto& file = files[idxFile];
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::io::loadPCDFile(file.string(), cloud);
    const auto start = std::vector<size_t>{idxFile, 0, 0, 0};
    const auto count = std::vector<size_t>{1, height, width, io::positionAxesDimSize};
    const auto pointSize = sizeof(cloud[0]) / sizeof(cloud[0].x);
    const auto distanceBetweenFrames =
        gsl::narrow<ptrdiff_t>(height) *
        gsl::narrow<ptrdiff_t>(width * pointSize);
    const auto distanceRows = gsl::narrow<ptrdiff_t>(width * pointSize);
    [[gsl::suppress(es .84)]] var.putVar(
        start,
        count,
        {1, 1, 1, 1},
        {
            distanceBetweenFrames,
            distanceRows,
            pointSize,
            1,
        },
        &cloud.points[0]);
  }
}
