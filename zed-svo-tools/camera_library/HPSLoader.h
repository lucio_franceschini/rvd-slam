#pragma once
#include <iostream>

#include <filesystem>
#include <vector>
#include <netcdf>
#include <algorithm>

#include "io_data.h"

namespace converter
{
void exportHps160RawData(
    const std::filesystem::path& rootFolder,
    const netCDF::NcGroup& sensor,
    std::ostream& ostream);

}