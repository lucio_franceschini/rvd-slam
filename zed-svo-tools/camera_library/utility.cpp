#include "utility.h"

#include <hdf5.h>

namespace
{
algorithms::AlgorithmParameters extractAlgorithmData(const H5::Group& algorithm)
{
  const auto name =
      io::extractAttribute<std::string>(algorithm, io::algorithmAliasMetadata);

  if (name == algorithms::difodoAlgorithmName)
  {
    return algorithms::AlgorithmParameters{
        algorithms::readDifodoAlgorithmInfo(algorithm)};
  }
  else if (name == algorithms::zedSdkAlgorithmName)
  {
    return algorithms::AlgorithmParameters{
        algorithms::readZedSdkInfo(algorithm)};
  }
  else
  {
    throw std::logic_error("Not implemented");
  }
}

herr_t op_func(hid_t, const char* name, const H5L_info_t*, void* operator_data)
{
  if (true)
  {
    const auto groupNames =
        reinterpret_cast<std::vector<std::string>*>(operator_data);
    groupNames->push_back(name);
  }
  return 0;
}

auto findById(
    const H5::Group& rootGroup,
    const int id,
    const std::string& fieldIdName)
{
  const auto groupNames = [&rootGroup] {
    std::vector<std::string> groupNames;
    const auto idx = H5Literate(
        rootGroup.getId(),
        H5_INDEX_NAME,
        H5_ITER_NATIVE,
        nullptr,
        op_func,
        &groupNames);
    return groupNames;
  }();

  const auto res = std::find_if(
      std::cbegin(groupNames),
      std::cend(groupNames),
      [id, &fieldIdName, &rootGroup](const std::string& group) {
        int groupId;
        const auto attribute =
            rootGroup.openGroup(group).openAttribute(fieldIdName);
        attribute.read(attribute.getDataType(), &groupId);
        return id == groupId;
      });

  if (res != groupNames.end())
  {
    return rootGroup.openGroup(*res);
  }
  else
  {
    throw std::runtime_error("Unknown id");
  }
}

auto extractNetCDFDataPath(const path_tracking::Settings& settings)
{
  const auto rootFile = H5::H5File(settings.rootPath.string(), H5F_ACC_RDONLY);
  const auto sessions = rootFile.openGroup(io::sessionsGroupName);
  const auto session = sessions.openGroup(settings.sessionName);
  const auto sensor = session.openGroup(sensorName(settings.sensor));
  const auto sources = sensor.openGroup(io::sourcesGroupName);
  return std::filesystem::path(sources.getFileName());
}

auto readDepthZed(
    const netCDF::NcGroup& depthGroup,
    const filters::FilterParameters& parameters,
    const size_t timestampIdx)
{
  cv::Mat output;
  std::visit(
      [&](auto&& arg) {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, filters::BoxFilterParameters>)
        {
          output = boxFilter(
              io::readDepthFrameZed(depthGroup, timestampIdx),
              std::get<filters::BoxFilterParameters>(parameters));
        }
        else if constexpr (std::is_same_v<T, filters::NoFilterParameters>)
        {
          output = io::readDepthFrameZed(depthGroup, timestampIdx);
        }
        else
        {
          static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
      },
      parameters);
  return output;
}

auto readDepthD435(
    const netCDF::NcGroup& depthGroup,
    const filters::FilterParameters& parameters,
    const size_t timestampIdx)
{
  cv::Mat output;
  std::visit(
      [&](auto&& arg) {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, filters::BoxFilterParameters>)
        {
          output = boxFilter(
              io::readDepthFrameD435(depthGroup, timestampIdx),
              std::get<filters::BoxFilterParameters>(parameters));
        }
        else if constexpr (std::is_same_v<T, filters::NoFilterParameters>)
        {
          output = io::readDepthFrameD435(depthGroup, timestampIdx);
        }
        else
        {
          static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
      },
      parameters);
  return output;
}

auto readDepthHPS160(
    const netCDF::NcGroup& depthGroup,
    const filters::FilterParameters& parameters,
    const size_t timestampIdx)
{
  cv::Mat output;
  std::visit(
      [&](auto&& arg) {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, filters::BoxFilterParameters>)
        {
          output = boxFilter(
              io::readDepthFrameHPS160(depthGroup, timestampIdx),
              std::get<filters::BoxFilterParameters>(parameters));
        }
        else if constexpr (std::is_same_v<T, filters::NoFilterParameters>)
        {
          output = io::readDepthFrameHPS160(depthGroup, timestampIdx);
        }
        else
        {
          static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
      },
      parameters);
  return output;
}

auto readDepth(
    const netCDF::NcGroup& depthGroup,
    const sensors::Sensor sensor,
    const filters::FilterParameters& parameters,
    const size_t timestampIdx)
{
  switch (sensor)
  {
    case sensors::Sensor::ZED:
      return readDepthZed(depthGroup, parameters, timestampIdx);
    case sensors::Sensor::D435:
      return readDepthD435(depthGroup, parameters, timestampIdx);
    case sensors::Sensor::HPS160:
      return readDepthHPS160(depthGroup, parameters, timestampIdx);
    default:
      throw std::logic_error("Not implemented");
  }
}

}  // namespace

namespace path_tracking
{
cv::Mat scaleInRange(const cv::Mat& depth, const double min, const double max)
{
  auto depthNorm = cv::Mat();
  // Scale values to the range from 0 to 255
  const auto scale = 255.0 / (max - min);
  const auto offset = -min * scale;
  depth.convertTo(depthNorm, CV_8UC1, scale, offset);
  return depthNorm;
}

cv::Mat normalizeDepth(const cv::Mat& depth)
{
  double min, max;
  cv::minMaxIdx(depth, &min, &max, nullptr, nullptr, depth >= 0);

  return scaleInRange(depth, min, max);
}

void showWindows(const std::string& name, const cv::Mat& mat)
{
  cv::namedWindow(name, cv::WINDOW_AUTOSIZE);
  cv::imshow(name, mat);
}

bool isTimestampValid(
    const netCDF::NcVar& timestampVar,
    const unsigned int idxTimestamp)
{
  double timestamp;
  timestampVar.getVar({idxTimestamp}, &timestamp);
  return !isnan(timestamp);
}

std::filesystem::path createOutputPath(
    const Settings& settings,
    const std::string& id)
{
  return settings.outputFolder / (id + io::outputFileExtension);
}

H5::Group findFilter(const H5::H5File& inputFile, const Settings& settings)
{
  return findById(
      inputFile.openGroup(io::filtersGroupName),
      settings.filterId,
      io::filterIdMetadata);
}

H5::Group findAlgorithm(const H5::H5File& inputFile, const Settings& settings)
{
  return findById(
      inputFile.openGroup(io::algorithmsGroupName),
      settings.algorithmId,
      io::algorithmIdMetadata);
}

void saveIdsMetadata(
    const netCDF::NcGroup& outputGroup,
    const int algorithmId,
    const int filterId)
{
  io::saveMetadata(
      outputGroup,
      {{"algorithm_id", algorithmId}, {"filter_id", filterId}});
}

std::tuple<netCDF::NcVar, netCDF::NcDim> extractTimestampVarDim(
    const netCDF::NcGroup& depthGroup)
{
  const auto var = depthGroup.getVar(io::dataFieldName);
  const auto dims = var.getDims();
  const auto timestampsDim = dims[io::timestampDimLocation];
  const auto timestampsVar = depthGroup.getVar(io::timestampDimName);
  return std::make_tuple(timestampsVar, timestampsDim);
}

netCDF::NcGroup createOutputGroupForPath(
    const netCDF::NcFile& pathGroup,
    const sensors::Sensor sensor,
    const std::string& originalSessionName)
{
  [[gsl::suppress(es .84)]] pathGroup.putAtt(
      io::sensorAttributeName,
      sensorName(sensor));
  [[gsl::suppress(es .84)]] pathGroup.putAtt(
      io::sessionTimestampAttributeName,
      originalSessionName);

  const auto timestampDim = pathGroup.addDim(io::timestampDimName);
  const auto timestampVar =
      pathGroup.addVar(io::timestampDimName, netCDF::ncDouble, {timestampDim});

  const auto xyzDim = [&pathGroup] {
    const auto xyzDim =
        pathGroup.addDim(io::positionAxesDimName, io::positionAxesDimSize);
    pathGroup.addVar(io::positionAxesDimName, netCDF::ncString, {xyzDim})
        .putVar(io::positionAxesValues.data());
    return xyzDim;
  }();

  const auto xyzrDim = [&pathGroup] {
    const auto xyzrDim = pathGroup.addDim(
        io::orientationAxesDimName,
        io::orientationAxesDimSize);

    pathGroup.addVar(io::orientationAxesDimName, netCDF::ncString, {xyzrDim})
        .putVar(io::orientationAxesValues.data());
    return xyzrDim;
  }();

  const auto positionVar = pathGroup.addVar(
      io::positionVarName,
      netCDF::ncDouble,
      {timestampDim, xyzDim});
  positionVar.setFill(true, std::numeric_limits<double>::quiet_NaN());
  io::setUnitAttribute(positionVar, io::pathUnit);

  const auto orientationVar = pathGroup.addVar(
      io::orientationVarName,
      netCDF::ncDouble,
      {timestampDim, xyzrDim});
  orientationVar.setFill(true, std::numeric_limits<double>::quiet_NaN());

  return pathGroup;
}

netCDF::NcGroup readSourcesGroup(
    const netCDF::NcFile& dataFile,
    const sensors::Sensor sensor)
{
  return dataFile.getGroup(sensors::sensorName(sensor));
}

netCDF::NcGroup readDepthGroup(
    const netCDF::NcGroup& sensorGroup,
    const sensors::Sensor& sensor)
{
  return sensorGroup.getGroup(depthGroupName(sensor));
}

void insertPosition(
    const netCDF::NcVar* const positionVar,
    const unsigned int& idxTimestamp,
    const mrpt::poses::CPose3DQuat& pose)
{
  positionVar->putVar(
      {idxTimestamp, 0},
      {1, io::positionAxesDimSize},
      pose.xyz().data());
}

void insertOrientation(
    const netCDF::NcVar* const orientationVar,
    const unsigned int& idxTimestamp,
    const mrpt::poses::CPose3DQuat& pose)
{
  orientationVar->putVar(
      {idxTimestamp, 0},
      {1, io::orientationAxesDimSize},
      pose.quat().data());
}

void applyDifodoAlgorithm(
    const algorithms::DifodoSettings& difodoSettings,
    const sensors::Sensor sensor,
    const netCDF::NcGroup& depthGroup,
    const netCDF::NcGroup& outputGroup,
    const filters::FilterParameters& parameters,
    const std::optional<io::Timestamp>& fromTimestamp,
    const std::optional<io::Timestamp>& toTimestamp,
    std::ostream& output)
{
  const auto startingTimestamp =
      io::readStartingDate(depthGroup.getVar(io::timestampDimName));
  const auto fromSeconds = [&fromTimestamp, &startingTimestamp]() {
    if (fromTimestamp)
    {
      const auto dt = (*fromTimestamp - startingTimestamp);
      return io::nanosecondsToSeconds(dt);
    }
    else
    {
      return std::chrono::duration<double>(0.0);
    }
  }();

  const auto toSeconds = [&toTimestamp, &startingTimestamp]() {
    if (toTimestamp)
    {
      const auto dt = (*toTimestamp - startingTimestamp);
      return io::nanosecondsToSeconds(dt);
    }
    else
    {
      return std::chrono::duration<double>(
          std::numeric_limits<double>::infinity());
    }
  }();

  auto difodo = algorithms::createDifodo(depthGroup, difodoSettings, sensor);

  const auto [timestampsVar, timestampsDim] =
      extractTimestampVarDim(depthGroup);
  const auto numTimestamps = timestampsDim.getSize();
  const auto positionVar = outputGroup.getVar(io::positionVarName);
  const auto orientationVar = outputGroup.getVar(io::orientationVarName);
  const auto timestampVar = outputGroup.getVar(io::timestampDimName);
  [[gsl::suppress(es .84)]] timestampVar.putAtt(
      io::unitsAttrName,
      io::timestampAttributeData(startingTimestamp));
  const auto timestampDim = outputGroup.getDim(io::timestampDimName);

  const auto sourceTimestampVar = depthGroup.getVar(io::timestampDimName);

  const auto tracking = [&](const size_t idxTimestamp) {

    const auto depth = readDepth(depthGroup, sensor, parameters, idxTimestamp);

    const auto pose = algorithms::applyDifodo(&difodo, depth);

    const auto lastIdx = timestampDim.getSize();

    insertPosition(&positionVar, lastIdx, pose);
    insertOrientation(&orientationVar, lastIdx, pose);

    const auto timestampSec = [&sourceTimestampVar, idxTimestamp] {
      double sec;
      sourceTimestampVar.getVar({idxTimestamp}, &sec);
      return std::chrono::duration<double>(sec);
    }();

    timestampVar.putVar({lastIdx}, timestampSec.count());

    output << "Frame: " << idxTimestamp << "/" << numTimestamps << '\n';
  };

  foreachValidFrame(
      timestampsVar,
      timestampsDim,
      difodoSettings.skipFrames,
      fromSeconds,
      toSeconds,
      tracking);
}

auto extractAlgorithmFilterData(const Settings& settings)
{
  const auto rootFile = H5::H5File(settings.rootPath.string(), H5F_ACC_RDONLY);
  const auto filterGroup = findFilter(rootFile, settings);
  const auto algorithmGroup = findAlgorithm(rootFile, settings);

  const auto filterData = filters::extractFilterData(filterGroup);
  const auto algorithmData = extractAlgorithmData(algorithmGroup);
  return std::make_tuple(
      algorithmData,
      io::extractAttribute<int>(algorithmGroup, io::algorithmIdMetadata),
      filterData,
      io::extractAttribute<int>(filterGroup, io::filterIdMetadata));
}

void calculateTracking(
    const Settings& settings,
    const std::optional<io::Timestamp>& fromTimestamp,
    const std::optional<io::Timestamp>& toTimestamp,
    std::ostream& output)
{
  const auto dataFile = netCDF::NcFile(
      extractNetCDFDataPath(settings).string(),
      netCDF::NcFile::read);
  const auto [algorithmData, algorithmId, filterData, filterId] =
      extractAlgorithmFilterData(settings);

  const auto dataGroup = readSourcesGroup(dataFile, settings.sensor);

  const auto depthGroup = readDepthGroup(dataGroup, settings.sensor);

  const auto id = io::generatePathId();
  const auto outputFile = netCDF::NcFile(
      createOutputPath(settings, id).string(),
      netCDF::NcFile::FileMode::newFile);

  const auto outputGroup = createOutputGroupForPath(
      outputFile,
      settings.sensor,
      settings.sessionName);

  saveIdsMetadata(outputGroup, algorithmId, filterId);

  std::visit(
      [&](auto&& arg) {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, algorithms::DifodoSettings>)
        {
          applyDifodoAlgorithm(
              std::get<algorithms::DifodoSettings>(algorithmData),
              settings.sensor,
              depthGroup,
              outputGroup,
              filterData,
              fromTimestamp,
              toTimestamp,
              output);
        }
        else if constexpr (std::is_same_v<T, algorithms::ZedSdkSettings>)
        {
          throw std::logic_error("Wrong function called");
        }
        else
        {
          static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
      },
      algorithmData);
}

#ifdef CUDA
void calculateTrackingZed(
    const Settings& settings,
    const std::filesystem::path& svoFile,
    std::ostream& output)
{
  const auto dataFile = netCDF::NcFile(
      extractNetCDFDataPath(settings).string(),
      netCDF::NcFile::read);
  const auto [algorithmData, algorithmId, filterData, filterId] =
      extractAlgorithmFilterData(settings);

  const auto zedAlgorithmData =
      std::get<algorithms::ZedSdkSettings>(algorithmData);
  const auto camera = converter::loadZedCameraLoader(svoFile);
  const auto path = converter::exportSdkPath(
      camera,
      zedAlgorithmData.confidenceThreshold,
      zedAlgorithmData.depthMaxRangeValue,
      output);

  std::vector<std::array<double, io::positionAxesDimSize>> positions;
  positions.reserve(path.size());

  std::vector<std::array<double, io::orientationAxesDimSize>> orientations;
  orientations.reserve(path.size());

  std::vector<double> timestamps;
  timestamps.reserve(path.size());
  std::optional<io::Timestamp> firstTimestamp;

  std::vector<int> poseConfidence;
  poseConfidence.reserve(path.size());


  std::for_each(
      std::cbegin(path),
      std::cend(path),
      [&positions,
       &orientations,
       &timestamps,
       &firstTimestamp,
       &poseConfidence](
         const sl::Pose& element) {
        const auto position = element.pose_data.getTranslation();

        positions.push_back({position.tx, position.ty, position.tz});

        const auto orientation = element.pose_data.getOrientation();
        orientations.push_back(
            {orientation.ox, orientation.oy, orientation.oz, orientation.ow});

        poseConfidence.push_back(element.pose_confidence);

        const auto now =
            io::Timestamp{std::chrono::nanoseconds(element.timestamp)};
        if (!firstTimestamp)
        {
          firstTimestamp = now;
          timestamps.push_back(0.0);
        }
        else
        {
          timestamps.push_back(
              io::timestampDifference(now, *firstTimestamp).count());
        }
      });

  const auto id = io::generatePathId();
  const auto outputFile = netCDF::NcFile(
      createOutputPath(settings, id).string(),
      netCDF::NcFile::FileMode::newFile);

  const auto outputGroup = createOutputGroupForPath(
      outputFile,
      settings.sensor,
      settings.sessionName);

  const auto timestampVar = outputGroup.getVar(io::timestampDimName);
  timestampVar.putVar({0}, {timestamps.size()}, timestamps.data());
  [[gsl::suppress(es .84)]] timestampVar.putAtt(
      io::unitsAttrName,
      io::timestampAttributeData(*firstTimestamp));

  saveIdsMetadata(outputGroup, algorithmId, filterId);

  const auto positionVar = outputGroup.getVar(io::positionVarName);
  const auto orientationVar = outputGroup.getVar(io::orientationVarName);

  const auto confidenceVar = outputGroup.addVar(
      io::confidenceVarName,
      netCDF::ncInt,
      {outputGroup.getDim(io::timestampDimName)});


  positionVar.putVar(
      {0, 0},
      {positions.size(), io::positionAxesDimSize},
      positions.data());
  orientationVar.putVar(
      {0, 0},
      {orientations.size(), io::orientationAxesDimSize},
      orientations.data());
  confidenceVar.putVar({0}, {poseConfidence.size()}, poseConfidence.data());
}
#endif

}  // namespace path_tracking
