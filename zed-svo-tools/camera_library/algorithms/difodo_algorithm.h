#pragma once
#include <H5Cpp.h>

#include "../io_data.h"
#include "DIFODO.h"
#include "../sensors.h"

namespace algorithms
{
// Everything is already in meters
constexpr auto defaultDifodoScale = 1;
const auto difodoAlgorithmName = std::string("DIFODO");

struct DifodoSettings
{
  const int skipFrames;
  const int downsample;
  const int cols;
  const int rows;
  const int ctfLevels;
  const float maxDepth;
};

Difodo createDifodo(
    const io::CameraParameters& cameraParameters,
    const FullPixel width,
    const FullPixel height,
    const DifodoSettings& settings);

Difodo createDifodo(
    const Radiant fovHorizontal,
    const Radiant fovVertical,
    const float fps,
    const FullPixel width,
    const FullPixel height,
    const DifodoSettings& settings);

Difodo createDifodo(
    const netCDF::NcGroup& depthGroup,
    const DifodoSettings& settings,
    const sensors::Sensor sensor);

mrpt::poses::CPose3DQuat applyDifodo(
    Difodo* const difodo,
    const cv::Mat& depth,
    const unsigned int difodoScale = defaultDifodoScale);

DifodoSettings readDifodoAlgorithmInfo(const H5::Group& algorithmInfo);
}  // namespace algorithms