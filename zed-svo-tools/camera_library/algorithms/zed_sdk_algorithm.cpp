#include "zed_sdk_algorithm.h"
#include "../io_data.h"

algorithms::ZedSdkSettings algorithms::readZedSdkInfo(
    const H5::Group& algorithmInfo)
{
  return {
      io::extractAttribute<int>(algorithmInfo, "confidence_threshold"),
      io::extractAttribute<float>(algorithmInfo, "depth_max_range_value")};
}
