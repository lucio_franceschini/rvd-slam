#pragma once

#include <H5Cpp.h>

#include <filesystem>

namespace algorithms
{
const auto zedSdkAlgorithmName = std::string("ZedSDK");

struct ZedSdkSettings
{
  const int confidenceThreshold;
  const float depthMaxRangeValue;
};

algorithms::ZedSdkSettings readZedSdkInfo(const H5::Group& algorithmInfo);


}  // namespace algorithms