#pragma once

#include <variant>

#include "difodo_algorithm.h"
#include "zed_sdk_algorithm.h"

namespace algorithms
{
using AlgorithmParameters = std::variant<DifodoSettings, ZedSdkSettings>;
}  // namespace algorithms