#include "difodo_algorithm.h"

#include <gsl/gsl>

namespace
{
auto removeNaN(const cv::Mat& source)
{
  const auto nanMask = [&source] {
    const auto mask = cv::Mat(source > 0);
    auto res = cv::Mat();
    cv::bitwise_not(mask, res);
    return res;
  }();
  auto destination = source.clone();
  destination.setTo(0.0f, nanMask);
  return destination.clone();
}

float estimateFps(const netCDF::NcGroup& depthGroup)
{
  const auto timestamps = io::readTimestamps(depthGroup);
  const auto totalTimestamps = timestamps.timestampsSec.size();
  const auto dt = timestamps.timestampsSec[totalTimestamps - 1] -
                  timestamps.timestampsSec[0];
  return gsl::narrow_cast<float>(totalTimestamps / dt);
}

}  // namespace
namespace algorithms
{
Difodo createDifodo(
    const io::CameraParameters& cameraParameters,
    const FullPixel width,
    const FullPixel height,
    const DifodoSettings& settings)
{
  const auto fovHorizontal = Radiant{
      2.0 * atan2(cameraParameters.cx.value(), cameraParameters.fx.value())};
  const auto fovVertical = Radiant{
      2.0 * atan2(cameraParameters.cy.value(), cameraParameters.fy.value())};
  const auto fps = cameraParameters.fps / (settings.skipFrames + 1);
  return createDifodo(fovHorizontal, fovVertical, fps, width, height, settings);
}

Difodo createDifodo(
    const Radiant fovHorizontal,
    const Radiant fovVertical,
    const float fps,
    const FullPixel width,
    const FullPixel height,
    const DifodoSettings& settings)
{
  auto difodo = Difodo(
      gsl::narrow<unsigned int>(width.value()),
      gsl::narrow<unsigned int>(height.value()),
      settings.downsample,
      settings.cols,
      settings.rows,
      settings.ctfLevels,
      settings.maxDepth);
  difodo.setFOV(
      gsl::narrow_cast<float>(fovHorizontal.toDegree().value()),
      gsl::narrow_cast<float>(fovVertical.toDegree().value()));
  difodo.fps = fps;
  return difodo;
}

Difodo createDifodo(
    const netCDF::NcGroup& depthGroup,
    const DifodoSettings& settings,
    const sensors::Sensor sensor)
{
  const auto var = depthGroup.getVar(io::dataFieldName);
  const auto dims = var.getDims();
  switch (sensor)
  {
    case sensors::Sensor::ZED:
    case sensors::Sensor::D435:
      return createDifodo(
          io::readCameraParameters(depthGroup),
          {dims[io::uDimLocation].getSize()},
          {dims[io::vDimLocation].getSize()},
          settings);
    case sensors::Sensor::HPS160:
      return createDifodo(
          io::hps160HorizontalFieldOfView,
          io::hps160VerticalFieldOfView,
          estimateFps(depthGroup),
          {dims[io::uDimLocation].getSize()},
          {dims[io::vDimLocation].getSize()},
          settings);
    default:
      throw std::logic_error("Not implemented");
  }
}

mrpt::poses::CPose3DQuat applyDifodo(
    Difodo* const difodo,
    const cv::Mat& depth,
    const unsigned int difodoScale)
{
  const auto finalDepth = removeNaN(depth);
  difodo->loadFrame(finalDepth, difodoScale);
  // Do calculations
  difodo->odometryCalculation();
  // Retreive calculated pose
  const auto camPose = difodo->cam_pose;
  const auto pose = mrpt::poses::CPose3D(
      camPose.y(),
      camPose.z(),
      camPose.x(),
      camPose.pitch(),
      camPose.yaw(),
      camPose.roll());
  mrpt::math::CQuaternionDouble difodoRotation;
  const auto difodoTranslation = pose.m_coords;
  pose.getAsQuaternion(difodoRotation);
  return mrpt::poses::CPose3DQuat(
      difodoTranslation.x(),
      difodoTranslation.y(),
      difodoTranslation.z(),
      difodoRotation);
}

DifodoSettings readDifodoAlgorithmInfo(const H5::Group& algorithmInfo)
{
  return {
      io::extractAttribute<int>(algorithmInfo, "skip_frames"),
      io::extractAttribute<int>(algorithmInfo, "downsample"),
      io::extractAttribute<int>(algorithmInfo, "cols"),
      io::extractAttribute<int>(algorithmInfo, "rows"),
      io::extractAttribute<int>(algorithmInfo, "ctf_levels"),
      gsl::narrow_cast<float>(
          io::extractAttribute<double>(algorithmInfo, "max_depth")),
  };
}
}  // namespace algorithms