#include "RealsenseCameraLoader.h"

#pragma warning(push, 0)
#include <librealsense2/rsutil.h>
#pragma warning(pop)

#include <algorithm>
#include <array>
#include <charconv>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <gsl/gsl>
#include <optional>
#include <tuple>
#include <vector>

#include "io_data.h"

namespace converter
{
namespace
{
constexpr auto chunkTimeSize = 500;

/**
 * Precision-aware cast
 */
auto millisecondsToNanoseconds(const double msFloating)
{
  const auto ms = std::chrono::milliseconds(
      gsl::narrow_cast<unsigned long long>(msFloating));
  const auto ns = gsl::narrow_cast<unsigned long long>(
                      round((msFloating - ms.count()) * 1e4)) *
                  100;
  return std::chrono::duration_cast<std::chrono::nanoseconds>(ms) +
         std::chrono::nanoseconds(gsl::narrow_cast<unsigned long long>(ns));
}

auto retrieveTimestamp(const rs2::frame& frame)
{
  const auto durationNs = millisecondsToNanoseconds(frame.get_timestamp());
  const auto t = date::sys_time<io::Timestamp::duration>(durationNs);
  return date::to_utc_time<io::Timestamp::duration>(t);
}

auto setConfig(const std::filesystem::path& file)
{
  rs2::config cfg;
  cfg.enable_device_from_file(file.string(), false);
  cfg.enable_all_streams();
  return cfg;
}

auto findStreamIndex(
    const rs2::stream_profile& profile,
    const std::vector<rs2::video_stream_profile>& infos)
{
  const auto it = std::find_if(
      std::cbegin(infos),
      std::cend(infos),
      [profile](const rs2::video_stream_profile& other) {
        return other.format() == profile.format() &&
               other.stream_index() == profile.stream_index() &&
               other.stream_type() == profile.stream_type();
      });
  if (it == std::cend(infos))
  {
    return -1;
  }
  else
  {
    return gsl::narrow<int>(std::distance(std::cbegin(infos), it));
  }
}

void setStartingDateTimestamp(
    std::vector<std::optional<io::Timestamp>>* startingDates,
    const ptrdiff_t& streamIndex,
    const rs2::video_frame& img,
    const std::vector<netCDF::NcVar>& timestampVars)
{
  if (!(*startingDates)[streamIndex])
  {
    const auto startingDate = retrieveTimestamp(img);
    (*startingDates)[streamIndex] = startingDate;
    [[gsl::suppress(es .84)]] timestampVars[streamIndex].putAtt(
        io::unitsAttrName,
        io::timestampAttributeData(startingDate));
  }
}

auto createVariableGroup(
    const rs2_format format,
    const netCDF::NcGroup& streamGroup,
    const netCDF::NcDim& timestampDim,
    const netCDF::NcDim& vDim,
    const netCDF::NcDim& uDim,
    const netCDF::NcDim& thirdDim,
    const size_t chunkSize)
{
  netCDF::NcVar var;
  mrpt::vector_size_t chunks;
  switch (format)
  {
    case RS2_FORMAT_RGB8:
      chunks = {chunkSize, vDim.getSize(), uDim.getSize(), thirdDim.getSize()};
      var = streamGroup.addVar(
          io::dataFieldName,
          netCDF::ncUbyte,
          {timestampDim, vDim, uDim, thirdDim});
      var.setFill(true, uint8_t{0});
      break;
    case RS2_FORMAT_Y8:
      chunks = {chunkSize, vDim.getSize(), uDim.getSize()};
      var = streamGroup.addVar(
          io::dataFieldName,
          netCDF::ncUbyte,
          {timestampDim, vDim, uDim});
      var.setFill(true, uint8_t{0});
      break;
    case RS2_FORMAT_Z16:
      chunks = {chunkSize, vDim.getSize(), uDim.getSize()};
      var = streamGroup.addVar(
          io::dataFieldName,
          netCDF::ncUshort,
          {timestampDim, vDim, uDim});
      var.setFill(true, uint16_t{0});
      break;
    default:
      throw std::logic_error("Not implemented");
  }
  var.setChunking(netCDF::NcVar::ChunkMode::nc_CHUNKED, chunks);
  return var;
}

io::Metadata streamInfoToMetadata(const rs2::video_stream_profile& streamInfo)
{
  return {{"fps", streamInfo.fps()}};
}

auto cameraParameters(
    const rs2::pipeline& pipe,
    const rs2_stream stream = RS2_STREAM_DEPTH,
    const int streamIndex = -1)
{
  const auto streamProfile = pipe.get_active_profile()
                                 .get_stream(stream, streamIndex)
                                 .as<rs2::video_stream_profile>();
  return streamProfile.get_intrinsics();
}

void exportStreamInfo(
    const std::vector<rs2::video_stream_profile>& streamInfos,
    const std::vector<netCDF::NcGroup>& streamGroups)
{
  for (auto idx = size_t{0}; idx < streamGroups.size(); idx++)
  {
    io::saveMetadata(streamGroups[idx], streamInfoToMetadata(streamInfos[idx]));
  }
}

io::Metadata cameraParamsToMetadata(const rs2_intrinsics params)
{
  return {
      {"fx", params.fx},
      {"fy", params.fy},
      {"cx", params.ppx},
      {"cy", params.ppy},
      {"height", params.height},
      {"width", params.width},
      {"distorsion_coeffs_0", params.coeffs[0]},
      {"distorsion_coeffs_1", params.coeffs[1]},
      {"distorsion_coeffs_2", params.coeffs[2]},
      {"distorsion_coeffs_3", params.coeffs[3]},
      {"distorsion_coeffs_4", params.coeffs[4]},
      {"distorsion_model", std::string(rs2_distortion_to_string(params.model))},
  };
}

void setDescriptiveAttributes(
    const netCDF::NcVar& var,
    const RealsenseStreamProfile& profile)
{
  switch (profile.type)
  {
    case RS2_STREAM_DEPTH:
      io::setUnitAttribute(var, UNIT::METER);
      [[gsl::suppress(es .84)]] var.putAtt(io::nameAttrName, "Depth View");
      [[gsl::suppress(es .84)]] var.putAtt(
          io::descriptionAttrName,
          "Distances from the D435 left IR camera of the scene scaled by the "
          "scale_factor");
      break;
    case RS2_STREAM_INFRARED:
      if (profile.stream_idx == 1)
      {
        [[gsl::suppress(es .84)]] var.putAtt(io::nameAttrName, "Left View");
        [[gsl::suppress(es .84)]] var.putAtt(
            io::descriptionAttrName,
            "Frames from the left camera of the D435");
      }
      else if (profile.stream_idx == 2)
      {
        [[gsl::suppress(es .84)]] var.putAtt(io::nameAttrName, "Right View");
        [[gsl::suppress(es .84)]] var.putAtt(
            io::descriptionAttrName,
            "Frames from the right camera of the D435");
      }
      else
      {
        throw std::logic_error("Not implemented");
      }
      break;
    case RS2_STREAM_COLOR:
      [[gsl::suppress(es .84)]] var.putAtt(io::nameAttrName, "Color View");
      [[gsl::suppress(es .84)]] var.putAtt(
          io::descriptionAttrName,
          "Frames from the color camera of the D435");
      break;
    default:
      throw std::logic_error("Not implemented");
  }
}

auto extractFrameNumber(const rs2::frame& img)
{
  const auto frameNumber = img.get_frame_number();
  if (frameNumber >= 1)
  {
    return frameNumber - 1;
  }
  else
  {
    throw std::runtime_error(
        "Unexpected frame number (it should be >= 1): " +
        std::to_string(frameNumber));
  }
}

void exportStreams(
    rs2::playback& playback,
    rs2::pipeline& pipe,
    const netCDF::NcGroup& sensor,
    const std::vector<RealsenseStreamProfile>& streamProfiles,
    const float depthScale,
    std::ostream& ostream)
{
  const auto profile = pipe.get_active_profile();
  const auto streamGroups = [&streamProfiles, &sensor, &pipe, depthScale] {
    std::vector<netCDF::NcGroup> groups;
    std::transform(
        std::cbegin(streamProfiles),
        std::cend(streamProfiles),
        std::back_inserter(groups),
        [&sensor, &pipe, depthScale](const RealsenseStreamProfile& profile) {
          auto group = sensor.addGroup(profile.name);

          const auto streamCameraParams = cameraParamsToMetadata(
              cameraParameters(pipe, profile.type, profile.stream_idx));
          io::saveMetadata(group, streamCameraParams);
          if (profile.type == RS2_STREAM_DEPTH)
          {
            io::saveMetadata(group, {{"scale_factor", depthScale}});
          }
          return group;
        });
    return groups;
  }();
  const auto [timestampDims, timestampVars] = [&streamGroups] {
    std::vector<netCDF::NcDim> dimensions;
    std::vector<netCDF::NcVar> variables;
    for (const auto& streamGroup : streamGroups)
    {
      const auto [var, dim] = io::addTimestampsDimension(streamGroup);
      dimensions.push_back(dim);
      variables.push_back(var);
    }
    return std::make_tuple(dimensions, variables);
  }();

  const auto streamInfos = [&streamProfiles, &profile] {
    std::vector<rs2::video_stream_profile> infos;
    std::transform(
        std::cbegin(streamProfiles),
        std::cend(streamProfiles),
        std::back_inserter(infos),
        [&profile](const RealsenseStreamProfile& stream) {
          const auto streamInfo =
              profile.get_stream(stream.type, stream.stream_idx)
                  .as<rs2::video_stream_profile>();

          return streamInfo;
        });
    return infos;
  }();

  exportStreamInfo(streamInfos, streamGroups);

  const auto [uDims, vDims, thirdDims] = [&streamGroups, &streamInfos] {
    std::vector<netCDF::NcDim> uDims;
    std::vector<netCDF::NcDim> vDims;
    std::vector<netCDF::NcDim> thirdDims;
    for (auto idx = size_t{0}; idx < streamGroups.size(); idx++)
    {
      const auto& streamGroup = streamGroups[idx];
      const auto& streamInfo = streamInfos[idx];
      uDims.push_back(io::createAndFillUVDim(
          streamGroup,
          io::uAxisVarName,
          streamInfo.width()));
      vDims.push_back(io::createAndFillUVDim(
          streamGroup,
          io::vAxisVarName,
          streamInfo.height()));
      switch (streamInfo.format())
      {
        case RS2_FORMAT_RGB8:
          thirdDims.push_back(io::createThirdDimensionRGB(streamGroup));
          break;
        case RS2_FORMAT_Y8:
        case RS2_FORMAT_Z16:
          thirdDims.push_back(netCDF::NcDim());
          break;
        default:
          throw std::logic_error("Not implemented");
      }
    }
    return std::make_tuple(uDims, vDims, thirdDims);
  }();

  const auto dataVars = [&timestampDims,
                         &streamGroups,
                         &streamInfos,
                         &uDims,
                         &vDims,
                         &thirdDims,
                         &streamProfiles] {
    std::vector<netCDF::NcVar> vars;
    for (auto idx = size_t{0}; idx < streamGroups.size(); idx++)
    {
      const auto& streamGroup = streamGroups[idx];
      const auto& timestampDim = timestampDims[idx];
      const auto& vDim = vDims[idx];
      const auto& uDim = uDims[idx];
      const auto& thirdDim = thirdDims[idx];

      const auto var = createVariableGroup(
          streamInfos[idx].format(),
          streamGroup,
          timestampDim,
          vDim,
          uDim,
          thirdDim,
          chunkTimeSize);
      setDescriptiveAttributes(var, streamProfiles[idx]);
      vars.push_back(var);
    }
    return vars;
  }();

  std::vector<std::optional<io::Timestamp>> startingDates(
      streamGroups.size(),
      std::optional<io::Timestamp>{});

  try
  {
    const auto totalDuration = playback.get_duration();
    while (true)
    {
      for (const auto frame : pipe.wait_for_frames())
      {
        const auto img = frame.as<rs2::video_frame>();
        const auto frameNumber = extractFrameNumber(img);
        const auto streamIndex =
            findStreamIndex(img.get_profile(), streamInfos);

        io::showPercentualCompleted(
            io::toPercentual(
                gsl::narrow_cast<double>(playback.get_position()),
                gsl::narrow_cast<double>(totalDuration.count())),
            ostream);
        if (streamIndex == -1)
        {
          continue;
        }
        setStartingDateTimestamp(
            &startingDates,
            streamIndex,
            img,
            timestampVars);
        const auto timestamp = io::timestampDifference(
            retrieveTimestamp(img),
            *startingDates[streamIndex]);
        timestampVars[streamIndex].putVar({frameNumber}, timestamp.count());
        const auto [start, count] = [&img, &ostream] {
          const auto frameNumber = extractFrameNumber(img);
          switch (img.get_profile().format())
          {
            case RS2_FORMAT_RGB8:
            {
              const auto start = std::vector<size_t>{frameNumber, 0, 0, 0};
              const auto count = std::vector<size_t>{
                  1,
                  static_cast<size_t>(img.get_height()),
                  static_cast<size_t>(img.get_width()),
                  io::dimension3ChannelVarSize};
              return std::make_tuple(start, count);
            }
            case RS2_FORMAT_Y8:
            case RS2_FORMAT_Z16:
            {
              const auto start = std::vector<size_t>{frameNumber, 0, 0};
              const auto count = std::vector<size_t>{
                  1,
                  static_cast<size_t>(img.get_height()),
                  static_cast<size_t>(img.get_width())};
              return std::make_tuple(start, count);
            }
            default:
              throw std::logic_error("Not implemented");
          }
        }();

        dataVars[streamIndex].putVar(start, count, img.get_data());
      }
    }
  }
  catch (const rs2::error&)
  {
  }
}

auto tolower(const std::string_view value)
{
  std::string result;
  std::transform(
      std::cbegin(value),
      std::cend(value),
      std::back_inserter(result),
      [](const char c) { return gsl::narrow<char>(::tolower(c)); });
  return result;
}

std::any parseInfo(const std::string_view value)
{
  const auto parse = [&value](auto& result) {
    auto [p, ec] =
        std::from_chars(value.data(), value.data() + value.size(), result);
    return ec == std::errc();
  };

  const std::string lowerCase = tolower(value);
  if (lowerCase == "true")
  {
    return uint8_t{1};
  }
  if (lowerCase == "false")
  {
    return uint8_t{0};
  }

  int resultInt;
  if (parse(resultInt))
  {
    return resultInt;
  }

  float resultFloat;
  if (parse(resultFloat))
  {
    return resultFloat;
  }

  double resultDouble;
  if (parse(resultDouble))
  {
    return resultDouble;
  }

  long double resultLongDouble;
  if (parse(resultLongDouble))
  {
    return resultLongDouble;
  }

  return std::string(value);
}

auto cameraMetadata(const rs2::playback& playback)
{
  io::Metadata metadata;
  for (auto infoIdx = int{0}; infoIdx < static_cast<int>(RS2_CAMERA_INFO_COUNT);
       ++infoIdx)
  {
    const auto info = static_cast<rs2_camera_info>(infoIdx);
    const auto name = rs2_camera_info_to_string(info);
    switch (info)
    {
      case RS2_CAMERA_INFO_ASIC_SERIAL_NUMBER:
      case RS2_CAMERA_INFO_SERIAL_NUMBER:
        metadata[name] = std::string(playback.get_info(info));
        break;
      case RS2_CAMERA_INFO_FIRMWARE_UPDATE_ID:
      case RS2_CAMERA_INFO_CAMERA_LOCKED:
        break;
      default:
        metadata[name] = parseInfo(playback.get_info(info));
        break;
    }
  }
  return metadata;
}

auto sensorMetadata(rs2::sensor& sensor)
{
  io::Metadata metadata;
  for (const auto option : sensor.get_supported_options())
  {
    if (sensor.supports(option))
    {
      const auto name = sensor.get_option_name(option);
      metadata[name] = sensor.get_option(option);
    }
  }
  return metadata;
}

auto saveSensorsMetadata(const netCDF::NcGroup& camera, rs2::playback& playback)
{
  const auto sensorsData = camera.addGroup(io::modulesDataGroupName);
  for (auto& sensor : playback.query_sensors())
  {
    const auto sensorInfoGroup =
        sensorsData.addGroup(sensor.get_info(RS2_CAMERA_INFO_NAME));
    io::saveMetadata(sensorInfoGroup, sensorMetadata(sensor));
  }
}

auto advanceModeMetadata(const YAML::Node& advanceSettings)
{
  io::Metadata metadata;
  for (const auto& node : advanceSettings)
  {
    metadata[node.first.as<std::string>()] = [&node] {
      if (node.second.IsScalar())
      {
        return parseInfo(node.second.as<std::string>());
      }
      else
      {
        throw std::logic_error("Not implemented");
      }
    }();
  }
  return metadata;
}

rs2::config enableSelectedStreams(
    rs2::config cfg,
    const std::vector<RealsenseStreamProfile>& streams)
{
  cfg.disable_all_streams();

  std::for_each(
      std::cbegin(streams),
      std::cend(streams),
      [&cfg](const RealsenseStreamProfile& stream) {
        cfg.enable_stream(stream.type, stream.stream_idx);
      });
  return cfg;
}

bool metadataAreNotAttributes(
    const io::Metadata& cmMetadata,
    const netCDF::NcGroup& sensor)
{
  return !cmMetadata.empty() &&
         sensor.getAtt(std::cbegin(cmMetadata)->first).isNull();
}
}  // namespace

RealsenseCameraData loadRealsenseCameraData(
    const std::filesystem::path& file,
    const std::filesystem::path& advanceSettingsPath)
{
  return {setConfig(file), YAML::LoadFile(advanceSettingsPath.string())};
}

void exportSensorsRawDatas(
    const RealsenseCameraData& data,
    const netCDF::NcGroup& sensor,
    const std::vector<RealsenseStreamProfile>& streams,
    std::ostream& ostream)
{
  for (size_t idxStream = 0; idxStream < streams.size(); idxStream++)
  {
    const auto& selectedStream = streams[idxStream];
    ostream << "Exporting " << selectedStream.name << " (Channel "
            << selectedStream.stream_idx << ')' << std::endl;
    const std::vector<RealsenseStreamProfile> selectedStreams = {
        selectedStream};
    rs2::pipeline pipe;
    const auto cfg = enableSelectedStreams(data.cfg, selectedStreams);

    rs2::playback playback = pipe.start(cfg).get_device();
    playback.set_real_time(false);

    const auto profile = pipe.get_active_profile();
    const auto depthSensor = profile.get_device().first<rs2::depth_sensor>();

    if (idxStream == 0)
    {
      const auto cmMetadata = cameraMetadata(playback);
      if (metadataAreNotAttributes(cmMetadata, sensor))
      {
        io::saveMetadata(sensor, cmMetadata);
        saveSensorsMetadata(sensor, playback);
        io::saveMetadata(sensor, advanceModeMetadata(data.advanceSettings));
      }
    }
    const auto depthScale = depthSensor.get_depth_scale();

    exportStreams(playback, pipe, sensor, selectedStreams, depthScale, ostream);
    playback.stop();
    pipe.stop();
  }
};
}  // namespace converter