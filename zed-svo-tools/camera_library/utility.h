#pragma once

#include <algorithm>
#include <array>
#include <filesystem>
#include <gsl/gsl>
#include <iostream>
#pragma warning(push, 0)
#include <opencv2/opencv.hpp>
#pragma warning(pop)
#include <string>
#include <tuple>
#include <utility>
#include <variant>

#include <H5Cpp.h>

#include "ZedCameraLoader.h"
#include "algorithms/algorithms.h"
#include "filters/filters.h"
#include "io_data.h"
#include "sensors.h"
#include "units.h"
#include "uuid/uuid.h"

namespace path_tracking
{
struct Settings
{
  const sensors::Sensor sensor;
  const std::filesystem::path rootPath;
  const std::filesystem::path outputFolder;
  const std::string sessionName;
  const int filterId;
  const int algorithmId;
};

cv::Mat scaleInRange(const cv::Mat& depth, const double min, const double max);

cv::Mat normalizeDepth(const cv::Mat& depth);

void showWindows(const std::string& name, const cv::Mat& mat);

bool isTimestampValid(
    const netCDF::NcVar& timestampVar,
    const unsigned int idxTimestamp);

template <typename Tracking>
auto foreachValidFrame(
    const netCDF::NcVar& timestampsVar,
    const netCDF::NcDim& timestampsDim,
    const size_t skipFrames,
    const std::chrono::duration<double> fromSeconds,
    const std::chrono::duration<double> toSeconds,
    Tracking&& tracking)
{
  const auto numTimestamps = timestampsDim.getSize();

  const auto steps = 1 + skipFrames;
  for (size_t idxTimestamp = 0; idxTimestamp < numTimestamps; ++idxTimestamp)
  {
    double timestamp;
    timestampsVar.getVar({idxTimestamp}, &timestamp);
    if (!isnan(timestamp) && timestamp >= fromSeconds.count() &&
        timestamp <= toSeconds.count() && idxTimestamp % steps == 0)
    {
      tracking(idxTimestamp);
    }
  }
}

std::filesystem::path createOutputPath(
    const Settings& settings,
    const std::string& id);

H5::Group findFilter(const H5::H5File& inputFile, const Settings& settings);

H5::Group findAlgorithm(const H5::H5File& inputFile, const Settings& settings);

void saveIdsMetadata(
    const netCDF::NcGroup& outputGroup,
    const int algorithmId,
    const int filterId);

std::tuple<netCDF::NcVar, netCDF::NcDim> extractTimestampVarDim(
    const netCDF::NcGroup& depthGroup);


netCDF::NcGroup createOutputGroupForPath(
    const netCDF::NcFile& outputFile,
    const sensors::Sensor sensor,
    const std::string& originalSessionName);

netCDF::NcGroup readSourcesGroup(
    const netCDF::NcFile& dataFile,
    const sensors::Sensor sensor);

netCDF::NcGroup readDepthGroup(
    const netCDF::NcGroup& sensorGroup,
    const sensors::Sensor& sensor);

void insertPosition(
    const netCDF::NcVar* const positionVar,
    const unsigned int& idxTimestamp,
    const mrpt::poses::CPose3DQuat& pose);

void insertOrientation(
    const netCDF::NcVar* const orientationVar,
    const unsigned int& idxTimestamp,
    const mrpt::poses::CPose3DQuat& pose);

void applyDifodoAlgorithm(
    const algorithms::DifodoSettings& difodoSettings,
    const sensors::Sensor sensor,
    const netCDF::NcGroup& depthGroup,
    const netCDF::NcGroup& outputGroup,
    const filters::FilterParameters& parameters,
    const std::optional<io::Timestamp>& fromTimestamp,
    const std::optional<io::Timestamp>& toTimestamp,
    std::ostream& output);

void calculateTracking(
    const Settings& settings,
    const std::optional<io::Timestamp>& fromTimestamp,
    const std::optional<io::Timestamp>& toTimestamp,
    std::ostream& output);

#ifdef CUDA
void calculateTrackingZed(
    const Settings& settings,
    const std::filesystem::path& svoFile,
    std::ostream& output);
#endif

}  // namespace path_tracking