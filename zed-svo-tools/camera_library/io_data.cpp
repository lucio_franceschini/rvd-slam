#include "io_data.h"

#include <algorithm>
#include <gsl/gsl>

#include "uuid/uuid.h"

namespace io
{
namespace
{
const auto notImplemented = std::logic_error("Not implemented");
template <typename T>
auto readDepthFrame(
    const netCDF::NcGroup& depthGroup,
    const size_t timestampIdx)
{
  const auto var = depthGroup.getVar(dataFieldName);
  const auto dims = var.getDims();
  const auto vDim = dims[vDimLocation];
  const auto uDim = dims[uDimLocation];
  const auto typeCV = [type = var.getType()] {
    if (type.getTypeClass() == netCDF::NcType::nc_FLOAT)
    {
      return CV_32F;
    }
    else if (type.getTypeClass() == netCDF::NcType::nc_USHORT)
    {
      return CV_16U;
    }
    else
    {
      throw notImplemented;
    }
  }();
  auto output = cv::Mat(
      gsl::narrow<int>(vDim.getSize()),
      gsl::narrow<int>(uDim.getSize()),
      typeCV);

  var.getVar(
      {timestampIdx, 0, 0},
      {1, vDim.getSize(), uDim.getSize()},
      output.ptr<T>());

  return output;
}

auto readDepthFrameHps(
    const netCDF::NcGroup& depthGroup,
    const size_t timestampIdx)
{
  const auto var = depthGroup.getVar(dataFieldName);
  const auto dims = var.getDims();
  const auto vDim = dims[vDimLocation];
  const auto uDim = dims[uDimLocation];
  const auto wDim = dims[wDimLocation];
  auto output = cv::Mat(
      gsl::narrow<int>(vDim.getSize()),
      gsl::narrow<int>(uDim.getSize()),
      CV_32F);

  var.getVar(
      {timestampIdx, 0, 0, 2},
      {1, vDim.getSize(), uDim.getSize(), 1},
      output.ptr<float>());

  return output;
}

auto multiplyForScalar(
    const cv::Mat& original,
    const float scaleFactor,
    int opencvResultType)
{
  auto scaled = cv::Mat(original.size(), opencvResultType);
  original.convertTo(scaled, -1, scaleFactor, 0);
  return scaled;
}

}  // namespace

CameraParameters readCameraParameters(const netCDF::NcGroup& group)
{
  return {
      Pixel(extractAttribute<float>(group, "fx")),
      Pixel(extractAttribute<float>(group, "fy")),
      Pixel(extractAttribute<float>(group, "cx")),
      Pixel(extractAttribute<float>(group, "cy")),
      extractAttribute<float>(group, "fps")};
}

cv::Mat readDepthFrameZed(
    const netCDF::NcGroup& depthGroup,
    const size_t timestampIdx)
{
  return readDepthFrame<float>(depthGroup, timestampIdx);
}

cv::Mat readDepthFrameD435(
    const netCDF::NcGroup& depthGroup,
    const size_t timestampIdx)
{
  const auto scaleFactor = extractAttribute<float>(depthGroup, "scale_factor");
  const auto original = readDepthFrame<uint16_t>(depthGroup, timestampIdx);

  return multiplyForScalar(original, scaleFactor, CV_32F);
}

cv::Mat readDepthFrameHPS160(
    const netCDF::NcGroup& depthGroup,
    const size_t timestampIdx)
{
  const auto units = extractAttribute<std::string>(
      depthGroup.getVar(io::dataFieldName),
      io::unitsAttrName);
  const auto original = readDepthFrameHps(depthGroup, timestampIdx);
  if (units == "millimeter")
  {
    return multiplyForScalar(original, 0.001f, CV_32F);
  }
  else if (units == "meter")
  {
    return original;
  }
  else
  {
    throw std::logic_error("Not implemented");
  }
}

Timestamps readTimestamps(const netCDF::NcGroup& depthGroup)
{
  auto timestampVar = depthGroup.getVar(timestampDimName);
  auto timestampDim = timestampVar.getDim(timestampDimLocation);
  const auto timestampsSec = [&] {
    std::vector<double> timestampsSec(timestampDim.getSize());
    timestampVar.getVar(timestampsSec.data());
    return timestampsSec;
  }();
  const auto since = [&] {
    const auto sinceAttr = timestampVar.getAtt(unitsAttrName);
    std::string since;
    sinceAttr.getValues(since);
    return since;
  }();
  return Timestamps{timestampsSec, since};
}

Timestamp stringToTimestamp(const std::string source, const std::string& format)
{
  std::istringstream stream{source};
  Timestamp t;
  std::chrono::minutes offset;
  stream >> date::parse(format, t, offset);
  if (stream.fail())
  {
    throw std::runtime_error("Failed to parse " + source);
  }
  return t;
}

std::string timestampToString(const Timestamp timestamp)
{
  return date::format(timestampFormat, timestamp);
}

std::string timestampAttributeData(const Timestamp startingDate)
{
  return unitsTimestampAttrPreamble + timestampToString(startingDate);
}

constexpr std::chrono::duration<double> nanosecondsToSeconds(
    const std::chrono::nanoseconds value)
{
  return std::chrono::duration<double>(value);
}

constexpr std::chrono::nanoseconds secondsToNanoseconds(
    const std::chrono::duration<double> seconds)
{
  return std::chrono::duration_cast<std::chrono::nanoseconds>(seconds);
}

constexpr std::chrono::nanoseconds nanosecondsFromBeginning(
    const Timestamp instant,
    const Timestamp beginning)
{
  return std::chrono::duration_cast<std::chrono::nanoseconds>(
      instant.time_since_epoch() - beginning.time_since_epoch());
}

std::chrono::duration<double> timestampDifference(
    const Timestamp instant,
    const Timestamp beginning)
{
  return io::nanosecondsToSeconds(
      io::nanosecondsFromBeginning(instant, beginning));
}

void saveSingleMetadata(
    const netCDF::NcGroup& output,
    const std::string& key,
    const std::any& value)
{
  const auto& type = value.type();
  if (type == typeid(float))
  {
    [[gsl::suppress(es .84)]] output.putAtt(
        key,
        netCDF::ncFloat,
        std::any_cast<float>(value));
  }
  else if (type == typeid(double))
  {
    [[gsl::suppress(es .84)]] output.putAtt(
        key,
        netCDF::ncDouble,
        std::any_cast<double>(value));
  }
  else if (type == typeid(int))
  {
    [[gsl::suppress(
        es .84)]] output.putAtt(key, netCDF::ncInt, std::any_cast<int>(value));
  }
  else if (type == typeid(unsigned int))
  {
    [[gsl::suppress(es .84)]] output.putAtt(
        key,
        netCDF::ncUint,
        std::any_cast<unsigned int>(value));
  }
  else if (type == typeid(long long))
  {
    [[gsl::suppress(es .84)]] output.putAtt(
        key,
        netCDF::ncInt64,
        std::any_cast<long long>(value));
  }
  else if (type == typeid(unsigned long long))
  {
    [[gsl::suppress(es .84)]] output.putAtt(
        key,
        netCDF::ncUint64,
        std::any_cast<unsigned long long>(value));
  }
  else if (type == typeid(uint8_t))
  {
    [[gsl::suppress(es .84)]] output.putAtt(
        key,
        netCDF::ncUbyte,
        std::any_cast<uint8_t>(value));
  }
  else if (type == typeid(std::string))
  {
    [[gsl::suppress(es .84)]] output.putAtt(
        key,
        std::any_cast<std::string>(value));
  }
  else if (type == typeid(bool))
  {
    [[gsl::suppress(es .84)]] output.putAtt(
        key,
        netCDF::ncUbyte,
        static_cast<uint8_t>(std::any_cast<bool>(value)));
  }
  else
  {
    throw std::logic_error("Not implemented");
  }
}

double toPercentual(double fraction, double total)
{
  return fraction / total * 100;
}

void setUnitAttribute(const netCDF::NcVar& group, const UNIT unit)
{
  [[gsl::suppress(es .84)]] group.putAtt(io::unitsAttrName, unitToString(unit));
}

Timestamp readStartingDate(const netCDF::NcVar& var)
{
  const auto units = var.getAtt(unitsAttrName);
  if (units.isNull())
  {
    throw std::runtime_error(
        "Attribute " + std::string(unitsAttrName) + " is missing");
  }
  
  std::string unitsValue;
  units.getValues(unitsValue);
  return stringToTimestamp(
      unitsValue,
      std::string(unitsTimestampAttrPreamble) + timestampFormat);
}

void showPercentualCompleted(const double percentual, std::ostream& ostream)
{
  ostream << percentual << "%" << std::endl;
}

void saveMetadata(const netCDF::NcGroup& output, const Metadata& options)
{
  for (const auto& [key, value] : options)
  {
    saveSingleMetadata(output, key, value);
  }
}

void saveMetadata(const netCDF::NcGroup& output, const YAML::Node& node)
{
  const auto nodeToValue = [](const YAML::Node& node) {
    try
    {
      return std::any(node.as<double>());
    }
    catch (const std::exception&)
    {
    }
    try
    {
      return std::any(node.as<int>());
    }
    catch (const std::exception&)
    {
    }
    return std::any(node.as<std::string>());
  };

  if (node.IsMap())
  {
    for (const auto& subnode : node)
    {
      const auto key = subnode.first.as<std::string>();
      const auto& value = subnode.second;

      switch (value.Type())
      {
        case YAML::NodeType::Map:
          saveMetadata(output.addGroup(key), value);
          break;
        case YAML::NodeType::Scalar:
          saveSingleMetadata(output, key, nodeToValue(value));
          break;
        default:
          throw std::logic_error("Not Implemented");
      }
    }
  }
  else
  {
    throw std::logic_error("Not Implemented");
  }
}

std::string generatePathId()
{
  return uuids::to_string(uuids::uuid_system_generator{}());
}

Sensor readOrCreateNode(const Session& experiment, const std::string_view name)
{
  const auto groupName = std::string(name);
  if (const auto node = experiment.getGroup(groupName); !node.isNull())
  {
    return node;
  }
  else
  {
    return experiment.addGroup(groupName);
  }
}

std::tuple<netCDF::NcVar, netCDF::NcDim> addTimestampsDimension(
    const netCDF::NcGroup& stream)
{
  auto timestampsDimension = stream.addDim(io::timestampDimName);

  const auto timestampVar = stream.addVar(
      io::timestampDimName,
      netCDF::ncDouble,
      {timestampsDimension});
  timestampVar.setFill(true, std::numeric_limits<double>::quiet_NaN());
  return std::make_tuple(timestampVar, timestampsDimension);
}

netCDF::NcDim createAndFillUVDim(
    const netCDF::NcGroup& stream,
    const std::string& name,
    const int size)
{
  const auto dim = stream.addDim(name, size);
  std::vector<unsigned int> data(size);
  std::generate(data.begin(), data.end(), [n = 0]() mutable { return n++; });

  stream.addVar(name, netCDF::ncUint, {dim}).putVar(data.data());
  return dim;
}

netCDF::NcDim createThirdDimensionRGB(const netCDF::NcGroup& streamGroup)
{
  const auto thirdDim = streamGroup.addDim(
      io::color3ChannelsAxesVarName,
      io::color3ChannelsAxesData.size());
  streamGroup
      .addVar(io::color3ChannelsAxesVarName, netCDF::ncString, {thirdDim})
      .putVar(io::color3ChannelsAxesData.data());
  return thirdDim;
}
netCDF::NcDim createThirdDimensionRGBA(const netCDF::NcGroup& streamGroup)
{
  const auto thirdDim = streamGroup.addDim(
      io::color4ChannelsAxesVarName,
      io::color4ChannelsAxesData.size());
  streamGroup
      .addVar(io::color4ChannelsAxesVarName, netCDF::ncString, {thirdDim})
      .putVar(io::color4ChannelsAxesData.data());
  return thirdDim;
}

}  // namespace io