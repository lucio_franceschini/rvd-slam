#pragma once
#include <H5Cpp.h>
#pragma warning(push, 0)
#include <opencv2/opencv.hpp>
#pragma warning(pop)
#include <string>

#include "../io_data.h"
#include "../units.h"

namespace filters
{
const auto boxFilterName = std::string("BoxFilter");

struct BoxFilterParameters
{
  const cv::Rect validArea;
  const Meter minDistance;
  const Meter maxDistance;
};

cv::Mat boxFilter(const cv::Mat& depth, const BoxFilterParameters& parameters);

BoxFilterParameters readBoxFilterInfo(const H5::Group& filterInfo);
}  // namespace filters