#include "filters.h"

namespace filters
{
FilterParameters extractFilterData(const H5::Group& filter)
{
  const auto name =
      io::extractAttribute<std::string>(filter, io::filterAliasMetadata);

  if (name == boxFilterName)
  {
    return readBoxFilterInfo(filter);
  }
  else if (name == noFilterName)
  {
    return NoFilterParameters{};
  }
  else
  {
    throw std::logic_error("Not implemented");
  }
}
}  // namespace filters