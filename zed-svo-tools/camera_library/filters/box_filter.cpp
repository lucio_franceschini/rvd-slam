#include "box_filter.h"

namespace filters
{
cv::Mat boxFilter(const cv::Mat& depth, const BoxFilterParameters& parameters)
{
  const auto bordersMask = [&depth, &parameters] {
    auto mask = cv::Mat(cv::Mat::zeros(depth.size(), CV_8U));
    mask(parameters.validArea) = 255;
    return mask;
  }();

  const auto depthMask = [&depth, &parameters] {
    cv::Mat depthMask;
    cv::inRange(
        depth,
        cv::Scalar(parameters.minDistance.value()),
        cv::Scalar(parameters.maxDistance.value()),
        depthMask);
    return depthMask;
  }();

  const auto finalMask = [&bordersMask, &depthMask] {
    cv::Mat finalMask;
    cv::bitwise_and(bordersMask, depthMask, finalMask);
    return finalMask;
  }();

  cv::Mat maskedDepth;
  depth.copyTo(maskedDepth, finalMask);
  return maskedDepth;
}

BoxFilterParameters readBoxFilterInfo(const H5::Group& filterInfo)
{
  const auto attributeArray = [&filterInfo](const std::string& attributeName) {
    const auto attrInfo = filterInfo.openAttribute(attributeName);

    std::array<int, 2> value;
    attrInfo.read(attrInfo.getDataType(), value.data());
    return value;
  };
  const auto corner_1 = attributeArray("corner_1");
  const auto point_1 = cv::Point(corner_1[0], corner_1[1]);

  const auto corner_2 = attributeArray("corner_2");
  const auto point_2 = cv::Point(corner_2[0], corner_2[1]);

  return {cv::Rect(point_1, point_2),
          Meter(io::extractAttribute<double>(filterInfo, "min_distance")),
          Meter(io::extractAttribute<double>(filterInfo, "max_distance"))};
}
}  // namespace filters