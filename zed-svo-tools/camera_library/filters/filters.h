#pragma once

#include <variant>

#include "box_filter.h"
#include "no_filter.h"

namespace filters
{
using FilterParameters = std::variant<NoFilterParameters, BoxFilterParameters>;

FilterParameters extractFilterData(const H5::Group& filter);
}  // namespace filters