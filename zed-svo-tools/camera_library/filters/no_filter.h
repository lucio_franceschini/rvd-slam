#pragma once
#include <string>

namespace filters
{
const auto noFilterName = std::string("None");

struct NoFilterParameters
{
};

}  // namespace filters
