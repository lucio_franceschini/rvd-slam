#%%
from datetime import datetime, timezone, timedelta
from itertools import product
from pathlib import Path
import matplotlib.pyplot as plt
from dateutil.parser import parse
from export_video_command import create_video_frame

import h5py
import netCDF4
import numpy as np
import pandas as pd
import scipy as sp
import xarray as xr

import plotting
import utility
from names import (
    DEVICE_D435,
    DEVICE_ZED,
    GROUP_PATHS,
    GROUP_SESSIONS,
    DEVICE_HPS160,
    GROUP_FILTERS,
)

#%%
root = Path("D:/recordings/root.nc")
sessions = ["20200221T132133.478275900+0000"]


def depth_sensor_for_sensor(sensor: str) -> str:
    if DEVICE_ZED == sensor:
        return "VIEW_DEPTH"
    elif DEVICE_D435 == sensor or DEVICE_HPS160 == sensor:
        return "DEPTH"


def _paths_sensor(root_path: Path, session: str, sensor: str):
    with h5py.File(root_path, "r") as root_file:
        return list(
            root_file[f"/{GROUP_SESSIONS}/{session}/{sensor}/{GROUP_PATHS}"].keys()
        )


#%%
for session, sensor in product(sessions, [DEVICE_D435, DEVICE_ZED, DEVICE_HPS160]):
    for path in _paths_sensor(root, session, sensor):
        fig, ax = plotting.open_and_plot_path(root, session, sensor, path)
        ax.set_title(f"Sensor: {sensor}\nPath: {path}")
