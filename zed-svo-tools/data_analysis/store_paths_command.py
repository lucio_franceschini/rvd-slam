import argparse
import logging
from pathlib import Path
import h5py
from names import GROUP_SESSIONS, GROUP_PATHS, META_TIMESTAMP


def _retrive_paths_group(root_group, path_group):
    sessions = root_group.require_group(GROUP_SESSIONS)
    timestamp = path_group.attrs["session_timestamp"]
    session = sessions.require_group(timestamp)
    if META_TIMESTAMP not in session.attrs:
        session.attrs[META_TIMESTAMP] = timestamp
    sensor = session.require_group(path_group.attrs["sensor"])
    return sensor.require_group(GROUP_PATHS)


def _merge_file(root_group, root_path, path_file):
    with h5py.File(path_file, "a") as path_group:
        uuid_path = path_file.stem
        logging.info(f"Merging {path_group}")
        paths = _retrive_paths_group(root_group, path_group)
        try:
            logging.info(f"{paths[uuid_path]} already exists, skipping...")
        except KeyError:
            paths[uuid_path] = h5py.ExternalLink(
                path_file.relative_to(root_path.parent), "/"
            )


def store_paths(args):
    with h5py.File(args.dataset_path, "a") as root_file:
        for result in list(args.results_path.glob("*.nc")):
            _merge_file(root_file, args.dataset_path, result)
