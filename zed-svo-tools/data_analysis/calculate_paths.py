#%%
import subprocess
import h5py
import logging
from pathlib import Path
import dask
from dask.distributed import Client
from typing import List
from itertools import product
from names import DEVICE_D435, DEVICE_ZED, DEVICE_HPS160
import data
from datetime import datetime, timedelta, timezone

#%%
def _translate_dict_to_params(option: dict):
    return [f"--{key}={value}" for key, value in option.items()]


def _run_process(
    root_command: Path, root_path: Path, output_folder: Path, option: dict
):
    try:
        params = [
            root_command / "path_tracking" / "path_tracking.exe",
            *_translate_dict_to_params(
                {**option, "root_path": root_path, "output_folder": output_folder}
            ),
        ]
        subprocess.run(params, check=True, creationflags=subprocess.CREATE_NEW_CONSOLE)
        return 1
    except subprocess.CalledProcessError as ex:
        logging.error(f"Error for the following case: {option}\n{ex}")
        return 0


def compute_paths(
    root_command: Path, root_path: Path, output_folder: Path, options: List[dict]
):
    Path(output_folder).mkdir(exist_ok=True)
    results_codes = [
        dask.delayed(lambda x: _run_process(root_command, root_path, output_folder, x))(
            option
        )
        for option in options
    ]
    total = dask.delayed(sum)(results_codes)
    print(f"{total.compute()}/{len(results_codes)}")


def executables_root_folder():
    root_base = Path(__file__).parent.parent / "x64"
    release_folder = root_base / "Release"
    if release_folder.exists():
        return release_folder
    else:
        raise NotADirectoryError()


#%%
base_path = Path("S:/02-Studentenprojekte/RACOON/2020-02_Lucio_Data/")

zed_algorithms_ids = [1, 2, 3]
zed_filters_ids = [0, 1]

d435_algorithms_ids = [4, 5, 6]
d435_filters_ids = [0, 2]

hps_algorithms_ids = [7, 8, 9]
hps_filters_ids = [0, 3]


algorithm_zed_sdk_id = 0
filter_none_id = 0
path_sources_zed_sdk = base_path / Path("sources_albedo")


format_ts = "%Y%m%dT%H%M%S%z"


def generate_zed_sdk_algorithms(
    sessions_ts, algorithm_zed_sdk_id, filter_none_id, path_sources_zed_sdk
):
    return [
        {
            "session_name": session_ts["name"],
            "sensor": DEVICE_ZED,
            "algorithm_id": algorithm_zed_sdk_id,
            "filter_id": filter_none_id,
            "svo_file": path_sources_zed_sdk
            / (session_ts["name"][:-7] + "+0000")
            / "recording.svo",
        }
        for session_ts in sessions_ts
    ]


def generate_all_combinations(sessions_ts, sensor, algorithms_ids, filters_ids):
    return [
        {
            "session_name": session_ts["name"],
            "sensor": sensor,
            "algorithm_id": algorithm_id,
            "filter_id": filter_id,
            "from": (
                session_ts["timestamp_at_stop"] - timedelta(seconds=90)
            ).__format__(format_ts),
            "to": session_ts["timestamp_at_stop"].__format__(format_ts),
        }
        for session_ts, algorithm_id, filter_id in product(
            sessions_ts, algorithms_ids, filters_ids
        )
    ]


options = (
    generate_zed_sdk_algorithms(
        data.albedo + data.albedo_reference,
        algorithm_zed_sdk_id,
        filter_none_id,
        path_sources_zed_sdk,
    )
    + generate_all_combinations(
        data.albedo + data.albedo_reference,
        DEVICE_ZED,
        zed_algorithms_ids,
        zed_filters_ids,
    )
    + generate_all_combinations(
        data.no_albedo
        + data.no_albedo_refences_without_ir_cameras
        + data.albedo
        + data.albedo_reference,
        DEVICE_D435,
        d435_algorithms_ids,
        d435_filters_ids,
    )
    + generate_all_combinations(
        data.no_albedo
        + data.no_albedo_refences_without_ir_cameras
        + data.albedo
        + data.albedo_reference,
        DEVICE_HPS160,
        hps_algorithms_ids,
        hps_filters_ids,
    )
)

#%%
compute_paths(
    executables_root_folder(),
    root_path=base_path / "root.nc",
    output_folder=base_path / "results",
    options=options,
)


# %%
