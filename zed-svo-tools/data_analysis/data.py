from datetime import datetime, timedelta, timezone

generic = ["20200123T144211.916932000+0000", "20200219T095152.343512000+0000"]


def _convert_ts_to_datetime(ts_ms: int) -> datetime:
    return datetime.fromtimestamp(ts_ms / 1000, tz=timezone.utc)

tests = [
    {"name": "20200123T160747.504198000+0000"},
    {"name": "20200123T160814.621387000+0000"},
    {"name": "20200123T160841.118004000+0000"},
    {"name": "20200123T160907.401074000+0000"},
    {"name": "20200123T160934.405277000+0000"},
    {"name": "20200123T161001.009059000+0000"},
    {"name": "20200123T161027.350321000+0000"},
    {"name": "20200123T161054.708239000+0000"},
    {"name": "20200123T161121.309665000+0000"},
    {"name": "20200123T161147.585534000+0000"},
    {"name": "20200123T161214.667065000+0000"},
    {"name": "20200123T161241.232225000+0000"},
    {"name": "20200123T161307.610476000+0000"},
    {"name": "20200123T161334.559360000+0000"},
    {"name": "20200123T161401.301657000+0000"},
    {"name": "20200123T161427.436367000+0000"},
    {"name": "20200123T161454.462999000+0000"},
    {"name": "20200123T161520.907231000+0000"},
    {"name": "20200123T162229.378557000+0000"},
    {"name": "20200123T162256.729614000+0000"},
    {"name": "20200123T162323.698311000+0000"},
    {"name": "20200123T162350.245299000+0000"},
    {"name": "20200123T162416.568143000+0000"},
    {"name": "20200123T162442.695440000+0000"},
    {"name": "20200123T162508.991297000+0000"},
    {"name": "20200123T162535.899813000+0000"},
    {"name": "20200123T162602.756703000+0000"},
    {"name": "20200123T162629.142569000+0000"},
    {"name": "20200123T162655.720254000+0000"},
    {"name": "20200123T162722.118243000+0000"},
    {"name": "20200123T162748.456761000+0000"},
    {"name": "20200123T162815.425253000+0000"},
    {"name": "20200123T162842.505067000+0000"},
    {"name": "20200123T162909.160462000+0000"},
    {"name": "20200123T162935.768964000+0000"},
    {"name": "20200123T163002.082532000+0000"},
]

no_albedo_refences_with_ir_cameras = [
    {
        "name": "20200219T130214.805603100+0000",
        "room_lights_on": False,
        "timestamp_at_stop": _convert_ts_to_datetime(1582117432850),
    },
    {"name": "20200219T131023.310175800+0000", "room_lights_on": True},
    {"name": "20200219T131454.966276400+0000", "room_lights_on": True},
    {"name": "20200219T131901.266908800+0000", "room_lights_on": True},
    {"name": "20200219T132305.411017900+0000", "room_lights_on": True},
    {"name": "20200219T132748.217478000+0000", "room_lights_on": True},
    {"name": "20200219T133211.285480200+0000", "room_lights_on": True},
    {"name": "20200219T133653.818413900+0000", "room_lights_on": True},
    {"name": "20200219T134205.072023000+0000", "room_lights_on": True},
    {"name": "20200219T134654.850822900+0000", "room_lights_on": True},
]

no_albedo_refences_without_ir_cameras = [
    {
        "name": "20200221T112910.816101100+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582284649752),
    },
    {
        "name": "20200221T113414.852025200+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582284953812),
    },
    {
        "name": "20200221T113916.747543400+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582285255691),
    },
    {
        "name": "20200221T114353.883023100+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582285532623),
    },
    {
        "name": "20200221T114727.299898300+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582285746263),
    },
    {
        "name": "20200221T115048.859353000+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582285947583),
    },
    {
        "name": "20200221T115425.395165000+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582286164212),
    },
    {
        "name": "20200221T121110.346419800+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582287168906),
    },
    {
        "name": "20200221T121643.594799800+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582287502228),
    },
    {
        "name": "20200221T122104.438632700+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582287762814),
    },
]

no_albedo = [
    {
        "name": "20200219T102226.408819700+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582107860687),
    },
    {
        "name": "20200219T103238.261749100+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582108455264),
    },
    {
        "name": "20200219T103917.124311700+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582108854534),
    },
    {
        "name": "20200219T104555.202113800+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582109253070),
    },
    {
        "name": "20200219T105238.612877200+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582109655737),
    },
    {
        "name": "20200219T105755.025045000+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582109972019),
    },
    {
        "name": "20200219T110236.726511600+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582110252490),
    },
    {
        "name": "20200219T110740.957500100+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582110558041),
    },
    {
        "name": "20200219T111211.666802000+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582110828312),
    },
    {
        "name": "20200219T111652.051880200+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582111109187),
    },
    {
        "name": "20200219T112208.502657600+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582111425837),
    },
    {
        "name": "20200219T112727.095824800+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582111744165),
    },
    {
        "name": "20200219T113209.695873900+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582112026755),
    },
    {
        "name": "20200219T113707.153126400+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582112324453),
    },
    {
        "name": "20200219T114203.112219300+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582112620214),
    },
    {
        "name": "20200219T114657.199483100+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582112914462),
    },
    {
        "name": "20200219T151018.273167000+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582125116102),
    },
    {
        "name": "20200219T151555.614665900+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582125453221),
    },
    {
        "name": "20200219T151938.180721300+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582125675844),
    },
    {
        "name": "20200219T152329.916258300+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582125907029),
    },
    {
        "name": "20200219T152720.870705300+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582126138042),
    },
    {
        "name": "20200219T153121.562514800+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582126378531),
    },
    {
        "name": "20200219T153608.343811100+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582126665718),
    },
    {
        "name": "20200219T154125.664013700+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582126982882),
    },
    {
        "name": "20200219T154758.821108500+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582127375970),
    },
    {
        "name": "20200219T155450.356839200+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582127787597),
    },
    {
        "name": "20200219T160452.891532700+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582128390016),
    },
    {
        "name": "20200219T161016.123760700+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582128713242),
    },
    {
        "name": "20200219T161514.394626700+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582129010606),
    },
    {
        "name": "20200219T162135.779043900+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582129392883),
    },
    {
        "name": "20200219T162615.144356800+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582129672280),
    },
]

albedo_reference = [
    {
        "name": "20200221T132133.478275900+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582291391820),
    },
    {
        "name": "20200221T132658.259450500+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582291716747),
    },
    {
        "name": "20200221T133126.068860900+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582291984333),
    },
    {
        "name": "20200221T133554.389464200+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582292252842),
    },
    {
        "name": "20200221T134014.731426300+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582292513081),
    },
    {
        "name": "20200221T134417.153217400+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582292756135),
    },
    {
        "name": "20200221T134814.064140800+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582292992566),
    },
    {
        "name": "20200221T135214.786923200+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582293233367),
    },
    {
        "name": "20200221T135630.824964900+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582293488932),
    },
    {
        "name": "20200221T140028.538258700+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582293727022),
    },
]

albedo = [
    {
        "name": "20200221T141238.484460800+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582294457330),
    },
    {
        "name": "20200221T141659.279895200+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582294717979),
    },
    {
        "name": "20200221T142050.233259500+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582294950853),
    },
    {
        "name": "20200221T142457.796636200+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582295196241),
    },
    {
        "name": "20200221T142903.949621000+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582295441954),
    },
    {
        "name": "20200221T143334.109409500+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582295712198),
    },
    {
        "name": "20200221T143823.593815400+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582296001706),
    },
    {
        "name": "20200221T144246.127202700+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582296263911),
    },
    {
        "name": "20200221T144718.507568600+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582296536261),
    },
    {
        "name": "20200221T145212.814385400+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582296830459),
    },
    {
        "name": "20200221T145708.933453100+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582297126597),
    },
    {
        "name": "20200221T150247.607317800+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582297464775),
    },
    {
        "name": "20200221T150748.034147900+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582297765290),
    },
    {
        "name": "20200221T160126.440225500+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582300985458),
    },
    {
        "name": "20200221T160508.816318100+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582301207794),
    },
    {
        "name": "20200221T160927.596425500+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582301466326),
    },
    {
        "name": "20200221T161511.426084100+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582301810122),
    },
    {
        "name": "20200221T161943.775978400+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582302082634),
    },
    {
        "name": "20200221T162351.771345000+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582302330487),
    },
    {
        "name": "20200221T162835.450825500+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582302614267),
    },
    {
        "name": "20200221T163218.195298000+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582302836925),
    },
    {
        "name": "20200221T163657.058093600+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582303115758),
    },
    {
        "name": "20200221T164213.349318100+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582303431995),
    },
    {
        "name": "20200221T164611.522823500+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582303670146),
    },
    {
        "name": "20200221T165041.852401600+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582303940328),
    },
    {
        "name": "20200221T165449.757374100+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582304188243),
    },
    {
        "name": "20200221T171052.672544400+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582305151044),
    },
    {
        "name": "20200221T171450.308880000+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582305388627),
    },
    {
        "name": "20200221T171911.279664300+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582305649614),
    },
    {
        "name": "20200221T172336.671331000+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582305914785),
    },
    {
        "name": "20200221T172819.844047400+0000",
        "timestamp_at_stop": _convert_ts_to_datetime(1582306198034),
    },
]

