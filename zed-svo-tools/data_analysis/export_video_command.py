import logging
from datetime import datetime, timedelta
from pathlib import Path
from importlib import reload
import cv2

import h5py
import numpy as np
import pandas as pd
import xarray as xr
from dateutil.parser import parse

from names import (
    DEVICE_D435,
    DEVICE_HPS160,
    DEVICE_ZED,
    GROUP_ALGORITHMS,
    GROUP_FILTERS,
    GROUP_SESSIONS,
    GROUP_SOURCES,
)


def _depth_frame_to_meters(data: h5py.Dataset, ds: h5py.Group, sensor_name: str):
    if sensor_name == DEVICE_D435:
        res = data * ds.attrs["scale_factor"][0]
        return res
    elif sensor_name == DEVICE_HPS160:
        return data * 0.001
    else:
        return data


def _create_color_map(height: int, width: int, ticks: list, unit: str):
    colormap = np.linspace(1, 0, height)
    colormap_mat = cv2.applyColorMap(
        (colormap * 255).astype(np.uint8), cv2.COLORMAP_VIRIDIS
    )
    total_width = int(2.5 * width)
    white_part = int(total_width * 0.6)
    final_image = np.empty((height, total_width, 3))
    final_image[:, :white_part, :] = 255
    final_image[:, white_part:, :] = colormap_mat

    font = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 0.7
    font_thickness = 2
    text_size, _ = cv2.getTextSize(str(ticks[0]), font, font_scale, font_thickness)

    for idx, tick in enumerate(ticks):
        height_text_line = text_size[0] / height
        y = int(height * (1 - height_text_line) * (1 - idx / (len(ticks) - 1))) + int(
            height * height_text_line
        )

        cv2.putText(
            final_image,
            f"{tick} {unit}",
            (20, y),
            font,
            font_scale,
            (0, 0, 0),
            font_thickness,
            cv2.LINE_AA,
        )

    return final_image


def _resize(data, scale_factor):
    width = int(data.shape[1] * scale_factor)
    height = int(data.shape[0] * scale_factor)
    return cv2.resize(data, (width, height), interpolation=cv2.INTER_AREA)


def _create_depth_image(
    idx_timestamp,
    ds,
    data,
    sensor_name,
    skip_empty,
    range_values,
    frame_number,
    scale_factor,
    filter_function,
):
    if len(data.shape) == 3:
        cleaned_data = data[idx_timestamp, :, :]
    else:
        cleaned_data = data[idx_timestamp, :, :, -1]
        cleaned_data[cleaned_data == 65300.0] = 0.0
    frame_meters = filter_function(
        _depth_frame_to_meters(_resize(cleaned_data, scale_factor), ds, sensor_name)
    )

    if skip_empty and np.sum(frame_meters) == 0.0:
        return None
    scaled_data = (frame_meters - range_values[0]) / (
        range_values[-1] - range_values[0]
    )

    im_color = cv2.applyColorMap(
        (scaled_data * 255).astype(np.uint8), cv2.COLORMAP_VIRIDIS
    )

    colormap = _create_color_map(
        height=frame_meters.shape[0], width=50, ticks=range_values, unit="m"
    )

    final_image = np.empty(
        (im_color.shape[0], im_color.shape[1] + colormap.shape[1], im_color.shape[2])
    )
    final_image[:, : im_color.shape[1], :] = im_color
    final_image[:, im_color.shape[1] :, :] = colormap

    cv2.putText(
        final_image,
        frame_number,
        (10, 40),
        cv2.FONT_HERSHEY_SIMPLEX,
        1,
        (0, 0, 250),
        2,
        cv2.LINE_AA,
    )
    return final_image


def _create_color_image_D435(
    idx_timestamp, ds, data, skip_empty, frame_number, scale_factor
):
    frame = _resize(data[idx_timestamp, :, :, :], scale_factor)
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(frame, frame_number, (10, 40), font, 1, (250, 0, 0), 2, cv2.LINE_AA)
    return np.flip(frame, axis=2)


def _create_color_image_zed(
    idx_timestamp, ds, data, skip_empty, frame_number, scale_factor
):
    frame = _resize(data[idx_timestamp, :, :, :], scale_factor)
    cv2.putText(
        frame,
        frame_number,
        (10, 40),
        cv2.FONT_HERSHEY_SIMPLEX,
        1,
        (0, 0, 250),
        2,
        cv2.LINE_AA,
    )
    return frame[:, :, :3]


def _create_confidence_image(
    idx_timestamp, ds, data, skip_empty, frame_number, scale_factor
):
    range_values = list(range(0, 101, 25))
    frame = _resize(data[idx_timestamp, :, :], scale_factor)
    if skip_empty and np.sum(frame) == 0.0:
        return None

    scaled_data = frame / range_values[-1]

    im_color = cv2.applyColorMap(
        (scaled_data * 255).astype(np.uint8), cv2.COLORMAP_VIRIDIS
    )

    colormap = _create_color_map(
        height=data.shape[1], width=60, ticks=range_values, unit="%"
    )

    final_image = np.empty(
        (im_color.shape[0], im_color.shape[1] + colormap.shape[1], im_color.shape[2])
    )
    final_image[:, : im_color.shape[1], :] = im_color
    final_image[:, im_color.shape[1] :, :] = colormap

    cv2.putText(
        final_image,
        frame_number,
        (10, 40),
        cv2.FONT_HERSHEY_SIMPLEX,
        1,
        (0, 0, 250),
        2,
        cv2.LINE_AA,
    )
    return final_image


def _create_infrared_image(
    idx_timestamp, ds, data, skip_empty, frame_number, scale_factor
):
    range_values = np.linspace(0, 255, 6, dtype=int)
    frame = _resize(data[idx_timestamp, :, :], scale_factor)
    if skip_empty and np.sum(frame) == 0.0:
        return None

    scaled_data = frame / range_values[-1]

    im_color = cv2.applyColorMap(
        (scaled_data * 255).astype(np.uint8), cv2.COLORMAP_VIRIDIS
    )

    colormap = _create_color_map(
        height=data.shape[1], width=60, ticks=range_values, unit=""
    )

    final_image = np.empty(
        (im_color.shape[0], im_color.shape[1] + colormap.shape[1], im_color.shape[2])
    )
    final_image[:, : im_color.shape[1], :] = im_color
    final_image[:, im_color.shape[1] :, :] = colormap

    cv2.putText(
        final_image,
        frame_number,
        (10, 40),
        cv2.FONT_HERSHEY_SIMPLEX,
        1,
        (0, 0, 250),
        2,
        cv2.LINE_AA,
    )
    return final_image


def _read_filter_data(root_path: Path, filter_id: int):
    with h5py.File(root_path, "r") as root_file:
        for filter_name, filter_data in root_file[f"/{GROUP_FILTERS}"].items():
            if filter_data.attrs["filter_id"] == filter_id:
                return {**filter_data.attrs}
        else:
            raise KeyError()


def _box_filter(data: np.ndarray, filter_data: dict) -> np.array:
    filter_name = filter_data["alias"]
    if filter_name != "BoxFilter":
        raise KeyError(filter_name)
    mask_box = np.empty_like(data, dtype=bool)
    corner_1 = filter_data["corner_1"]
    corner_2 = filter_data["corner_2"]
    mask_box[:, :] = True
    mask_box[corner_1[1] : corner_2[1], corner_1[0] : corner_2[0]] = False
    data[mask_box] = 0
    data[data < filter_data["min_distance"]] = 0
    data[data > filter_data["max_distance"]] = 0
    return data


def _find_filter_function(root_path: Path, filter_id: int = None):
    if filter_id == None:
        return lambda x: x
    filter_data = _read_filter_data(root_path, filter_id)
    alias = filter_data["alias"]
    if alias == "None":
        return lambda x: x
    elif alias == "BoxFilter":
        return lambda x: _box_filter(x, filter_data)
    else:
        raise NotImplementedError()


def create_video_frame(
    root_path: Path,
    session_name: str,
    sensor_name: str,
    section_name: str,
    idx_timestamp: int,
    range_values: list = None,
    skip_empty: bool = False,
    scale_factor: float = 1,
    show_frame_number: bool = True,
    filter_id: int = None,
) -> np.array:
    with h5py.File(root_path, "r") as file:
        ds = file[
            f"/{GROUP_SESSIONS}/{session_name}/{sensor_name}/{GROUP_SOURCES}/{section_name}"
        ]
        data = ds["data"]
        total_frames = data.shape[0]
        if show_frame_number:
            frame_number = f"{idx_timestamp+1}/{total_frames}"
        else:
            frame_number = ""
        if section_name in ["DEPTH", "VIEW_DEPTH"]:
            filter_function = _find_filter_function(root_path, filter_id=filter_id)
            return _create_depth_image(
                idx_timestamp,
                ds,
                data,
                sensor_name,
                skip_empty,
                range_values,
                frame_number,
                scale_factor,
                filter_function=filter_function,
            )
        elif section_name == "COLOR":
            return _create_color_image_D435(
                idx_timestamp, ds, data, skip_empty, frame_number, scale_factor
            )
        elif section_name in ["VIEW_LEFT", "VIEW_RIGHT"]:
            return _create_color_image_zed(
                idx_timestamp, ds, data, skip_empty, frame_number, scale_factor
            )
        elif section_name == "VIEW_CONFIDENCE":
            return _create_confidence_image(
                idx_timestamp, ds, data, skip_empty, frame_number, scale_factor
            )
        elif section_name in ["LEFT_INFRARED", "RIGHT_INFRARED"]:
            return _create_infrared_image(
                idx_timestamp, ds, data, skip_empty, frame_number, scale_factor
            )
        else:
            raise KeyError(section_name)


def create_video_source(
    root_path: Path,
    output_path: Path,
    session_name: str,
    sensor_name: str,
    section_name: str,
    range_values: list = None,
    skip_empty: bool = False,
    scale_factor: float = 1,
    filter_id: int = None,
) -> None:
    output_path.mkdir(exist_ok=True)
    with h5py.File(root_path, "r") as file:
        ds = file[
            f"/{GROUP_SESSIONS}/{session_name}/{sensor_name}/{GROUP_SOURCES}/{section_name}"
        ]
        data = ds["data"]
        saved_images = 0
        total_frames = data.shape[0]
        for idx_timestamp in range(total_frames):
            frame_number = f"{idx_timestamp+1}/{total_frames}"
            print(frame_number)
            final_image = create_video_frame(
                root_path=root_path,
                session_name=session_name,
                sensor_name=sensor_name,
                section_name=section_name,
                idx_timestamp=idx_timestamp,
                range_values=range_values,
                skip_empty=skip_empty,
                scale_factor=scale_factor,
                show_frame_number=True,
                filter_id=filter_id,
            )
            if final_image is not None:
                output_file = output_path / f"{saved_images:08d}.png"
                cv2.imwrite(str(output_file), final_image)
                saved_images += 1

