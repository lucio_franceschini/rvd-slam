import argparse
import logging
import uuid
from ruamel.yaml import YAML
from pathlib import Path
import h5py
from names import GROUP_FILTERS, GROUP_ALGORITHMS

YAML_FILTERS_NAME = "filters"
YAML_ALGORITHMS_NAME = "algorithms"
ATTRIBUTE_NAME_FILTER_ID = "filter_id"
ATTRIBUTE_NAME_ALGORITHMS_ID = "algorithm_id"


def _find_max_id_used(root_group, id_field_name):
    return max(
        [
            root_group[group_name].attrs[id_field_name]
            for group_name in root_group
        ],
        default=-1,
    )


def _save_list_dicts_as_metadata(root_group, dicts, id_field_name):
    first_id = _find_max_id_used(root_group, id_field_name) + 1
    if dicts:
        for idx_dict, dictionary in enumerate(dicts):
            group_uuid = str(uuid.uuid1())
            logging.info(f"Saving new group with UUID {group_uuid}")
            group = root_group.create_group(group_uuid)
            group.attrs.create(id_field_name, first_id + idx_dict)

            for key, value in dictionary.items():
                group.attrs.create(key, value)


def store_metadata(args):
    logging.info(f"Opening {args.dataset_path}")
    with h5py.File(args.dataset_path, "a") as ds:
        yaml = YAML(typ="safe")
        data = yaml.load(args.data_path)
        if YAML_FILTERS_NAME in data:
            logging.info(f"Saving new filters")
            _save_list_dicts_as_metadata(
                ds.require_group(GROUP_FILTERS),
                data[YAML_FILTERS_NAME],
                id_field_name=ATTRIBUTE_NAME_FILTER_ID,
            )
        if YAML_ALGORITHMS_NAME in data:
            logging.info(f"Saving new algorithms")
            _save_list_dicts_as_metadata(
                ds.require_group(GROUP_ALGORITHMS),
                data[YAML_ALGORITHMS_NAME],
                id_field_name=ATTRIBUTE_NAME_ALGORITHMS_ID,
            )
