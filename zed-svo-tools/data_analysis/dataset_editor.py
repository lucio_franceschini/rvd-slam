import argparse
import logging
from pathlib import Path
from store_metadata_command import store_metadata
from store_paths_command import store_paths
from store_sources_command import store_sources
from export_video_command import create_video_source
from export_path import export_path


def store_metadata_parser(subparsers):
    parser = subparsers.add_parser("metadata", help="Store metadata on the dataset")
    parser.add_argument("dataset_path", type=Path, help="Path to the root dataset")
    parser.add_argument("data_path", type=Path, help="Path to the .yaml file")
    parser.set_defaults(func=store_metadata)


def store_sources_parser(subparsers):
    parser = subparsers.add_parser("sources", help="Add link to data on the dataset")
    parser.add_argument("dataset_path", type=Path, help="Path to the root dataset")
    parser.add_argument(
        "data_path",
        type=Path,
        help="Path to the data folder containing the experiments",
    )
    parser.set_defaults(func=store_sources)


def store_paths_parser(subparsers):
    parser = subparsers.add_parser("paths", help="Merge results on the main dataset")
    parser.add_argument("dataset_path", type=Path, help="Path to the root dataset")
    parser.add_argument(
        "results_path", type=Path, help="Path of the folder containing the results"
    )
    parser.set_defaults(func=store_paths)


def export_source_parser(subparsers):
    parser = subparsers.add_parser("source", help="Export a video")
    parser.add_argument("dataset_path", type=Path, help="Path to the root dataset")
    parser.add_argument(
        "results_path", type=Path, help="Path of the folder containing the results"
    )
    parser.add_argument("session", help="Name of the session to export")
    parser.add_argument("sensor", help="Name of the sensor to export")
    parser.add_argument("channel", help="Name of the channel to export")
    parser.add_argument("--from_value", type=int, help="First value of the colorband")
    parser.add_argument("--to_value", type=int, help="Last value of the colorband")
    parser.add_argument(
        "--scale_factor", default=1.0, type=int, help="Last value of the colorband"
    )
    parser.add_argument(
        "--filter_id", default=None, type=int, help="Filter to apply to the depth map"
    )

    def range_if_exists(args):
        if args.from_value != None:
            return list(range(args.from_value, args.to_value))
        else:
            return None

    parser.set_defaults(
        func=lambda args: create_video_source(
            args.dataset_path,
            args.results_path,
            args.session,
            args.sensor,
            args.channel,
            range_values=range_if_exists(args),
            skip_empty=True,
            scale_factor=args.scale_factor,
            filter_id=args.filter_id,
        )
    )


def export_path_parser(subparsers):
    parser = subparsers.add_parser("path", help="Export path and metric")
    parser.add_argument("dataset_path", type=Path, help="Path to the root dataset")
    parser.add_argument(
        "results_path", type=Path, help="Path of the folder containing the results"
    )
    parser.add_argument("session", help="Name of the session to export")
    parser.add_argument("sensor", help="Name of the sensor to export")
    parser.add_argument("--path_name", help="Name of the path to export", default=None)

    parser.set_defaults(
        func=lambda args: export_path(
            root_path=args.dataset_path,
            output_path=args.results_path,
            session_name=args.session,
            sensor_name=args.sensor,
            path_name=args.path_name,
        )
    )


def main():
    parser = argparse.ArgumentParser(description="Manage RACOON Lab's cameras dataset.")
    subparsers = parser.add_subparsers(help="sub-command help")
    subparsers_store = subparsers.add_parser(
        "store", help="Store data to the HDF5 files"
    ).add_subparsers(help="sub-command help")
    subparsers_export = subparsers.add_parser(
        "export", help="Store data from the HDF5 file"
    ).add_subparsers(help="sub-command help")

    store_metadata_parser(subparsers_store)
    store_paths_parser(subparsers_store)
    store_sources_parser(subparsers_store)
    export_source_parser(subparsers_export)
    export_path_parser(subparsers_export)
    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
