import re
import uuid
from pathlib import Path

import h5py
import numpy as np
import pandas as pd
import xarray as xr
from scipy.spatial import cKDTree

from names import (
    DEVICE_ZED,
    GROUP_ALGORITHMS,
    GROUP_FILTERS,
    GROUP_SESSIONS,
    GROUP_SOURCES,
    GROUP_PATHS,
)
from plotting import find_netCDF_file_from_HDF5


def load_old_file_format(path):
    with open(path, "r") as txt:
        lines = txt.readlines()
        num_elements = len(lines) - 2
        positions = np.empty([num_elements, 3])
        orientations = np.empty([num_elements, 4])
        timestamps = np.empty([num_elements], dtype="datetime64[ns]")

        def list_to_float(lst):
            return list(map(float, lst))

        for idx, line in enumerate(lines[2:]):
            frags = line.split()
            timestamps[idx] = np.datetime64(int(frags[0]), "ns")
            positions[idx, :] = list_to_float(frags[1:4])
            orientations[idx, :] = list_to_float(frags[4:])
    positions_dr = xr.DataArray(
        positions,
        coords=[timestamps, ["x", "y", "z"]],
        dims=["timestamp", "position_axes"],
    )

    orientations_dr = xr.DataArray(
        orientations,
        coords=[timestamps, ["x", "y", "z", "r"]],
        dims=["timestamp", "quaternion_axes"],
    )
    return xr.Dataset({"position": positions_dr, "orientation": orientations_dr})


def _sun_target(name: str):
    groups = re.search(r"trajectory[A-Za-z]+_(\d+)_(\d+)", name)
    return {
        "sun_position": int(groups.group(1)),
        "target_orientation": int(groups.group(2)),
    }


def _sun_only(name: str):
    groups = re.search(r"trajectory[A-Za-z]+_S_(\d+)", name)
    return {"sun_position": int(groups.group(1))}


def _sun_reference(name: str):
    groups = re.search(r"trajectory[A-Za-z]+_R_90_(\d+)", name)
    return {"sun_position": int(groups.group(1))}


def _target_only(name: str):
    groups = re.search(r"trajectory[A-Za-z]+_T_(\d+)", name)
    return {"target_orientation": int(groups.group(1))}


def _reference(name: str):
    groups = re.search(r"trajectory[A-Za-z]+_Referenz \((\d+)\)", name)
    return {"reference": int(groups.group(1))}


def _other_metadata(name: str) -> dict:
    try:
        return _sun_target(name)
    except:
        pass
    try:
        return _sun_only(name)
    except:
        pass
    try:
        return _sun_reference(name)
    except:
        pass
    try:
        return _target_only(name)
    except:
        pass
    return _reference(name)


def _decode_filename(filename: Path):
    stem = str(filename.stem)
    algorithm = re.search(r"trajectory([A-Za-z]+)", stem).group(1)
    metadata = _other_metadata(stem)
    return algorithm, metadata


def _skipped_frames(path: Path):
    groups = re.search(r"(\d)°s", str(path))
    if groups:
        return int(groups.group(1)) - 1
    else:
        raise KeyError()


def _algorithm_to_id(
    algorithm: str,
    skipped_frames: int,
    zed_algorithm_to_id: dict,
    difodo_algorithm_to_id: dict,
):
    if algorithm.upper() == "DIFODO":
        return difodo_algorithm_to_id[skipped_frames]
    else:
        return zed_algorithm_to_id[skipped_frames]


def convert_old_results(
    root_path: Path,
    output_path: Path,
    zed_algorithm_to_id: dict,
    difodo_algorithm_to_id: dict,
) -> None:
    # zed_algorithm_to_id = {0: 0, 1: 0, 2: 0, 3: 0}
    # difodo_algorithm_to_id = {0: 1, 1: 2, 2: 3, 3: 4, 4: 5}
    for speed_path in root_path.iterdir():
        try:
            skipped_frames = _skipped_frames(speed_path)
            for file in speed_path.glob("./*/*/*"):
                algorithm, metadata = _decode_filename(file)
                algorithm_id = _algorithm_to_id(
                    algorithm,
                    skipped_frames,
                    zed_algorithm_to_id,
                    difodo_algorithm_to_id,
                )

                ds = load_old_file_format(file)
                timestamp = (
                    pd.to_datetime(ds.timestamp.values[0])
                    .tz_localize("UTC")
                    .strftime("%Y%m%dT%H%M00.000000000%z")
                )
                new_name = str(uuid.uuid1())
                ds.attrs = {
                    "filter_id": 0,
                    "algorithm_id": algorithm_id,
                    "sensor": DEVICE_ZED,
                    "session_timestamp": timestamp,
                    **metadata,
                }

                ds.to_netcdf(path=output_path / (new_name + ".nc"))
        except KeyError:
            pass


def _projections(positions, relative_positions, circle_normal) -> np.array:
    # Project trajectory on Best-fit plane with a form of Rodriguez Rotation
    return (
        positions
        - np.dot(relative_positions.T, circle_normal)[None, :] * circle_normal[:, None]
    )


def _normalize_vector(v) -> np.array:
    return v / np.linalg.norm(v)


def _normalize_vectors(v) -> np.array:
    return v / np.linalg.norm(v, axis=0)


def _circle_normal(relative_positions) -> np.array:
    eigval, eigvec = np.linalg.eig(relative_positions @ relative_positions.T)
    return eigvec[:, np.argmin(eigval)]


def _local_x(projections, projections_origin) -> np.array:
    local_x = projections[:, 41] - projections_origin  # any Point >1 is possible
    return _normalize_vector(local_x)


def _transformation_matrix(projections, circle_normal) -> np.array:
    projections_origin = projections[:, 0]
    x_axis = _local_x(projections, projections_origin)
    z_axis = circle_normal
    y_axis = _normalize_vector(np.cross(z_axis, x_axis))
    T = np.eye(4, 4)
    T[:3, 0] = x_axis
    T[:3, 1] = y_axis
    T[:3, 2] = z_axis
    T[:3, 3] = projections_origin
    return T


def to_affine_space(v):
    res = np.empty((v.shape[0] + 1, v.shape[1]))
    res[:3] = v
    res[3] = 1
    return res


def space_to_plane(transformation_matrix, projections):
    projections_affine = to_affine_space(projections)
    return np.linalg.solve(transformation_matrix, projections_affine)[:2, :]


def _least_square_circle_fit(points_xy):
    # centroid of the data set
    centroid = np.mean(points_xy, axis=1)

    # Vector centroid to Points XY
    v = points_xy - centroid[:, None]

    # Defining Parameters
    x_points = v[0, :]
    y_points = v[1, :]

    Suu = np.sum(x_points ** 2)
    Suuu = np.sum(x_points ** 3)

    Svv = np.sum(y_points ** 2)
    Svvv = np.sum(y_points ** 3)
    Suv = np.sum(x_points * y_points)
    Suvv = np.sum(x_points * y_points * y_points)
    Suuv = np.sum((x_points ** 2) * y_points)

    M = np.array([[Suu, Suv], [Suv, Svv]])
    q = 0.5 * np.array([Suuu + Suvv, Svvv + Suuv])

    # Compute Center of fitted circle
    uv_center = np.linalg.solve(M, q)
    C_K = uv_center + centroid
    # Compute Radius of fitted cirlce
    r_K = np.sqrt(np.sum(uv_center ** 2) + ((Suu + Svv) / points_xy.shape[1]))
    return C_K, r_K


def _bestfit_circle(projections, transformation_matrix):
    points_on_plane = space_to_plane(transformation_matrix, projections)
    return _least_square_circle_fit(points_on_plane)


def plane_to_space(transformation_matrix, points) -> np.array:
    if len(points.shape) == 1:
        affine_space = np.array([*points, 0.0, 1.0])
    else:
        affine_space = np.empty((4, points.shape[1]))
        affine_space[:2, :] = points
        affine_space[2, :] = 0
        affine_space[3, :] = 1
    return np.linalg.solve(np.linalg.inv(transformation_matrix), affine_space)[:3]


def read_positions(ds) -> np.array:
    """Numpy array of the positions (xyz order)"""
    return np.vstack(
        [
            ds.position.sel(position_axes="x").values,
            ds.position.sel(position_axes="y").values,
            ds.position.sel(position_axes="z").values,
        ]
    )


def bestfit_plane(positions):
    # The mean of the samples belongs to the plane
    mean_position = np.mean(positions, axis=1)

    relative_positions = positions - mean_position[:, None]
    circle_normal = _circle_normal(relative_positions)

    # Project trajectory on Best-fit plane
    positions_projected = _projections(positions, relative_positions, circle_normal)

    # Tranformation matrix to get coordinates (Coor_2DXY) on bestfit plane
    transformation_matrix = _transformation_matrix(positions_projected, circle_normal)
    return mean_position, positions_projected, transformation_matrix


def bestfit_circle_parameters(projections, transformation_matrix):

    # Bestfit Circle parameters
    C_K_plane, r_K = _bestfit_circle(projections, transformation_matrix)
    return C_K_plane, r_K


def bestfit_circle_errors(C_K, r_K, C_G, r_G):
    """
    Compute euclidean error to centre and radius
    """
    C_F = np.linalg.norm(C_K - C_G)
    r_F = r_G - r_K
    return C_F, r_F


def elevation(n_K, n_G):
    return np.arccos(np.dot(n_K, n_G) / (np.linalg.norm(n_K) * np.linalg.norm(n_G)))


def reference_trajectory(from_angle, to_angle, steps, C_K_plane, r_K):
    angles_range = np.linspace(from_angle, to_angle, steps)
    points = r_K * np.vstack([np.sin(angles_range), np.cos(angles_range)])
    K_plane = C_K_plane[:, None] + points
    return angles_range, K_plane


def _knnsearch(x, y, k=1):
    return cKDTree(x).query(y, k=k)


def _average_sum_minimum_distances(x, y):
    minimum_distances, _ = _knnsearch(x, y)
    return np.mean(minimum_distances)


def residuum(positions_projected_plane, positions, K, K_plane):
    # 2D distance in XY plane
    min_distances_plane, _ = _knnsearch(K_plane.T, positions_projected_plane.T)
    R_plane = np.mean(min_distances_plane)

    # Euclidean distance of the trajectory to the nearest point on the circle
    min_distances_trajectory, _ = _knnsearch(K.T, positions.T)
    R = np.mean(min_distances_trajectory)

    # Distance in the perpendicular direction to the bestfit plane to closest point
    min_distances_perp = np.sqrt(
        min_distances_trajectory ** 2 - min_distances_plane ** 2
    )
    R_Z = np.mean(min_distances_perp)

    return (
        (R, R_plane, R_Z),
        (min_distances_trajectory, min_distances_plane, min_distances_perp),
    )


def _calculate_α_for_drift(C_K_plane, positions_projected_plane):
    p = _normalize_vectors(positions_projected_plane - C_K_plane[:, None])
    first_p = p[:, 0]
    dot_produts = first_p.reshape(1, -1) @ p
    cross_produts = np.cross(first_p, p, axisb=0)
    return np.arctan2(cross_produts, dot_produts)


def _flip_if_sum_negative(α):
    if np.sum(α[:1000]) < 0:  # Clockwise rotation of the Target
        return -α
    else:  # Counterclockwise rotation of the Target
        return α


def _correct_outliers(α_source):
    α = np.copy(α_source)
    α[α < 0] += 2 * np.pi

    # For outliers at the start
    first_range = 400
    correction_start = α[:first_range]
    correction_start[correction_start > np.deg2rad(300)] -= 2 * np.pi

    # For outliers at the end
    # last_range = -100
    # correction_end = α[last_range:]
    # correction_end[correction_end < np.deg2rad(6)] += 2 * np.pi
    return α


def drift(positions, C_K, C_K_plane, positions_projected_plane, β):
    # Vectors for Drift computation
    α = _correct_outliers(
        _flip_if_sum_negative(
            _calculate_α_for_drift(C_K_plane, positions_projected_plane)
        )
    )
    return np.squeeze(β - α)


def datetime64_to_seconds(dt):
    return dt / np.timedelta64(1, "s")


def estimated_ground_truth_range(timestamps, angular_velocity):
    """
    Calculate the ground truth angles given the angular velocity and the skipped frames
    """
    total_time = datetime64_to_seconds(timestamps[-1] - timestamps[0])
    final_angle = total_time * angular_velocity
    return np.linspace(0.0, final_angle, timestamps.shape[0])


def _default_reference():
    xz_normal = np.array([0.0, 1.0, 0.0])
    return 0.0, 4.0, xz_normal


def extract_angular_velocity(value: float, unit: str):
    if unit == "deg/s":
        return np.deg2rad(value)
    elif unit == "rad/s":
        return value
    else:
        raise NotImplementedError()


def extract_session_sensor_metadata(
    root_path: Path, session_name: str, sensor_name: str
):
    with h5py.File(
        find_netCDF_file_from_HDF5(
            root_path, f"/{GROUP_SESSIONS}/{session_name}/{sensor_name}/{GROUP_SOURCES}"
        ),
        "r",
    ) as file:

        def convert(x):
            try:
                return x.decode("utf8")
            except AttributeError:
                return x[0]

        return dict([(key, convert(value)) for key, value in file.attrs.items()])


def bestfit_circle(ds: xr.Dataset, angular_velocity: float) -> xr.Dataset:
    positions = read_positions(ds)
    mean_position, positions_projected, transformation_matrix = bestfit_plane(positions)
    C_K_plane, r_K = bestfit_circle_parameters(
        positions_projected, transformation_matrix
    )
    C_K = plane_to_space(transformation_matrix, C_K_plane)
    n_K = np.linalg.solve(np.linalg.inv(transformation_matrix), np.array([0, 0, 1, 1]))[
        :3
    ]
    K_angles_range, K_plane = reference_trajectory(
        from_angle=0,
        to_angle=2 * np.pi,
        steps=positions.shape[1],
        C_K_plane=C_K_plane,
        r_K=r_K,
    )
    K = plane_to_space(transformation_matrix, K_plane)
    C_G, r_G, n_G = _default_reference()
    C_F, r_F = bestfit_circle_errors(C_K_plane, r_K, C_G=C_G, r_G=r_G)
    ε = elevation(n_K, n_G=n_G)

    positions_projected_plane = space_to_plane(
        transformation_matrix, positions_projected
    )
    R, min_distances = residuum(positions_projected_plane, positions, K, K_plane)

    β = estimated_ground_truth_range(ds.timestamp.values, angular_velocity)
    D = drift(positions, C_K, C_K_plane, positions_projected_plane, β)

    return (
        ds.assign_coords({"plane_axes": ["x", "y"]})
        .assign_attrs(
            {
                "transformation_matrix": transformation_matrix,
                "r_K": r_K,
                "mean_position": mean_position,
                "C_K": C_K,
                "C_K_plane": C_K_plane,
                "C_F": C_F,
                "r_F": r_F,
                "n_K": n_K,
                "ε": ε,
                "R": R[0],
                "R_plane": R[1],
                "R_Z": R[2],
                "C_G": C_G,
                "r_G": r_G,
                "n_G": n_G,
            }
        )
        .assign(
            {
                "positions_projected": (
                    ("position_axes", "timestamp"),
                    positions_projected,
                ),
                "positions_projected_plane": (
                    ("plane_axes", "timestamp"),
                    positions_projected_plane,
                ),
                "K": (("position_axes", "timestamp"), K),
                "K_plane": (("plane_axes", "timestamp"), K_plane),
                "D": (("timestamp"), D),
                "K_angles_range": (("timestamp"), K_angles_range),
                "min_distances_trajectory": (("timestamp"), min_distances[0]),
                "min_distances_plane": (("timestamp"), min_distances[1]),
                "min_distances_perpendicular": (("timestamp"), min_distances[2]),
                "β": (("timestamp"), β),
            }
        )
    )


def _informations(
    root_path: Path, object_id: int, source_group: str, id_name: str
) -> dict:
    with h5py.File(root_path, "r") as root_file:
        for single_object in root_file[source_group].values():

            if single_object.attrs[id_name] == object_id:
                return dict(single_object.attrs.items())
        else:
            raise KeyError


def algorithm_informations(root_path: Path, algorithm_id: int) -> dict:
    return _informations(
        root_path, algorithm_id, GROUP_ALGORITHMS, id_name="algorithm_id"
    )


def filter_informations(root_path: Path, filter_id: int) -> dict:
    return _informations(root_path, filter_id, GROUP_FILTERS, id_name="filter_id")


def load_paths(root_path: Path) -> pd.DataFrame:
    records = []
    with h5py.File(root_path, "r") as root_file:
        for session_name, session in root_file[GROUP_SESSIONS].items():
            for sensor_name, sensor in session.items():
                for path_name, path in sensor.get(GROUP_PATHS, {}).items():
                    records.append(
                        {
                            "session": session_name,
                            "sensor": sensor_name,
                            "path": path_name,
                            "file": Path(path.file.filename),
                            "filter_id": path.attrs["filter_id"][0],
                            "algorithm_id": path.attrs["algorithm_id"][0],
                            **extract_session_sensor_metadata(
                                root_path, session_name, sensor_name
                            ),
                        }
                    )
    return pd.DataFrame.from_records(records)


def compute_metrics(paths: pd.DataFrame) -> pd.Series:
    def apply_metric(row):
        "spacecraft_angular_velocity", "spacecraft_angular_velocity_unit"
        with xr.open_dataset(row["file"]) as ds:
            return bestfit_circle(
                ds,
                angular_velocity=extract_angular_velocity(
                    row["spacecraft_angular_velocity"],
                    row["spacecraft_angular_velocity_unit"],
                ),
            )

    return paths.apply(apply_metric, axis="columns")


def extract_metadata(root_path: Path, group_name: str) -> pd.DataFrame:
    with h5py.File(root_path, "r") as root_file:
        return pd.DataFrame.from_records(
            [
                {"name": filter_name, **dict(filter_data.attrs.items())}
                for filter_name, filter_data in root_file[group_name].items()
            ]
        )


def extract_filters(root_path: Path) -> pd.DataFrame:
    return extract_metadata(root_path, GROUP_FILTERS).set_index("filter_id")


def extract_algorithms(root_path: Path) -> pd.DataFrame:
    return extract_metadata(root_path, GROUP_ALGORITHMS).set_index("algorithm_id")

