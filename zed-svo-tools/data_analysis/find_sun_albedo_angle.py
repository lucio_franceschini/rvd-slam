#%%
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy.optimize import minimize_scalar

#%%
LARGE_RADIUS = 2 * (37625 - 34493.5) / np.pi
STRAIGHT_LINE_LENGTH = 4901.0
SMALL_RADIUS = 2 * 1232.5 / np.pi
DX = 7181.5 - 3131.5


# %%
def create_profile(
    large_radius: float,
    straight_line_length: float,
    small_radius: float,
    circle_sections: int = 100,
) -> np.array:
    θ = np.linspace(0, np.pi / 2, circle_sections)

    quarter_circle = large_radius * np.vstack([np.cos(θ), np.sin(θ)])

    straight_line = np.linspace(0, -straight_line_length, 2)

    straight_line_2D = quarter_circle[:, -1][:, None] + np.vstack(
        [straight_line, np.zeros_like(straight_line)]
    )

    small_quarter_circle = (
        straight_line_2D[:, -1][:, None]
        + small_radius * np.vstack([-np.sin(θ), np.cos(θ)])
        - np.array([[0], [small_radius]])
    )

    vertical_line = np.linspace(0, large_radius - small_radius, 2)
    vertical_line_2D = small_quarter_circle[:, -1][:, None] + np.vstack(
        [np.zeros_like(vertical_line), -vertical_line]
    )
    upper_half = np.hstack(
        [quarter_circle, straight_line_2D, small_quarter_circle, vertical_line_2D]
    )
    bottom_half = np.array(upper_half[:, ::-1])
    bottom_half[1, :] *= -1
    return np.hstack([upper_half, bottom_half])


def move_profile(profile: np.array, dx: float, dy: float) -> np.array:
    return profile + np.array([dx, dy])[:, None]


def create_angle_segment(θ: float, radius: float = 4.0):
    return np.array([[0, np.cos(θ)], [0, np.sin(θ)]]) * radius


def find_bounding_box_profile(profile: np.array):
    x = profile[0, :]
    y = profile[1, :]

    return np.array([[np.amin(x), np.amax(x)], [np.amin(y), np.amax(y)]])


def plot(θ: float, margin_bb: float = 100.0) -> None:
    origin = np.zeros(2)
    profile = move_profile(
        create_profile(LARGE_RADIUS, STRAIGHT_LINE_LENGTH, SMALL_RADIUS), dx=DX, dy=0.0
    )
    limits = find_bounding_box_profile(profile)
    segment = create_angle_segment(θ, np.max(limits) * 2)
    fig, ax = plt.subplots(1, 1)
    ax.plot(profile[0, :], profile[1, :])
    ax.plot(segment[0, :], segment[1, :])
    ax.scatter(*origin, c="r")

    ax.set_xlim(limits[0, 0] - margin_bb, limits[0, 1] + margin_bb)
    ax.set_ylim(limits[1, 0] - margin_bb, limits[1, 1] + margin_bb)
    ax.set_aspect("equal", "box")
    fig.show()


def relative_angle_to_center(angle: float, tangent_angle: float) -> float:
    return np.pi - tangent_angle + angle


def distance_to_angle(dist: float) -> float:
    a = 3131.5
    b = 7181.5
    c = 8032.5
    d = 9265.0

    R = 2 * a / np.pi
    l_1 = b - a
    l_2 = c - b
    r = 2 * (d - c) / np.pi
    l_3 = R - r

    half_circle_large = R * np.pi / 2
    if 0 <= dist <= a:
        α = dist / R
        tangent_angle = np.pi / 2 + α
        return np.arctan2(R * np.sin(α), l_1 + R * np.cos(α)), tangent_angle
    elif a < dist <= b:
        hip = np.sqrt(R ** 2 + (l_1 - (dist - half_circle_large)) ** 2)
        tangent_angle = np.pi
        return np.arcsin(R / hip), tangent_angle
    elif b < dist <= c:
        hip = np.sqrt(R ** 2 + (l_1 - (dist - half_circle_large - b)) ** 2)
        tangent_angle = np.pi
        return np.pi / 2 + np.arcsin(R / hip), tangent_angle
    elif c < dist <= d:
        β = (dist - half_circle_large - l_1 - l_2) / r
        tangent_angle = np.pi + β
        return (
            np.arctan2(l_2 + r * np.sin(β), l_3 + r * np.cos(β)) + np.pi / 2,
            tangent_angle,
        )
    elif d < dist <= d + l_3:
        hip = np.sqrt((l_2 + r) ** 2 + (l_3 - (dist - d)) ** 2)
        tangent_angle = np.pi * 3 / 2
        return np.pi - np.arccos((l_2 + r) / hip), tangent_angle
    else:
        return dist, 0


def angle_to_distance(angle):
    return minimize_scalar(
        lambda x: np.abs(angle - distance_to_angle(x)[0]),
        bounds=(0, distance_to_angle(9265.0)),
    ).x


# %%


def test():
    options_0 = [3131.5 / 2, 3131.5, 7181.5, 8032.5, 9265.0, 9265.0 + 300, 9265.0 + 600]
    options_1 = np.linspace(0, 9865.0, 10)
    options_2 = options_0
    for distance in options_2:
        angle, tangent_angle = distance_to_angle(distance)
        print(
            f"{distance:.3f}: Angle {np.rad2deg(angle):.3f}°, Tangent angle {np.rad2deg(tangent_angle):.3f}°"
        )
        # print(f"{np.rad2deg(angle):.3f}°: {angle_to_distance(angle):.3f} mm\n")
        plot(θ=angle)


test()

# %%

def main():
    cf_position = 1000000/6.252 # steps/mm
    angle = np.deg2rad(15)
    distance = angle_to_distance(angle)
    print(f"{np.rad2deg(angle):.3f}°: {distance:.3f} mm, {cf_position*distance:.3f} mm\n")
    _, tangent_angle = distance_to_angle(distance)
    print(f"{np.rad2deg(angle):.3f}°: {90-np.rad2deg(relative_angle_to_center(angle, tangent_angle)):.3f}°\n")
    plot(θ=angle)

main()
# %%
