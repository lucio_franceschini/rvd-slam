import logging
from pathlib import Path

import altair as alt
import h5py
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
import numpy as np
import pandas as pd
import xarray as xr
from mpl_toolkits.mplot3d import Axes3D
from names import (
    DEVICE_ZED,
    DEVICE_D435,
    DEVICE_HPS160,
    GROUP_ALGORITHMS,
    GROUP_FILTERS,
    GROUP_SESSIONS,
    GROUP_SOURCES,
    GROUP_PATHS,
)

plt.rcParams.update({"font.size": 22})

alt.data_transformers.disable_max_rows()


def _find_netCDF_file_from_HDF5(root_path: Path, path_to_group: str):
    with h5py.File(root_path, "r") as root_file:
        return Path(root_file[path_to_group].file.filename)


def find_netCDF_file_from_HDF5(root_path: Path, path_to_group: str):
    return _find_netCDF_file_from_HDF5(root_path, path_to_group)


def _sections(camera_name):
    if camera_name == DEVICE_ZED:
        return ["VIEW_DEPTH", "VIEW_LEFT", "VIEW_RIGHT", "VIEW_CONFIDENCE"]
    elif camera_name == DEVICE_D435:
        return ["DEPTH", "LEFT_INFRARED", "RIGHT_INFRARED", "COLOR"]
    elif camera_name == DEVICE_HPS160:
        return ["DEPTH"]
    else:
        raise NotImplementedError(camera_name)


def extract_timestamps(
    root_file: Path, session_name: str, camera_name: str, section_name: str
) -> np.ndarray:
    group = f"/{GROUP_SESSIONS}/{session_name}/{camera_name}/{GROUP_SOURCES}/"
    file = _find_netCDF_file_from_HDF5(root_file, group)
    relative_group = f"{camera_name}/{section_name}"
    with xr.open_dataset(file, group=relative_group) as ds:
        return ds.timestamp.values


def open_timestamp_data(
    root_file: Path,
    timestamp: str,
    session_name: str,
    camera_name: str,
    section_name: str = None,
):
    if section_name:
        sections = [section_name]
    else:
        sections = _sections(camera_name)
    imgs = []
    for section in sections:
        group = f"/{GROUP_SESSIONS}/{session_name}/{camera_name}/{GROUP_SOURCES}/"
        file = _find_netCDF_file_from_HDF5(root_file, group)
        relative_group = f"{camera_name}/{section}"
        try:
            with xr.open_dataset(file, group=relative_group) as ds:
                logging.info(f"Found {section}")
                nearest_ds = ds.loc[{"timestamp": ~np.isnat(ds.timestamp)}].sel(
                    timestamp=timestamp, method="nearest"
                )
                imgs.append(nearest_ds)
        except OSError:
            logging.info(f"Missing {section}")
    return imgs


def plot_drift(ds):
    dataset = ds[["D"]].to_dataframe().reset_index()
    first_date = dataset["timestamp"][0]
    dataset["dt"] = dataset["timestamp"].apply(
        lambda x: (x - first_date).total_seconds()
    )
    dataset["Legend"] = "Drift"
    dataset["D"] = np.rad2deg(dataset["D"])
    return (
        alt.Chart(dataset)
        .mark_line()
        .encode(
            x=alt.X("dt", type="quantitative", title="Time [s]"),
            y=alt.Y("D", type="quantitative", title="Drift [°]"),
            color=alt.Color("Legend", type="nominal"),
        )
    )


def plot_path(ds, title=None, figsize=None, normal=None, centering=False):
    if figsize is None:
        figsize = (15, 15)
    fig = plt.figure(figsize=figsize)
    ax = Axes3D(fig)
    linewidth = 3
    data_position = ds.position.dropna(dim="timestamp")

    positions_x = data_position.sel(position_axes="x").values
    positions_y = data_position.sel(position_axes="y").values
    positions_z = data_position.sel(position_axes="z").values

    if centering:
        positions_x -= ds.C_K[0]
        positions_y -= ds.C_K[1]
        positions_z -= ds.C_K[2]
        pos_center = np.zeros_like(ds.C_K)
    else:
        pos_center = ds.C_K

    def draw_line_text(end, label):
        ax.plot(*list(zip(pos_center, end)))
        ax.text(end[0], end[1], end[2], label)

    if normal is not None:
        draw_line_text(pos_center + normal, "$n_G$")
    for idx, label in zip(range(3), ["x", "y", "$n_K$"]):
        draw_line_text(pos_center + ds.transformation_matrix[:3, idx], label)
    ax.plot(
        positions_x,
        positions_y,
        positions_z,
        label="Estimated trajectory",
        linewidth=linewidth,
    )
    ax.plot(
        [positions_x[0]],
        [positions_y[0]],
        [positions_z[0]],
        "rx",
        label="Starting point",
        linewidth=linewidth,
        markersize=linewidth * 10,
        markeredgewidth=linewidth,
    )
    try:
        x_K = ds.K.sel(position_axes="x").values
        y_K = ds.K.sel(position_axes="y").values
        z_K = ds.K.sel(position_axes="z").values
        if centering:
            x_K -= ds.C_K[0]
            y_K -= ds.C_K[1]
            z_K -= ds.C_K[2]
        ax.plot(x_K, y_K, z_K, label="Bestfit-circle", linewidth=linewidth)
    except AttributeError:
        pass
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    ax.legend()
    formatter = EngFormatter(unit="m")
    ax.xaxis.set_major_formatter(formatter)
    ax.yaxis.set_major_formatter(formatter)
    ax.zaxis.set_major_formatter(formatter)
    if title:
        ax.set_title(title)
    return fig, ax


def open_and_plot_path(
    root_path: Path, session_name: str, camera_name: str, path_id: str
):
    path_dir = _find_netCDF_file_from_HDF5(
        root_path,
        f"/{GROUP_SESSIONS}/{session_name}/{camera_name}/{GROUP_PATHS}/{path_id}",
    )
    with xr.open_dataset(path_dir, engine="h5netcdf") as ds:
        return plot_path(ds, f"{session_name}/{path_id}")


def plot_residuum(ds):
    dataset = (
        ds[
            [
                "min_distances_trajectory",
                "min_distances_plane",
                "min_distances_perpendicular",
            ]
        ]
        .to_dataframe()
        .reset_index()
        .rename(
            columns={
                "min_distances_trajectory": "Nearest point on the circle",
                "min_distances_plane": "On the XY plane",
                "min_distances_perpendicular": "Perpendicular direction",
            }
        )
    )
    first_timestamp = dataset["timestamp"][0]
    dataset["dt"] = dataset["timestamp"].apply(
        lambda x: (x - first_timestamp).total_seconds()
    )

    return (
        alt.Chart(dataset.melt(id_vars=["timestamp", "dt"]))
        .mark_line()
        .encode(
            x=alt.X("dt", type="quantitative", title="Time [s]"),
            y=alt.Y("value", type="quantitative", title="Distance [m]"),
            color=alt.Color("variable", type="nominal", title="Distances"),
        )
    )
