import logging
from datetime import datetime, timedelta
from pathlib import Path
from utility import bestfit_circle, extract_angular_velocity
from plotting import find_netCDF_file_from_HDF5, plot_drift, plot_path, plot_residuum

import h5py
import numpy as np
import pandas as pd
import xarray as xr
from dateutil.parser import parse
import matplotlib.pyplot as plt

from names import (
    DEVICE_D435,
    DEVICE_HPS160,
    DEVICE_ZED,
    GROUP_ALGORITHMS,
    GROUP_FILTERS,
    GROUP_SESSIONS,
    GROUP_PATHS,
    GROUP_SOURCES,
)


def _find_paths_files(
    root_path: Path,
    output_path: Path,
    session_name: str,
    sensor_name: str,
    path_name: str = None,
):
    with h5py.File(root_path, "r") as file:
        path_to_paths = f"/{GROUP_SESSIONS}/{session_name}/{sensor_name}/{GROUP_PATHS}"
        paths_group = file[path_to_paths]
        if path_name is None:
            paths = [f"{path_to_paths}/{path}" for path in paths_group.keys()]
        else:
            paths = [f"{path_to_paths}/{path_name}"]
    paths_files = []
    for path in paths:
        paths_files.append(find_netCDF_file_from_HDF5(root_path, path))
    return paths_files


def export_path(
    root_path: Path,
    output_path: Path,
    session_name: str,
    sensor_name: str,
    path_name: str = None,
) -> None:
    files = _find_paths_files(
        root_path=root_path,
        output_path=output_path,
        session_name=session_name,
        sensor_name=sensor_name,
        path_name=path_name,
    )
    angular_velocity = extract_angular_velocity(root_path, session_name, sensor_name)
    for filename in files:
        with xr.open_dataset(filename) as ds:
            logging.info(f"Exporting {filename.stem}")
            output_folder = output_path / filename.stem
            output_folder.mkdir(exist_ok=True)
            ds_bestfit = bestfit_circle(ds, angular_velocity=angular_velocity)
            plot_drift(ds_bestfit).interactive().save(
                str(output_folder / "drift.html"),
                format="html",
                embed_options={"renderer": "svg"},
            )
            fig, _ = plot_path(ds_bestfit)
            fig.savefig(str(output_folder / "path.pdf"))
            plt.close(fig)

            plot_residuum(ds_bestfit).interactive().save(
                str(output_folder / "residuum.html"),
                format="html",
                embed_options={"renderer": "svg"},
            )

