import argparse
import logging
import numpy
from pathlib import Path
from names import META_TIMESTAMP, GROUP_SOURCES, GROUP_SESSIONS

import h5py


def _retrieve_timestamp(src):
    value = src.attrs[META_TIMESTAMP]
    if isinstance(value, numpy.ndarray):
        return value[0]
    else:
        return value


def store_sources(args):
    with h5py.File(args.dataset_path, "a") as root_group:
        sessions = root_group.require_group(GROUP_SESSIONS)
        for file in list(args.data_path.glob("*.nc")):
            logging.info(f"Reading {file}")
            with h5py.File(file, "r") as src:
                timestamp = _retrieve_timestamp(src)
                session = sessions.require_group(timestamp)
                if META_TIMESTAMP not in session.attrs:
                    session.attrs[META_TIMESTAMP] = timestamp
                for sensor_name in src:
                    sensor = session.require_group(sensor_name)
                    if GROUP_SOURCES not in sensor:
                        sensor[GROUP_SOURCES] = h5py.ExternalLink(
                            file.relative_to(args.dataset_path.parent), sensor_name
                        )
                        logging.info(f"Created group {sensor_name}")
                    else:
                        logging.info(f"{sensor_name} already stored, skipping...")

