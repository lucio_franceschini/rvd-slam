# Recorder, converter, path exporter and data analysis tools

The software requires the installation of the following libraries:
* Microsoft Visual Studio 2019: https://go.microsoft.com/fwlink/?LinkId=532606&clcid=0x409
* Stereolabs Zed SDK 3.0.2: https://www.stereolabs.com/developers/release/3.0.2/
* Intel Realsense SDK 2.31: https://www.intelrealsense.com/sdk-2/
* MRPT 1.5.7: https://www.mrpt.org/download-mrpt/
* vcpkg 2020.01: https://github.com/microsoft/vcpkg/releases/tag/2020.01

Their root path must be saved in the following environment variable:
* MRPT_PATH
* RS_SDK_ROOT_PATH
* ZED_SDK_ROOT_DIR

The vcpkg package manager allows to install additional dependencies. This software requires the following C++ libraries:
* ms-gsl
* yaml-cpp
* hdf5
* hdf5[cpp]
* netcdf-cxx4
* date
* pcl
* cxxopts

It is possible to install them via the following command:
```
vcpkg install ms-gsl:x64-windows yaml-cpp:x64-windows hdf5:x64-windows hdf5[cpp]:x64-windows netcdf-cxx4:x64-windows date:x64-windows date[remote-api]:x64-windows pcl:x64-windows cxxopts:x64-windows
```