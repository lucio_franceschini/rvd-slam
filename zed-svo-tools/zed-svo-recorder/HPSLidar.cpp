﻿#include "HPSLidar.h"

#include <constants.h>

#include <chrono>
#include <exception>
#include <iostream>
#include <thread>

#pragma warning(push, 0)
#include <mrpt/utils.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <yaml-cpp/yaml.h>
#pragma warning(pop)

#define LOAD_DLL_FUNCTION(moduleLibrary, function) \
  loadFunction(moduleLibrary, function, #function)

#define DEFINE_DLL_FUNCTION(function) \
  decltype(HPS3D_##function)(*function) = nullptr

namespace
{
constexpr auto libraryName = "libhps3d64.dll";
// Global variable where files will be stored, very ugly but not other choice to
// communicate with the relative callback
std::filesystem::path outputDir;

const std::string functionsPrefix = "HPS3D_";
// Define function Pointers to call dynamic link libraries
DEFINE_DLL_FUNCTION(SetDebugEnable);
DEFINE_DLL_FUNCTION(SetDebugFunc);
DEFINE_DLL_FUNCTION(Connect);
DEFINE_DLL_FUNCTION(DisConnect);
DEFINE_DLL_FUNCTION(ConfigInit);
DEFINE_DLL_FUNCTION(AddObserver);
DEFINE_DLL_FUNCTION(SetRunMode);
DEFINE_DLL_FUNCTION(RemoveDevice);
DEFINE_DLL_FUNCTION(SingleMeasurement);
DEFINE_DLL_FUNCTION(GetDeviceList);
DEFINE_DLL_FUNCTION(SetPointCloudEn);
DEFINE_DLL_FUNCTION(GetPointCloudEn);
DEFINE_DLL_FUNCTION(SetEthernetServerInfo);
DEFINE_DLL_FUNCTION(SavePlyFile);
DEFINE_DLL_FUNCTION(SetMeasurePacketType);
DEFINE_DLL_FUNCTION(SelectROIGroup);
DEFINE_DLL_FUNCTION(SetOpticalEnable);
DEFINE_DLL_FUNCTION(AutoConnectAndInitConfigDevice);
DEFINE_DLL_FUNCTION(GetDeviceVersion);
DEFINE_DLL_FUNCTION(GetOpticalParamConf);
DEFINE_DLL_FUNCTION(GetHDRConfig);
DEFINE_DLL_FUNCTION(GetDistanceFilterConf);
DEFINE_DLL_FUNCTION(GetSmoothFilterConf);
DEFINE_DLL_FUNCTION(GetDistanceOffset);
DEFINE_DLL_FUNCTION(GetNumberOfROI);
DEFINE_DLL_FUNCTION(RemoveObserver);
DEFINE_DLL_FUNCTION(SetHDRConfig);

void debugFunction(char *str) { std::cout << str << '\n'; }

template <typename T>
void loadFunction(
    const HMODULE &module,
    T &function,
    const std::string &functionName)
{
  function =
      (T)GetProcAddress(module, (functionsPrefix + functionName).c_str());
}

void loadFunctions()
{
  const auto moduleLibrary = LoadLibraryA(libraryName);
  if (moduleLibrary == nullptr)
  {
    throw std::runtime_error(
        std::string("Error while loading ") + libraryName +
        " - Error ID: " + std::to_string(GetLastError()));
  }
  LOAD_DLL_FUNCTION(moduleLibrary, SetDebugEnable);
  LOAD_DLL_FUNCTION(moduleLibrary, SetDebugFunc);
  LOAD_DLL_FUNCTION(moduleLibrary, Connect);
  LOAD_DLL_FUNCTION(moduleLibrary, DisConnect);
  LOAD_DLL_FUNCTION(moduleLibrary, ConfigInit);
  LOAD_DLL_FUNCTION(moduleLibrary, AddObserver);
  LOAD_DLL_FUNCTION(moduleLibrary, SetRunMode);
  LOAD_DLL_FUNCTION(moduleLibrary, RemoveDevice);
  LOAD_DLL_FUNCTION(moduleLibrary, SingleMeasurement);
  LOAD_DLL_FUNCTION(moduleLibrary, GetDeviceList);
  LOAD_DLL_FUNCTION(moduleLibrary, SetPointCloudEn);
  LOAD_DLL_FUNCTION(moduleLibrary, GetPointCloudEn);
  LOAD_DLL_FUNCTION(moduleLibrary, SetEthernetServerInfo);
  LOAD_DLL_FUNCTION(moduleLibrary, SavePlyFile);
  LOAD_DLL_FUNCTION(moduleLibrary, SetMeasurePacketType);
  LOAD_DLL_FUNCTION(moduleLibrary, SelectROIGroup);
  LOAD_DLL_FUNCTION(moduleLibrary, SetOpticalEnable);
  LOAD_DLL_FUNCTION(moduleLibrary, AutoConnectAndInitConfigDevice);
  LOAD_DLL_FUNCTION(moduleLibrary, GetDeviceVersion);
  LOAD_DLL_FUNCTION(moduleLibrary, GetOpticalParamConf);
  LOAD_DLL_FUNCTION(moduleLibrary, GetHDRConfig);
  LOAD_DLL_FUNCTION(moduleLibrary, GetDistanceFilterConf);
  LOAD_DLL_FUNCTION(moduleLibrary, GetSmoothFilterConf);
  LOAD_DLL_FUNCTION(moduleLibrary, GetDistanceOffset);
  LOAD_DLL_FUNCTION(moduleLibrary, GetNumberOfROI);
  LOAD_DLL_FUNCTION(moduleLibrary, RemoveObserver);
  LOAD_DLL_FUNCTION(moduleLibrary, SetHDRConfig);
}

pcl::PointCloud<pcl::PointXYZ> fillCloud(const MeasureDataTypeDef &MeasureData)
{
  pcl::PointCloud<pcl::PointXYZ> cloud;
  // Fill the PCL cloud parameters
  cloud.width = MeasureData.point_cloud_data->width;
  cloud.height = MeasureData.point_cloud_data->height;

  // Specifies if all the data in points is finite (true), or whether
  // the XYZ values of certain points might contain Inf/NaN values
  // (false).
  cloud.is_dense = false;
  cloud.points.resize(static_cast<size_t>(cloud.width) * cloud.height);

  // Fill the PCL cloud data
  for (size_t j = 0; j < cloud.points.size(); ++j)
  {
    cloud.points[j].x = MeasureData.point_cloud_data->point_data[j].x;
    cloud.points[j].y = MeasureData.point_cloud_data->point_data[j].y;
    cloud.points[j].z = MeasureData.point_cloud_data->point_data[j].z;
  }
  return cloud;
}

auto loadSensorInfo(HPS3D_HandleTypeDef &handle)
{
  Version_t version;
  GetDeviceVersion(&handle, &version);

  OpticalParamConfTypeDef opticalParamConf;
  GetOpticalParamConf(&handle, &opticalParamConf);

  DistanceFilterConfTypeDef distanceFilter;
  GetDistanceFilterConf(&handle, &distanceFilter);

  SmoothFilterConfTypeDef smoothFilter;
  GetSmoothFilterConf(&handle, &smoothFilter);

  int16_t distanceOffset;
  GetDistanceOffset(&handle, &distanceOffset);

  HDRConf hdr;
  GetHDRConfig(&handle, &hdr);

  // uint8_t roiNumber, thresholdNumber;
  // GetNumberOfROI(&handle, &roiNumber, &thresholdNumber);

  return std::make_tuple(
      version,
      opticalParamConf,
      distanceFilter,
      smoothFilter,
      distanceOffset,
      hdr);
}

template <typename T>
void addKeyValueYAML(YAML::Emitter &out, const std::string &key, const T &value)
{
  out << YAML::Key << key;
  out << YAML::Value << value;
}

void exportSensorInfo(
    HPS3D_HandleTypeDef &handle,
    const std::filesystem::path &exportFile)
{
  const auto
      [version,
       opticalParamConf,
       distanceFilter,
       smoothFilter,
       distanceOffset,
       hdr] = loadSensorInfo(handle);

  constexpr auto disabled = "DISABLED";

  YAML::Emitter out;
  out << YAML::BeginMap;
  addKeyValueYAML(out, "version-day", static_cast<int>(version.day));
  addKeyValueYAML(out, "version-major", static_cast<int>(version.major));
  addKeyValueYAML(out, "version-minor", static_cast<int>(version.minor));
  addKeyValueYAML(out, "version-month", static_cast<int>(version.month));
  addKeyValueYAML(out, "version-rev", static_cast<int>(version.rev));
  addKeyValueYAML(out, "version-year", static_cast<int>(version.year));

  addKeyValueYAML(out, "optical-parameters-enabled", opticalParamConf.enable);
  if (opticalParamConf.enable)
  {
    addKeyValueYAML(
        out,
        "optical-parameters-illum-angle-horiz",
        static_cast<int>(opticalParamConf.illum_angle_horiz));
    addKeyValueYAML(
        out,
        "optical-parameters-illum-angle-vertical",
        static_cast<int>(opticalParamConf.illum_angle_vertical));
    addKeyValueYAML(
        out,
        "optical-parameters-viewing-angle-horiz",
        static_cast<int>(opticalParamConf.viewing_angle_horiz));
    addKeyValueYAML(
        out,
        "optical-parameters-viewing-angle-vertical",
        static_cast<int>(opticalParamConf.viewing_angle_vertical));
  }

  constexpr auto distanceFilterType = "distance-filter-type";
  switch (distanceFilter.filter_type)
  {
    case DISTANCE_FILTER_SIMPLE_KALMAN:
      addKeyValueYAML(out, distanceFilterType, "SIMPLE_KALMAN");
      addKeyValueYAML(out, "distance-filter-kalman-k", distanceFilter.kalman_K);
      addKeyValueYAML(
          out,
          "distance-filter-kalman-threshold",
          distanceFilter.kalman_threshold);
      addKeyValueYAML(
          out,
          "distance-filter-num-check",
          distanceFilter.num_check);
      break;
    case DISTANCE_FILTER_DISABLE:
      addKeyValueYAML(out, distanceFilterType, disabled);
      break;
    default:
      throw std::logic_error("Not implemented");
  }

  constexpr auto smoothFilterType = "smooth-filter-type";
  constexpr auto arg1 = "arg1";
  switch (smoothFilter.type)
  {
    case SMOOTH_FILTER_AVERAGE:
      addKeyValueYAML(out, smoothFilterType, "AVERAGE");
      addKeyValueYAML(out, arg1, smoothFilter.arg1);
      break;
    case SMOOTH_FILTER_GAUSS:
      addKeyValueYAML(out, smoothFilterType, "GAUSS");
      addKeyValueYAML(out, arg1, smoothFilter.arg1);
      break;
    case SMOOTH_FILTER_DISABLE:
      addKeyValueYAML(out, smoothFilterType, disabled);
      break;
    default:
      throw std::logic_error("Not implemented");
  }

  addKeyValueYAML(out, "distance-offset", distanceOffset);

  constexpr auto hdrMode = "hdr-mode";
  switch (hdr.hdr_mode)
  {
    case HDR_DISABLE:
      addKeyValueYAML(out, hdrMode, disabled);
      addKeyValueYAML(
          out,
          "hdr-integration-time",
          hdr.hdr_disable_integration_time);
      break;
    case AUTO_HDR:
      addKeyValueYAML(out, hdrMode, "AUTO");
      addKeyValueYAML(
          out,
          "hdr-qualtity-overexposed",
          hdr.qualtity_overexposed);
      addKeyValueYAML(
          out,
          "hdr-qualtity-overexposed-serious",
          hdr.qualtity_overexposed_serious);
      addKeyValueYAML(out, "hdr-qualtity-weak", hdr.qualtity_weak);
      addKeyValueYAML(
          out,
          "hdr-qualtity-weak-serious",
          hdr.qualtity_weak_serious);
      break;
    case SUPER_HDR:
      addKeyValueYAML(out, hdrMode, "SUPER");
      addKeyValueYAML(
          out,
          "hdr-super-hdr-frame-number",
          static_cast<int>(hdr.super_hdr_frame_number));
      addKeyValueYAML(
          out,
          "hdr-super-hdr-max-integration",
          hdr.super_hdr_max_integration);
      break;
    case SIMPLE_HDR:
      addKeyValueYAML(out, hdrMode, "SIMPLE");
      addKeyValueYAML(
          out,
          "hdr-simple-hdr-max-integration",
          hdr.simple_hdr_max_integration);
      addKeyValueYAML(
          out,
          "hdr-simple-hdr-min-integration",
          hdr.simple_hdr_min_integration);
      break;
    default:
      throw std::logic_error("Not implemented");
  }

  out << YAML::EndMap;

  std::ofstream ofs(exportFile, std::ofstream::out);
  ofs << out.c_str();
  ofs.close();
}

void *retrieveDepthData(HPS3D_HandleTypeDef *, AsyncIObserver_t *event)
{
  static const auto out = outputDir;
  if (event->AsyncEvent == ISubject_Event_DataRecvd)
  {
    switch (event->RetPacketType)
    {
      case FULL_DEPTH_PACKET:
      {
        const auto cloud = fillCloud(event->MeasureData);

        // Save cloud to file
        const auto now =
            date::make_zoned("UTC", std::chrono::system_clock::now());
        const auto filename = date::format(io::timestampFormat, now) +
                              std::string(".") + io::hps160OutputFormat;
        pcl::io::savePCDFileASCII((out / filename).string(), cloud);
      }
      break;
      case SIMPLE_ROI_PACKET:
      case FULL_ROI_PACKET:
      case SIMPLE_DEPTH_PACKET:
      case NULL_PACKET:
        break;
      default:
        throw std::runtime_error("Error from the HPS-160 LIDAR");
    }
  }
  return nullptr;
}

auto autoconnect()
{
  static std::array<HPS3D_HandleTypeDef, 10> handles;
  if (const auto connectedDevices =
          AutoConnectAndInitConfigDevice(handles.data());
      connectedDevices == 0)
  {
    throw std::runtime_error("Device connect failed");
  }
  else if (connectedDevices > 1)
  {
    for (size_t idxDevice = 0; idxDevice < connectedDevices; idxDevice++)
    {
      RemoveDevice(&handles[idxDevice]);
    }
    throw std::runtime_error(
        "This software works only with a single device connected");
  }
  return handles[0];
}

void connectDevice(HPS3D_HandleTypeDef &handle, const std::string &deviceName)
{
  if (deviceName.empty())
  {
    handle = autoconnect();
  }
  else
  {
    auto name = deviceName;
    handle.DeviceName = name.data();

    if (const auto ret = Connect(&handle); ret != RET_OK)
    {
      throw std::logic_error(
          std::string("Impossible to connect device, error code: ") +
          std::to_string(ret));
    }
    if (const auto ret = ConfigInit(&handle); ret != RET_OK)
    {
      throw std::logic_error(
          std::string("Impossible to configure device, error code: ") +
          std::to_string(ret));
    }
  }
}

void setObserver(HPS3D_HandleTypeDef &handle, AsyncIObserver_t &observer)
{
  observer.AsyncEvent = ISubject_Event_DataRecvd;
  observer.NotifyEnable = true;
  observer.ObserverID = 0;
  // Adding asynchronous observers
  if (const auto ret = AddObserver(&retrieveDepthData, &handle, &observer);
      ret != RET_OK)
  {
    throw std::logic_error(
        std::string("Observer add failed, error code: ") + std::to_string(ret));
  }
}

void setHDRMode(
    HPS3D_HandleTypeDef &handle,
    const std::variant<
        HPSLidar::SuperHDR,
        HPSLidar::SimpleHDR,
        HPSLidar::AutoHDR,
        HPSLidar::DisabledHDR>
        &hdr)
{
  HDRConf hdrConf;
  std::visit(
      [&](auto &&arg) {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, HPSLidar::SuperHDR>)
        {
          const auto settings = std::get<HPSLidar::SuperHDR>(hdr);
          hdrConf.hdr_mode = SUPER_HDR;
          hdrConf.super_hdr_frame_number = settings.frameNumber;
          hdrConf.super_hdr_max_integration = settings.maxIntegrationMilliseconds;
        }
        else if constexpr (std::is_same_v<T, HPSLidar::SimpleHDR>)
        {
          const auto settings = std::get<HPSLidar::SimpleHDR>(hdr);
          hdrConf.hdr_mode = SIMPLE_HDR;
          hdrConf.simple_hdr_max_integration = settings.maxIntegrationMilliseconds;
          hdrConf.simple_hdr_min_integration =
              settings.minIntegrationMilliseconds;
        }
        else if constexpr (std::is_same_v<T, HPSLidar::AutoHDR>)
        {
          const auto settings = std::get<HPSLidar::AutoHDR>(hdr);
          hdrConf.hdr_mode = AUTO_HDR;
          hdrConf.qualtity_overexposed = settings.qualtityOverexposed;
          hdrConf.qualtity_overexposed_serious =
              settings.qualtityOverexposedSerious;
          hdrConf.qualtity_weak = settings.qualtityWeak;
          hdrConf.qualtity_weak_serious =
              settings.qualtityWeakSerious;
        }
        else if constexpr (std::is_same_v<T, HPSLidar::DisabledHDR>)
        {
          const auto settings = std::get<HPSLidar::DisabledHDR>(hdr);
          hdrConf.hdr_mode = HDR_DISABLE;
          hdrConf.hdr_disable_integration_time = settings.integrationTime;
        }
        else
        {
          static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
      },
      hdr);
  SetHDRConfig(&handle, hdrConf);
}

}  // namespace

HPSLidar::HPSLidar(const Settings &settings) : BaseDevice("HPS-3D160")
{
  loadFunctions();

  SetDebugEnable(settings.enableDebug);
  if (settings.enableDebug)
  {
    SetDebugFunc(&debugFunction);
  }

  connectDevice(handle, settings.deviceName);

  // An observer subscribes to an event as a data receive event
  setObserver(handle, observer);

  setHDRMode(handle, settings.hdr);

  // Before enabling the point cloud data, ensure that the optical
  // compensation enable is enabled, otherwise the correct point
  // cloud result cannot be obtained.
  if (const auto res = SetOpticalEnable(&handle, true); res != RET_OK)
  {
    throw std::runtime_error("Error while enabling the optical compensation");
  }
  // Enables point cloud output
  if (const auto res = SetPointCloudEn(true); res != RET_OK)
  {
    throw std::runtime_error("Error while enabling the point cloud");
  }
}

void HPSLidar::start(
    const std::filesystem::path &outputPath,
    const std::string_view recordingsBaseName)
{
  if (isRecording())
  {
    return;
  }
  outputDir = outputPath / recordingsBaseName;
  std::filesystem::create_directory(outputDir);
  exportSensorInfo(
      handle,
      outputDir / (std::string(recordingsBaseName) + ".yaml"));
  handle.RunMode = RUN_CONTINUOUS;
  if (const auto res = SetRunMode(&handle); res != RET_OK)
  {
    throw std::runtime_error("Error while setting the run mode to continuous");
  }
  setAsRecording();
}

void HPSLidar::stop()
{
  if (isRecording())
  {
    handle.RunMode = RUN_IDLE;
    SetRunMode(&handle);
    setAsNonRecording();
  }
}

HPSLidar::~HPSLidar()
{
  try
  {
    stop();
    // RemoveObserver(&observer);
  }
  catch (const std::exception &)
  {
  }
}

HPSLidar::Settings parseHpsConfig(
    const YAML::Node &config,
    const std::string &deviceName)
{
  const auto parseHdr = [&] {
    if (const auto superHDR = config["super_hdr"]; !superHDR.IsNull())
    {
      return HPSLidar::HDRSettings(HPSLidar::SuperHDR{
          superHDR["frame_number"].as<uint8_t>(),
          superHDR["max_integration_milliseconds"].as<uint32_t>(),
      });
    }
    else if (const auto simpleHDR = config["simple_hdr"]; !simpleHDR.IsNull())
    {
      return HPSLidar::HDRSettings(HPSLidar::SimpleHDR{
          simpleHDR["max_integration_milliseconds"].as<uint32_t>(),
          simpleHDR["min_integration_milliseconds"].as<uint32_t>(),
      });
    }
    else if (const auto autoHDR = config["auto_hdr"]; !autoHDR.IsNull())
    {
      return HPSLidar::HDRSettings(HPSLidar::AutoHDR{
          autoHDR["qualtity_overexposed"].as<float32_t>(),
          autoHDR["qualtity_overexposed_serious"].as<float32_t>(),
          autoHDR["qualtity_weak"].as<float32_t>(),
          autoHDR["qualtity_weak_serious"].as<float32_t>(),
      });
    }
    else if (const auto disabledHDR = config["disabled_hdr"]; !disabledHDR.IsNull())
    {
      return HPSLidar::HDRSettings(HPSLidar::DisabledHDR{
          disabledHDR["integration_time"].as<uint32_t>(),
      });
    }
    else
    {
      throw std::runtime_error("Unknown HDR mode");
    }
  };
  return {deviceName, config["enable_debug"].as<bool>(), parseHdr()};
}
