#pragma once
#include <thread>

#pragma warning(push, 0)
#include <date/tz.h>
#pragma warning(pop)

#include "BaseDevice.h"
#include "ThreadStopper.h"

#pragma warning(push, 0)
#include <yaml-cpp/yaml.h>

#include "api.h"
#pragma warning(pop)

#include <array>
#include <string>
#include <variant>

class HPSLidar : public BaseDevice
{
 private:
  HPS3D_HandleTypeDef handle;
  AsyncIObserver_t observer;

 public:
  struct SuperHDR
  {
    const uint8_t frameNumber;
    const uint32_t maxIntegrationMilliseconds;
  };
  struct SimpleHDR
  {
    const uint32_t maxIntegrationMilliseconds;
    const uint32_t minIntegrationMilliseconds;
  };
  struct AutoHDR
  {
    const float32_t qualtityOverexposed;
    const float32_t qualtityOverexposedSerious;
    const float32_t qualtityWeak;
    const float32_t qualtityWeakSerious;
  };
  struct DisabledHDR
  {
    const uint32_t integrationTime;
  };
  using HDRSettings = std::variant<SuperHDR, SimpleHDR, AutoHDR, DisabledHDR>;
  struct Settings
  {
    const std::string& deviceName;
    const bool enableDebug;
    const HDRSettings hdr;
  };

  HPSLidar(const Settings& settings);
  HPSLidar(const HPSLidar&) = delete;
  HPSLidar& operator=(const HPSLidar&) = delete;
  virtual void start(
      const std::filesystem::path& outputPath,
      const std::string_view recordingsBaseName) override;
  virtual void stop() override;
  virtual ~HPSLidar();
};

HPSLidar::Settings parseHpsConfig(
    const YAML::Node& config,
    const std::string& deviceName);
