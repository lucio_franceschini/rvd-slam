#pragma once
#include <optional>

#include <units.h>
#include "BaseDevice.h"
#pragma warning(push, 0)
#include <yaml-cpp/yaml.h>

#include <librealsense2/rs.hpp>
#include <opencv2/opencv.hpp>
#pragma warning(pop)

class RealSenseCamera : public BaseDevice
{
 public:
  struct DepthSettings
  {
    // Controls exposure time of color camera. Setting any value will disable
    // auto exposure RS2_OPTION_EXPOSURE Enable / disable color image
    // auto-exposure RS2_OPTION_ENABLE_AUTO_EXPOSURE
    const std::optional<float> manualExposure;

    // Color image gain
    // RS2_OPTION_GAIN
    const float gain;

    // Provide access to several recommend sets of option presets for the depth
    // camera RS2_OPTION_VISUAL_PRESET
    const rs2_rs400_visual_preset preset;

    // Power of the F200 / SR300 projector, with 0 meaning projector off
    // RS2_OPTION_LASER_POWER
    // Manual laser power in mw. applicable only when laser power mode is set to
    // Manual
    const MilliWatt laserPower;

    // Laser Emitter enabled
    // Power Control for D400 Projector, 0-off, 1-on
    // RS2_OPTION_EMITTER_ENABLED
    const bool emitterEnabled;

    // Number of meters represented by a single depth unit
    // RS2_OPTION_DEPTH_UNITS
    const float depthUnits;

    // When supported, this option make the camera to switch the emitter state
    // every frame. 0 for disabled, 1 for enabled
    // RS2_OPTION_EMITTER_ON_OFF
    // Alternating Emitter Pattern: 0:disabled(default), 1:enabled( emitter is
    // toggled on/off on per-frame basis)
    const bool emitterOnOff;

    const FullPixel width;
    const FullPixel height;
    const unsigned int fps;
    const bool infrared1Enabled;
    const bool infrared2Enabled;
  };

  struct ColorSettings
  {
    // RS2_OPTION_BACKLIGHT_COMPENSATION
    // Enable / disable color backlight compensation
    const bool backlightCompensationEnabled;

    // RS2_OPTION_BRIGHTNESS
    // Color image brightness
    const float brightness;

    // RS2_OPTION_CONTRAST
    // Color image contrast
    const float contrast;

    // RS2_OPTION_EXPOSURE
    // Controls exposure time of color camera. Setting any value will disable
    // auto exposure RS2_OPTION_ENABLE_AUTO_EXPOSURE Enable / disable color
    // image auto -  exposure
    const std::optional<float> manualExposure;

    // RS2_OPTION_GAIN
    // Color image gain
    const float gain;

    // RS2_OPTION_GAMMA
    // Color image gamma setting
    const float gamma;

    // RS2_OPTION_HUE
    // Color image hue
    const float hue;

    // RS2_OPTION_SATURATION
    // Color image saturation setting
    const float saturation;

    // RS2_OPTION_SHARPNESS
    // Color image sharpness setting
    const float sharpness;

    // RS2_OPTION_WHITE_BALANCE
    // Controls white balance of color image. Setting any value will disable
    // auto white RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE Enable / disable color
    // image auto - white - balance
    const std::optional<float> manualWhiteBalance;

    // RS2_OPTION_AUTO_EXPOSURE_PRIORITY
    // Allows sensor to dynamically adjust the frame rate depending on lighting
    // conditions Limit exposure time when auto exposure is ON to preserve
    // constant fps rate
    const bool autoExposurePriority;

    const FullPixel width;
    const FullPixel height;
    const unsigned int fps;
    const rs2_format format;
  };

 private:
  rs2::pipeline pipe;
  rs2::config cfg;
  const DepthSettings depthSettings;
  const ColorSettings colorSettings;

 public:
  RealSenseCamera(
      const DepthSettings& depthSettings,
      const ColorSettings& colorSettings);
  RealSenseCamera(const RealSenseCamera&) = delete;
  RealSenseCamera& operator=(const RealSenseCamera&) = delete;
  virtual void start(
      const std::filesystem::path& outputPath,
      const std::string_view recordingsBaseName) override;
  virtual void stop() override;
  virtual ~RealSenseCamera();
};

RealSenseCamera::DepthSettings parseRealsenseDepthConfig(
    const YAML::Node& config);
RealSenseCamera::ColorSettings parseRealsenseColorConfig(
    const YAML::Node& config);
