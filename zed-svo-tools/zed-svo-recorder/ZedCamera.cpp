#include "ZedCamera.h"

#include <map>
#include <stdexcept>
#include <string>
#include <constants.h>


namespace
{
template <typename T, typename K>
auto valueOrException(const T map, const K key)
{
  if (const auto kv = map.find(key); kv != map.end())
  {
    return kv->second;
  }
  else
  {
    throw std::logic_error("Unknown key " + key);
  }
}

auto detectCompressionMode(const std::string& mode)
{
  static const auto modes = std::map<std::string, sl::SVO_COMPRESSION_MODE>{
      {"H264", sl::SVO_COMPRESSION_MODE::H264},
      {"H265", sl::SVO_COMPRESSION_MODE::H265},
      {"LOSSLESS", sl::SVO_COMPRESSION_MODE::LOSSLESS},
  };
  return valueOrException(modes, mode);
}

auto detectResolution(const std::string& resolution)
{
  static const auto resolutions = std::map<std::string, sl::RESOLUTION>{
      {"HD720", sl::RESOLUTION::HD720},
      {"HD1080", sl::RESOLUTION::HD1080},
      {"HD2K", sl::RESOLUTION::HD2K},
      {"VGA", sl::RESOLUTION::VGA}};
  return valueOrException(resolutions, resolution);
}

auto detectDepthMode(const std::string& depthMode)
{
  static const auto depthModes = std::map<std::string, sl::DEPTH_MODE>{
      {"NONE", sl::DEPTH_MODE::NONE},
      {"PERFORMANCE", sl::DEPTH_MODE::PERFORMANCE},
      {"QUALITY", sl::DEPTH_MODE::QUALITY},
      {"ULTRA", sl::DEPTH_MODE::ULTRA}};
  return valueOrException(depthModes, depthMode);
}

auto detectSensingMode(const std::string& sensingMode)
{
  static const auto sensingModes = std::map<std::string, sl::SENSING_MODE>{
      {"STANDARD", sl::SENSING_MODE::STANDARD},
      {"FILL", sl::SENSING_MODE::FILL}};
  return valueOrException(sensingModes, sensingMode);
}

auto detectReferenceFrame(const std::string& referenceFrame)
{
  static const auto referenceFrames =
      std::map<std::string, sl::REFERENCE_FRAME>{
      {"CAMERA", sl::REFERENCE_FRAME::CAMERA},
      {"WORLD", sl::REFERENCE_FRAME::WORLD}};

  return valueOrException(referenceFrames, referenceFrame);
}

void setZedCameraSettings(sl::Camera& zedCamera)
{
  // Configure ZED capture parameters according to API documentation (Setting,
  // Value, useDefault)
  zedCamera.setCameraSettings(
      sl::VIDEO_SETTINGS::BRIGHTNESS,
      sl::VIDEO_SETTINGS_VALUE_AUTO);
  zedCamera.setCameraSettings(
      sl::VIDEO_SETTINGS::CONTRAST,
      sl::VIDEO_SETTINGS_VALUE_AUTO);
  zedCamera.setCameraSettings(
      sl::VIDEO_SETTINGS::HUE,
      sl::VIDEO_SETTINGS_VALUE_AUTO);
  zedCamera.setCameraSettings(
      sl::VIDEO_SETTINGS::SATURATION,
      sl::VIDEO_SETTINGS_VALUE_AUTO);
  zedCamera.setCameraSettings(
      sl::VIDEO_SETTINGS::GAIN,
      sl::VIDEO_SETTINGS_VALUE_AUTO);
  zedCamera.setCameraSettings(
      sl::VIDEO_SETTINGS::EXPOSURE,
      sl::VIDEO_SETTINGS_VALUE_AUTO);
  zedCamera.setCameraSettings(
      sl::VIDEO_SETTINGS::WHITEBALANCE_TEMPERATURE,
      sl::VIDEO_SETTINGS_VALUE_AUTO);
  zedCamera.setCameraSettings(
      sl::VIDEO_SETTINGS::WHITEBALANCE_AUTO,
      sl::VIDEO_SETTINGS_VALUE_AUTO);
}

void createOutputFile(
    sl::Camera& zedCamera,
    const sl::SVO_COMPRESSION_MODE compressionMode,
    const std::filesystem::path& outputFile)
{
  const sl::RecordingParameters recordingParameters(
      sl::String(outputFile.string().c_str()),
      compressionMode);

  if (const auto zedError = zedCamera.enableRecording(recordingParameters);
      zedError != sl::ERROR_CODE::SUCCESS)
  {
    const auto errorMessage = std::string("Recording initialization error: ") +
                              sl::toString(zedError).get();
    if (zedError == sl::ERROR_CODE::SVO_RECORDING_ERROR)
    {
      const auto additionalInfo =
          " Note : This error mostly comes from a wrong path or missing writing permissions.";
      throw std::runtime_error(errorMessage + additionalInfo);
    }
    else
    {
      throw std::runtime_error(errorMessage);
    }
  }
}

void recordFrame(
    sl::Camera& zedCamera,
    const sl::RuntimeParameters& runtimeParameters)
{
  if (const auto statusCode = zedCamera.grab(runtimeParameters);
      statusCode != sl::ERROR_CODE::SUCCESS)
  {
    throw std::runtime_error(sl::toString(statusCode).c_str());
  };
}

auto extractInitializationParameters(const ZedCamera::Settings& settings)
{
  // First set initial configuration parameters according to API documentation
  // These parameters can't be changed during runtime without re-initialization
  sl::InitParameters initParameters;
  initParameters.camera_resolution = settings.resolution;
  initParameters.camera_fps = settings.fps;

  initParameters.depth_mode = settings.depthMode;
  initParameters.coordinate_units = sl::UNIT::METER;
  initParameters.coordinate_system = sl::COORDINATE_SYSTEM::IMAGE;

  initParameters.depth_minimum_distance = settings.depthMinimumDistance;
  initParameters.camera_disable_self_calib = settings.cameraDisableSelfCalib;
  initParameters.camera_image_flip = settings.cameraImageFlip;
  initParameters.enable_right_side_measure = settings.enableRightSideMeasure;
  initParameters.depth_stabilization = settings.depthStabilization;

  initParameters.sdk_verbose = settings.sdkVerbose;
  return initParameters;
}

auto extractRuntimeParameters(const ZedCamera::Settings& settings)
{
  // Parameters that defines the behavior of the grab
  // These parameters are application specific and should not be changed
  sl::RuntimeParameters runtimeParameters;
  runtimeParameters.sensing_mode = settings.sensingMode;
  runtimeParameters.enable_depth = true;
  // runtimeParameters.enable_point_cloud = true;
  runtimeParameters.measure3D_reference_frame = settings.referenceFrame;
  return runtimeParameters;
}

}  // namespace

ZedCamera::ZedCamera(const Settings& settings) :
    BaseDevice("ZED"),
    settings{settings},
    initParameters{extractInitializationParameters(settings)},
    runtimeParameters{extractRuntimeParameters(settings)}
{
  // Attempt to open the camera
  if (const auto zedError = zedCamera.open(initParameters);
      zedError != sl::ERROR_CODE::SUCCESS)
  {
    throw std::runtime_error(sl::toString(zedError));
  }
}

void ZedCamera::start(
    const std::filesystem::path& outputPath,
    const std::string_view recordingsBaseName)
{
  if (const auto state = isRecording())
  {
    throw WrongStateCamera(state);
  }

  createOutputFile(
      zedCamera,
      settings.compressionMode,
      outputPath / (std::string(recordingsBaseName) + std::string(".") +
                    io::zedOutputFormat));
  setAsRecording();

  setZedCameraSettings(zedCamera);
  auto grabFrame = [this,
                    &recordingStopper = std::as_const(recordingStopper)]() {
    while (!recordingStopper.isStopped())
    {
      recordFrame(zedCamera, runtimeParameters);
    }
  };
  recordingStopper.start();
  recordingThread = std::thread(grabFrame);
}

void ZedCamera::stop()
{
  if (auto state = isRecording(); !state)
  {
    throw WrongStateCamera(state);
  }
  recordingStopper.stop();
  recordingThread.join();
  zedCamera.disableRecording();
  setAsNonRecording();
}

ZedCamera::~ZedCamera()
{
  if (isRecording())
  {
    stop();
  }
  zedCamera.close();
}

ZedCamera::Settings parseZedConfig(const YAML::Node& config)
{
  return {
      detectCompressionMode(config["compression_mode"].as<std::string>()),
      detectResolution(config["resolution"].as<std::string>()),
      detectDepthMode(config["depth_mode"].as<std::string>()),
      detectSensingMode(config["sensing_mode"].as<std::string>()),
      detectReferenceFrame(config["reference_frame"].as<std::string>()),
      config["depth_minimum_distance"].as<float>(),
      config["camera_disable_self_calib"].as<bool>(),
      config["camera_image_flip"].as<bool>(),
      config["enable_right_side_measure"].as<bool>(),
      config["depth_stabilization"].as<bool>(),
      config["sdk_verbose"].as<bool>(),
      config["fps"].as<int>()};
}
