#pragma once
#include <exception>

class WrongStateCamera : public std::exception
{
 private:
  const bool wasRunningState;

 public:
  WrongStateCamera(bool wasRunning) : wasRunningState{wasRunning} {};
  bool wasRunnung() const;
};
