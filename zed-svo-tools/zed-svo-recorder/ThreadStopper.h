#pragma once
#include <atomic>

class ThreadStopper
{
 private:
  std::atomic_bool stopped;

 public:
  ThreadStopper(bool stopped) : stopped{stopped} {}
  bool isStopped() const { return stopped.load(); }
  void start() { stopped.store(false); }
  void stop() { stopped.store(true); }
};
