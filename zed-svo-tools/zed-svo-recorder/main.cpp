#include "ZedCamera.h"
#include "HPSLidar.h"
#include "RealSenseCamera.h"

#include <constants.h>

#pragma warning(push, 0)
#include <date/tz.h>
#include <yaml-cpp/yaml.h>

#include <cxxopts.hpp>
#pragma warning(pop)

#include <chrono>
#include <filesystem>
#include <iostream>
#include <string>
#include <vector>

using namespace std::string_literals;
constexpr auto settingsPathArgName = "settings_path";
constexpr auto outputPathArgName = "output_path";
constexpr auto durationArgName = "duration";
constexpr auto metadataKeyArgName = "key";
constexpr auto metadataValueArgName = "value";
constexpr auto recordingsBaseName = "recording";
constexpr auto hpsDeviceNameArgName = "hps160_name";
constexpr auto tzPath = ".\\tzdata";

auto createDevices(const YAML::Node& config, const std::string& hpsDeviceName)
{
  std::vector<std::unique_ptr<BaseDevice>> devices;
  // The HPS must be first, otherwise it is not able to find the right device by
  // itself
  if (const auto hpsConfig = config[io::hps160Name])
  {
    std::cout << "Initializing HPS-3D160\n";
    devices.push_back(
        std::make_unique<HPSLidar>(parseHpsConfig(hpsConfig, hpsDeviceName)));
  }
  if (const auto zedConfig = config[io::zedName])
  {
    std::cout << "Initializing ZED\n";
    devices.push_back(std::make_unique<ZedCamera>(parseZedConfig(zedConfig)));
  }
  if (const auto realsenseConfig = config[io::d435Name])
  {
    std::cout << "Initializing D435\n";
    devices.push_back(std::make_unique<RealSenseCamera>(
        parseRealsenseDepthConfig(realsenseConfig["Depth"]),
        parseRealsenseColorConfig(realsenseConfig["Color"])));
  }
  return devices;
}

void startDevices(
    const std::vector<std::unique_ptr<BaseDevice>>& devices,
    const std::filesystem::path& outputPath)
{
  for (auto& device : devices)
  {
    std::cout << ("Starting " + device->name() + '\n');
    device->start(outputPath, recordingsBaseName);
  }
}

void stopDevices(std::vector<std::unique_ptr<BaseDevice>>& devices)
{
  std::for_each(
      std::crbegin(devices),
      std::crend(devices),
      [](const std::unique_ptr<BaseDevice>& device) {
        std::cout << ("Stopping " + device->name() + '\n');
        device->stop();
      });
}

void pressEnterToContinue(const std::string_view message)
{
  std::cout << message << std::endl;
  std::cin.get();
}

using MetadataList = std::vector<std::pair<std::string, std::string>>;

auto readMetadaCommandLine(const cxxopts::ParseResult& args)
{
  const auto numberKeys = args.count(metadataKeyArgName);
  const auto numberValues = args.count(metadataValueArgName);
  if (numberKeys != numberValues)
  {
    throw std::runtime_error(
        "The key parameters are in a different quantity of value parameters");
  }
  MetadataList metadata;
  if (numberKeys > 0)
  {
    const auto keys = args[metadataKeyArgName].as<std::vector<std::string>>();
    const auto values =
        args[metadataValueArgName].as<std::vector<std::string>>();

    for (size_t idx = 0; idx < keys.size(); ++idx)
    {
      metadata.emplace_back(keys[idx], values[idx]);
    }
  }
  return metadata;
}

void parseMetadataTypeAndSave(const std::string& value, YAML::Emitter& out)
{
  try
  {
    out << std::stod(value);
  }
  catch (const std::invalid_argument&)
  {
    try
    {
      out << std::stoi(value);
    }
    catch (const std::invalid_argument&)
    {
      out << value;
    }
  }
}

void saveMetadata(
    const std::filesystem::path& outputPath,
    const MetadataList& metadata,
    const YAML::Node& metadataFromFime)
{
  if (!metadata.empty())
  {
    YAML::Emitter out;
    out << YAML::BeginMap;
    for (const auto& [key, value] : metadata)
    {
      out << YAML::Key << key << YAML::Value;
      parseMetadataTypeAndSave(value, out);
    }
    for (const auto& node : metadataFromFime)
    {
      out << YAML::Key << node.first << YAML::Value << node.second;
    }
    out << YAML::EndMap;
    std::ofstream outputFile(outputPath / io::sessionMetadataFilename);
    outputFile << out.c_str();
  }
}

int main(int argc, char** argv)
{
  try
  {
    date::set_install(tzPath);
    cxxopts::Options options("Recorder", "Record data from the RACOON-Lab");
    options.add_options()(
        durationArgName,
        "Record data for a fixed amount of time [milliseconds]",
        cxxopts::value<unsigned int>())(
        outputPathArgName,
        "Where to create the resulting folder with all the recordings",
        cxxopts::value<std::filesystem::path>())(
        settingsPathArgName,
        "Path to the .yaml file with the devices settings",
        cxxopts::value<std::filesystem::path>())(
        metadataKeyArgName,
        "Additional metadata parameter key",
        cxxopts::value<std::vector<std::string>>())(
        metadataValueArgName,
        "Additional metadata parameter value",
        cxxopts::value<std::vector<std::string>>())(
        hpsDeviceNameArgName,
        "HPS-3D160 device name (e.g. \\\\.\\COM3)",
        cxxopts::value<std::string>());

    const auto args = options.parse(argc, argv);
    const auto root = args[outputPathArgName].as<std::filesystem::path>();
    if (!std::filesystem::exists(root))
    {
      throw std::runtime_error("Root path does not exist");
    }

    const auto settingsFile =
        args[settingsPathArgName].as<std::filesystem::path>();
    if (!std::filesystem::exists(settingsFile))
    {
      throw std::runtime_error(settingsFile.string() + " does not exist");
    }

    const auto settings = YAML::LoadFile(settingsFile.string());
    auto devices = createDevices(settings["devices"], [&args] {
      if (args.count(hpsDeviceNameArgName) > 0)
      {
        return args[hpsDeviceNameArgName].as<std::string>();
      }
      else
      {
        return std::string();
      }
    }());

    pressEnterToContinue(
        "Devices initialized, press Enter to start the recording session");
    const auto now = date::make_zoned("UTC", std::chrono::system_clock::now());
    const auto outputPath = root / date::format(io::timestampFormat, now);

    if (!std::filesystem::create_directory(outputPath))
    {
      throw std::runtime_error("Unable to create directory");
    }

    std::cout << "The recordings will be written in " << outputPath
              << std::endl;

    saveMetadata(outputPath, readMetadaCommandLine(args), settings["metadata"]);

    startDevices(devices, outputPath);

    if (args.count(durationArgName) > 0)
    {
      std::cout << "Recording..." << std::endl;
      const auto delay =
          std::chrono::milliseconds(args[durationArgName].as<unsigned int>());
      std::this_thread::sleep_for(delay);
    }
    else
    {
      pressEnterToContinue("Recording, press Enter to stop");
    }

    stopDevices(devices);

    std::cout << "All devices have been stopped" << std::endl;
  }
  catch (std::exception& ex)
  {
    std::cout << ex.what() << std::endl;
  }
  return 0;
}
