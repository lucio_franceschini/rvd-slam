#pragma once
#pragma warning(push, 0)
#include <sl/Camera.hpp>
#pragma warning(pop)

#include "BaseDevice.h"
#include "ThreadStopper.h"

#pragma warning(push, 0)
#include <yaml-cpp/yaml.h>
#include <opencv2/opencv.hpp>
#pragma warning(pop)

#include <thread>

class ZedCamera : public BaseDevice
{
 public:
  struct Settings
  {
    const sl::SVO_COMPRESSION_MODE compressionMode;
    const sl::RESOLUTION resolution;
    const sl::DEPTH_MODE depthMode;
    const sl::SENSING_MODE sensingMode;
    const sl::REFERENCE_FRAME referenceFrame;
    const float depthMinimumDistance;
    const bool cameraDisableSelfCalib;
    const bool cameraImageFlip;
    const bool enableRightSideMeasure;
    const bool depthStabilization;
    const bool sdkVerbose;
    const int fps;
  };

 private:
  sl::Camera zedCamera;
  const sl::InitParameters initParameters;
  const sl::RuntimeParameters runtimeParameters;
  ThreadStopper recordingStopper{false};
  std::thread recordingThread;
  const Settings settings;

 public:
  ZedCamera(const Settings& settings);
  ZedCamera(const ZedCamera&) = delete;
  ZedCamera& operator=(const ZedCamera&) = delete;
  virtual void start(
      const std::filesystem::path& outputPath,
      const std::string_view recordingsBaseName) override;
  virtual void stop() override;
  virtual ~ZedCamera();
};

ZedCamera::Settings parseZedConfig(const YAML::Node& config);