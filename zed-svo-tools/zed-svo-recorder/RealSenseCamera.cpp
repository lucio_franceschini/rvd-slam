#include "RealSenseCamera.h"

#include <fstream>
#include <gsl/gsl>
#pragma warning(push, 0)
#include <librealsense2/rs_advanced_mode.hpp>
#include "cv-helpers.hpp"
#pragma warning(pop)

#include <constants.h>


namespace
{
rs2_format parseFormat(const YAML::Node& config)
{
  const auto formatName = config["format"].as<std::string>();
  for (auto formatIdx = 0; formatIdx < RS2_FORMAT_COUNT; formatIdx++)
  {
    const auto format = static_cast<rs2_format>(formatIdx);
    if (formatName == rs2_format_to_string(format))
    {
      return format;
    }
  }
  throw std::logic_error("Unknow format");
}

template <typename T>
std::optional<T> parseOptional(const YAML::Node& config, const std::string& key)
{
  const auto option = config[key];
  if (option)
  {
    return option.as<T>();
  }
  else
  {
    return {};
  }
}

template <typename T>
T parseOrDefault(
    const YAML::Node& config,
    const std::string& key,
    const T& default_value)
{
  const auto option = config[key];
  if (option)
  {
    return option.as<T>();
  }
  else
  {
    return default_value;
  }
}

rs2_rs400_visual_preset parsePreset(const YAML::Node& config)
{
  const auto presetName = config["preset"].as<std::string>();
  for (auto presetIdx = 0; presetIdx < RS2_RS400_VISUAL_PRESET_COUNT;
       presetIdx++)
  {
    const auto preset = static_cast<rs2_rs400_visual_preset>(presetIdx);
    if (presetName == rs2_rs400_visual_preset_to_string(preset))
    {
      return preset;
    }
  }
  throw std::logic_error("Unknown preset");
}

auto setBoolField(
    rs2::sensor* sensor,
    const rs2_option option,
    const bool value)
{
  if (value)
  {
    sensor->set_option(option, 1.0f);
  }
  else
  {
    sensor->set_option(option, 0.0f);
  }
}

void setDepthSettings(
    rs2::sensor* sensor,
    const RealSenseCamera::DepthSettings& settings)
{
  if (settings.manualExposure.has_value())
  {
    setBoolField(sensor, RS2_OPTION_ENABLE_AUTO_EXPOSURE, false);
    sensor->set_option(RS2_OPTION_EXPOSURE, settings.manualExposure.value());
    sensor->set_option(RS2_OPTION_GAIN, settings.gain);
  }
  else
  {
    setBoolField(sensor, RS2_OPTION_ENABLE_AUTO_EXPOSURE, true);
  }
  sensor->set_option(RS2_OPTION_DEPTH_UNITS, settings.depthUnits);

  if (settings.emitterEnabled)
  {
    sensor->set_option(
        RS2_OPTION_LASER_POWER,
        gsl::narrow_cast<float>(settings.laserPower.value()));
    setBoolField(sensor, RS2_OPTION_EMITTER_ON_OFF, settings.emitterOnOff);
  }
  setBoolField(sensor, RS2_OPTION_EMITTER_ENABLED, settings.emitterEnabled);

  sensor->set_option(
      RS2_OPTION_VISUAL_PRESET,
      gsl::narrow_cast<float>(settings.preset));
}

void setColorSettings(
    rs2::sensor* sensor,
    const RealSenseCamera::ColorSettings& settings)
{
  setBoolField(
      sensor,
      RS2_OPTION_BACKLIGHT_COMPENSATION,
      settings.backlightCompensationEnabled);

  sensor->set_option(RS2_OPTION_BRIGHTNESS, settings.brightness);
  sensor->set_option(RS2_OPTION_CONTRAST, settings.contrast);

  if (settings.manualExposure.has_value())
  {
    setBoolField(sensor, RS2_OPTION_ENABLE_AUTO_EXPOSURE, false);
    sensor->set_option(RS2_OPTION_EXPOSURE, settings.manualExposure.value());
    sensor->set_option(RS2_OPTION_GAIN, settings.gain);
  }
  else
  {
    setBoolField(sensor, RS2_OPTION_ENABLE_AUTO_EXPOSURE, true);
  }

  sensor->set_option(RS2_OPTION_GAMMA, settings.gamma);
  sensor->set_option(RS2_OPTION_HUE, settings.hue);
  sensor->set_option(RS2_OPTION_SATURATION, settings.saturation);
  sensor->set_option(RS2_OPTION_SHARPNESS, settings.sharpness);

  if (settings.manualWhiteBalance)
  {
    sensor->set_option(
        RS2_OPTION_WHITE_BALANCE,
        settings.manualWhiteBalance.value());
  }
  setBoolField(
      sensor,
      RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE,
      !settings.manualWhiteBalance);

  setBoolField(
      sensor,
      RS2_OPTION_AUTO_EXPOSURE_PRIORITY,
      settings.autoExposurePriority);
}

auto isDepthSensor(const rs2::sensor& sensor)
{
  return sensor.supports(RS2_OPTION_DEPTH_UNITS);
}

auto isColorSensor(const rs2::sensor& sensor)
{
  return sensor.supports(RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE);
}

auto enableInfraredStream(
    rs2::config* const cfg,
    const int id,
    const RealSenseCamera::DepthSettings& depthSettings)
{
  cfg->enable_stream(
      rs2_stream::RS2_STREAM_INFRARED,
      id,
      gsl::narrow<int>(depthSettings.width.value()),
      gsl::narrow<int>(depthSettings.height.value()),
      RS2_FORMAT_Y8,
      depthSettings.fps);
}
}  // namespace

RealSenseCamera::RealSenseCamera(
    const DepthSettings& depthSettings,
    const ColorSettings& colorSettings) :
    BaseDevice("D435"),
    depthSettings{depthSettings},
    colorSettings{colorSettings}
{
  cfg.enable_stream(
      rs2_stream::RS2_STREAM_DEPTH,
      -1,
      gsl::narrow<int>(depthSettings.width.value()),
      gsl::narrow<int>(depthSettings.height.value()),
      RS2_FORMAT_Z16,
      depthSettings.fps);
  cfg.enable_stream(
      rs2_stream::RS2_STREAM_COLOR,
      -1,
      gsl::narrow<int>(colorSettings.width.value()),
      gsl::narrow<int>(colorSettings.height.value()),
      colorSettings.format,
      colorSettings.fps);
  if (depthSettings.infrared1Enabled)
  {
    enableInfraredStream(&cfg, 1, depthSettings);
  }
  if (depthSettings.infrared2Enabled)
  {
    enableInfraredStream(&cfg, 2, depthSettings);
  }
}

void RealSenseCamera::start(
    const std::filesystem::path& outputPath,
    const std::string_view recordingsBaseName)
{
  if (auto state = isRecording())
  {
    throw WrongStateCamera(state);
  }
  const auto pathWithoutExtension = outputPath / recordingsBaseName;
  cfg.enable_record_to_file(
      pathWithoutExtension.string() + std::string(".") +
      io::realsenseOutputFormat);

  rs2::context ctx;
  auto sensors = ctx.query_all_sensors();
  for (auto& sensor : sensors)
  {
    if (isColorSensor(sensor))
    {
      setColorSettings(&sensor, colorSettings);
    }
    else if (isDepthSensor(sensor))
    {
      setDepthSettings(&sensor, depthSettings);
    }
  }
  const auto device = pipe.start(cfg).get_device();

  if (device.is<rs400::advanced_mode>())
  {
    const auto outputFileJson = [&pathWithoutExtension] {
      auto outputFileJson = pathWithoutExtension;
      outputFileJson.replace_extension(".json");
      return outputFileJson;
    }();

    std::ofstream ofs(outputFileJson, std::ofstream::out);

    const auto adv = device.as<rs400::advanced_mode>();
    ofs << adv.serialize_json();
  }
  setAsRecording();
}

void RealSenseCamera::stop()
{
  if (auto state = isRecording(); !state)
  {
    throw WrongStateCamera(state);
  }
  pipe.stop();
  setAsNonRecording();
}

RealSenseCamera::~RealSenseCamera() {}

RealSenseCamera::DepthSettings parseRealsenseDepthConfig(
    const YAML::Node& config)
{
  return {
      parseOptional<float>(config, "exposure"),
      parseOrDefault<float>(config, "gain", 0.0f),
      parsePreset(config),
      MilliWatt{config["laser_power"].as<float>()},
      config["emitter_enabled"].as<bool>(),
      config["depth_units"].as<float>(),
      config["emitter_on_off"].as<bool>(),
      FullPixel{config["width"].as<unsigned int>()},
      FullPixel{config["height"].as<unsigned int>()},
      config["fps"].as<unsigned int>(),
      config["infrared_1_enabled"].as<bool>(),
      config["infrared_2_enabled"].as<bool>(),
  };
}
RealSenseCamera::ColorSettings parseRealsenseColorConfig(
    const YAML::Node& config)
{
  return {
      config["backlight_compensation_enabled"].as<bool>(),
      config["brightness"].as<float>(),
      config["contrast"].as<float>(),
      parseOptional<float>(config, "exposure"),
      parseOrDefault<float>(config, "gain", 0.0f),
      config["gamma"].as<float>(),
      config["hue"].as<float>(),
      config["saturation"].as<float>(),
      config["sharpness"].as<float>(),
      parseOptional<float>(config, "white_balance"),
      config["auto_exposure_priority"].as<bool>(),
      FullPixel{config["width"].as<unsigned int>()},
      FullPixel{config["height"].as<unsigned int>()},
      config["fps"].as<unsigned int>(),
      parseFormat(config),
  };
}
