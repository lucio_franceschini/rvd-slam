#pragma once
#include <filesystem>
#include <string>

#include "WrongStateCamera.h"

class BaseDevice
{
 private:
  bool isRecordingState{false};
  const std::string cameraName;

 protected:
  void setAsRecording() { isRecordingState = true; }
  void setAsNonRecording() { isRecordingState = false; }

 public:
  BaseDevice(const std::string& name) : cameraName{name} {};
  virtual void start(
      const std::filesystem::path& outputPath,
      const std::string_view recordingsBaseName) = 0;
  virtual void stop() = 0;
  virtual ~BaseDevice() = default;
  bool isRecording() const { return isRecordingState; }
  std::string name() const { return cameraName; };
};
