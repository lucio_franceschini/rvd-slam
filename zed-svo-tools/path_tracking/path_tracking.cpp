#include <utility.h>

#include <cxxopts.hpp>

constexpr auto tzPath = ".\\tzdata";

constexpr auto sensorArgName = "sensor";
constexpr auto rootPathArgName = "root_path";
constexpr auto outputFolderArgName = "output_folder";
constexpr auto sessionNameArgName = "session_name";
constexpr auto filterIdArgName = "filter_id";
constexpr auto algorithmIdArgName = "algorithm_id";
constexpr auto svoFileArgName = "svo_file";
constexpr auto fromTimestampArgName = "from";
constexpr auto toTimestampArgName = "to";

std::optional<io::Timestamp> decodeTimestampIfExists(
    const cxxopts::ParseResult& args,
    const std::string& argName)
{
  if (args.count(argName) > 0)
  {
    const auto timestampStr = args[argName].as<std::string>();
    return io::stringToTimestamp(timestampStr);
  }
  else
  {
    return {};
  }
}

bool eitherBothOrNeitherDefined(
    const std::optional<io::Timestamp>& fromTimestamp,
    const std::optional<io::Timestamp>& toTimestamp)
{
  return (fromTimestamp && toTimestamp) || (!fromTimestamp && !toTimestamp);
}

int main(int argc, char** argv)
{
  date::set_install(tzPath);
  cxxopts::Options options("Path tracking", "Calculate path");
  options.add_options()(
      sensorArgName,
      "Sensor to analyze",
      cxxopts::value<std::string>())(
      rootPathArgName,
      "Where to find the source file",
      cxxopts::value<std::filesystem::path>())(
      outputFolderArgName,
      "Where to store the calculated path",
      cxxopts::value<std::filesystem::path>())(
      sessionNameArgName,
      "Session to analyze",
      cxxopts::value<std::string>())(
      filterIdArgName,
      "Filter to use",
      cxxopts::value<int>())(
      algorithmIdArgName,
      "Algorithm to use",
      cxxopts::value<int>())(
      svoFileArgName,
      "SVO file path (optional)",
      cxxopts::value<std::filesystem::path>())(
      fromTimestampArgName,
      "From timestamp - Does not work with ZED SDK's path algorithm - Required "
      "only with '" +
          std::string(toTimestampArgName),
      cxxopts::value<std::string>())(
      toTimestampArgName,
      "To timestamp - Does not work with ZED SDK's path algorithm - Required "
      "only with '" +
          std::string(fromTimestampArgName),
      cxxopts::value<std::string>());

  const auto args = options.parse(argc, argv);

  const auto settings = path_tracking::Settings{
      sensors::detectSensor(args[sensorArgName].as<std::string>()),
      args[rootPathArgName].as<std::filesystem::path>(),
      args[outputFolderArgName].as<std::filesystem::path>(),
      args[sessionNameArgName].as<std::string>(),
      args[filterIdArgName].as<int>(),
      args[algorithmIdArgName].as<int>()};
  std::cout << "Session name: " << settings.sessionName << std::endl;
  std::cout << "Sensor: " << sensors::sensorName(settings.sensor) << std::endl;
  std::cout << "Filter id: " << settings.filterId << std::endl;
  std::cout << "Algorithm id: " << settings.algorithmId << std::endl;
  std::cout << "Root path: " << settings.rootPath << std::endl;
  std::cout << "Output folder: " << settings.outputFolder << std::endl;

  const std::optional<io::Timestamp> fromTimestamp =
      decodeTimestampIfExists(args, fromTimestampArgName);
  const std::optional<io::Timestamp> toTimestamp =
      decodeTimestampIfExists(args, toTimestampArgName);

  if (args.count(svoFileArgName) > 0)
  {
    if (fromTimestamp || toTimestamp)
    {
      throw std::runtime_error(
          "Timerange not implemented with ZED SDK's path algorithm");
    }
    const auto svoFile = args[svoFileArgName].as<std::filesystem::path>();
    std::cout << "SVO file: " << svoFile << std::endl;
#ifdef CUDA
    path_tracking::calculateTrackingZed(settings, svoFile, std::cout);
#else
    throw std::runtime_error("CUDA disabled at compile time");
#endif
  }
  else
  {
    if (!eitherBothOrNeitherDefined(fromTimestamp, toTimestamp))
    {
      throw std::runtime_error(
          "It is either possible to define both timestamps or neither of them");
    }
    path_tracking::calculateTracking(
        settings,
        fromTimestamp,
        toTimestamp,
        std::cout);
  }

  return 0;
}