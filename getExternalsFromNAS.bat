@echo off

set REMOTE=\\nas.ads.mwn.de\tumw\lrt\Staff\04-Infrastruktur\09-RACOON-Lab\06_Code\external
set LOCAL=%~dp0\..\..\external

echo Source: %REMOTE%
echo Destination: %LOCAL%
echo .
echo Copying externals my take a while. Please stay patient until 'Finished.'
echo .

xcopy %REMOTE%\cpu_tsdf %LOCAL%\cpu_tsdf /eqyi
xcopy %REMOTE%\Cuda\8.0 %LOCAL%\Cuda\8.0 /eqyi
xcopy %REMOTE%\Eigen\3.2.8 %LOCAL%\Eigen\3.2.8 /eqyi
xcopy %REMOTE%\fastfusion %LOCAL%\fastfusion /eqyi
xcopy %REMOTE%\Kinect\1.8 %LOCAL%\Kinect\1.8 /eqyi
xcopy %REMOTE%\Kinect\2.0 %LOCAL%\Kinect\2.0 /eqyi
xcopy %REMOTE%\MRPT\1.3.2 %LOCAL%\MRPT\1.3.2 /eqyi
xcopy %REMOTE%\OpenCV\3.0.0 %LOCAL%\OpenCV\3.0.0 /eqyi
xcopy %REMOTE%\OpenCV\3.0.0 %LOCAL%\OpenCV\3.1.0 /eqyi
xcopy %REMOTE%\PCL\1.7.2 %LOCAL%\PCL\1.7.2 /eqyi
xcopy %REMOTE%\Qt\5.2.1 %LOCAL%\Qt\5.2.1 /eqyi
xcopy %REMOTE%\VTK\6.3.0 %LOCAL%\VTK\6.3.0 /eqyi
xcopy %REMOTE%\Zed\1.2.0 %LOCAL%\Zed\1.2.0 /eqyi

echo .
echo Finished.

pause > nul