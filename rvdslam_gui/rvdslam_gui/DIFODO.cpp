#include "stdafx.h"

#include "DIFODO.h"

#include <opencv2/imgproc/imgproc.hpp>

racoon::rvdslam::DIFODO::DIFODO(unsigned int downsample, unsigned int ctf_levels, float max_depth, unsigned int cols, unsigned int rows) : max_depth(max_depth)
{
	this->cols = cols / downsample;
	this->rows = rows / downsample;
	this->downsample = downsample;
	this->ctf_levels = ctf_levels;

	this->width = cols / downsample;
	this->height = rows / downsample;

	//Resize pyramid
	const unsigned int pyr_levels = floor(log(float(this->width / this->cols)) / log(2.f) + 0.5) + this->ctf_levels; //round(log(float(width / cols)) / log(2.f)) + ctf_levels; (C++11)
	depth.resize(pyr_levels);
	depth_old.resize(pyr_levels);
	depth_inter.resize(pyr_levels);
	depth_warped.resize(pyr_levels);
	xx.resize(pyr_levels);
	xx_inter.resize(pyr_levels);
	xx_old.resize(pyr_levels);
	xx_warped.resize(pyr_levels);
	yy.resize(pyr_levels);
	yy_inter.resize(pyr_levels);
	yy_old.resize(pyr_levels);
	yy_warped.resize(pyr_levels);
	transformations.resize(pyr_levels);

	for (unsigned int i = 0; i<pyr_levels; i++)
	{
		unsigned int s = pow(2.f, int(i));
		cols_i = this->width / s; rows_i = this->height / s;
		depth[i].resize(rows_i, cols_i);
		depth_inter[i].resize(rows_i, cols_i);
		depth_old[i].resize(rows_i, cols_i);
		depth[i].assign(0.0f);
		depth_old[i].assign(0.0f);
		xx[i].resize(rows_i, cols_i);
		xx_inter[i].resize(rows_i, cols_i);
		xx_old[i].resize(rows_i, cols_i);
		xx[i].assign(0.0f);
		xx_old[i].assign(0.0f);
		yy[i].resize(rows_i, cols_i);
		yy_inter[i].resize(rows_i, cols_i);
		yy_old[i].resize(rows_i, cols_i);
		yy[i].assign(0.0f);
		yy_old[i].assign(0.0f);
		transformations[i].resize(4, 4);

		if (cols_i <= this->cols)
		{
			depth_warped[i].resize(rows_i, cols_i);
			xx_warped[i].resize(rows_i, cols_i);
			yy_warped[i].resize(rows_i, cols_i);
		}
	}

	depth_wf.setSize(this->height, this->width);

	mrpt::poses::CPose3D trans;
	trans.setFromValues(0, 0, 0, 0, -0.5*M_PI, -0.5*M_PI);

	cam_pose = trans;
}

void racoon::rvdslam::DIFODO::setInputDepthImage(cv::Mat image, unsigned int scale)
{
	depth_image = image;
	depth_scale = scale;
}

void racoon::rvdslam::DIFODO::loadFrame()
{
	cv::Mat depth_image_downsampled;

	cv::resize(depth_image, depth_image_downsampled, cv::Size(), 1.0 / downsample, 1.0 / downsample,cv::INTER_NEAREST);

	cv::cv2eigen(depth_image_downsampled, depth_wf);

	depth_wf /= depth_scale;

	float max_depth = this->max_depth;

	std::for_each(depth_wf.data(), depth_wf.data() + depth_wf.size(), [max_depth](float &n){ if (n > max_depth) n = 0.0f; });

	depth_wf.reverseInPlace();
}