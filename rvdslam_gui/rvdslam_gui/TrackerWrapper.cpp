#include "stdafx.h"

#include "FramePackage.h"
#include "TrackerWrapper.h"

racoon::rvdslam::TrackerWrapper::TrackerWrapper()
{
}

racoon::rvdslam::TrackerWrapper::~TrackerWrapper()
{
}

void racoon::rvdslam::TrackerWrapper::addFramePackage(std::shared_ptr<FramePackage> frame_package)
{
	input_queue.push(frame_package);
	emit queueSizeChanged(input_queue.size());

	if (currentState() == State::IDLE)
		switchState(State::RUNNING);
}

void racoon::rvdslam::TrackerWrapper::setProcessAll(bool process_all)
{
	this->process_all = process_all;
}

void racoon::rvdslam::TrackerWrapper::clearQueue()
{
	input_queue = std::queue<std::shared_ptr<FramePackage>>();

	emit queueSizeChanged(input_queue.size());
}

bool racoon::rvdslam::TrackerWrapper::doRunning()
{
	if (input_queue.empty())
	{
		return switchState(State::IDLE);
	}

	std::shared_ptr<FramePackage> frame_package;

	if (process_all)
	{
		frame_package = input_queue.front();
		input_queue.pop();
	}
	else
	{
		frame_package = input_queue.back();
		input_queue = std::queue<std::shared_ptr<FramePackage>>();
	}

	emit queueSizeChanged(input_queue.size());

	computeNextPose(*frame_package);

	Eigen::Affine3d pose = getPose();
	frame_package->setEstimatedPose(pose);

	emit poseEstimated(frame_package);

	return switchState(State::RUNNING);
}