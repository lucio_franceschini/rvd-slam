#pragma once

#include "CameraFrame.h"

#include <memory>
#include <vector>
#include <array>
#include <opencv2/core/core.hpp>

namespace racoon
{
	namespace rvdslam
	{
		/**
		 * Implementation of CameraFrame class for a color frame.
		 *
		 * Includes RGB data as a char array of length width_px * height_px * 3.
		 * Provides several data access methods for raw and vector format additionally to cv::Mat.
		 */
		class CameraFrameColor : public CameraFrame
		{
		public:
			/** Default constructor, calls base class constructor without parameters. Generally should not be called. */
			CameraFrameColor();
			
			/** Constructor to create a new color camera frame from raw data. */
			CameraFrameColor(const double timestamp_secs,
				const unsigned int width_px,
				const unsigned int height_px,
				const EncodingType encoding_type,
				const FramePackage::Intrinsics intrinsics,
				const unsigned char* color_data);

			/** Copy constructor */
			CameraFrameColor(const CameraFrameColor& color_frame);

			/** Assignment operator */
			CameraFrameColor& operator= (const CameraFrameColor& color_frame);

			/** Default destructor */
			~CameraFrameColor();

			/** Returns a pointer to the raw data */
			unsigned char* getDataRaw() const;

			/** Returns the data as an std::vector of length width_px * height_px * 3 */
			std::vector<unsigned char> getDataRawVector() const;

			/**
			  * Returns the data as an std::vector of length width_px * height_px.
			  * Each element contains the data of one pixel as an std::array of length 3 with one element per channel.
			  */
			std::vector<std::array<unsigned char, 3>> getDataVector() const;

			/** Returns the data as a cv::Mat with size width_px * height_px and three channels with three 8 bit color values per pixel (format CV_8UC3, channel order BGR) */	
			cv::Mat getDataCVMat() const;
		private:
			unsigned char* color_data;
		};
	}
}