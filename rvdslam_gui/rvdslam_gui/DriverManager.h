#pragma once

#include "FramePackage.h"
#include "DriverWrapper.h"

#include <QObject>
#include <QThread>

#include <memory>
#include <chrono>

namespace racoon
{
	namespace rvdslam
	{
		class DataManager;
		class ViewManager;
		class SLAMPipeline;

		/**
	     * This class manages data capture through different real and virtual camera devices that can be switched during runtime.
		 * Whenever a new frame package is captured, the DriverManager processes it by adding a filter (if specified) and handing it over to the rest of the system.
		 * Devices can be added by implementing the DriverWraper interface and adding the specific class to the initDriver() method together with adding a corresponding DriverType.
		 */
		class DriverManager : public QObject
		{
			Q_OBJECT
		public:
			/** The driver type is used to distinguish different capture devices */
			enum class DriverType
			{
				KINECT_V1, KINECT_V2, XTION, ZED, RGBD_FILE_READER, RGBD_VIRTUAL_CAMERA, BUMBLEBEE
			};

			/**
			 * Default constructor.
			 * driver_type specifies the initial device driver module to be loaded. 
			 */
			DriverManager(DataManager & data_manager, ViewManager & view_manager, SLAMPipeline & slam_pipeline, DriverType driver_type);

			/** Default destructor */
			~DriverManager();
			
			/** Stops the current device driver module and switches to the specified one */
			void switchDriver(DriverType new_driver_type);

			/** Requests the current device driver module to start capturing */
			void startDriver();

			/** Requests the current device driver module to pause capturing */
			void pauseDriver();

			/** Requests the current device driver module to resume capturing */
			void resumeDriver();

			/** Requests the current device driver module to reset itself */
			void resetDriver();

			/** Returns the state of the current device driver module */
			GenericWrapper::State getDriverState();

			/** Returns a pointer to the current device driver module's settings */
			std::unique_ptr<GenericWrapper::Settings> getDriverSettings();

			/** Sets the current device driver module's settings */
			void setDriverSettings(std::shared_ptr<const GenericWrapper::Settings> settings);

			/** Sets the current frame package filter to be applied to every captured frame package */
			void setFramePackageFilter(std::shared_ptr<FramePackage::Filter> filter);

			/** Removes the current frame package filter */
			void removeFramePackageFilter();
		private:
			DataManager & data_manager;
			ViewManager & view_manager;
			SLAMPipeline & slam_pipeline;
			DriverWrapper * driver;

			QThread driver_thread;

			GenericWrapper::State driver_state;

			std::shared_ptr<FramePackage::Filter> filter;

			std::chrono::time_point<std::chrono::high_resolution_clock> systemtime_old;

			/** Sets the current capture device and initializes it */
			void initDriver(DriverType driver_type);
		private slots:
			/** 
			 * Propagates the device driver module's state to the UI.
			 * Clears the captured frame package queue in the data manager and the tracker queue in the SLAM pipeline in case of a reset.
			 */
			void handleDriverStateChanged(GenericWrapper::State driver_state);

			/**
			 * Processes a newly captured frame package by adding a filter (if specified) and handing it over to data manager, view manager and SLAM pipeline.
			 * Also computes capture frame rate for UI.
			 */
			void handleNewFramePackage(std::shared_ptr<FramePackage>);
		signals:
			void initDriverSignal();
			void startDriverSignal();
			void pauseDriverSignal();
			void resumeDriverSignal();
			void resetDriverSignal();
			void applyDriverSettings(std::shared_ptr<const GenericWrapper::Settings> settings);
		};
	}
}