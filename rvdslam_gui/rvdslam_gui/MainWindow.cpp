#include "stdafx.h"

#include "MainWindow.h"
#include "ViewManager.h"
#include "DriverSettingsDialogRGBDFileReader.h"
#include "DriverSettingsDialogRGBDVirtualCamera.h"
#include "DriverSettingsDialogKinectV2.h"
#include "TrackerSettingsDialogDIFODO.h"
#include "MapperSettingsDialogFastFusion.h"

#include <QFileDialog>
#include <QIcon>
#include <QStyle>
#include <QDesktopWidget>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

racoon::rvdslam::MainWindow::MainWindow(ViewManager & view_manager, QWidget *parent) : view_manager(view_manager), QMainWindow(parent), driver_saving_progress_dialog(this)
{
	ui.setupUi(this);

	this->setGeometry(0, 30, this->size().width(), this->size().height());
	this->setWindowTitle("RVDSLAM");
	this->setWindowIcon(QIcon("racoon.jpg"));

	// Connect ui elements with functions
	connect(ui.combobox_driver_select, SIGNAL(currentIndexChanged(int)), this, SLOT(driverChanged(int)));
	connect(ui.combobox_tracker_select, SIGNAL(currentIndexChanged(int)), this, SLOT(trackerChanged(int)));
	connect(ui.combobox_mapper_select, SIGNAL(currentIndexChanged(int)), this, SLOT(mapperChanged(int)));

	connect(ui.checkbox_boxfilter, SIGNAL(stateChanged(int)), this, SLOT(boxfilterStateChanged(int)));
	connect(ui.spinbox_boxfilter_umin, SIGNAL(valueChanged(int)), this, SLOT(boxfilterValueChanged()));
	connect(ui.spinbox_boxfilter_umax, SIGNAL(valueChanged(int)), this, SLOT(boxfilterValueChanged()));
	connect(ui.spinbox_boxfilter_vmin, SIGNAL(valueChanged(int)), this, SLOT(boxfilterValueChanged()));
	connect(ui.spinbox_boxfilter_vmax, SIGNAL(valueChanged(int)), this, SLOT(boxfilterValueChanged()));
	connect(ui.doublespinbox_boxfilter_zmin, SIGNAL(valueChanged(double)), this, SLOT(boxfilterValueChanged()));
	connect(ui.doublespinbox_boxfilter_zmax, SIGNAL(valueChanged(double)), this, SLOT(boxfilterValueChanged()));

	connect(ui.button_driver_start_pause, SIGNAL(clicked()), this, SLOT(driverStartPauseButtonPressed()));
	connect(ui.button_driver_reset, SIGNAL(clicked()), this, SLOT(driverResetButtonPressed()));
	connect(ui.button_driver_settings, SIGNAL(clicked()), this, SLOT(driverSettingsButtonPressed()));
	connect(ui.button_driver_savedataset, SIGNAL(clicked()), this, SLOT(driverSaveDatasetButtonPressed()));

	connect(ui.button_tracker_start_pause, SIGNAL(clicked()), this, SLOT(trackerStartPauseButtonPressed()));
	connect(ui.button_tracker_reset, SIGNAL(clicked()), this, SLOT(trackerResetButtonPressed()));
	connect(ui.button_tracker_settings, SIGNAL(clicked()), this, SLOT(trackerSettingsButtonPressed()));
	connect(ui.button_tracker_savetrajectory, SIGNAL(clicked()), this, SLOT(trackerSaveTrajectoryButtonPressed()));

	connect(ui.button_mapper_start_pause, SIGNAL(clicked()), this, SLOT(mapperStartPauseButtonPressed()));
	connect(ui.button_mapper_reset, SIGNAL(clicked()), this, SLOT(mapperResetButtonPressed()));
	connect(ui.button_mapper_settings, SIGNAL(clicked()), this, SLOT(mapperSettingsButtonPressed()));
	connect(ui.button_mapper_savemesh, SIGNAL(clicked()), this, SLOT(mapperSaveMeshButtonPressed()));

	connect(ui.radiobutton_tracker_processall, SIGNAL(toggled(bool)), this, SLOT(trackerProcessAllToggled(bool)));
	connect(ui.radiobutton_tracker_processrecent, SIGNAL(toggled(bool)), this, SLOT(trackerProcessRecentToggled(bool)));
	connect(ui.radiobutton_mapper_processall, SIGNAL(toggled(bool)), this, SLOT(mapperProcessAllToggled(bool)));
	connect(ui.radiobutton_mapper_processrecent, SIGNAL(toggled(bool)), this, SLOT(mapperProcessRecentToggled(bool)));
}

racoon::rvdslam::MainWindow::~MainWindow()
{

}

void racoon::rvdslam::MainWindow::viewDriverFrameColor(cv::Mat frame) const
{
	cv::resize(frame, frame, cv::Size(320, 240), 0, 0, cv::INTER_LINEAR);
	cv::cvtColor(frame, frame, CV_BGR2RGB);

	QImage imdisplay((uchar*)frame.data, frame.cols, frame.rows, frame.step, QImage::Format_RGB888);

	ui.label_driver_image_rgb->setPixmap(QPixmap::fromImage(imdisplay));
}

void racoon::rvdslam::MainWindow::viewDriverFrameDepth(cv::Mat frame) const
{
	cv::resize(frame, frame, cv::Size(320, 240), 0, 0, cv::INTER_LINEAR);
	frame.convertTo(frame, CV_8UC1, 1.0 / 256.0);

//	QImage imdisplay((uchar*)frame.data, frame.cols, frame.rows, frame.step, QImage::Format_Grayscale8);
	QImage imdisplay((uchar*)frame.data, frame.cols, frame.rows, frame.step, QImage::Format_Indexed8);

	QVector<QRgb> my_table;

	for (int i = 0; i < 256; i++)
		my_table.push_back(qRgb(i,i,i));

	imdisplay.setColorTable(my_table);

	ui.label_driver_image_depth->setPixmap(QPixmap::fromImage(imdisplay));
}

void racoon::rvdslam::MainWindow::viewTrackerFrameColor(cv::Mat frame) const
{
	cv::resize(frame, frame, cv::Size(320, 240), 0, 0, cv::INTER_LINEAR);
	cv::cvtColor(frame, frame, CV_BGR2RGB);

	QImage imdisplay((uchar*)frame.data, frame.cols, frame.rows, frame.step, QImage::Format_RGB888);

	ui.label_tracker_image_rgb->setPixmap(QPixmap::fromImage(imdisplay));
}

void racoon::rvdslam::MainWindow::viewTrackerFrameDepth(cv::Mat frame) const
{
	cv::resize(frame, frame, cv::Size(320, 240), 0, 0, cv::INTER_LINEAR);
	frame.convertTo(frame, CV_8UC1, 1.0 / 256.0);

//	QImage imdisplay((uchar*)frame.data, frame.cols, frame.rows, frame.step, QImage::Format_Grayscale8);
	QImage imdisplay((uchar*)frame.data, frame.cols, frame.rows, frame.step, QImage::Format_Indexed8);

	QVector<QRgb> my_table;

	for (int i = 0; i < 256; i++)
		my_table.push_back(qRgb(i,i,i));

	imdisplay.setColorTable(my_table);

	ui.label_tracker_image_depth->setPixmap(QPixmap::fromImage(imdisplay));
}

void racoon::rvdslam::MainWindow::viewMapperFrameColor(cv::Mat frame) const
{
	cv::resize(frame, frame, cv::Size(320, 240), 0, 0, cv::INTER_LINEAR);
	cv::cvtColor(frame, frame, CV_BGR2RGB);

	QImage imdisplay((uchar*)frame.data, frame.cols, frame.rows, frame.step, QImage::Format_RGB888);

	ui.label_mapper_image_rgb->setPixmap(QPixmap::fromImage(imdisplay));
}

void racoon::rvdslam::MainWindow::viewMapperFrameDepth(cv::Mat frame) const
{
	cv::resize(frame, frame, cv::Size(320, 240), 0, 0, cv::INTER_LINEAR);
	frame.convertTo(frame, CV_8UC1, 1.0 / 256.0);

//	QImage imdisplay((uchar*)frame.data, frame.cols, frame.rows, frame.step, QImage::Format_Grayscale8);
	QImage imdisplay((uchar*)frame.data, frame.cols, frame.rows, frame.step, QImage::Format_Indexed8);

	QVector<QRgb> my_table;

	for (int i = 0; i < 256; i++)
		my_table.push_back(qRgb(i,i,i));

	imdisplay.setColorTable(my_table);

	ui.label_mapper_image_depth->setPixmap(QPixmap::fromImage(imdisplay));
}

void racoon::rvdslam::MainWindow::setTrackerQueueSize(unsigned int queue_size)
{
	ui.progressbar_tracker_queue->setValue(queue_size);
	ui.lcdnumber_tracker_queue->display((int)queue_size);
}

void racoon::rvdslam::MainWindow::setMapperQueueSize(unsigned int queue_size)
{
	ui.progressbar_mapper_queue->setValue(queue_size);
	ui.lcdnumber_mapper_queue->display((int)queue_size);
}

void racoon::rvdslam::MainWindow::setDriverState(GenericWrapper::State driver_state)
{
	switch (driver_state)
	{
	case (GenericWrapper::State::INIT):
		ui.button_driver_start_pause->setEnabled(false);
		ui.button_driver_start_pause->setText("Start Driver");
		ui.label_driver_state->setText("INIT");
		ui.label_driver_image_rgb->clear();
		ui.label_driver_image_depth->clear();
		ui.lcdnumber_driver_fps->display(0);
	case (GenericWrapper::State::STOPPED):
		ui.button_driver_start_pause->setEnabled(true);
		ui.button_driver_start_pause->setText("Start Driver");
		ui.label_driver_state->setText("STOPPED");
		ui.lcdnumber_driver_fps->display(0);
		break;
	case (GenericWrapper::State::RUNNING):
		ui.button_driver_start_pause->setText("Pause Driver");
		ui.label_driver_state->setText("RUNNING");
		break;
	case (GenericWrapper::State::IDLE):
		ui.button_driver_start_pause->setText("Pause Driver");
		ui.label_driver_state->setText("IDLE");
		break;
	case (GenericWrapper::State::PAUSED):
		ui.button_driver_start_pause->setText("Resume Driver");
		ui.label_driver_state->setText("PAUSED");
		ui.lcdnumber_driver_fps->display(0);
		break;
	case (GenericWrapper::State::RESET):
		ui.button_driver_start_pause->setEnabled(false);
		ui.label_driver_state->setText("RESET");
		ui.label_driver_image_rgb->clear();
		ui.label_driver_image_depth->clear();
		ui.lcdnumber_driver_fps->display(0);
		break;
	case (GenericWrapper::State::_ERROR_):
		ui.button_driver_start_pause->setEnabled(false);
		ui.label_driver_state->setText("ERROR");
		ui.lcdnumber_driver_fps->display(0);
		break;
	}
}
	
void racoon::rvdslam::MainWindow::setTrackerState(GenericWrapper::State tracker_state)
{
	switch (tracker_state)
	{
	case (GenericWrapper::State::INIT):
		ui.button_tracker_start_pause->setEnabled(false);
		ui.button_tracker_start_pause->setText("Start Tracker");
		ui.label_tracker_state->setText("INIT");
		ui.label_tracker_image_rgb->clear();
		ui.label_tracker_image_depth->clear();
		ui.lcdnumber_tracker_fps->display(0);
	case (GenericWrapper::State::STOPPED):
		ui.button_tracker_start_pause->setEnabled(true);
		ui.button_tracker_start_pause->setText("Start Tracker");
		ui.label_tracker_state->setText("STOPPED");
		ui.lcdnumber_tracker_fps->display(0);
		break;
	case (GenericWrapper::State::RUNNING):
		ui.button_tracker_start_pause->setText("Pause Tracker");
		ui.label_tracker_state->setText("RUNNING");
		break;
	case (GenericWrapper::State::IDLE):
		ui.button_tracker_start_pause->setText("Pause Tracker");
		ui.label_tracker_state->setText("IDLE");
		break;
	case (GenericWrapper::State::PAUSED):
		ui.button_tracker_start_pause->setText("Resume Tracker");
		ui.label_tracker_state->setText("PAUSED");
		ui.lcdnumber_tracker_fps->display(0);
		break;
	case (GenericWrapper::State::RESET):
		ui.button_tracker_start_pause->setEnabled(false);
		ui.label_tracker_state->setText("RESET");
		ui.label_tracker_image_rgb->clear();
		ui.label_tracker_image_depth->clear();
		ui.lcdnumber_tracker_fps->display(0);
		break;
	case (GenericWrapper::State::_ERROR_):
		ui.button_tracker_start_pause->setEnabled(false);
		ui.label_tracker_state->setText("ERROR");
		ui.lcdnumber_tracker_fps->display(0);
		break;
	}
}

void racoon::rvdslam::MainWindow::setMapperState(GenericWrapper::State mapper_state)
{
	switch (mapper_state)
	{
	case (GenericWrapper::State::INIT):
		ui.button_mapper_start_pause->setEnabled(false);
		ui.button_mapper_start_pause->setText("Start Mapper");
		ui.label_mapper_state->setText("INIT");
		ui.label_mapper_image_rgb->clear();
		ui.label_mapper_image_depth->clear();
		ui.lcdnumber_mapper_fps->display(0);
	case (GenericWrapper::State::STOPPED):
		ui.button_mapper_start_pause->setEnabled(true);
		ui.button_mapper_start_pause->setText("Start Mapper");
		ui.label_mapper_state->setText("STOPPED");
		ui.lcdnumber_mapper_fps->display(0);
		break;
	case (GenericWrapper::State::RUNNING):
		ui.button_mapper_start_pause->setText("Pause Mapper");
		ui.label_mapper_state->setText("RUNNING");
		break;
	case (GenericWrapper::State::IDLE):
		ui.button_mapper_start_pause->setText("Pause Mapper");
		ui.label_mapper_state->setText("IDLE");
		break;
	case (GenericWrapper::State::PAUSED):
		ui.button_mapper_start_pause->setText("Resume Mapper");
		ui.label_mapper_state->setText("PAUSED");
		ui.lcdnumber_mapper_fps->display(0);
		break;
	case (GenericWrapper::State::RESET):
		ui.button_mapper_start_pause->setEnabled(false);
		ui.label_mapper_state->setText("RESET");
		ui.label_mapper_image_rgb->clear();
		ui.label_mapper_image_depth->clear();
		ui.lcdnumber_mapper_fps->display(0);
		break;
	case (GenericWrapper::State::_ERROR_):
		ui.button_mapper_start_pause->setEnabled(false);
		ui.label_mapper_state->setText("ERROR");
		ui.lcdnumber_mapper_fps->display(0);
		break;
	}
}

void racoon::rvdslam::MainWindow::driverChanged(int index)
{
	switch (index)
	{
	case 0:
		view_manager.switchDriver(DriverManager::DriverType::RGBD_FILE_READER);
		ui.button_driver_settings->setEnabled(true);
		break;
	case 1:
		view_manager.switchDriver(DriverManager::DriverType::RGBD_VIRTUAL_CAMERA);
		ui.button_driver_settings->setEnabled(true);
		break;
	case 2:
		view_manager.switchDriver(DriverManager::DriverType::KINECT_V1);
		ui.button_driver_settings->setEnabled(false);
		break;
	case 3:
		view_manager.switchDriver(DriverManager::DriverType::KINECT_V2);
		ui.button_driver_settings->setEnabled(true);
		break;
	case 4:
		view_manager.switchDriver(DriverManager::DriverType::BUMBLEBEE);
		ui.button_driver_settings->setEnabled(false);
		break;
	case 5:
		view_manager.switchDriver(DriverManager::DriverType::ZED);
		ui.button_driver_settings->setEnabled(false);
		break;
	}
}

void racoon::rvdslam::MainWindow::driverStartPauseButtonPressed()
{
	GenericWrapper::State driver_state = view_manager.getDriverState();

	switch (driver_state)
	{
	case (GenericWrapper::State::STOPPED):
		view_manager.startDriver();
		break;
	case (GenericWrapper::State::RUNNING):
	case (GenericWrapper::State::IDLE):
		view_manager.pauseDriver();
		break;
	case (GenericWrapper::State::PAUSED):
		view_manager.resumeDriver();
		break;
	}
}

void racoon::rvdslam::MainWindow::driverResetButtonPressed()
{
	view_manager.resetDriver();
}

void racoon::rvdslam::MainWindow::driverSettingsButtonPressed()
{
	int current_driver = ui.combobox_driver_select->currentIndex();

	switch (current_driver)
	{
	case 0:
		DriverSettingsDialogRGBDFileReader(view_manager, this).exec();
		break;
	case 1:
		DriverSettingsDialogRGBDVirtualCamera(view_manager, this).exec();
		break;
	case 3:
		DriverSettingsDialogKinectV2(view_manager, this).exec();
		break;
	}
}

void racoon::rvdslam::MainWindow::driverSaveDatasetButtonPressed()
{
	QString dir = QFileDialog::getExistingDirectory(this, "Choose directory to save in", "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	
	driver_saving_progress_dialog.show();

	view_manager.saveCapturedDataset(dir.toStdString());

	driver_saving_progress_dialog.close();
}

void racoon::rvdslam::MainWindow::boxfilterStateChanged(int state)
{
	if (state == Qt::Checked)
	{
		ui.spinbox_boxfilter_umin->setEnabled(true);
		ui.spinbox_boxfilter_umax->setEnabled(true);
		ui.spinbox_boxfilter_vmin->setEnabled(true);
		ui.spinbox_boxfilter_vmax->setEnabled(true);
		ui.doublespinbox_boxfilter_zmin->setEnabled(true);
		ui.doublespinbox_boxfilter_zmax->setEnabled(true);

		unsigned int umin = ui.spinbox_boxfilter_umin->value();
		unsigned int umax = ui.spinbox_boxfilter_umax->value();
		unsigned int vmin = ui.spinbox_boxfilter_vmin->value();
		unsigned int vmax = ui.spinbox_boxfilter_vmax->value();
		float zmin = ui.doublespinbox_boxfilter_zmin->value();
		float zmax = ui.doublespinbox_boxfilter_zmax->value();

		auto filter = std::shared_ptr<FramePackage::BoxFilter>(new FramePackage::BoxFilter(umin, umax, vmin, vmax, zmin, zmax));

		view_manager.setFramePackageFilter(filter);
	}
	else
	{
		ui.spinbox_boxfilter_umin->setEnabled(false);
		ui.spinbox_boxfilter_umax->setEnabled(false);
		ui.spinbox_boxfilter_vmin->setEnabled(false);
		ui.spinbox_boxfilter_vmax->setEnabled(false);
		ui.doublespinbox_boxfilter_zmin->setEnabled(false);
		ui.doublespinbox_boxfilter_zmax->setEnabled(false);

		view_manager.removeFramePackageFilter();
	}
}

void racoon::rvdslam::MainWindow::boxfilterValueChanged()
{
	unsigned int umin = ui.spinbox_boxfilter_umin->value();
	unsigned int umax = ui.spinbox_boxfilter_umax->value();
	unsigned int vmin = ui.spinbox_boxfilter_vmin->value();
	unsigned int vmax = ui.spinbox_boxfilter_vmax->value();
	float zmin = ui.doublespinbox_boxfilter_zmin->value();
	float zmax = ui.doublespinbox_boxfilter_zmax->value();

	auto filter = std::shared_ptr<FramePackage::BoxFilter>(new FramePackage::BoxFilter(umin, umax, vmin, vmax, zmin, zmax));

	view_manager.setFramePackageFilter(filter);
}


void racoon::rvdslam::MainWindow::trackerChanged(int index)
{
	switch (index)
	{
	case 0:
		view_manager.switchTracker(SLAMPipeline::TrackerType::DIFODO);
		ui.button_tracker_settings->setEnabled(true);
		break;
	}
}

void racoon::rvdslam::MainWindow::trackerStartPauseButtonPressed()
{
	GenericWrapper::State tracker_state = view_manager.getTrackerState();

	switch (tracker_state)
	{
	case (GenericWrapper::State::STOPPED):
		view_manager.startTracker();
		break;
	case (GenericWrapper::State::RUNNING):
	case (GenericWrapper::State::IDLE):
		view_manager.pauseTracker();
		break;
	case (GenericWrapper::State::PAUSED):
		view_manager.resumeTracker();
		break;
	}
}

void racoon::rvdslam::MainWindow::trackerResetButtonPressed()
{
	view_manager.resetTracker();
	ui.button_tracker_start_pause->setText("Start Tracker");

	ui.label_tracker_image_rgb->clear();
	ui.label_tracker_image_depth->clear();
}

void racoon::rvdslam::MainWindow::trackerSettingsButtonPressed()
{
	int current_tracker = ui.combobox_tracker_select->currentIndex();

	switch (current_tracker)
	{
	case 0:
		TrackerSettingsDialogDIFODO dialog(view_manager, this);
		dialog.exec();
	}
}

void racoon::rvdslam::MainWindow::trackerSaveTrajectoryButtonPressed()
{
	QString filepath = QFileDialog::getSaveFileName(this, "Save trajectory to file", "", "Text files (*.txt)");

	view_manager.saveEstimatedTrajectory(filepath.toStdString());
}

void racoon::rvdslam::MainWindow::trackerProcessAllToggled(bool checked)
{
	if (checked)
		view_manager.setTrackerProcessAll(true);
	else
		view_manager.setTrackerProcessAll(false);
}

void racoon::rvdslam::MainWindow::trackerProcessRecentToggled(bool checked)
{
	if (checked)
		view_manager.setTrackerProcessAll(false);
	else
		view_manager.setTrackerProcessAll(true);
}

void racoon::rvdslam::MainWindow::mapperChanged(int index)
{
	switch (index)
	{
	case 0:
		view_manager.switchMapper(SLAMPipeline::MapperType::FASTFUSION);
		ui.button_mapper_settings->setEnabled(true);
		break;
	}
}

void racoon::rvdslam::MainWindow::mapperStartPauseButtonPressed()
{
	GenericWrapper::State mapper_state = view_manager.getMapperState();

	switch (mapper_state)
	{
	case (GenericWrapper::State::STOPPED):
		view_manager.startMapper();
		break;
	case (GenericWrapper::State::RUNNING):
	case (GenericWrapper::State::IDLE):
		view_manager.pauseMapper();
		break;
	case (GenericWrapper::State::PAUSED):
		view_manager.resumeMapper();
		break;
	}
}

void racoon::rvdslam::MainWindow::mapperResetButtonPressed()
{
	view_manager.resetMapper();
	ui.button_mapper_start_pause->setText("Start Mapper");

	ui.label_mapper_image_rgb->clear();
	ui.label_mapper_image_depth->clear();
}

void racoon::rvdslam::MainWindow::mapperSettingsButtonPressed()
{
	int current_mapper = ui.combobox_mapper_select->currentIndex();

	switch (current_mapper)
	{
	case 0:
		MapperSettingsDialogFastFusion dialog(view_manager, this);
		dialog.exec();
		break;
	}
}

void racoon::rvdslam::MainWindow::mapperSaveMeshButtonPressed()
{
	QString filepath = QFileDialog::getSaveFileName(this, "Save mesh to file", "", "PLY (*.ply);; STL (*.stl);; VTK (*.vtk)");

	view_manager.saveMesh(filepath.toStdString());
}

void racoon::rvdslam::MainWindow::mapperProcessAllToggled(bool checked)
{
	if (checked)
		view_manager.setMapperProcessAll(true);
	else
		view_manager.setMapperProcessAll(false);
}

void racoon::rvdslam::MainWindow::mapperProcessRecentToggled(bool checked)
{
	if (checked)
		view_manager.setMapperProcessAll(false);
	else
		view_manager.setMapperProcessAll(true);
}

void racoon::rvdslam::MainWindow::setDriverFps(const double fps)
{
	ui.lcdnumber_driver_fps->display((int)fps);
}

void racoon::rvdslam::MainWindow::setTrackerFps(const double fps)
{
	ui.lcdnumber_tracker_fps->display((int)fps);
}

void racoon::rvdslam::MainWindow::setMapperFps(const double fps)
{
	ui.lcdnumber_mapper_fps->display((int)fps);
}

void racoon::rvdslam::MainWindow::setDriverSavingProgress(int percent)
{
	driver_saving_progress_dialog.setProgress(percent);
}