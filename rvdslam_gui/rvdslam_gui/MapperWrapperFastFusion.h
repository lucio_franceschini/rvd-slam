#pragma once

#include "MapperWrapper.h"

#include <fusion/geometryfusion_mipmap_cpu.hpp>

#include <limits>

namespace racoon
{
	namespace rvdslam
	{
		class FramePackage;

		/**
	     * Mapper module for the FastFusion mapper. Implements the MapperWrapper interface.
		 */
		class MapperWrapperFastFusion : public MapperWrapper
		{
		public:
			/** Settings struct for the FastFusion mapper module */
			struct FastFusionSettings : public Settings
			{
				/**
				* Struct constructor
				*
				* @param voxel_scale - side length of the voxels of the truncated signed distance function
				* @param max_depth - maximum distance for a point in the input depth image to still be taken into account
				*/
				FastFusionSettings(float voxel_scale = 0.005, float max_depth = 5.0);

				float voxel_scale;
				float max_depth;
			};

			/** Default constructor */
			MapperWrapperFastFusion(FastFusionSettings settings = FastFusionSettings());

			/** Default destructor */
			~MapperWrapperFastFusion();

			/** Returns a pointer to the current settings struct */
			std::unique_ptr<Settings> getSettings() const override final;
		private:
			FastFusionSettings settings;

			FusionMipMapCPU * fast_fusion;	// Normally, an object variable would be preferred. This is a workaround to avoid calling the destructor, which causes errors.

			/** Initializes mapper module */
			bool doInit() override final;

			/** Resets mapper module */
			bool doReset() override final;

			/** Sets settings and applies them by resetting module */
			void setSettings(const Settings & settings) override final;

			/** Calls the addMap() and updateMeshes() methods of FastFusion to incorporate the given frame package into the internal map */
			void updateMap(const FramePackage & frame_package) override final;

			/** Obtains a mesh from FastFusion and converts it into the PolygonMesh data type */
			std::shared_ptr<pcl::PolygonMesh> getMesh() override final;

			/** Not implemented yet. Currently returns an empty point cloud. */
			std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> getPointCloud() override final;

			/** Creates a CameraInfo object from estimated pose and camera intrinsics for use in updateMap() */
			CameraInfo createCameraInfo(const FramePackage & frame_package, const Eigen::Affine3d & pose);
		};
	}
}