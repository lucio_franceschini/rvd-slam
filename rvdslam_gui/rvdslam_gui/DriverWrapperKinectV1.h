#pragma once

#include "DriverWrapper.h"

#include <NuiApi.h>

namespace racoon
{
	namespace rvdslam
	{
		class FramePackage;

		/**
	     * Device driver module for the Kinect v1 sensor. Implements the DriverWrapper interface.
		 */
		class DriverWrapperKinectV1 : public DriverWrapper
		{
		public:
			/** Default constructor */
			DriverWrapperKinectV1();

			/** Default destructor */
			~DriverWrapperKinectV1();

		private:
			INuiSensor* sensor;				// The kinect sensor
			HANDLE rgbStream;				// The identifier of the Kinect's RGB Camera
			HANDLE depthStream;				// The identifier of the Kinect's depth Camera

			unsigned char* cframe;			// color frame raw data
			unsigned short* dframe;			// depth frame raw data

			unsigned int ctimestamp;		// color frame timestamp
			unsigned int dtimestamp;		// depth frame timestamp

			bool error;

			/** Initializes device driver module. Returns true if initialization successful, false otherwise. */
			bool doInit() override final;

			/** Resets device driver module. Returns true if reset successful, false otherwise. */
			bool doReset() override final;

			/** Captures a new frame package. Returns true if capture successful, false otherwise. */
			bool getFramePackage(std::shared_ptr<FramePackage>& frame_package) override final;
			
			/** Internal method for initialization */
			bool initKinect();

			/** Internal method for capturing color frame */
			bool acquireKinectColorData();

			/** Internal method for capturing depth frame */
			bool acquireKinectDepthData();
		};
	}
}