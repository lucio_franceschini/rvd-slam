#include "stdafx.h"

#include "ViewManager.h"
#include "DriverManager.h"
#include "SLAMPipeline.h"
#include "DataManager.h"

#include "FramePackage.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

racoon::rvdslam::ViewManager::ViewManager(RVDSLAM & rvdslam) : main_window(*this), vis_window(*this, &main_window), driver_manager(NULL)
{
	main_window.show();
	vis_window.show();
}

racoon::rvdslam::ViewManager::~ViewManager()
{
}

void racoon::rvdslam::ViewManager::setDriverManager(DriverManager * driver_manager)
{
	this->driver_manager = driver_manager;
}

void racoon::rvdslam::ViewManager::setPipeline(SLAMPipeline * slam_pipeline)
{
	this->slam_pipeline = slam_pipeline;
}

void racoon::rvdslam::ViewManager::setDataManager(DataManager * data_manager)
{
	this->data_manager = data_manager;
}

void racoon::rvdslam::ViewManager::viewFrame(cv::Mat frame, const std::string window_title) const
{
	if (frame.type() == CV_16UC1)
	{
		cv::applyColorMap(frame, frame, cv::COLORMAP_AUTUMN);
	}

	cv::imshow(window_title, frame);
	cv::waitKey(1);
}

void racoon::rvdslam::ViewManager::viewDriverFrames(std::shared_ptr<const FramePackage> frame_package) const
{
	main_window.viewDriverFrameColor(frame_package->getColorImage());
	main_window.viewDriverFrameDepth(frame_package->getDepthImage());

	//viewFrame(frame_package->getColorImage(), "Input color image");
	//viewFrame(frame_package->getDepthImage(), "Input depth image");
}

void racoon::rvdslam::ViewManager::viewTrackerFrames(std::shared_ptr<const FramePackage> frame_package) const
{
	main_window.viewTrackerFrameColor(frame_package->getColorImage());
	main_window.viewTrackerFrameDepth(frame_package->getDepthImage());
}

void racoon::rvdslam::ViewManager::viewMapperFrames(std::shared_ptr<const FramePackage> frame_package) const
{
	main_window.viewMapperFrameColor(frame_package->getColorImage());
	main_window.viewMapperFrameDepth(frame_package->getDepthImage());
}

void racoon::rvdslam::ViewManager::updatePointCloud(std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloud, const Eigen::Affine3f & pose)
{
	vis_window.updatePointCloud(cloud, pose);
}

void racoon::rvdslam::ViewManager::updateMesh(std::shared_ptr<pcl::PolygonMesh> mesh)
{
	vis_window.updateMesh(mesh);
}

void racoon::rvdslam::ViewManager::updateCameraPose(const Eigen::Affine3f & pose)
{
	vis_window.updateCameraPose(pose);
}

void racoon::rvdslam::ViewManager::updateGroundtruthPose(const Eigen::Affine3f & pose)
{
	vis_window.updateGroundtruthPose(pose);
}

void racoon::rvdslam::ViewManager::switchDriver(DriverManager::DriverType driver_type)
{
	driver_manager->switchDriver(driver_type);
}

void racoon::rvdslam::ViewManager::startDriver()
{
	driver_manager->startDriver();
}

void racoon::rvdslam::ViewManager::pauseDriver()
{
	driver_manager->pauseDriver();
}

void racoon::rvdslam::ViewManager::resumeDriver()
{
	driver_manager->resumeDriver();
}

void racoon::rvdslam::ViewManager::resetDriver()
{
	driver_manager->resetDriver();
}

racoon::rvdslam::GenericWrapper::State racoon::rvdslam::ViewManager::getDriverState()
{
	return driver_manager->getDriverState();
}

void racoon::rvdslam::ViewManager::setDriverState(GenericWrapper::State driver_state)
{
	main_window.setDriverState(driver_state);
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::ViewManager::getDriverSettings()
{
	return std::move(driver_manager->getDriverSettings());
}

void racoon::rvdslam::ViewManager::setDriverSettings(std::shared_ptr<const racoon::rvdslam::GenericWrapper::Settings> settings)
{
	driver_manager->setDriverSettings(settings);
}

void racoon::rvdslam::ViewManager::setFramePackageFilter(std::shared_ptr<FramePackage::Filter> filter)
{
	driver_manager->setFramePackageFilter(filter);
}

void racoon::rvdslam::ViewManager::removeFramePackageFilter()
{
	driver_manager->removeFramePackageFilter();
}

void racoon::rvdslam::ViewManager::switchTracker(SLAMPipeline::TrackerType tracker_type)
{
	slam_pipeline->switchTracker(tracker_type);
}

void racoon::rvdslam::ViewManager::startTracker()
{
	slam_pipeline->startTracker();
	vis_window.show();
}

void racoon::rvdslam::ViewManager::pauseTracker()
{
	slam_pipeline->pauseTracker();
}

void racoon::rvdslam::ViewManager::resumeTracker()
{
	slam_pipeline->resumeTracker();
}

void racoon::rvdslam::ViewManager::resetTracker()
{
	slam_pipeline->resetTracker();
	vis_window.removePoses();
}

racoon::rvdslam::GenericWrapper::State racoon::rvdslam::ViewManager::getTrackerState()
{
	return slam_pipeline->getTrackerState();
}

void racoon::rvdslam::ViewManager::setTrackerState(GenericWrapper::State tracker_state)
{
	main_window.setTrackerState(tracker_state);
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::ViewManager::getTrackerSettings()
{
	return std::move(slam_pipeline->getTrackerSettings());
}

void racoon::rvdslam::ViewManager::setTrackerSettings(std::shared_ptr<const racoon::rvdslam::GenericWrapper::Settings> settings)
{
	slam_pipeline->setTrackerSettings(settings);
}

void racoon::rvdslam::ViewManager::setTrackerQueueSize(unsigned int queue_size)
{
	main_window.setTrackerQueueSize(queue_size);
}

void racoon::rvdslam::ViewManager::setTrackerProcessAll(bool process_all)
{
	slam_pipeline->setTrackerProcessAll(process_all);
}

void racoon::rvdslam::ViewManager::switchMapper(SLAMPipeline::MapperType mapper_type)
{
	slam_pipeline->switchMapper(mapper_type);
}

void racoon::rvdslam::ViewManager::startMapper()
{
	slam_pipeline->startMapper();
	vis_window.show();
}

void racoon::rvdslam::ViewManager::pauseMapper()
{
	slam_pipeline->pauseMapper();
}

void racoon::rvdslam::ViewManager::resumeMapper()
{
	slam_pipeline->resumeMapper();
}

void racoon::rvdslam::ViewManager::resetMapper()
{
	slam_pipeline->resetMapper();
	vis_window.removeMesh();
}

racoon::rvdslam::GenericWrapper::State racoon::rvdslam::ViewManager::getMapperState()
{
	return slam_pipeline->getMapperState();
}

void racoon::rvdslam::ViewManager::setMapperState(GenericWrapper::State mapper_state)
{
	main_window.setMapperState(mapper_state);
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::ViewManager::getMapperSettings()
{
	return std::move(slam_pipeline->getMapperSettings());
}

void racoon::rvdslam::ViewManager::setMapperSettings(std::shared_ptr<const racoon::rvdslam::GenericWrapper::Settings> settings)
{
	slam_pipeline->setMapperSettings(settings);
}

void racoon::rvdslam::ViewManager::setMapperQueueSize(unsigned int queue_size)
{
	main_window.setMapperQueueSize(queue_size);
}

void racoon::rvdslam::ViewManager::setMapperProcessAll(bool process_all)
{
	slam_pipeline->setMapperProcessAll(process_all);
}

void racoon::rvdslam::ViewManager::saveCapturedDataset(const std::string dirpath) const
{
	data_manager->saveCapturedDataset(dirpath);
}

void racoon::rvdslam::ViewManager::saveEstimatedTrajectory(const std::string filepath) const
{
	data_manager->saveEstimatedTrajectory(filepath);
}

void racoon::rvdslam::ViewManager::saveMesh(const std::string filepath) const
{
	data_manager->saveMesh(filepath);
}

void racoon::rvdslam::ViewManager::setDriverFps(const double fps)
{
	main_window.setDriverFps(fps);
}

void racoon::rvdslam::ViewManager::setTrackerFps(const double fps)
{
	main_window.setTrackerFps(fps);
}

void racoon::rvdslam::ViewManager::setMapperFps(const double fps)
{
	main_window.setMapperFps(fps);
}

void racoon::rvdslam::ViewManager::setDriverSavingProgress(int percent)
{
	main_window.setDriverSavingProgress(percent);
}