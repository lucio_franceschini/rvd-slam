#pragma once

#include "FramePackage.h"

#include "CameraFrameColor.h"
#include "CameraFrameDepth.h"

#include <memory>

namespace racoon
{
	namespace rvdslam
	{
		/**
		 * FramePackage implementation for an RGBD sensor.
		 *
		 * Contains sensor data in form of a CameraFrameColor and a CameraFrameDepth object.
		 */
		class FramePackageRGBD : public FramePackage
		{
		public:
			/** Default constructor */
			FramePackageRGBD();

			/** Constructor with CameraFrameColor and CameraFrameDepth parameters) */
			FramePackageRGBD(CameraFrameColor color_frame, CameraFrameDepth depth_frame);

			/** Constructor with additional ground truth parameter */
			FramePackageRGBD(CameraFrameColor color_frame, CameraFrameDepth depth_frame, const Groundtruth groundtruth);

			/** Returns the timestamp of the depth frame as the common timestamp */
			double getTimestampSecs() const;

			/** Returns the depth image scale factor. Dividing a depth value from the depth image by the scale factor yields the distance in meters: depth [m] = depth [raw] / depth_scale */
			unsigned int getDepthImageScale() const;

			/** Returns the depth frame width as the common width in px */
			unsigned int getImageWidth() const;

			/** Returns the depth frame height as the common height in px */
			unsigned int getImageHeight() const;

			/** Return the depth frame intrinsics as the common intrinsic parameters */
			Intrinsics getImageIntrinsics() const;
		private:
			CameraFrameColor color_frame;
			CameraFrameDepth depth_frame;

			/** Returns the color_frame image (format CV_8UC3, channel order BGR) */
			cv::Mat getColorImageUnfiltered() const;

			/** Returns the depth_frame image (format CV_16U) */
			cv::Mat getDepthImageUnfiltered() const;
		};
	}
}