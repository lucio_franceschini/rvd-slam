#include "stdafx.h"

#include "FramePackage.h"

#include <Eigen\src\Geometry\Quaternion.h>

racoon::rvdslam::FramePackage::FramePackage() : groundtruth(), groundtruth_set(false), filter()
{
}

racoon::rvdslam::FramePackage::FramePackage(const Groundtruth groundtruth) : groundtruth(groundtruth), groundtruth_set(true)
{
}

racoon::rvdslam::FramePackage::~FramePackage()
{
}

racoon::rvdslam::FramePackage::Groundtruth racoon::rvdslam::FramePackage::getGroundtruth() const
{
	return groundtruth;
}

bool racoon::rvdslam::FramePackage::isGroundtruthSet() const
{
	return groundtruth_set;
}

Eigen::Affine3d racoon::rvdslam::FramePackage::getEstimatedPose() const
{
	return estimated_pose;
}

void racoon::rvdslam::FramePackage::setEstimatedPose(Eigen::Affine3d estimated_pose)
{
	this->estimated_pose = estimated_pose;
}

void racoon::rvdslam::FramePackage::setFilter(std::shared_ptr<Filter> filter)
{
	this->filter = filter;
}

pcl::PointCloud<pcl::PointXYZRGB> racoon::rvdslam::FramePackage::getPointCloud() const
{
	pcl::PointCloud<pcl::PointXYZRGB> cloud;
	cloud.is_dense = true;
	cloud.clear();

	FramePackage::Intrinsics intrinsics = getImageIntrinsics();

	const float fx = intrinsics.fx;
	const float fy = intrinsics.fy;
	const float cx = intrinsics.cx;
	const float cy = intrinsics.cy;

	cv::Mat color_image = getColorImage();
	cv::Mat depth_image = getDepthImage();

	cv::MatIterator_<cv::Vec3b> color_it = color_image.begin<cv::Vec3b>();
	cv::MatIterator_<unsigned short> depth_it = depth_image.begin<unsigned short>();

	pcl::PointXYZRGB point;

	float depth;

	for (int v = 0; v < getImageHeight(); v++)
	{
		for (int u = 0; u < getImageWidth(); u++)
		{
			depth = *depth_it / (getDepthImageScale() * 1.0);

			point.b = (*color_it)[0];
			point.g = (*color_it)[1];
			point.r = (*color_it)[2];

			point.x = (u - cx) / fx * depth;
			point.y = (v - cy) / fy * depth;
			point.z = depth;

			color_it++;
			depth_it++;

			cloud.push_back(point);
		}
	}

	cloud.height = getImageHeight();
	cloud.width = getImageWidth();

	return cloud;
}

pcl::PointCloud<pcl::PointXYZRGB> racoon::rvdslam::FramePackage::getPointCloudUnfiltered() const
{
	pcl::PointCloud<pcl::PointXYZRGB> cloud;
	cloud.is_dense = true;
	cloud.clear();

	FramePackage::Intrinsics intrinsics = getImageIntrinsics();

	const float fx = intrinsics.fx;
	const float fy = intrinsics.fy;
	const float cx = intrinsics.cx;
	const float cy = intrinsics.cy;

	cv::Mat color_image = getColorImageUnfiltered();
	cv::Mat depth_image = getDepthImageUnfiltered();

	cv::MatIterator_<cv::Vec3b> color_it = color_image.begin<cv::Vec3b>();
	cv::MatIterator_<unsigned short> depth_it = depth_image.begin<unsigned short>();

	pcl::PointXYZRGB point;

	float depth;

	for (int v = 0; v < getImageHeight(); v++)
	{
		for (int u = 0; u < getImageWidth(); u++)
		{
			depth = *depth_it / (getDepthImageScale() * 1.0);

			point.b = (*color_it)[0];
			point.g = (*color_it)[1];
			point.r = (*color_it)[2];

			point.x = (u - cx) / fx * depth;
			point.y = (v - cy) / fy * depth;
			point.z = depth;

			color_it++;
			depth_it++;

			cloud.push_back(point);
		}
	}

	cloud.height = getImageHeight();
	cloud.width = getImageWidth();

	return cloud;
}

racoon::rvdslam::FramePackage::Groundtruth::Groundtruth() : rotation(1.0, 0.0, 0.0, 0.0), translation(0.0, 0.0, 0.0)
{
}

racoon::rvdslam::FramePackage::Groundtruth::Groundtruth(double tx, double ty, double tz, double qx, double qy, double qz, double qw) : rotation(qw, qx, qy, qz), translation(tx, ty, tz)
{
}

Eigen::Affine3d racoon::rvdslam::FramePackage::Groundtruth::getPoseMatrix()
{
	Eigen::Affine3d transform;

	transform = translation * rotation;

	return transform;
}

racoon::rvdslam::FramePackage::Intrinsics::Intrinsics() : fx(0.0), fy(0.0), cx(0.0), cy(0.0)
{
}

racoon::rvdslam::FramePackage::Intrinsics::Intrinsics(float fx, float fy, float cx, float cy) : fx(fx), fy(fy), cx(cx), cy(cy)
{
}

Eigen::Matrix3f racoon::rvdslam::FramePackage::Intrinsics::getIntrinsicMatrix()
{
	return Eigen::Matrix3f((Eigen::Matrix3f() << fx, 0.0, cx, 0.0, fy, cy, 0.0, 0.0, 1.0).finished());
}

cv::Mat racoon::rvdslam::FramePackage::getColorImage() const
{
	if (filter != nullptr)
		return filter->getColorImageFiltered(*this);
	else
		return getColorImageUnfiltered();
}

cv::Mat racoon::rvdslam::FramePackage::getDepthImage() const
{
	if (filter != nullptr)
		return filter->getDepthImageFiltered(*this);
	else
		return getDepthImageUnfiltered();
}

racoon::rvdslam::FramePackage::CubeFilter::CubeFilter(float xmin, float xmax, float ymin, float ymax, float zmin, float zmax)
	: xmin(xmin), xmax(xmax), ymin(ymin), ymax(ymax), zmin(zmin), zmax(zmax)
{
}

cv::Mat racoon::rvdslam::FramePackage::CubeFilter::getColorImageFiltered(const FramePackage & frame_package) const
{
	pcl::PointCloud<pcl::PointXYZRGB> cloud = frame_package.getPointCloudUnfiltered();

	cv::Mat image = frame_package.getColorImageUnfiltered();

	for (int v = 0; v < image.rows; v++)
	{
		for (int u = 0; u < image.cols; u++)
		{
			float x = cloud.at(u,v).x;
			float y = cloud.at(u,v).y;
			float z = cloud.at(u,v).z;

			if (x < xmin || x >= xmax || y < ymin || y >= ymax || z < zmin || z >= zmax)
			{
				image.at<cv::Vec3b>(v,u)[0] = 0;
				image.at<cv::Vec3b>(v,u)[1] = 0;
				image.at<cv::Vec3b>(v,u)[2] = 0;
			}
		}
	}

	return image;
}

cv::Mat racoon::rvdslam::FramePackage::CubeFilter::getDepthImageFiltered(const FramePackage & frame_package) const
{
	pcl::PointCloud<pcl::PointXYZRGB> cloud = frame_package.getPointCloudUnfiltered();

	cv::Mat image = frame_package.getDepthImageUnfiltered();

	for (int v = 0; v < image.rows; v++)
	{
		for (int u = 0; u < image.cols; u++)
		{
			float x = cloud.at(u,v).x;
			float y = cloud.at(u,v).y;
			float z = cloud.at(u,v).z;

			if (x < xmin || x >= xmax || y < ymin || y >= ymax || z < zmin || z >= zmax)
			{
				image.at<unsigned short>(v,u) = 0;
			}
		}
	}

	return image;
}

racoon::rvdslam::FramePackage::BoxFilter::BoxFilter(unsigned int umin, unsigned int umax, unsigned int vmin, unsigned int vmax, float zmin, float zmax)
	: umin(umin), umax(umax), vmin(vmin), vmax(vmax), zmin(zmin), zmax(zmax)
{
}

cv::Mat racoon::rvdslam::FramePackage::BoxFilter::getColorImageFiltered(const FramePackage & frame_package) const
{
	cv::Mat image = frame_package.getColorImageUnfiltered();

	int umin_capped = (umin >= image.cols) ? image.cols - 1 : umin;
	int umax_capped = (umax > image.cols) ? image.cols : umax;
	int vmin_capped = (vmin >= image.rows) ? image.rows - 1 : vmin;
	int vmax_capped = (vmax >= image.rows) ? image.rows : vmax;

	if (umin_capped >= umax_capped)
		umax_capped = umin_capped + 1;

	if (vmin_capped >= vmax_capped)
		vmax_capped = vmin_capped + 1;

	cv::Mat left = image.colRange(0, umin_capped);
	cv::Mat right = image.colRange(umax_capped, image.cols);
	cv::Mat top = image.colRange(umin_capped, umax_capped).rowRange(0, vmin_capped);
	cv::Mat bottom = image.colRange(umin_capped, umax_capped).rowRange(vmax_capped, image.rows);

	left = cv::Scalar(0, 0, 0);
	right = cv::Scalar(0, 0, 0);
	top = cv::Scalar(0, 0, 0);
	bottom = cv::Scalar(0, 0, 0);

	cv::Mat depth_image = frame_package.getDepthImageUnfiltered();

	unsigned int depth_scale = frame_package.getDepthImageScale();

	for (int v = vmin; v < vmax && v < depth_image.rows; v++)
	{
		for (int u = umin; u < umax && u < depth_image.cols; u++)
		{
			float z = (depth_image.at<unsigned short>(v,u) * 1.0) / (depth_scale * 1.0);

			if (z < zmin || z >= zmax)
			{
				image.at<cv::Vec3b>(v,u)[0] = 0;
				image.at<cv::Vec3b>(v,u)[1] = 0;
				image.at<cv::Vec3b>(v,u)[2] = 0;
			}
		}
	}

	return image;
}

cv::Mat racoon::rvdslam::FramePackage::BoxFilter::getDepthImageFiltered(const FramePackage & frame_package) const
{
	cv::Mat image = frame_package.getDepthImageUnfiltered();

	int umin_capped = (umin >= image.cols) ? image.cols - 1 : umin;
	int umax_capped = (umax > image.cols) ? image.cols : umax;
	int vmin_capped = (vmin >= image.rows) ? image.rows - 1 : vmin;
	int vmax_capped = (vmax >= image.rows) ? image.rows : vmax;

	if (umin_capped >= umax_capped)
		umax_capped = umin_capped + 1;

	if (vmin_capped >= vmax_capped)
		vmax_capped = vmin_capped + 1;

	cv::Mat left = image.colRange(0, umin_capped);
	cv::Mat right = image.colRange(umax_capped, image.cols);
	cv::Mat top = image.colRange(umin_capped, umax_capped).rowRange(0, vmin_capped);
	cv::Mat bottom = image.colRange(umin_capped, umax_capped).rowRange(vmax_capped, image.rows);

	left = cv::Scalar(0);
	right = cv::Scalar(0);
	top = cv::Scalar(0);
	bottom = cv::Scalar(0);

	unsigned int depth_scale = frame_package.getDepthImageScale();

	for (int v = vmin; v < vmax && v < image.rows; v++)
	{
		for (int u = umin; u < umax && u < image.cols; u++)
		{
			float z = (image.at<unsigned short>(v,u) * 1.0) / (depth_scale * 1.0);

			if (z < zmin || z >= zmax)
			{
				image.at<unsigned short>(v,u) = 0;
			}
		}
	}

	return image;
}