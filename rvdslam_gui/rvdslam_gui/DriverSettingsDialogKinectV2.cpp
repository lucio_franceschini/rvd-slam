#include "stdafx.h"

#include "DriverSettingsDialogKinectV2.h"

#include "ViewManager.h"
#include "DriverWrapperKinectV2.h"

racoon::rvdslam::DriverSettingsDialogKinectV2::DriverSettingsDialogKinectV2(ViewManager & view_manager, QWidget *parent)
	: view_manager(view_manager), QDialog(parent)
{
	ui.setupUi(this);

	auto settings = view_manager.getDriverSettings();
	auto kinectv2_settings = dynamic_cast<DriverWrapperKinectV2::KinectV2Settings*>(settings.get());

	ui.radiobutton_cameramode_color->setChecked(!kinectv2_settings->infrared);
	ui.radiobutton_cameramode_infrared->setChecked(kinectv2_settings->infrared);
}

racoon::rvdslam::DriverSettingsDialogKinectV2::~DriverSettingsDialogKinectV2()
{
}

void racoon::rvdslam::DriverSettingsDialogKinectV2::accept()
{
	DriverWrapperKinectV2::KinectV2Settings settings;

	settings.infrared = ui.radiobutton_cameramode_infrared->isChecked();

	auto settings_ptr = std::make_shared<DriverWrapperKinectV2::KinectV2Settings>(settings);

	view_manager.setDriverSettings(settings_ptr);

	done(QDialog::Accepted);
}