#pragma once

#include "ui_MapperSettingsDialogFastFusion.h"

#include <QtWidgets/QDialog>

namespace racoon
{
	namespace rvdslam
	{
		class ViewManager;

		class MapperSettingsDialogFastFusion : public QDialog
		{
			Q_OBJECT
		public:
			/**
			 * Constructor
			 *
			 * Sets up the UI elements of the settings dialog.
			 */
			MapperSettingsDialogFastFusion(ViewManager & view_manager, QWidget *parent = 0);

			/** Destructor */
			~MapperSettingsDialogFastFusion();
		private:
			Ui::MapperSettingsDialogFastFusion ui;
			ViewManager & view_manager;
		private slots:
			/**
			 * Gets called when dialog is accepted.
			 * Creates settings struct from UI element values
			 * and requests view manager to set mapper module settings accordingly.
			 */
			void accept();
		};
	}
}