#include "stdafx.h"

#include "DriverSettingsDialogRGBDFileReader.h"

#include "ViewManager.h"
#include "DriverWrapperRGBDFileReader.h"

#include <QFileDialog>

racoon::rvdslam::DriverSettingsDialogRGBDFileReader::DriverSettingsDialogRGBDFileReader(ViewManager & view_manager, QWidget *parent)
	: view_manager(view_manager), QDialog(parent), path("")
{
	ui.setupUi(this);

	connect(ui.button_selectfolder, SIGNAL(clicked()), this, SLOT(selectFolderButtonPressed()));

	auto settings = view_manager.getDriverSettings();

	auto rgbdfilereader_settings = dynamic_cast<DriverWrapperRGBDFileReader::RGBDFileReaderSettings*>(settings.get());

	std::string label_string = "Path: ";

	path = rgbdfilereader_settings->path;

	label_string += path;

	ui.label_folder->setText(QString::fromStdString(label_string));
}

racoon::rvdslam::DriverSettingsDialogRGBDFileReader::~DriverSettingsDialogRGBDFileReader()
{
}

void racoon::rvdslam::DriverSettingsDialogRGBDFileReader::selectFolderButtonPressed()
{
	QString dir = QFileDialog::getExistingDirectory(this, "Choose directory of dataset", "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

	path = dir.toStdString();

	std::string label_string = "Path: ";
	label_string += path;

	ui.label_folder->setText(QString::fromStdString(label_string));
}

void racoon::rvdslam::DriverSettingsDialogRGBDFileReader::accept()
{
	DriverWrapperRGBDFileReader::RGBDFileReaderSettings settings;

	settings.path = path;

	auto settings_ptr = std::make_shared<DriverWrapperRGBDFileReader::RGBDFileReaderSettings>(settings);

	view_manager.setDriverSettings(settings_ptr);

	done(QDialog::Accepted);
}