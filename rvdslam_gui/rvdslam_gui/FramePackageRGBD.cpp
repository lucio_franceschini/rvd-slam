#include "stdafx.h"
#include "FramePackageRGBD.h"

#include <memory>
#include <algorithm>

racoon::rvdslam::FramePackageRGBD::FramePackageRGBD() : FramePackage(), color_frame(), depth_frame()
{
}

racoon::rvdslam::FramePackageRGBD::FramePackageRGBD(CameraFrameColor color_frame, CameraFrameDepth depth_frame) : FramePackage(), color_frame(color_frame), depth_frame(depth_frame) // to check regarding std::move usage
{
}

racoon::rvdslam::FramePackageRGBD::FramePackageRGBD(CameraFrameColor color_frame, CameraFrameDepth depth_frame, const Groundtruth groundtruth) : FramePackage(groundtruth), color_frame(color_frame), depth_frame(depth_frame) // to check regarding std::move usage
{
}

double racoon::rvdslam::FramePackageRGBD::getTimestampSecs() const
{
	return depth_frame.getTimestampSecs();
}

cv::Mat racoon::rvdslam::FramePackageRGBD::getColorImageUnfiltered() const
{
	return color_frame.getDataCVMat();
}

cv::Mat racoon::rvdslam::FramePackageRGBD::getDepthImageUnfiltered() const
{
	return depth_frame.getDataCVMat();
}

unsigned int racoon::rvdslam::FramePackageRGBD::getDepthImageScale() const
{
	return depth_frame.getDepthScale();
}

unsigned int racoon::rvdslam::FramePackageRGBD::getImageWidth() const
{
	return depth_frame.getWidthPx();
}

unsigned int racoon::rvdslam::FramePackageRGBD::getImageHeight() const
{
	return depth_frame.getHeightPx();
}

racoon::rvdslam::FramePackage::Intrinsics racoon::rvdslam::FramePackageRGBD::getImageIntrinsics() const
{
	return depth_frame.getIntrinsics();
}
