#include "stdafx.h"

#include "DriverManager.h"
#include "DriverWrapperKinectV1.h"
#include "DriverWrapperKinectV2.h"
#include "DriverWrapperZed.h"
#include "DriverWrapperRGBDFileReader.h"
#include "DriverWrapperRGBDVirtualCamera.h"
#include "DataManager.h"
#include "ViewManager.h"
#include "SLAMPipeline.h"

#include <QTimer>

#include <limits>

racoon::rvdslam::DriverManager::DriverManager(DataManager & data_manager, ViewManager & view_manager, SLAMPipeline & slam_pipeline, DriverType driver_type)
	: data_manager(data_manager),
	  view_manager(view_manager),
	  slam_pipeline(slam_pipeline),
	  driver(NULL),
	  filter(),
	  systemtime_old(std::chrono::high_resolution_clock::now()),
	  driver_state(GenericWrapper::State::STOPPED)
{
	slam_pipeline.setDriverManager(this);

	initDriver(driver_type);
}

racoon::rvdslam::DriverManager::~DriverManager()
{
	driver_thread.quit();
	driver_thread.wait();
}

void racoon::rvdslam::DriverManager::initDriver(DriverType driver_type)
{
	switch (driver_type)
	{
	case DriverType::KINECT_V1:
		driver = new DriverWrapperKinectV1();
		break;
	case DriverType::KINECT_V2:
		driver = new DriverWrapperKinectV2();
		break;
	case DriverType::ZED:
		driver = new DriverWrapperZed();
		break;
	case DriverType::RGBD_FILE_READER:
		driver = new DriverWrapperRGBDFileReader();
		break;
	case DriverType::RGBD_VIRTUAL_CAMERA:
		driver = new DriverWrapperRGBDVirtualCamera();
		break;
	}

	driver->moveToThread(&driver_thread);

	QObject::connect(&driver_thread, &QThread::finished, driver, &QObject::deleteLater);
	QObject::connect(this, &DriverManager::initDriverSignal, driver, &DriverWrapper::init);
	QObject::connect(this, &DriverManager::startDriverSignal, driver, &DriverWrapper::start);
	QObject::connect(this, &DriverManager::resetDriverSignal, driver, &DriverWrapper::reset);
	QObject::connect(this, &DriverManager::pauseDriverSignal, driver, &DriverWrapper::pause);
	QObject::connect(this, &DriverManager::resumeDriverSignal, driver, &DriverWrapper::resume);
	QObject::connect(this, &DriverManager::applyDriverSettings, driver, &DriverWrapper::applySettings);
	QObject::connect(driver, &DriverWrapper::stateChanged, this, &DriverManager::handleDriverStateChanged);
    QObject::connect(driver, &DriverWrapper::newFramePackageReady, this, &DriverManager::handleNewFramePackage);
	
	driver_thread.start();

	emit initDriverSignal();
}

void racoon::rvdslam::DriverManager::switchDriver(DriverType new_driver_type)
{
	driver_thread.quit();
	driver_thread.wait();

	initDriver(new_driver_type);
}

void racoon::rvdslam::DriverManager::startDriver()
{
	emit startDriverSignal();
}

void racoon::rvdslam::DriverManager::pauseDriver()
{
	emit pauseDriverSignal();
}

void racoon::rvdslam::DriverManager::resumeDriver()
{
	emit resumeDriverSignal();
}

void racoon::rvdslam::DriverManager::resetDriver()
{
	emit resetDriverSignal();
}

racoon::rvdslam::GenericWrapper::State racoon::rvdslam::DriverManager::getDriverState()
{
	return driver_state;
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::DriverManager::getDriverSettings()
{
	return driver->getSettings();
}

void racoon::rvdslam::DriverManager::setDriverSettings(std::shared_ptr<const racoon::rvdslam::GenericWrapper::Settings> settings)
{
	emit applyDriverSettings(settings);
}

void racoon::rvdslam::DriverManager::setFramePackageFilter(std::shared_ptr<FramePackage::Filter> filter)
{
	this->filter = filter;
}

void racoon::rvdslam::DriverManager::removeFramePackageFilter()
{
	this->filter = nullptr;
}

void racoon::rvdslam::DriverManager::handleDriverStateChanged(GenericWrapper::State driver_state)
{
	this->driver_state = driver_state;
	view_manager.setDriverState(driver_state);

	if (driver_state == GenericWrapper::State::RESET)
	{
		data_manager.clearCapturedFramePackages();
		slam_pipeline.clearTrackerQueue();
	}
}

void racoon::rvdslam::DriverManager::handleNewFramePackage(std::shared_ptr<FramePackage> frame_package)
{
	if (filter != nullptr)
		frame_package->setFilter(filter);

	data_manager.addCapturedFramePackage(frame_package);
	view_manager.viewDriverFrames(frame_package);

	std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
	std::chrono::milliseconds timediff = std::chrono::duration_cast<std::chrono::milliseconds>(now - systemtime_old);

	double fps = 1000.0 / (timediff.count() * 1.0);

	systemtime_old = now;

	view_manager.setDriverFps(fps);

	slam_pipeline.addFramePackage(frame_package);
}