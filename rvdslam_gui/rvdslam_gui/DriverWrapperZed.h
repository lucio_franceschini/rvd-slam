#pragma once

#include "DriverWrapper.h"

#include <opencv2/opencv.hpp>
#include <zed/Camera.hpp>

namespace racoon
{
	namespace rvdslam
	{
		class FramePackage;

		class DriverWrapperZed : public DriverWrapper
		{
		public:
			DriverWrapperZed();
			~DriverWrapperZed();

		private:
			bool error;

			sl::zed::Camera* zedCamera;
			int zedWidth;
			int zedHeight;

			cv::Mat zedImage;
			cv::Mat zedDepth;

			bool doInit() override final;
			bool doReset() override final;
			
			bool getFramePackage(std::shared_ptr<FramePackage>& frame_package) override final;
			
			bool initZed();
			void waitForCalibration();
			bool getImage();
			bool getDepth();
		};
	}
}