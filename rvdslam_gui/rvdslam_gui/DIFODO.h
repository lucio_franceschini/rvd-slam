#pragma once

#include <mrpt/vision/CDifodo.h>

#include <opencv2/core/eigen.hpp>

namespace racoon
{
	namespace rvdslam
	{
		/**
	     * This class implements the mrpt::vision::CDifodo interface.
		 * It can be used to create a new instance of the DIFODO algorithm.
		 * 
		 * Additional information about DIFODO:
		 * Paper:			http://mapir.isa.uma.es/mjaimez/Papers/Jaimez_and_Gonzalez_DIFODO_2015.pdf
		 * Documentation:	http://reference.mrpt.org/devel/classmrpt_1_1vision_1_1_c_difodo.html
		 * Header:			https://github.com/MRPT/mrpt/blob/master/libs/vision/include/mrpt/vision/CDifodo.h
		 * Source:			https://github.com/MRPT/mrpt/blob/master/libs/vision/src/CDifodo.cpp
		 *
		 * Usage:
		 * After instanciation, set field of view from camera intrinsics by calling setFOV() with horizontal and vertical field of view in degrees as parameters.
		 * Use setInputDepthImage() followed by loadFrame() to load a new depth frame into the algorithm. Set the current frame rate through public member variable fps.
		 * Call odometryCalculation() to let the algorithm process the current frame. The estimated pose can then be extracted from public member variable cam_pose.
		 */
		class DIFODO : public mrpt::vision::CDifodo
		{
		public:
			/**
			 * Default constructor. Creates a new DIFODO instance.
			 *
			 * Parameters are settings for the DIFODO algorithm:
			 *
			 * downsample:	Specifies the downsampling factor for the input image.
			 *				E.g. a value of 2 would lead to half of the original horizontal and vertical resolution - one fourth of the total number of pixels.
			 * ctf_levels:	Number of levels of the gaussian image pyramid. Suggested values: 3 to 5
			 * max_depth:	Maximum depth value in meters taken into account.
			 * cols:		Depth image width in pixels.
			 * rows:		Depth image height in pixels.
			 */
			DIFODO(unsigned int downsample = 1, unsigned int ctf_levels = 4, float max_depth = 10.0, unsigned int cols = 640, unsigned int rows = 480);
		public:
			/**
			 * Sets the next input depth image for pose estimation.
			 * Parameters are assumed to be a cv::Mat image with 16 bit unsigned depth values
			 * and a scale factor to transform these raw depth values to depth values in meters according to the formula depth [m] = depth [raw] / scale.
			 */
			void setInputDepthImage(cv::Mat image, unsigned int scale);

			/** Loads the input depth frame into the algorithm. Performs depth scaling and max_depth thresholding. */
			void loadFrame();
		private:
			cv::Mat depth_image;
			unsigned int depth_scale;
			float max_depth;
		};
	}
}