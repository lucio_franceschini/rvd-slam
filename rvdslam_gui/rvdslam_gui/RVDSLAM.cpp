#include "stdafx.h"

#include "RVDSLAM.h"

#include "FramePackage.h"

#include <pcl/visualization/cloud_viewer.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

racoon::rvdslam::RVDSLAM::RVDSLAM()
	: view_manager(*this),
	  data_manager(view_manager),
	  slam_pipeline(data_manager, view_manager, SLAMPipeline::TrackerType::DIFODO, SLAMPipeline::MapperType::FASTFUSION),
	  driver_manager(data_manager, view_manager, slam_pipeline, DriverManager::DriverType::RGBD_FILE_READER)
{
	view_manager.setDriverManager(&driver_manager);
	view_manager.setPipeline(&slam_pipeline);
	view_manager.setDataManager(&data_manager);

	file.open("cout.txt");
	std::streambuf* sbuf = std::cout.rdbuf();
	std::cout.rdbuf(file.rdbuf());
}

racoon::rvdslam::RVDSLAM::~RVDSLAM()
{
}