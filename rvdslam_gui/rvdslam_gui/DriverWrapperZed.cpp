#include "stdafx.h"

#include "DriverWrapperZed.h"
#include "FramePackageRGBD.h"

racoon::rvdslam::DriverWrapperZed::DriverWrapperZed() : error(false), zedCamera(nullptr)
{
	// Create camera driver and get camera resolution
	// see https://www.stereolabs.com/developers/documentation/API/v1.2.0/classsl_1_1zed_1_1Camera.html#a4c16238a83ff9bd468b1210c1ffa7bf4
	// Camera (ZEDResolution_mode zedRes_mode=ZEDResolution_mode::HD720, float fps=0.0)
	this->zedCamera = new sl::zed::Camera(sl::zed::ZEDResolution_mode::VGA, 30)  ;
}

racoon::rvdslam::DriverWrapperZed::~DriverWrapperZed()
{
	this->waitForCalibration();
	delete this->zedCamera;
}

bool racoon::rvdslam::DriverWrapperZed::doInit()
{
	return this->initZed();
}

bool racoon::rvdslam::DriverWrapperZed::doReset()
{
	this->waitForCalibration();
	delete this->zedCamera;

	// Create camera driver and get camera resolution
	this->zedCamera = new sl::zed::Camera(sl::zed::VGA);

	return this->initZed();
}

bool racoon::rvdslam::DriverWrapperZed::getFramePackage(std::shared_ptr<FramePackage>& frame_package)
{	
	if (error)
		return false;

	// Grab new image from camera
	if (!zedCamera->grab(sl::zed::SENSING_MODE::STANDARD))
	{
		if (!this->getImage() || !this->getDepth())
			return false;

		// Get timestamp of the time the frame has been extracted
		unsigned long long timestamp = this->zedCamera->getCameraTimestamp();

		// Get left camera intrinsics
		sl::zed::CamParameters cameraParameters = this->zedCamera->getParameters()->LeftCam;
		FramePackage::Intrinsics intrinsics;
		intrinsics.cx = cameraParameters.cx;
		intrinsics.cy = cameraParameters.cy;
		intrinsics.fx = cameraParameters.fx;
		intrinsics.fy = cameraParameters.fy;

		// Build the frame package
		CameraFrameColor cfc = CameraFrameColor((double) timestamp / 1000000.0, this->zedWidth, this->zedHeight, CameraFrame::EncodingType::ET_8U3_BGR, intrinsics, this->zedImage.data);
		CameraFrameDepth cfd = CameraFrameDepth((double) timestamp / 1000000.0, this->zedWidth, this->zedHeight, 1000, CameraFrame::EncodingType::ET_16U1_GRAY, intrinsics, (ushort*) this->zedDepth.data);
		frame_package = std::make_shared<FramePackageRGBD>(cfc, cfd);
	}
	else
	{
		return false;
	}

	return true;
}

bool racoon::rvdslam::DriverWrapperZed::getImage()
{	
	this->zedImage = cv::Mat(this->zedHeight, this->zedWidth, CV_8UC4);

	// Readout image from camera
	slMat2cvMat(this->zedCamera->retrieveImage(sl::zed::SIDE::LEFT)).copyTo(this->zedImage);
	cv::cvtColor(this->zedImage, this->zedImage, CV_RGBA2RGB);
	cv::imwrite("left.png", this->zedImage);

	return true;
}

bool racoon::rvdslam::DriverWrapperZed::getDepth()
{
	cv::Mat zedDepthFloat = cv::Mat(this->zedHeight, this->zedWidth, CV_32FC1);
	this->zedDepth = cv::Mat(this->zedHeight, this->zedWidth, CV_16UC1);

	// Readout image from camera
	slMat2cvMat(this->zedCamera->retrieveMeasure(sl::zed::MEASURE::DEPTH)).copyTo(zedDepthFloat);
	zedDepthFloat.convertTo(this->zedDepth, CV_16UC1);

	return true;	
}

bool racoon::rvdslam::DriverWrapperZed::initZed()
{
	// Set initialization parameters
	// see https://www.stereolabs.com/developers/documentation/API/v1.2.0/structsl_1_1zed_1_1InitParams.html
	sl::zed::InitParams zedParams;
	zedParams.mode = sl::zed::MODE::QUALITY;
	zedParams.unit = sl::zed::UNIT::MILLIMETER;
	zedParams.coordinate = sl::zed::COORDINATE_SYSTEM::IMAGE;
	
	// Attempt to initialize ZED camera
	sl::zed::ERRCODE zedError = this->zedCamera->init(zedParams);

	if (zedError != sl::zed::SUCCESS) 
	{
		this->error = true;
		return false;
	}

	// Get camera resolution
	sl::zed::resolution resolution = this->zedCamera->getImageSize();
	this->zedHeight = resolution.height;
	this->zedWidth = resolution.width;

	// Wait for self-calibration
	this->waitForCalibration();
	// Set runtime parameters for camera
    // see https://www.stereolabs.com/developers/documentation/API/v1.2.0/group__Enumerations.html#ga4558a79b2aed89eb945a032b5fa7a1d7
	this->zedCamera->setCameraSettingsValue(sl::zed::ZED_BRIGHTNESS, -1);
	this->zedCamera->setCameraSettingsValue(sl::zed::ZED_CONTRAST, -1);
	this->zedCamera->setCameraSettingsValue(sl::zed::ZED_HUE, -1);
	this->zedCamera->setCameraSettingsValue(sl::zed::ZED_SATURATION, -1);
	this->zedCamera->setCameraSettingsValue(sl::zed::ZED_GAIN, -1);
	this->zedCamera->setCameraSettingsValue(sl::zed::ZED_EXPOSURE, -1);
	// Set runtime parameters for depth sensing
	this->zedCamera->setDepthClampValue(5000);
	this->zedCamera->setConfidenceThreshold(80);

	return true;
}

void racoon::rvdslam::DriverWrapperZed::waitForCalibration()
{
	auto status = sl::zed::ZED_SELF_CALIBRATION_STATUS::SELF_CALIBRATION_NOT_CALLED;
	do {
		status = this->zedCamera->getSelfCalibrationStatus();
	} while(status == sl::zed::SELF_CALIBRATION_RUNNING);
}