#pragma once

#include "MapperWrapper.h"

#include <cpu_tsdf/tsdf_volume_octree.h>

namespace racoon
{
	namespace rvdslam
	{
		/**
	     * Mapper module for the CPUTSDF mapper. Implements the MapperWrapper interface.
		 */
		class MapperWrapperCPUTSDF : public MapperWrapper
		{
		public:
			/** Default constructor */
			MapperWrapperCPUTSDF();
		private:
			cpu_tsdf::TSDFVolumeOctree::Ptr tsdf;

			 /** Calls the integrateCloud() method of CPUTSDF to incorporate the given frame package into the internal map */
			void updateMap(const FramePackage & frame_package);

			/**
			 * Obtains a mesh from CPUTSDF by requesting a reconstruction from the
			 * truncated signed distance function representing the internal map.
			 */
			std::shared_ptr<pcl::PolygonMesh> getMesh();

			/**
			 * Obtains a point cloud from CPUTSDF by requesting a rendering of the
			 * truncated signed distance function representing the internal map.
			 */
			std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> getPointCloud();
		};
	}
}