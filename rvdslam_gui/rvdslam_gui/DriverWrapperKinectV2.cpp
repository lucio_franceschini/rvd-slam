#include "stdafx.h"

#include "DriverWrapperKinectV2.h"
#include "FramePackageRGBD.h"

#include <thread>
#include <chrono>

racoon::rvdslam::DriverWrapperKinectV2::KinectV2Settings::KinectV2Settings(bool infrared) : infrared(infrared)
{
}

racoon::rvdslam::DriverWrapperKinectV2::DriverWrapperKinectV2(KinectV2Settings settings) : settings(settings), error(false), sensor(NULL), reader(NULL), mapper(NULL)
{
	cframe = (unsigned char*)malloc(512 * 424 * 3 * sizeof(unsigned char));
	dframe = (unsigned short*)malloc(512 * 424 * sizeof(unsigned short));
}

racoon::rvdslam::DriverWrapperKinectV2::~DriverWrapperKinectV2()
{
	if (reader)
	{
		reader->UnsubscribeMultiSourceFrameArrived(handle);
		reader->Release();
	}

	if (sensor)
	{
		sensor->Close();
		sensor->Release();
	}

	if (mapper)
		mapper->Release();

	free(cframe);
	free(dframe);
}

bool racoon::rvdslam::DriverWrapperKinectV2::doInit()
{
	return initKinect();
}

bool racoon::rvdslam::DriverWrapperKinectV2::doReset()
{
	if (reader)
	{
		reader->UnsubscribeMultiSourceFrameArrived(handle);
		reader->Release();
	}

	if (sensor)
	{
		sensor->Close();
		sensor->Release();
	}

	if (mapper)
		mapper->Release();

	return doInit();
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::DriverWrapperKinectV2::getSettings() const
{
	auto settings = std::unique_ptr<KinectV2Settings>(new KinectV2Settings(this->settings));

	return std::move(settings);
}

void racoon::rvdslam::DriverWrapperKinectV2::setSettings(const racoon::rvdslam::GenericWrapper::Settings & settings)
{
	auto kinectv2_settings = dynamic_cast<const KinectV2Settings &>(settings);

	this->settings = kinectv2_settings;

	switchState(State::RESET);
}

bool racoon::rvdslam::DriverWrapperKinectV2::getFramePackage(std::shared_ptr<FramePackage>& frame_package)
{	
	if (error)
		return false;

	if (!acquireKinectData())
		return false;

	FramePackage::Intrinsics fp_intrinsics;

	fp_intrinsics.fx = intrinsics.FocalLengthX;
	fp_intrinsics.fy = intrinsics.FocalLengthY;
	fp_intrinsics.cx = intrinsics.PrincipalPointX;
	fp_intrinsics.cy = intrinsics.PrincipalPointY;

	CameraFrameColor cfc = CameraFrameColor((double)(ctimestamp * 1.0 / 10000000.0), 512, 424, CameraFrame::EncodingType::ET_8U3_BGR, fp_intrinsics, cframe);
	CameraFrameDepth cfd = CameraFrameDepth((double)(dtimestamp * 1.0 / 10000000.0), 512, 424, 1000, CameraFrame::EncodingType::ET_16U1_GRAY, fp_intrinsics, dframe);

	frame_package = std::make_shared<FramePackageRGBD>(cfc, cfd);

	return true;
}

bool racoon::rvdslam::DriverWrapperKinectV2::initKinect()
{
    if (FAILED(GetDefaultKinectSensor(&sensor)))
	{
        return false;
    }

    if (sensor)
	{
		sensor->Open();

		BOOLEAN available;

		sensor->get_IsAvailable(&available);

		if (!available)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(2000));

			sensor->get_IsAvailable(&available);

			if (!available)
				return false;
		}

		sensor->get_CoordinateMapper(&mapper);
		mapper->GetDepthCameraIntrinsics(&intrinsics);

		if (settings.infrared)
		{
			HRESULT blubb = sensor->OpenMultiSourceFrameReader(FrameSourceTypes::FrameSourceTypes_Infrared | FrameSourceTypes::FrameSourceTypes_Depth, &reader);
		}
		else
		{
			HRESULT blubb = sensor->OpenMultiSourceFrameReader(FrameSourceTypes::FrameSourceTypes_Color | FrameSourceTypes::FrameSourceTypes_Depth, &reader);
		}

		HRESULT blubber = reader->SubscribeMultiSourceFrameArrived(&handle);

        return reader;
    } 
	else
	{
        return false;
    }
}

bool racoon::rvdslam::DriverWrapperKinectV2::acquireKinectData()
{
	bool success = false;

	for (int retry_count = 10; retry_count > 0 && success == false; retry_count--)
	{
		int result = WaitForSingleObject((HANDLE)handle, 100);

		if (!(result == WAIT_OBJECT_0))
			continue;

		IMultiSourceFrame* frame = NULL;

		HRESULT blubb = E_PENDING;

		do
		{
			blubb = reader->AcquireLatestFrame(&frame);
		}
		while (blubb == E_PENDING);

		if (!frame)
			continue;

		if (blubb != S_OK)
		{
			frame->Release();
			continue;
		}
	
		if (!acquireKinectDepthData(frame))
		{
			frame->Release();
			continue;
		}
		
		if (settings.infrared)
		{
			if (!acquireKinectInfraredData(frame))
			{
				frame->Release();
				continue;
			}
		}
		else
		{
			if (!acquireKinectColorData(frame))
			{
				frame->Release();
				continue;
			}
		}

		success = true;
	}

	return success;
}

bool racoon::rvdslam::DriverWrapperKinectV2::acquireKinectDepthData(IMultiSourceFrame* frame)
{
	IDepthFrame* depthframe = NULL;
	IDepthFrameReference* frameref = NULL;

    HRESULT blubb = frame->get_DepthFrameReference(&frameref);

	HRESULT blubber = E_PENDING;

	do
	{
		blubber = frameref->AcquireFrame(&depthframe);
	}
	while (blubber == E_PENDING);

    if (frameref)
		frameref->Release();

    if (!depthframe)
		return false;

	if (blubber != S_OK )
	{
		depthframe->Release();
		return false;
	}

	TIMESPAN timestamp;
	depthframe->get_RelativeTime(&timestamp);
	dtimestamp = timestamp;

	unsigned short* depthimage = (unsigned short*)malloc(512 * 424 * sizeof(unsigned short));

    depthframe->CopyFrameDataToArray(512 * 424, depthimage);

	unsigned short * dest = dframe + (512 - 1);
	unsigned short * curr = depthimage;

	for (int v = 0; v < 424; v++)
	{
		for (int u = 0; u < 512; u++)
		{
			*dest-- = *curr++;
		}

		dest += 512 * 2;
	}

    unsigned int sz;
    unsigned short* buf;

    depthframe->AccessUnderlyingBuffer(&sz, &buf);

	mapper->MapDepthFrameToColorSpace(512 * 424, buf, 512 * 424, depth2color);

    if (depthframe)
		depthframe->Release();

	return true; 
}

bool racoon::rvdslam::DriverWrapperKinectV2::acquireKinectColorData(IMultiSourceFrame* frame)
{
	IColorFrame* colorframe = NULL;
	IColorFrameReference* frameref = NULL;

    HRESULT blubb = frame->get_ColorFrameReference(&frameref);

	HRESULT blubber = E_PENDING;

	do
	{
		blubber = frameref->AcquireFrame(&colorframe);
	}
	while (blubber == E_PENDING);

    if (frameref)
		frameref->Release();

    if (!colorframe)
		return false;

	if (blubber != S_OK )
	{
		colorframe->Release();
		return false;
	}

	TIMESPAN timestamp;
	colorframe->get_RelativeTime(&timestamp);
	ctimestamp = timestamp;

	unsigned char* colorimage = (unsigned char*)malloc(1920 * 1080 * 4 * sizeof(unsigned char));

	colorframe->CopyConvertedFrameDataToArray(1920 * 1080 * 4, colorimage, ColorImageFormat_Bgra);

	unsigned char* dest = cframe + ((512 - 1) * 3);

	for (int v = 0; v < 424; v++)
	{
		for (int u = 0; u < 512; u++)
		{
			ColorSpacePoint p = depth2color[v * 512 + u];

			if (p.X < 0 || p.Y < 0 || p.X >= 1920 || p.Y >= 1080)
			{
				*dest++ = 0;
				*dest++ = 0;
				*dest++ = 0;
			}
			else
			{
				int idx = (int)p.X + 1920 * (int)p.Y;

				*dest++ = colorimage[4 * idx + 0];
				*dest++ = colorimage[4 * idx + 1];
				*dest++ = colorimage[4 * idx + 2];
			}

			dest -= 2 * 3;
		}

		dest += 512 * 2 * 3;
	}

    if (colorframe)
		colorframe->Release();

	free(colorimage);

	return true;
}

bool racoon::rvdslam::DriverWrapperKinectV2::acquireKinectInfraredData(IMultiSourceFrame* frame)
{
	IInfraredFrame* irframe = NULL;
	IInfraredFrameReference* frameref = NULL;

    HRESULT blubb = frame->get_InfraredFrameReference(&frameref);

	HRESULT blubber = E_PENDING;

	do
	{
		blubber = frameref->AcquireFrame(&irframe);
	}
	while (blubber == E_PENDING);

    if (frameref)
		frameref->Release();

    if (!irframe)
		return false;

	if (blubber != S_OK )
	{
		irframe->Release();
		return false;
	}

	TIMESPAN timestamp;
	irframe->get_RelativeTime(&timestamp);
	ctimestamp = timestamp;

	unsigned short* irimage = (unsigned short*)malloc(512 * 424 * sizeof(unsigned short));;

	irframe->CopyFrameDataToArray(512 * 424, irimage);

	unsigned char* dest = cframe + ((512 - 1) * 3);;

	for (int v = 0; v < 424; v++)
	{
		for (int u = 0; u < 512; u++)
		{
			unsigned short intensity = (irimage[v * 512 + u] >> 7);	//shifted by 7 instead of 8 to double the intensity
			unsigned char intensity_capped = intensity < 255 ? intensity : 255;

			*dest++ = intensity_capped;
			*dest++ = intensity_capped;
			*dest++ = intensity_capped;

			dest -= 2 * 3;
		}

		dest += 512 * 2 * 3;
	}

    if (irframe)
		irframe->Release();

	free(irimage);

	return true;
}