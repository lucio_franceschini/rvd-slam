#include "stdafx.h"

#include "RVDSLAM.h"
#include "DriverManager.h"

#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);	

	using namespace racoon::rvdslam;

	qRegisterMetaType<std::shared_ptr<const FramePackage>>("std::shared_ptr<const FramePackage>");
	qRegisterMetaType<std::shared_ptr<FramePackage>>("std::shared_ptr<FramePackage>");
	qRegisterMetaType<Eigen::Affine3f>("Eigen::Affine3f");
	qRegisterMetaType<Eigen::Affine3d>("Eigen::Affine3d");
	qRegisterMetaType<std::shared_ptr<pcl::PolygonMesh>>("std::shared_ptr<pcl::PolygonMesh>");
	qRegisterMetaType<std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>>>("std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>>");
	qRegisterMetaType<std::string>("std::string");
	qRegisterMetaType<std::shared_ptr<const GenericWrapper::Settings>>("std::shared_ptr<const GenericWrapper::Settings>");
	qRegisterMetaType<std::shared_ptr<GenericWrapper::Settings>>("std::shared_ptr<GenericWrapper::Settings>");
	qRegisterMetaType<GenericWrapper::State>("GenericWrapper::State");

	racoon::rvdslam::RVDSLAM rvdslam;

	return a.exec();
}