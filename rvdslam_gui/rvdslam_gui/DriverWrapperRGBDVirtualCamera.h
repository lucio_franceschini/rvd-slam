#pragma once

#include "DriverWrapper.h"
#include "RGBDVirtualCamera.h"

#include <QObject>

#include <fstream>
#include <string> 
#include <chrono>
#include <thread>
#include <memory>

namespace racoon
{
	namespace rvdslam
	{
		class FramePackage;

		/**
	     * Device driver module for the RGBD virtual camera sensor. Implements the DriverWrapper interface.
		 * This module reads RGBD datasets from disk as if they were supplied by a real sensor.
		 * The virtual camera is running in a separate thread, reading frames at a constant frame rate (if possible) and discarding them if they were not captured by this module in time.
		 * Frame rate of this module is limited by the frame rate of the virtual camera.
		 */
		class DriverWrapperRGBDVirtualCamera : public DriverWrapper
		{
		public:
			/** Settings struct for RGBD virtual camera device driver module */
			struct RGBDVirtualCameraSettings : public Settings
			{
				/**
				* Struct constructor
				*
				* @param path - Path to dataset
				*/
				RGBDVirtualCameraSettings(std::string path = "");

				std::string path;	// path to dataset
			};

			/** Default constructor */
			DriverWrapperRGBDVirtualCamera(RGBDVirtualCameraSettings settings = RGBDVirtualCameraSettings());

			/** Default destructor */
			~DriverWrapperRGBDVirtualCamera();

			/** Returns a pointer to the current settings struct */
			std::unique_ptr<Settings> getSettings() const override final;
		private:
			RGBDVirtualCamera* virtual_camera;		// virtual camera sensor
			RGBDVirtualCameraSettings settings;

			std::thread camera_thread;				// separate virtual camera thread

			/** Initializes device driver module */
			bool doInit() override final;

			/** Resets device driver module */
			bool doReset() override final;

			/** Sets settings and applies them by resetting module */
			void setSettings(const Settings & settings) override final;

			/** Reads the next frame package from the virtual camera sensor. Returns true if successful, false otherwise. */
			bool getFramePackage(std::shared_ptr<racoon::rvdslam::FramePackage>& frame_package) override final;
		};
	}
}