#pragma once

#include "ui_DriverSavingDatasetProgressDialog.h"

#include <QtWidgets/QDialog>

namespace racoon
{
	namespace rvdslam
	{
		class DriverSavingDatasetProgressDialog : public QDialog
		{
			Q_OBJECT
		public:
			/**
			 * Constructor
			 *
			 * Sets up the UI elements of the progress dialog.
			 */
			DriverSavingDatasetProgressDialog(QWidget *parent = 0);

			/** Destructor */
			~DriverSavingDatasetProgressDialog();

			/** Updates the progress bar value */
			void setProgress(int percent);
		private:
			Ui::DriverSavingDatasetProgressDialog ui;
		};
	}
}