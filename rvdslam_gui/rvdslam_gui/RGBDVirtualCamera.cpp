#include "stdafx.h"

#include "RGBDVirtualCamera.h"
#include "FramePackageRGBD.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>

#include <thread>

racoon::rvdslam::RGBDVirtualCamera::RGBDVirtualCamera(std::string path)
  : color_ready(false),	depth_ready(false),
	groundtruth_ready(false),
	path(path),
	color_file(path + "/rgb.txt"), depth_file(path + "/depth.txt"),
	groundtruth_file(path + "/groundtruth.txt"),
	cframe(NULL), dframe(NULL),
	quit_loop(false),
	error(false),
	fp_ready(false),
	color_timestamp(0.0), depth_timestamp(0.0),
	color_systemtime(std::chrono::high_resolution_clock::now()), depth_systemtime(std::chrono::high_resolution_clock::now())
{
	if (!readIntrinsics())
	{
		//fx = 525.0;
		//fy = 525.0;
		//cx = 319.5;
		//cy = 239.5;

		//fx = 535.4;
		//fy = 539.2;
		//cx = 320.1;
		//cy = 247.6;

		//Standard Kinect v2 intrinsics
		fx = 366.0;
		fy = 366.0;
		cx = 258.6;
		cy = 206.5;
	}

	if (!readDepthScale())
		depth_scale = 5000;
}

racoon::rvdslam::RGBDVirtualCamera::~RGBDVirtualCamera()
{
	free(cframe);
	free(dframe);
}

void racoon::rvdslam::RGBDVirtualCamera::run()
{
	if (path == "")
	{
		error = true;

		return;
	}

	while (!quit_loop)
	{
		color_ready = readNextColorFrame();
		depth_ready = readNextDepthFrame();
		groundtruth_ready = readNextGroundtruthPose();

		if (!color_ready || !depth_ready)
		{
			error = true;

			return;
		}

		FramePackage::Intrinsics intrinsics;

		intrinsics.fx = fx;
		intrinsics.fy = fy;
		intrinsics.cx = cx;
		intrinsics.cy = cy;

		CameraFrameColor cfc = CameraFrameColor(color_timestamp, cwidth, cheight, CameraFrame::EncodingType::ET_8U3_BGR, intrinsics, cframe);
		CameraFrameDepth cfd = CameraFrameDepth(depth_timestamp, dwidth, dheight, depth_scale, CameraFrame::EncodingType::ET_16U1_GRAY, intrinsics, dframe);

		FramePackage::Groundtruth groundtruth(tx, ty, tz, qx, qy, qz, qw);

		std::lock_guard<std::mutex> fp_lock(fp_mutex);

		if (!groundtruth_ready)
		{
			frame_package = new FramePackageRGBD(cfc, cfd);
		}
		else
		{
			frame_package = new FramePackageRGBD(cfc, cfd, groundtruth);
		}

		fp_ready = true;

		fp_cv.notify_all();

		color_ready = false;
		depth_ready = false;
		groundtruth_ready = false;
	}
}

void racoon::rvdslam::RGBDVirtualCamera::quit()
{
	quit_loop = true;
}

bool racoon::rvdslam::RGBDVirtualCamera::getFramePackage(FramePackageRGBD *& frame_package) const
{
	if (error)
		return false;

	std::unique_lock<std::mutex> fp_lock(fp_mutex);

	if (fp_ready || fp_cv.wait_for(fp_lock, std::chrono::milliseconds(1000)) == std::cv_status::no_timeout) // 1 s timeout if new fp not ready
	{
		if (!fp_ready)
		{
			fp_lock.unlock();
			return false;
		}

		frame_package = this->frame_package;
		fp_ready = false;

		fp_lock.unlock();

		return true;
	}
	else // timeout
	{
		fp_lock.unlock();

		return false;
	}
}

bool racoon::rvdslam::RGBDVirtualCamera::readNextColorFrame()
{
	if (!color_file.good())
		return false;

	std::string png_rel_path;

	std::string line;

	do
	{
		if (!std::getline(color_file, line))
			return false;
	} while (line.at(0) == '#');

	std::stringstream line_str;

	line_str.str(line);

	double new_timestamp;

	line_str >> new_timestamp >> png_rel_path;

	std::string png_abs_path = path + "/" + png_rel_path;
	
	cv::Mat image = cv::imread(png_abs_path, cv::IMREAD_COLOR);

	cwidth = image.cols;
	cheight = image.rows;

	cframe = (unsigned char*)realloc(cframe, image.cols * image.rows * 3 * sizeof(unsigned char));

	std::copy(image.data, image.data + image.cols * image.rows * 3, cframe);

	std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
	std::chrono::microseconds timestamp_diff = (new_timestamp - color_timestamp) > 1000.0 ? std::chrono::microseconds(0) : std::chrono::microseconds((long)((new_timestamp - color_timestamp) * 1000000.0));
	
	std::chrono::microseconds wait_time = timestamp_diff - std::chrono::duration_cast<std::chrono::microseconds>(now - color_systemtime);

	if (wait_time.count() > 0)
		std::this_thread::sleep_for(wait_time);

	color_timestamp = new_timestamp;
	color_systemtime = now;

	return true;
}

bool racoon::rvdslam::RGBDVirtualCamera::readNextDepthFrame()
{
	if (!depth_file.good())
		return false;

	std::string png_rel_path;

	std::string line;

	do
	{
		if (!std::getline(depth_file, line))
			return false;
	} while (line.at(0) == '#');

	std::stringstream line_str;

	line_str.str(line);

	double new_timestamp;

	line_str >> new_timestamp >> png_rel_path;

	std::string png_abs_path = path + "/" + png_rel_path;

	cv::Mat image = cv::imread(png_abs_path, cv::IMREAD_ANYDEPTH);

	dwidth = image.cols;
	dheight = image.rows;

	dframe = (unsigned short*)realloc(dframe, image.cols * image.rows * sizeof(unsigned short));

	unsigned short* dcurr = dframe;
	unsigned short* dend = dframe + image.cols * image.rows;
	unsigned char* tempcurr = image.data;

	//convert char array to short array, using little endian
	while (dcurr < dend)
	{
		*dcurr = (((unsigned short)*(tempcurr + 1)) << 8) + (unsigned short)*(tempcurr);
	
		dcurr++;
		tempcurr = tempcurr + 2;
	}

	//free(tempframe);

	std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
	std::chrono::microseconds timestamp_diff = (new_timestamp - depth_timestamp) > 1000.0 ? std::chrono::microseconds(0) : std::chrono::microseconds((long)((new_timestamp - depth_timestamp) * 1000000.0));
	
	std::chrono::microseconds wait_time = timestamp_diff - std::chrono::duration_cast<std::chrono::microseconds>(now - depth_systemtime);

	if (wait_time.count() > 0)
		std::this_thread::sleep_for(wait_time);

	depth_timestamp = new_timestamp;
	depth_systemtime = now;

	return true;
}

bool racoon::rvdslam::RGBDVirtualCamera::readNextGroundtruthPose()
{
	if (!groundtruth_file.good())
		return false;

	std::string line;

	double timestamp = 0.0;

	do
	{
		if (!std::getline(groundtruth_file, line))
			return false;
	
		if (line.at(0) == '#')
			continue;

		std::stringstream line_str;

		line_str.str(line);

		line_str >> timestamp >> tx >> ty >> tz >> qx >> qy >> qz >> qw;
	} while (timestamp < depth_timestamp);
	
	return true;
}

bool racoon::rvdslam::RGBDVirtualCamera::readIntrinsics()
{
	std::ifstream intrinsics_file(path + "/intrinsics.txt");

	if (!intrinsics_file.good())
		return false;

	std::string line;

	do
	{
		if (!std::getline(intrinsics_file, line))
			return false;
	} while (line.at(0) == '#');

	std::stringstream line_str;

	line_str.str(line);

	line_str >> fx >> fy >> cx >> cy;
	
	return true;
}

bool racoon::rvdslam::RGBDVirtualCamera::readDepthScale()
{
	std::ifstream scale_file(path + "/scale.txt");

	if (!scale_file.good())
		return false;

	std::string line;

	do
	{
		if (!std::getline(scale_file, line))
			return false;
	} while (line.at(0) == '#');

	std::stringstream line_str;

	line_str.str(line);

	line_str >> depth_scale;
	
	return true;
}