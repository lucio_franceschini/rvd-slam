#pragma once

#include <QObject>

#include <memory>

namespace racoon
{
	namespace rvdslam
	{
		class FramePackage;

		/**
	     * This abstract class provides the state machine functionality for a pipeline module.
		 * Every module needs to implement the doInit(), doRunning() and doReset() methods.
		 */
		class GenericWrapper : public QObject
		{
			Q_OBJECT
		public:
			/** Generic Settings struct */
			struct Settings
			{
				virtual ~Settings() = 0;
			};

			/** States of the generic module state machine */
			enum State
			{
				INIT, STOPPED, RUNNING, IDLE, PAUSED, RESET, _ERROR_
			};

			/** Default constructor */
			GenericWrapper();

			/** Virtual default destructor to avoid undefined behaviour when deleting base class pointer to derived class. To be implemented by derived class. */
			virtual ~GenericWrapper();

			/** To be implemented by derived class if it makes use of Settings. Should return a pointer to the current settings struct. */
			virtual	std::unique_ptr<Settings> getSettings() const;
		protected:
			/** Returns the current state of the state machine */
			State currentState();

			/** Requests the state machine to transistion to the state next_state. Returns true if transition allowed, false otherwise. */
			bool switchState(State next_state);
		private:
			State state;

			/** To be implemented by derived class if it makes use of Settings. Should set and apply the settings passed through the settings parameter. */
			virtual void setSettings(const Settings & settings);

			/** 
			 * Initializes the module. To be implemented by derived class. 
			 * Should return true if initialization successful, false otherwise.
			 */
			virtual bool doInit() = 0;

			/**
			 * Main method of a module. Performs the desired processing.
			 *
			 * Method is called each time the state machine (re-)enters the RUNNING state.
			 * If reiterative processing is desired, the method needs to request another transistion to the RUNNING state
			 * after its processing has finished by calling the switchState method.
			 *
			 * Returns true if processing successful. Otherwise, returns false and transistions to ERROR state.
			 */
			virtual bool doRunning() = 0;

			/** 
			 * Resets the module. To be implemented by derived class.
			 * Should return true if reset successful, false otherwise.
			 */
			virtual bool doReset() = 0;
		public slots:
			/** Requests a state transition to the INIT state to initialize the module */
			void init();

			/** Requests a state transition to the RUNNING state to start the module */
			void start();

			/** Requests a state transition to the PAUSE state to pause the module */
			void pause();

			/** Requests a state transition to the RUNNING state to resume the module*/
			void resume();

			/** Requests a state transition to the RESET state to reset the module */
			void reset();

			/** Sets and applies the settings passed through the settings parameter by calling the setSettings() method */
			void applySettings(std::shared_ptr<const GenericWrapper::Settings> settings);
		private slots:
			/** Runs the method corresponding to the current state of the state machine and requests follow-up transitions */
			void runState();
		signals:
			void runStateSignal();
			void stateChanged(GenericWrapper::State state);
		};
	}
}