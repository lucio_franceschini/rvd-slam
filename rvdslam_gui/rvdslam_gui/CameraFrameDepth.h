#pragma once

#include "CameraFrame.h"

#include <memory>
#include <vector>
#include <opencv2/core/core.hpp>
#include <Eigen/Core>

namespace racoon
{
	namespace rvdslam
	{
		/**
		 * Implementation of CameraFrame class for a depth frame.
		 * Includes depth data as a short array of length width_px * height_px.
		 * Provides access methods to the data in cv::Mat and Eigen::Matrix format.
		 */
		class CameraFrameDepth : public CameraFrame
		{
		public:
			/** Default constructor, calls base class constructor without parameters. Generally should not be called. */
			CameraFrameDepth();

			/**
			 * Constructor to create a new depth camera frame from raw data.
			 * depth_scale is the scale factor between raw depth value and depth in meters: depth [m] = depth [raw] / depth_scale
			 */
			CameraFrameDepth(const double timestamp_secs, const unsigned int width_px, const unsigned int height_px, const unsigned int depth_scale, const EncodingType encoding_type, const FramePackage::Intrinsics intrinsics, const unsigned short* depth_data);

			/** Copy constructor */
			CameraFrameDepth(const CameraFrameDepth& depth_frame);

			/** Assignment operator */
			CameraFrameDepth& operator= (const CameraFrameDepth& depth_frame);

			/** Default destructor */
			~CameraFrameDepth();

			/** Returns the depth scale value for the camera frame.
			  * The relationship between the raw depth value of a pixel, depth in meters for that pixel and depth scale is as follows:
			  * depth [m] = depth [raw] / depth_scale
			  */
			unsigned int getDepthScale() const;

			/** Returns a pointer to the raw data */
			unsigned short* getDataRaw() const;

			/** Returns the raw data as an std::vector of length width_px * height_px */
			std::vector<unsigned short> getDataVector() const;

			/** Returns the raw data as a cv::Mat with size width_px * height_px with one 16 bit depth value per pixel (format CV_16U) */	
			cv::Mat getDataCVMat() const;

			/** Returns the depth data in meters as an Eigen::Matrix with size width_px * height_px with one float depth value in meters per pixel*/
			Eigen::MatrixXf getDataEigenMatrixXf() const;
		private:
			unsigned short* depth_data;
			unsigned int depth_scale;
		};
	}
}