#pragma once

#include "ui_DriverSettingsDialogRGBDVirtualCamera.h"

#include <QtWidgets/QDialog>

namespace racoon
{
	namespace rvdslam
	{
		class ViewManager;

		class DriverSettingsDialogRGBDVirtualCamera : public QDialog
		{
			Q_OBJECT
		public:
			/**
			 * Constructor
			 *
			 * Sets up the UI elements of the settings dialog.
			 */
			DriverSettingsDialogRGBDVirtualCamera(ViewManager & view_manager, QWidget *parent = 0);

			/** Destructor */
			~DriverSettingsDialogRGBDVirtualCamera();
		private:
			Ui::DriverSettingsDialogRGBDVirtualCamera ui;
			ViewManager & view_manager;

			std::string path;
		private slots:
			/** Shows a folder selection dialog to let user choose path to dataset */
			void selectFolderButtonPressed();

			/**
			 * Gets called when dialog is accepted.
			 * Creates settings struct from UI element values
			 * and requests view manager to set driver module settings accordingly.
			 */
			void accept();
		};
	}
}