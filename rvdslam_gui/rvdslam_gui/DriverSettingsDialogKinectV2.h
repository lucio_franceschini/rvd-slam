#pragma once

#include "ui_DriverSettingsDialogKinectV2.h"

#include <QtWidgets/QDialog>

namespace racoon
{
	namespace rvdslam
	{
		class ViewManager;

		class DriverSettingsDialogKinectV2 : public QDialog
		{
			Q_OBJECT
		public:
			/**
			 * Constructor
			 *
			 * Sets up the UI elements of the settings dialog.
			 */
			DriverSettingsDialogKinectV2(ViewManager & view_manager, QWidget *parent = 0);

			/** Destructor */
			~DriverSettingsDialogKinectV2();
		private:
			Ui::DriverSettingsDialogKinectV2 ui;
			ViewManager & view_manager;
		private slots:
			/**
			 * Gets called when dialog is accepted.
			 * Creates settings struct from UI element values
			 * and requests view manager to set driver module settings accordingly.
			 */
			void accept();
		};
	}
}