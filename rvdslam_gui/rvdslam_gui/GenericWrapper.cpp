#include "stdafx.h"

#include "GenericWrapper.h"

racoon::rvdslam::GenericWrapper::Settings::~Settings()
{
}

racoon::rvdslam::GenericWrapper::GenericWrapper() : state(State::INIT)
{
	QObject::connect(this, &GenericWrapper::runStateSignal, this, &GenericWrapper::runState, Qt::QueuedConnection);
}

racoon::rvdslam::GenericWrapper::~GenericWrapper()
{
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::GenericWrapper::getSettings() const
{
	return nullptr;
}

void racoon::rvdslam::GenericWrapper::runState()
{
	switch (state)
	{
	case State::INIT:
		if (doInit())
			switchState(State::STOPPED);
		else
			switchState(State::_ERROR_);
		break;
	case State::RESET:
		if (doReset())
			switchState(State::STOPPED);
		else
			switchState(State::_ERROR_);
		break;
	case State::RUNNING:
		if (!doRunning())
			switchState(State::_ERROR_);
		break;
	case State::STOPPED:
	case State::PAUSED:
	case State::IDLE:
	case State::_ERROR_:
		break;
	}
}

bool racoon::rvdslam::GenericWrapper::switchState(State next_state)
{
	if (next_state == state)
	{
		emit runStateSignal();
		return true;
	}

	bool transition_ok = false;

	switch (state)
	{
	case State::INIT:
		if (next_state == State::STOPPED
		 || next_state == State::_ERROR_)
			transition_ok = true;
		break;
	case State::STOPPED:
		if (next_state == State::RUNNING
		 || next_state == State::RESET)
			transition_ok = true;
		break;
	case State::RUNNING:
		if (next_state == State::RUNNING
		 || next_state == State::IDLE
		 || next_state == State::PAUSED
		 || next_state == State::RESET
		 || next_state == State::_ERROR_)
			transition_ok = true;
		break;
	case State::IDLE:
		if (next_state == State::RUNNING
		 || next_state == State::PAUSED
		 || next_state == State::RESET)
			transition_ok = true;
		break;
	case State::PAUSED:
		if (next_state == State::RUNNING
		 || next_state == State::RESET)
			transition_ok = true;
		break;
	case State::RESET:
		if (next_state == State::STOPPED
		 || next_state == State::_ERROR_)
			transition_ok = true;
		break;
	case State::_ERROR_:
		if (next_state == State::RESET)
			transition_ok = true;
		break;
	}

	if (transition_ok)
	{
		state = next_state;
		emit stateChanged(next_state);
		runState();
		return true;
	}
	else
	{
		return false;
	}
}

racoon::rvdslam::GenericWrapper::State racoon::rvdslam::GenericWrapper::currentState()
{
	return state;
}

void racoon::rvdslam::GenericWrapper::init()
{
	switchState(State::INIT);
}

void racoon::rvdslam::GenericWrapper::start()
{
	switchState(State::RUNNING);
}

void racoon::rvdslam::GenericWrapper::pause()
{
	switchState(State::PAUSED);
}

void racoon::rvdslam::GenericWrapper::resume()
{
	switchState(State::RUNNING);
}

void racoon::rvdslam::GenericWrapper::reset()
{
	switchState(State::RESET);
}

void racoon::rvdslam::GenericWrapper::setSettings(const racoon::rvdslam::GenericWrapper::Settings & settings)
{
}

void racoon::rvdslam::GenericWrapper::applySettings(std::shared_ptr<const GenericWrapper::Settings> settings)
{
	setSettings(*settings);
}