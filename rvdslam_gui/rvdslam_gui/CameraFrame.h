#pragma once

#include "CameraFrame.h"
#include "FramePackage.h"

#include <opencv2/core/core.hpp>
#include <Eigen/Core>

namespace racoon
{
	namespace rvdslam
	{
	   /**
	    * This abstract class implements a datatype for storing generic rectangular camera frame data.
		*
		* Each camera frame includes a timestamp, intrinsic camera parameters and the frame size (width and height).
		* Frame data can be accessed as a cv::Mat object through a virtual method that needs to be implemented by the specific subclass.
		* The subclass also needs to handle data storage.
		*/
		class CameraFrame
		{
		public:
			/** The encoding type is used to distinguish data field size and byte order */
			enum class EncodingType : int
			{
				ET_8U3_BGR = 0,
				ET_8U3_RGB = 1,
				ET_16U1_GRAY = 2
			};

			/** Default constructor to be called by subclass to initialize class members */
			CameraFrame(const double timestamp_secs = 0.0,
				const unsigned int width_px = 0,
				const unsigned int height_px = 0,
				const EncodingType encoding_type = EncodingType::ET_8U3_BGR,
				const FramePackage::Intrinsics intrinsics = FramePackage::Intrinsics());

			/** Virtual default destructor to avoid undefined behaviour when deleting base class pointer to derived class. To be implemented by derived class. */
			virtual ~CameraFrame();

			/** Returns the frame timestamp in seconds */
			double getTimestampSecs() const;

			/** Returns the frame height in px */
			int getHeightPx() const;

			/** Returns the frame width in px */
			int getWidthPx() const;

			/** Returns the number of channels of the frame data based on the encoding type */
			int getNumChannels() const;

			/** Returns the frame's encoding type */
			EncodingType getEncodingType() const;

			/** Returns the frame's intrinsic parameters */
			FramePackage::Intrinsics getIntrinsics() const;

			/** Returns the frame's intrinsic parameters as a 3x3 intrinsic matrix */
			Eigen::Matrix3f getIntrinsicMatrix() const;

			/** Returns the frame data as a cv::Mat with size width_px * height_px * number of channels */
			virtual cv::Mat getDataCVMat() const = 0;
		protected:
			const double timestamp_secs;
			const unsigned int width_px;
			const unsigned int height_px;
			const EncodingType encoding_type;
			const FramePackage::Intrinsics intrinsics;
		};
	}
}