#include "stdafx.h"

#include "DriverSavingDatasetProgressDialog.h"

racoon::rvdslam::DriverSavingDatasetProgressDialog::DriverSavingDatasetProgressDialog(QWidget *parent) : QDialog(parent)
{
	ui.setupUi(this);

	ui.progressBar->setValue(0);
}

racoon::rvdslam::DriverSavingDatasetProgressDialog::~DriverSavingDatasetProgressDialog()
{
}

void racoon::rvdslam::DriverSavingDatasetProgressDialog::setProgress(int percent)
{
	ui.progressBar->setValue(percent);

	QCoreApplication::processEvents();
}