#include "stdafx.h"

#include "DriverWrapperRGBDVirtualCamera.h"
#include "FramePackageRGBD.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>

#include <thread>

racoon::rvdslam::DriverWrapperRGBDVirtualCamera::RGBDVirtualCameraSettings::RGBDVirtualCameraSettings(std::string path) : path(path)
{
}

racoon::rvdslam::DriverWrapperRGBDVirtualCamera::DriverWrapperRGBDVirtualCamera(RGBDVirtualCameraSettings settings) : settings(settings), virtual_camera(nullptr), camera_thread()
{
}

racoon::rvdslam::DriverWrapperRGBDVirtualCamera::~DriverWrapperRGBDVirtualCamera()
{
	if (virtual_camera)
		virtual_camera->quit();

	if (camera_thread.joinable())
		camera_thread.join();

	if (virtual_camera)
		delete(virtual_camera);
}

bool racoon::rvdslam::DriverWrapperRGBDVirtualCamera::doInit()
{
	virtual_camera = new RGBDVirtualCamera(settings.path);
	camera_thread = std::thread(&RGBDVirtualCamera::run, virtual_camera);

	return true;
}

bool racoon::rvdslam::DriverWrapperRGBDVirtualCamera::doReset()
{
	if (virtual_camera)
		virtual_camera->quit();

	if (camera_thread.joinable())
		camera_thread.join();

	if (virtual_camera)
		delete(virtual_camera);

	virtual_camera = new RGBDVirtualCamera(settings.path);
	camera_thread = std::thread(&RGBDVirtualCamera::run, virtual_camera);

	return true;
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::DriverWrapperRGBDVirtualCamera::getSettings() const
{
	auto settings = std::unique_ptr<RGBDVirtualCameraSettings>(new RGBDVirtualCameraSettings(this->settings));

	return std::move(settings);
}

void racoon::rvdslam::DriverWrapperRGBDVirtualCamera::setSettings(const racoon::rvdslam::GenericWrapper::Settings & settings)
{
	auto rgbdfilereader_settings = dynamic_cast<const RGBDVirtualCameraSettings &>(settings);

	this->settings = rgbdfilereader_settings;

	switchState(State::RESET);
}

bool racoon::rvdslam::DriverWrapperRGBDVirtualCamera::getFramePackage(std::shared_ptr<racoon::rvdslam::FramePackage>& frame_package)
{
	FramePackageRGBD* fp_rgbd_ptr;

	if (virtual_camera->getFramePackage(fp_rgbd_ptr))
	{
		frame_package = std::shared_ptr<FramePackageRGBD>(fp_rgbd_ptr);
		return true;
	}
	else
	{
		return false;
	}
}