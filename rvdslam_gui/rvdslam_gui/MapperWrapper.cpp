#include "stdafx.h"

#include "FramePackage.h"
#include "MapperWrapper.h"

racoon::rvdslam::MapperWrapper::MapperWrapper() : meshing_counter(0)
{
}

racoon::rvdslam::MapperWrapper::~MapperWrapper()
{
}

void racoon::rvdslam::MapperWrapper::addFramePackage(std::shared_ptr<FramePackage> frame_package)
{
	input_queue.push(frame_package);
	emit queueSizeChanged(input_queue.size());

	if (currentState() == State::IDLE)
		switchState(State::RUNNING);
}

void racoon::rvdslam::MapperWrapper::setProcessAll(bool process_all)
{
	this->process_all = process_all;
}

void racoon::rvdslam::MapperWrapper::clearQueue()
{
	input_queue = std::queue<std::shared_ptr<FramePackage>>();

	emit queueSizeChanged(input_queue.size());
}
 
bool racoon::rvdslam::MapperWrapper::doRunning()
{
	if (input_queue.empty())
	{
		return switchState(State::IDLE);
	}

	std::shared_ptr<FramePackage> frame_package;

	if (process_all)
	{
		frame_package = input_queue.front();
		input_queue.pop();
	}
	else
	{
		frame_package = input_queue.back();
		input_queue = std::queue<std::shared_ptr<FramePackage>>();
	}

	emit queueSizeChanged(input_queue.size());

	updateMap(*frame_package);

	emit updateDone(frame_package);

	meshing_counter++;

	if (meshing_counter >= 15)
	{
		emit meshReady(getMesh());
		emit pointCloudReady(getPointCloud());

		meshing_counter = 0;
	}

	return switchState(State::RUNNING);
}