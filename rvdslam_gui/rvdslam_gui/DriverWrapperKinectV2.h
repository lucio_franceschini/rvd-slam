#pragma once

#include "DriverWrapper.h"

#include <QObject>

#include <Kinect.h>

namespace racoon
{
	namespace rvdslam
	{
		class FramePackage;

		/**
	     * Device driver module for the Kinect v2 sensor. Implements the DriverWrapper interface.
		 */
		class DriverWrapperKinectV2 : public DriverWrapper
		{
		public:
			/** Settings struct for Kinect v2 device driver module */
			struct KinectV2Settings : public Settings
			{
				/**
				* Struct constructor
				*
				* @ param infrared - True if infrared camera data should be used for color frame, false for RGB camera (default)
				*/
				KinectV2Settings(bool infrared = false);

				bool infrared;
			};

			/** Default constructor */
			DriverWrapperKinectV2(KinectV2Settings settings = KinectV2Settings());

			/** Default destructor */
			~DriverWrapperKinectV2();

			/** Returns a pointer to the current settings struct */
			std::unique_ptr<Settings> getSettings() const override final;
		private:
			KinectV2Settings settings;

			IKinectSensor* sensor;					// Kinect sensor
			IMultiSourceFrameReader* reader;		// Kinect data source
			ICoordinateMapper* mapper;				// Kinect coordinate mapper

			CameraIntrinsics intrinsics;
			ColorSpacePoint depth2color[512 * 424];	// mapping table for depth to color registration

			WAITABLE_HANDLE handle;					// handle for Kinect frame arrived event

			unsigned char* cframe;					// color frame raw data
			unsigned short* dframe;					// depth frame raw data

			unsigned long long ctimestamp;			// color frame timestamp
			unsigned long long dtimestamp;			// depth frame timestamp

			bool error;

			/** Initializes device driver module. Returns true if initialization successful, false otherwise. */
			bool doInit() override final;

			/** Resets device driver module. Returns true if reset successful, false otherwise. */
			bool doReset() override final;

			/** Sets settings and applies them by resetting module */
			void setSettings(const Settings & settings) override final;

			/** Captures a new frame package. Returns true if capture successful, false otherwise. */
			bool getFramePackage(std::shared_ptr<FramePackage>& frame_package) override final;
			
			/** Internal method for initialization */
			bool initKinect();

			/** Internal method for capturing Kinect data */
			bool acquireKinectData();

			/** Internal method for capturing depth frame */
			bool acquireKinectDepthData(IMultiSourceFrame* frame);

			/** Internal method for capturing rgb frame */
			bool acquireKinectColorData(IMultiSourceFrame* frame);

			/** Internal method for capturing infrared frame */
			bool acquireKinectInfraredData(IMultiSourceFrame* frame);
		};
	}
}