#include "stdafx.h"

#include "DriverWrapperRGBDFileReader.h"
#include "FramePackageRGBD.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>

racoon::rvdslam::DriverWrapperRGBDFileReader::RGBDFileReaderSettings::RGBDFileReaderSettings(std::string path) : path(path)
{
}

racoon::rvdslam::DriverWrapperRGBDFileReader::DriverWrapperRGBDFileReader(RGBDFileReaderSettings settings) : settings(settings), cframe(NULL), dframe(NULL)
{
}

racoon::rvdslam::DriverWrapperRGBDFileReader::~DriverWrapperRGBDFileReader()
{
	free(cframe);
	free(dframe);
}

bool racoon::rvdslam::DriverWrapperRGBDFileReader::doInit()
{
	color_file = std::ifstream(settings.path + "/rgb.txt");
	depth_file = std::ifstream(settings.path + "/depth.txt");
	groundtruth_file = std::ifstream(settings.path + "/groundtruth.txt");

	if (!readIntrinsics())
		return false;

	if (!readDepthScale())
		return false;

	return true;
}

bool racoon::rvdslam::DriverWrapperRGBDFileReader::doReset()
{
	return doInit();
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::DriverWrapperRGBDFileReader::getSettings() const
{
	auto settings = std::unique_ptr<RGBDFileReaderSettings>(new RGBDFileReaderSettings(this->settings));

	return std::move(settings);
}

void racoon::rvdslam::DriverWrapperRGBDFileReader::setSettings(const racoon::rvdslam::GenericWrapper::Settings & settings)
{
	auto rgbdfilereader_settings = dynamic_cast<const RGBDFileReaderSettings &>(settings);

	this->settings = rgbdfilereader_settings;

	switchState(State::RESET);
}

bool racoon::rvdslam::DriverWrapperRGBDFileReader::getFramePackage(std::shared_ptr<racoon::rvdslam::FramePackage>& frame_package)
{
	if (!readNextColorFrame() || !readNextDepthFrame())
		return false;

	bool groundtruth_ready = readNextGroundtruthPose();

	FramePackage::Intrinsics intrinsics;

	intrinsics.fx = fx;
	intrinsics.fy = fy;
	intrinsics.cx = cx;
	intrinsics.cy = cy;

	CameraFrameColor cfc = CameraFrameColor(color_timestamp, cwidth, cheight, CameraFrame::EncodingType::ET_8U3_BGR, intrinsics, cframe);
	CameraFrameDepth cfd = CameraFrameDepth(depth_timestamp, dwidth, dheight, depth_scale, CameraFrame::EncodingType::ET_16U1_GRAY, intrinsics, dframe);

	FramePackage::Groundtruth groundtruth(tx, ty, tz, qx, qy, qz, qw);

	if (!groundtruth_ready)
	{
		frame_package = std::make_shared<FramePackageRGBD>(cfc, cfd);
	}
	else
	{
		groundtruth_ready = false;

		frame_package = std::make_shared<FramePackageRGBD>(cfc, cfd, groundtruth);
	}

	return true;
}

bool racoon::rvdslam::DriverWrapperRGBDFileReader::readNextColorFrame()
{
	if (!color_file.good())
		return false;

	std::string png_rel_path;

	std::string line;

	do
	{
		if (!std::getline(color_file, line))
			return false;
	} while (line.at(0) == '#');

	std::stringstream line_str;

	line_str.str(line);

	line_str >> color_timestamp >> png_rel_path;

	std::string png_abs_path = settings.path + "/" + png_rel_path;
	
	cv::Mat image = cv::imread(png_abs_path, cv::IMREAD_COLOR);

	cwidth = image.cols;
	cheight = image.rows;

	cframe = (unsigned char*)realloc(cframe, image.cols * image.rows * 3 * sizeof(unsigned char));

	std::copy(image.data, image.data + image.cols * image.rows * 3, cframe);

	//unsigned width = 512, height = 424;
	//unsigned char* tempframe;

	//int error = lodepng_decode_file(&tempframe, &width, &height, png_abs_path.c_str(), LodePNGColorType::LCT_RGB, 8);

	//unsigned char* ccurr = cframe;
	//unsigned char* cend = cframe + width * height * 3;
	//unsigned char* tempcurr = tempframe;

	//convert char array from rgb to bgr order
	//while (ccurr < cend)
	//{
	//	*ccurr++ = *(tempcurr + 2);
	//	*ccurr++ = *(tempcurr + 1);
	//	*ccurr++ = *(tempcurr + 0);
	//
	//	tempcurr = tempcurr + 3;
	//}

	//free(tempframe);

	return true;
}

bool racoon::rvdslam::DriverWrapperRGBDFileReader::readNextDepthFrame()
{
	if (!depth_file.good())
		return false;

	std::string png_rel_path;

	std::string line;

	do
	{
		if (!std::getline(depth_file, line))
			return false;
	} while (line.at(0) == '#');

	std::stringstream line_str;

	line_str.str(line);

	line_str >> depth_timestamp >> png_rel_path;

	std::string png_abs_path = settings.path + "/" + png_rel_path;

	cv::Mat image = cv::imread(png_abs_path, cv::IMREAD_ANYDEPTH);

	dwidth = image.cols;
	dheight = image.rows;

	dframe = (unsigned short*)realloc(dframe, image.cols * image.rows * sizeof(unsigned short));

	//unsigned width = 512, height = 424;
	//unsigned char* tempframe;

	//int error = lodepng_decode_file(&tempframe, &width, &height, png_abs_path.c_str(), LodePNGColorType::LCT_GREY, 16);

	//unsigned short* dcurr = dframe;
	//unsigned short* dend = dframe + width * height;
	//unsigned char* tempcurr = tempframe;

	//convert char array to short array, using big endian
	//while (dcurr < dend)
	//{
	//	*dcurr = (((unsigned short)*tempcurr) << 8) + (unsigned short)*(tempcurr + 1);
	//
	//	dcurr++;
	//	tempcurr = tempcurr + 2;
	//}

	unsigned short* dcurr = dframe;
	unsigned short* dend = dframe + image.cols * image.rows;
	unsigned char* tempcurr = image.data;

	//convert char array to short array, using little endian
	while (dcurr < dend)
	{
		*dcurr = (((unsigned short)*(tempcurr + 1)) << 8) + (unsigned short)*(tempcurr);
	
		dcurr++;
		tempcurr = tempcurr + 2;
	}

	//free(tempframe);

	return true;
}

bool racoon::rvdslam::DriverWrapperRGBDFileReader::readNextGroundtruthPose()
{
	if (!groundtruth_file.good())
		return false;

	std::string line;

	double timestamp = 0.0;

	do
	{
		if (!std::getline(groundtruth_file, line))
			return false;
	
		if (line.at(0) == '#')
			continue;

		std::stringstream line_str;

		line_str.str(line);

		line_str >> timestamp >> tx >> ty >> tz >> qx >> qy >> qz >> qw;
	} while (timestamp < depth_timestamp);
	
	return true;
}

bool racoon::rvdslam::DriverWrapperRGBDFileReader::readIntrinsics()
{
	std::ifstream intrinsics_file(settings.path + "/intrinsics.txt");

	if (!intrinsics_file.good())
		return false;

	std::string line;

	do
	{
		if (!std::getline(intrinsics_file, line))
			return false;
	} while (line.at(0) == '#');

	std::stringstream line_str;

	line_str.str(line);

	line_str >> fx >> fy >> cx >> cy;
	
	return true;
}

bool racoon::rvdslam::DriverWrapperRGBDFileReader::readDepthScale()
{
	std::ifstream scale_file(settings.path + "/scale.txt");

	if (!scale_file.good())
		return false;

	std::string line;

	do
	{
		if (!std::getline(scale_file, line))
			return false;
	} while (line.at(0) == '#');

	std::stringstream line_str;

	line_str.str(line);

	line_str >> depth_scale;
	
	return true;
}
