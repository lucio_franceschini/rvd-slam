#include "stdafx.h"

#include "VisWindow.h"

#include <vtkRenderWindow.h>
#include <QIcon>
#include <QStyle>
#include <QDesktopWidget>

racoon::rvdslam::VisWindow::VisWindow(ViewManager & view_manager, QWidget *parent) : view_manager(view_manager), QMainWindow(parent)
{
	ui.setupUi(this);

	this->setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignRight | Qt::AlignBottom, this->size(), qApp->desktop()->availableGeometry()));
	this->setWindowTitle("Visualization");
	this->setWindowIcon(QIcon("racoon.jpg"));

	// Set up the QVTK window
	viewer.reset(new pcl::visualization::PCLVisualizer("viewer", false));
	ui.qvtkWidget->SetRenderWindow(viewer->getRenderWindow());
	viewer->setupInteractor(ui.qvtkWidget->GetInteractor(), ui.qvtkWidget->GetRenderWindow());
	ui.qvtkWidget->update();

	// Initialize pcl viewer
	viewer->setBackgroundColor(0, 0, 0);
	viewer->addCoordinateSystem(0.5);
	viewer->initCameraParameters();
	viewer->setCameraPosition(0, 0, -2, 0, 0, 1, 0, 1, 0);

	pcl::PointXYZ text_position(-0.025, -0.025, -0.025);
	viewer->addText3D("World origin", text_position, 0.075, 1.0, 1.0, 1.0);

	Eigen::Affine3f initial_pose = Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitZ()) * Eigen::Affine3f::Identity();

		//viewer->addCoordinateSystem(0.5, initial_pose, "groundtruth coordinate system");

	viewer->addPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>()), "cloud");
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud");

	//viewer->addPolygonMesh(*pcl::PolygonMesh::Ptr(new pcl::PolygonMesh()), "mesh");
	//viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_PHONG, "mesh");

	ui.qvtkWidget->update();
}

racoon::rvdslam::VisWindow::~VisWindow()
{

}

void racoon::rvdslam::VisWindow::updatePointCloud(std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloud, const Eigen::Affine3f & pose)
{
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcl_cloud(cloud.get());

	if (!viewer->updatePointCloud(pcl_cloud, "cloud"))
		viewer->addPointCloud(pcl_cloud, "cloud");

	viewer->updatePointCloudPose("cloud", pose);

	ui.qvtkWidget->update();
}

void racoon::rvdslam::VisWindow::updateMesh(std::shared_ptr<pcl::PolygonMesh> mesh)
{
	//if (!viewer->updatepolygonmesh(*mesh, "mesh"))
	//	viewer->addpolygonmesh(*mesh, "mesh");

	viewer->removePolygonMesh("mesh");
	viewer->addPolygonMesh(*mesh, "mesh");

	ui.qvtkWidget->update();
}

void racoon::rvdslam::VisWindow::removeMesh()
{
	viewer->removePolygonMesh("mesh");
}

void racoon::rvdslam::VisWindow::updateCameraPose(const Eigen::Affine3f & pose)
{
	if (!viewer->updateCoordinateSystemPose("estimated pose coordinate system", pose))
		viewer->addCoordinateSystem(0.5, pose, "estimated pose coordinate system");

	pcl::PointXYZ text_position(pose.translation().x() - 0.025, pose.translation().y() - 0.025, pose.translation().z() - 0.025);

	//viewer->removeText3D("estimated pose text");
	//viewer->addText3D("Estimated pose", text_position, 0.075, 1.0, 1.0, 1.0, "estimated pose text");

	ui.qvtkWidget->update();
}

void racoon::rvdslam::VisWindow::updateGroundtruthPose(const Eigen::Affine3f & pose)
{
	if (!viewer->updateCoordinateSystemPose("groundtruth coordinate system", pose))
		viewer->addCoordinateSystem(0.5, pose,"groundtruth coordinate system");

	pcl::PointXYZ text_position(pose.translation().x() - 0.025, pose.translation().y() - 0.025, pose.translation().z() - 0.025);

	//viewer->removeText3D("groundtruth text");
	//viewer->addText3D("Groundtruth pose", text_position, 0.075, 1.0, 1.0, 1.0, "groundtruth text");

	ui.qvtkWidget->update();
}

void racoon::rvdslam::VisWindow::removePoses()
{
	viewer->removeCoordinateSystem("estimated pose coordinate system");
	viewer->removeCoordinateSystem("groundtruth coordinate system");

	//viewer->removeText3D("estimated pose text");
	//viewer->removeText3D("groundtruth text");
}