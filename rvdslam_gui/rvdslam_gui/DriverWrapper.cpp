#include "stdafx.h"

#include "DriverWrapper.h"

racoon::rvdslam::DriverWrapper::DriverWrapper()
{
}

racoon::rvdslam::DriverWrapper::~DriverWrapper()
{
}

bool racoon::rvdslam::DriverWrapper::doRunning()
{
	std::shared_ptr<FramePackage> frame_package;

	if (getFramePackage(frame_package))
	{
		emit newFramePackageReady(frame_package);
		switchState(GenericWrapper::State::RUNNING);
		return true;
	}
	else
	{
		return false;
	}
}