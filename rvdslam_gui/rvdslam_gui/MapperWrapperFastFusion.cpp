#include "stdafx.h"

#include "MapperWrapperFastFusion.h"

#include "FramePackage.h"

#include <opencv2/core/eigen.hpp>
#include <pcl/io/obj_io.h>

racoon::rvdslam::MapperWrapperFastFusion::FastFusionSettings::FastFusionSettings(float voxel_scale, float max_depth)
	: voxel_scale(voxel_scale), max_depth(max_depth)
{
}

racoon::rvdslam::MapperWrapperFastFusion::MapperWrapperFastFusion(FastFusionSettings settings) : settings(settings), fast_fusion(new FusionMipMapCPU(0,0,0,settings.voxel_scale,0.01))
{
}

racoon::rvdslam::MapperWrapperFastFusion::~MapperWrapperFastFusion()
{
	free(fast_fusion); // FastFusionMipMapCPU fast_fusion should be deleted here instead of freed, but its destructor causes errors. This could cause a memory leak.
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::MapperWrapperFastFusion::getSettings() const
{
	auto settings = std::unique_ptr<Settings>(new FastFusionSettings(this->settings));

	return std::move(settings);
}

void racoon::rvdslam::MapperWrapperFastFusion::setSettings(const racoon::rvdslam::GenericWrapper::Settings & settings)
{
	auto fastfusion_settings = dynamic_cast<const FastFusionSettings &>(settings);

	this->settings = fastfusion_settings;

	switchState(State::RESET);
}

bool racoon::rvdslam::MapperWrapperFastFusion::doInit()
{
	fast_fusion->setThreadMeshing(true);
	fast_fusion->setIncrementalMeshing(true);

	return true;
}

bool racoon::rvdslam::MapperWrapperFastFusion::doReset()
{
	Sleep(1000);	// Workaround to try to make sure meshing thread has finished

	free(fast_fusion); // FastFusionMipMapCPU fast_fusion should be deleted here instead of freed, but its destructor causes errors. This could cause a memory leak.

	fast_fusion = new FusionMipMapCPU(0,0,0,settings.voxel_scale,0.01);

	return doInit();
}

void racoon::rvdslam::MapperWrapperFastFusion::updateMap(const FramePackage & frame_package)
{
	Eigen::Affine3d pose = frame_package.getEstimatedPose();

	CameraInfo cam_info = createCameraInfo(frame_package, pose);

	fast_fusion->addMap(frame_package.getDepthImage(), cam_info, frame_package.getColorImage(), 1.0f / (frame_package.getDepthImageScale() * 1.0f), settings.max_depth);	//3.0f);

	fast_fusion->updateMeshes();
}

std::shared_ptr<pcl::PolygonMesh> racoon::rvdslam::MapperWrapperFastFusion::getMesh()
{
	MeshInterleaved mesh = fast_fusion->getMeshInterleavedMarchingCubes();

	pcl::PointCloud<pcl::PointXYZRGB> cloud;

	if (mesh.vertices.size() == 0)
		return std::make_shared<pcl::PolygonMesh>();

	cloud.reserve(mesh.vertices.size());

	std::vector<Color3b>::iterator itc = mesh.colors.begin();

	for (std::vector<Vertex3f>::iterator itv = mesh.vertices.begin(); itv != mesh.vertices.end(); itv++, itc++)
	{
		pcl::PointXYZRGB point;

		point.x = itv->x;
		point.y = itv->y;
		point.z = itv->z;

		point.r = itc->r;
		point.g = itc->g;
		point.b = itc->b;

		cloud.push_back(point);
	}

	std::shared_ptr<pcl::PolygonMesh> mesh_pcl = std::make_shared<pcl::PolygonMesh>();

	pcl::toPCLPointCloud2(cloud, mesh_pcl->cloud);

	mesh_pcl->polygons.reserve(mesh.faces.size() / 3);

	for (std::vector<unsigned int>::iterator it = mesh.faces.begin(); it != mesh.faces.end(); it += 3)
	{
		pcl::Vertices vertices;

		vertices.vertices = std::vector<unsigned int>(it, it+3);

		mesh_pcl->polygons.push_back(vertices);
	}

	return mesh_pcl;
}

std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> racoon::rvdslam::MapperWrapperFastFusion::getPointCloud()
{
	return std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>>(new pcl::PointCloud<pcl::PointXYZRGB>());
}

CameraInfo racoon::rvdslam::MapperWrapperFastFusion::createCameraInfo(const FramePackage & frame_package, const Eigen::Affine3d & pose)
{
	CameraInfo cam_info;

	FramePackage::Intrinsics intrinsics = frame_package.getImageIntrinsics();

	const float fx = intrinsics.fx;
	const float fy = intrinsics.fy;
	const float cx = intrinsics.cx;
	const float cy = intrinsics.cy;

	cv::Mat intrinsic = cv::Mat::eye(3, 3, cv::DataType<double>::type);

	intrinsic.at<double>(0, 0) = fx;
	intrinsic.at<double>(1, 1) = fy;
	intrinsic.at<double>(0, 2) = cx;
	intrinsic.at<double>(1, 2) = cy;

	cam_info.setIntrinsic(intrinsic);

	cv::Mat rotation;
	cv::Mat translation;

	Eigen::Matrix<double, 3, 1> translation_eigen = pose.translation();

	cv::eigen2cv(pose.rotation(), rotation);
	cv::eigen2cv(translation_eigen, translation);

	cam_info.setRotation(rotation);
	cam_info.setTranslation(translation);

	return cam_info;
}
