#include "stdafx.h"

#include "DriverWrapperKinectV1.h"
#include "FramePackageRGBD.h"

racoon::rvdslam::DriverWrapperKinectV1::DriverWrapperKinectV1() : error(false), sensor(NULL)
{
	cframe = (unsigned char*)malloc(640 * 480 * 3 * sizeof(unsigned char));
	dframe = (unsigned short*)malloc(640 * 480 * sizeof(unsigned short));
}

racoon::rvdslam::DriverWrapperKinectV1::~DriverWrapperKinectV1()
{
	if (sensor)
	{
		sensor->NuiShutdown();
		sensor->Release();
	}

	free(cframe);
	free(dframe);
}

bool racoon::rvdslam::DriverWrapperKinectV1::doInit()
{
	return initKinect();
}

bool racoon::rvdslam::DriverWrapperKinectV1::doReset()
{
	if (sensor)
	{
		sensor->NuiShutdown();
		sensor->Release();
	}

	return initKinect();
}

bool racoon::rvdslam::DriverWrapperKinectV1::getFramePackage(std::shared_ptr<FramePackage>& frame_package)
{	
	if (error)
		return false;

	if (!acquireKinectColorData() || !acquireKinectDepthData())
		return false;

	FramePackage::Intrinsics intrinsics;

	intrinsics.fx = 525.0;
	intrinsics.fy = 525.0;
	intrinsics.cx = 319.5;
	intrinsics.cy = 239.5;

	CameraFrameColor cfc = CameraFrameColor((double)(ctimestamp * 1.0 / 1000.0), 640, 480, CameraFrame::EncodingType::ET_8U3_BGR, intrinsics, cframe);
	CameraFrameDepth cfd = CameraFrameDepth((double)(dtimestamp * 1.0 / 1000.0), 640, 480, 1000, CameraFrame::EncodingType::ET_16U1_GRAY, intrinsics, dframe);

	frame_package = std::make_shared<FramePackageRGBD>(cfc, cfd);

	return true;
}

bool racoon::rvdslam::DriverWrapperKinectV1::acquireKinectColorData()
{
	NUI_IMAGE_FRAME imageFrame;
	NUI_LOCKED_RECT LockedRect;

	if (sensor->NuiImageStreamGetNextFrame(rgbStream, 100, &imageFrame) < 0)
		return false;

	ctimestamp = imageFrame.liTimeStamp.LowPart;

	INuiFrameTexture* texture = imageFrame.pFrameTexture;
	texture->LockRect(0, &LockedRect, NULL, 0);

	if (LockedRect.Pitch != 0)
	{
		const BYTE* curr = (const BYTE*)LockedRect.pBits;
		//const BYTE* dataEnd = curr + (640*480) * 4;

		BYTE* dest = cframe + ((640 - 1) * 3);

		for (int v = 0; v < 480; v++)
		{
			for (int u = 0; u < 640; u++)
			{
				*dest++ = *curr++;
				*dest++ = *curr++;
				*dest++ = *curr++;
				curr++;

				dest -= 2 * 3;
			}

			dest += 640 * 2 * 3;
		}
	}

	texture->UnlockRect(0);
	sensor->NuiImageStreamReleaseFrame(rgbStream, &imageFrame);

	return true;
}

bool racoon::rvdslam::DriverWrapperKinectV1::acquireKinectDepthData()
{
	NUI_IMAGE_FRAME imageFrame;
	NUI_LOCKED_RECT LockedRect;

	if (sensor->NuiImageStreamGetNextFrame(depthStream, 100, &imageFrame) < 0)
		return false;

	dtimestamp = imageFrame.liTimeStamp.LowPart;

	INuiFrameTexture* texture = imageFrame.pFrameTexture;
	texture->LockRect(0, &LockedRect, NULL, 0);

	if (LockedRect.Pitch != 0)
	{
		const unsigned short * curr = (const unsigned short *)LockedRect.pBits;
		//const unsigned short * dataEnd = curr + (640 * 480);
		//unsigned short * dest = dframe;

		//while (curr < dataEnd)
		//{
		//	*dest++ = NuiDepthPixelToDepth(*curr++);
		//}

		unsigned short * dest = dframe + (640 - 1);

		for (int v = 0; v < 480; v++)
		{
			for (int u = 0; u < 640; u++)
			{
				*dest-- = NuiDepthPixelToDepth(*curr++);
			}

			dest += 640 * 2;
		}
	}

	texture->UnlockRect(0);
	sensor->NuiImageStreamReleaseFrame(depthStream, &imageFrame);

	return true; 
}

bool racoon::rvdslam::DriverWrapperKinectV1::initKinect()
{
	// Get a working kinect sensor
	int numSensors;
	if (NuiGetSensorCount(&numSensors) < 0 || numSensors < 1) return false;
	if (NuiCreateSensorByIndex(0, &sensor) < 0) return false;

	// Initialize sensor
	sensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_DEPTH | NUI_INITIALIZE_FLAG_USES_COLOR);
	sensor->NuiImageStreamOpen(
		NUI_IMAGE_TYPE_COLOR,            // Depth camera or rgb camera?
		NUI_IMAGE_RESOLUTION_640x480,    // Image resolution
		0,      // Image stream flags, e.g. near mode
		2,      // Number of frames to buffer
		NULL,   // Event handle
		&rgbStream);
	sensor->NuiImageStreamOpen(
		NUI_IMAGE_TYPE_DEPTH,            // Depth camera or rgb camera?
		NUI_IMAGE_RESOLUTION_640x480,    // Image resolution
		0,      // Image stream flags, e.g. near mode
		2,      // Number of frames to buffer
		NULL,   // Event handle
		&depthStream);

	return true;
}