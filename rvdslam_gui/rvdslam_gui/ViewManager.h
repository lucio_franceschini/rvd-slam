#pragma once

#include "MainWindow.h"
#include "VisWindow.h"
#include "DriverManager.h"
#include "DriverWrapper.h"
#include "SLAMPipeline.h"
#include "TrackerWrapper.h"
#include "MapperWrapper.h"

#include <string>
#include <thread>
#include <mutex>
#include <pcl/visualization/cloud_viewer.h>

#include <opencv2/core/core.hpp>

#include <QObject>

namespace racoon
{
	namespace rvdslam
	{
		class RVDSLAM;
		class FramePackage;
		class DataManager;

		/**
	     * This class acts as an intermediary between front and back end.
		 * It manages the GUI windows of the application.
		 * All requests from the GUI to the data processing parts of the application and vice versa pass through the view manager.
		 */
		class ViewManager : public QObject
		{
			Q_OBJECT
		public:
			/**
			* Constructor
			*
			* Opens main and visualization window.
			*/
			ViewManager(RVDSLAM & rvdslam);

			/** Default destructor */
			~ViewManager();

			/** Sets the DriverManager */
			void setDriverManager(DriverManager * driver_manager);

			/** Sets the SLAMPipeline */
			void setPipeline(SLAMPipeline * slam_pipeline);

			/** Sets the DataManager */
			void setDataManager(DataManager * data_manager);

			/** Displays the driver output color/depth image pair contained in the given frame_package */
			void viewDriverFrames(std::shared_ptr<const FramePackage> frame_package) const;

			/** Displays the tracker input color/depth image pair contained in the given frame_package */
			void viewTrackerFrames(std::shared_ptr<const FramePackage> frame_package) const;

			/** Displays the mapper input color/depth image pair contained in the given frame_package */
			void viewMapperFrames(std::shared_ptr<const FramePackage> frame_package) const;

			/** Displays the given point cloud with the given pose of the origin */
			void updatePointCloud(std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloud, const Eigen::Affine3f & pose = Eigen::Affine3f::Identity());

			/** Displays the given mesh */
			void updateMesh(std::shared_ptr<pcl::PolygonMesh> mesh);

			/** Displays the given estimated camera pose */
			void updateCameraPose(const Eigen::Affine3f & pose);

			/** Displays the given ground truth pose */
			void updateGroundtruthPose(const Eigen::Affine3f & pose);

			/** Displays the given frame in a new OpenCV window with the given window_title */
			void viewFrame(cv::Mat frame, const std::string window_title) const;

			/** Forwards the request to switch the currently running driver module to the DriverManager */
			void switchDriver(DriverManager::DriverType driver_type);

			/** Forwards the request to start the driver module to the DriverManager */
			void startDriver();

			/** Forwards the request to pause the driver module to the DriverManager */
			void pauseDriver();

			/** Forwards the request to resume the driver module to the DriverManager */
			void resumeDriver();

			/** Forwards the request to reset the driver module to the DriverManager */
			void resetDriver();

			/** Requests the state of the driver module state machine from the DriverManager */
			GenericWrapper::State getDriverState();

			/** Forwards the request to switch the state of the driver module to the DriverManager */
			void setDriverState(GenericWrapper::State driver_state);

			/** Requests a pointer to the driver module settings struct from the DriverManager */
			std::unique_ptr<GenericWrapper::Settings> getDriverSettings();

			/** Forwards the request to set the driver settings to the DriverManager */
			void setDriverSettings(std::shared_ptr<const GenericWrapper::Settings> settings);

			/** Forwards the request to set the frame package filter to the DriverManager */
			void setFramePackageFilter(std::shared_ptr<FramePackage::Filter> filter);

			/** Forwards the request to remove the frame package filter to the DriverManager */
			void removeFramePackageFilter();

			/** Forwards the request to switch the currently running tracker module to the SLAMPipeline */
			void switchTracker(SLAMPipeline::TrackerType tracker_type);

			/** Forwards the request to start the tracker module to the SLAMPipeline and opens the visualization window */
			void startTracker();

			/** Forwards the request to pause the tracker module to the SLAMPipeline */
			void pauseTracker();

			/** Forwards the request to resume the tracker module to the SLAMPipeline */
			void resumeTracker();

			/** Forwards the request to reset the tracker module to the SLAMPipeline and removes the rendered poses */
			void resetTracker();

			/** Requests the state of the tracker module state machine from the SLAMPipeline */
			GenericWrapper::State getTrackerState();

			/** Forwards the request to switch the state of the tracker module to the SLAMPipeline */
			void setTrackerState(GenericWrapper::State tracker_state);

			/** Requests a pointer to the tracker module settings struct from the SLAMPipeline */
			std::unique_ptr<GenericWrapper::Settings> getTrackerSettings();

			/** Forwards the request to set the tracker settings to the SLAMPipeline */
			void setTrackerSettings(std::shared_ptr<const GenericWrapper::Settings> settings);

			/** Updates the tracker queue size display with the given size value */
			void setTrackerQueueSize(unsigned int queue_size);

			/** Forwards the request to set the tracker frame processing mode to the SLAMPipeline */
			void setTrackerProcessAll(bool process_all);

			/** Forwards the request to switch the currently running mapper module to the SLAMPipeline */
			void switchMapper(SLAMPipeline::MapperType mapper_type);

			/** Forwards the request to start the mapper module to the SLAMPipeline and opens the visualization window */
			void startMapper();

			/** Forwards the request to pause the mapper module to the SLAMPipeline */
			void pauseMapper();

			/** Forwards the request to resume the mapper module to the SLAMPipeline */
			void resumeMapper();

			/** Forwards the request to reset the mapper module to the SLAMPipeline and removes the rendered mesh */
			void resetMapper();

			/** Requests the state of the mapper module state machine from the SLAMPipeline */
			GenericWrapper::State getMapperState();

			/** Forwards the request to switch the state of the mapper module to the SLAMPipeline */
			void setMapperState(GenericWrapper::State mapper_state);

			/** Requests a pointer to the mapper module settings struct from the SLAMPipeline */
			std::unique_ptr<GenericWrapper::Settings> getMapperSettings();

			/** Forwards the request to set the mapper settings to the SLAMPipeline */
			void setMapperSettings(std::shared_ptr<const GenericWrapper::Settings> settings);

			/** Updates the mapper queue size display with the given size value */
			void setMapperQueueSize(unsigned int queue_size);

			/** Forwards the request to set the mapper frame processing mode to the SLAMPipeline */
			void setMapperProcessAll(bool process_all);

			/** Forwards the request to save the captured dataset to the data manager */
			void saveCapturedDataset(const std::string dirpath) const;

			/** Forwards the request to save the estimated camera trajectory to the data manager */
			void saveEstimatedTrajectory(const std::string filepath) const;

			/** Forwards the request to save the current 3D mesh to the data manager */
			void saveMesh(const std::string filepath) const;

			/** Updates the driver fps display with the given fps count */
			void setDriverFps(const double fps);

			/** Updates the tracker fps display with the given fps count */
			void setTrackerFps(const double fps);

			/** Updates the mapper fps display with the given fps count */
			void setMapperFps(const double fps);

			/** Updates the progress bar display for saving the captured dataset */
			void setDriverSavingProgress(int percent);
		private:
			MainWindow main_window;
			VisWindow vis_window;

			DriverManager * driver_manager;
			SLAMPipeline * slam_pipeline;
			DataManager * data_manager;
		};
	}
}