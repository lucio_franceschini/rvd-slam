#pragma once

#include "TrackerWrapper.h"
#include "DIFODO.h"

#include <limits>

namespace racoon
{
	namespace rvdslam
	{
		class FramePackage;

		/**
	     * Tracker module for the DIFODO tracker. Implements the TrackerWrapper interface.
		 */
		class TrackerWrapperDIFODO : public TrackerWrapper
		{
		public:
			/** Settings struct for the DIFODO tracker module */
			struct DIFODOSettings : public Settings
			{
				/**
				* Struct constructor
				*
				* @param downsample - downsampling factor by which the side length of the input image is divided
				* @param ctf_levels - number of pyramid levels
				* @param max_depth - maximum distance for a point in the input depth image to still be taken into account
				*/
				DIFODOSettings(unsigned int downsample = 2, unsigned int ctf_levels = 4, float max_depth = 10.0);

				unsigned int downsample;
				unsigned int ctf_levels;
				float max_depth;
			};

			/** Default constructor */
			TrackerWrapperDIFODO(DIFODOSettings settings = DIFODOSettings());

			/** Returns a pointer to the current settings struct */
			std::unique_ptr<Settings> getSettings() const override final;
		private:
			DIFODO difodo;
			DIFODOSettings settings;

			unsigned int cols;
			unsigned int rows;

			double timestamp_old;

			/** Initializes tracker module */
			bool doInit() override final;

			/** Resets tracker module */
			bool doReset() override final;

			/** Sets settings and applies them by resetting module */
			void setSettings(const Settings & settings) override final;

			/** Calls the setInputDepthImage(), loadFrame() and odometryCalcuation() methods of DIFODO to estimate the current camera pose based on the the given frame package */
			void computeNextPose(const FramePackage & frame_package) override final;

			/** Obtains the newly estimated camera pose from DIFODO and converts it into the Eigen::Affine3d data type */
			Eigen::Affine3d getPose() const override final;

			/**
			* Checks for changes in input image size or intrinsics.
			* If image size changed, resets module.
			* If intrinsics changed, calls setFOV() method of DIFODO with updated parameters.
			*/
			void checkMetadataChanges(const FramePackage & frame_package);
		};
	}
}