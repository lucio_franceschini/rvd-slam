#pragma once

#include "GenericWrapper.h"

#include <QObject>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include <memory>
#include <queue>

namespace racoon
{
	namespace rvdslam
	{
		class FramePackage;

		/**
		* This abstract class specifies the interface for a tracker module.
		* Provides a generic interface to a specific tracking library, hiding its implementation details.
		* Each tracker module needs to implement the doInit(), doReset(), computeNextPose() and getPose() methods.
		*/
		class TrackerWrapper : public GenericWrapper
		{
			Q_OBJECT
		public:
			/** Default constructor */
			TrackerWrapper();

			/** Virtual default destructor to avoid undefined behaviour when deleting base class pointer to derived class. To be implemented by derived class. */
			virtual ~TrackerWrapper();
			
			/**
			* Sets tracker input frame processing mode.
			* If parameter is true, all frame packages in input queue will be processed sequentially.
			* Otherwise, only the most recently added frame package will be processed, with the others being discarded.
			*/
			void setProcessAll(bool process_all);
		private:
			bool process_all;
			std::queue<std::shared_ptr<FramePackage>> input_queue;

			/** 
			 * Initializes tracker module. To be implemented by derived class. 
			 * Should return true if initialization successful, false otherwise.
			 */
			virtual bool doInit() = 0;

			/**
			 * Processes the next or latest frame package in the input queue (depending on the process_all variable).
			 *
			 * If input queue is empty, switches state to IDLE.
			 * Otherwise, the selected frame package is passed to the computeNextPose() method.
			 * Afterwards, the newly estimated pose is obtained by calling the getPose() method and the frame package is updated accordingly.
			 *
			 * At the end of processing, sets up another run.
			 */
			bool doRunning() override final;

			/** 
			 * Resets tracker module. To be implemented by derived class.
			 * Should return true if reset successful, false otherwise.
			 */
			virtual bool doReset() = 0;

			/** 
			 * To be implemented by derived class.
			 * Should call the specific functions of the designated tracking library
			 * to estimate the current camera pose based on the the given frame package.
			 */
			virtual void computeNextPose(const FramePackage & frame_package) = 0;

			/** 
			 * To be implemented by derived class.
			 * Should call the specific functions of the designated tracking library
			 * to obtain the newly estimated camera pose.
			 */
			virtual Eigen::Affine3d getPose() const = 0;
		public slots:
			/** Adds a frame package to the input queue */
			void addFramePackage(std::shared_ptr<FramePackage> frame_package);

			/** Clears the input queue */
			void clearQueue();
		signals:
			void poseEstimated(std::shared_ptr<FramePackage> frame_package);
			void queueSizeChanged(unsigned int queue_size);
		};
	}
}