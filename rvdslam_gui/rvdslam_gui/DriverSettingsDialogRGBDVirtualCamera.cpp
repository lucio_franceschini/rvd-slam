#include "stdafx.h"

#include "DriverSettingsDialogRGBDVirtualCamera.h"

#include "ViewManager.h"
#include "DriverWrapperRGBDVirtualCamera.h"

#include <QFileDialog>

racoon::rvdslam::DriverSettingsDialogRGBDVirtualCamera::DriverSettingsDialogRGBDVirtualCamera(ViewManager & view_manager, QWidget *parent)
	: view_manager(view_manager), QDialog(parent), path("")
{
	ui.setupUi(this);

	connect(ui.button_selectfolder, SIGNAL(clicked()), this, SLOT(selectFolderButtonPressed()));

	auto settings = view_manager.getDriverSettings();

	auto rgbdfilereader_settings = dynamic_cast<DriverWrapperRGBDVirtualCamera::RGBDVirtualCameraSettings*>(settings.get());

	std::string label_string = "Path: ";
	label_string += rgbdfilereader_settings->path;

	ui.label_folder->setText(QString::fromStdString(label_string));
}

racoon::rvdslam::DriverSettingsDialogRGBDVirtualCamera::~DriverSettingsDialogRGBDVirtualCamera()
{
}

void racoon::rvdslam::DriverSettingsDialogRGBDVirtualCamera::selectFolderButtonPressed()
{
	QString dir = QFileDialog::getExistingDirectory(this, "Choose directory of dataset", "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

	path = dir.toStdString();

	std::string label_string = "Path: ";
	label_string += path;

	ui.label_folder->setText(QString::fromStdString(label_string));
}

void racoon::rvdslam::DriverSettingsDialogRGBDVirtualCamera::accept()
{
	DriverWrapperRGBDVirtualCamera::RGBDVirtualCameraSettings settings;

	settings.path = path;

	auto settings_ptr = std::make_shared<DriverWrapperRGBDVirtualCamera::RGBDVirtualCameraSettings>(settings);

	view_manager.setDriverSettings(settings_ptr);

	done(QDialog::Accepted);
}