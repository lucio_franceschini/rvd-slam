#include "stdafx.h"

#include "SLAMPipeline.h"

#include "FramePackageRGBD.h"
#include "TrackerWrapperDIFODO.h"
#include "MapperWrapperFastFusion.h"
#include "MapperWrapperCPUTSDF.h"
#include "DataManager.h"
#include "ViewManager.h"
#include "DriverManager.h"

#include <QTimer>

#include <chrono>

racoon::rvdslam::SLAMPipeline::SLAMPipeline(
	 DataManager & data_manager,
	 ViewManager & view_manager,
	 TrackerType tracker_type,
	 MapperType mapper_type)
	 : data_manager(data_manager),
	   view_manager(view_manager),
	   systemtime_tracker_old(std::chrono::high_resolution_clock::now()), 
	   systemtime_mapper_old(std::chrono::high_resolution_clock::now())
{
	switchTracker(tracker_type);
	switchMapper(mapper_type);
}

racoon::rvdslam::SLAMPipeline::~SLAMPipeline()
{
	tracker_thread.quit();
	tracker_thread.wait();

	mapper_thread.quit();
	mapper_thread.wait();
}

void racoon::rvdslam::SLAMPipeline::setDriverManager(DriverManager * driver_manager)
{
	this->driver_manager = driver_manager;
}

void racoon::rvdslam::SLAMPipeline::switchTracker(TrackerType new_tracker_type)
{
	tracker_thread.quit();
	tracker_thread.wait();

	tracker.reset();

	switch(new_tracker_type)
	{
	case TrackerType::DIFODO:
		tracker = std::unique_ptr<TrackerWrapperDIFODO>(new TrackerWrapperDIFODO());
		break;
	}

	tracker->moveToThread(&tracker_thread);

	QObject::connect(this, &SLAMPipeline::initTrackerSignal, tracker.get(), &TrackerWrapper::init);
	QObject::connect(this, &SLAMPipeline::startTrackerSignal, tracker.get(), &TrackerWrapper::start);
	QObject::connect(this, &SLAMPipeline::pauseTrackerSignal, tracker.get(), &TrackerWrapper::pause);
	QObject::connect(this, &SLAMPipeline::resumeTrackerSignal, tracker.get(), &TrackerWrapper::resume);
	QObject::connect(this, &SLAMPipeline::resetTrackerSignal, tracker.get(), &TrackerWrapper::reset);
	QObject::connect(this, &SLAMPipeline::clearTrackerQueueSignal, tracker.get(), &TrackerWrapper::clearQueue);
	QObject::connect(this, &SLAMPipeline::applyTrackerSettings, tracker.get(), &TrackerWrapper::applySettings);
	QObject::connect(this, &SLAMPipeline::addTrackerFramePackage, tracker.get(), &TrackerWrapper::addFramePackage);
	QObject::connect(tracker.get(), &TrackerWrapper::stateChanged, this, &SLAMPipeline::handleTrackerStateChanged);
	QObject::connect(tracker.get(), &TrackerWrapper::queueSizeChanged, this, &SLAMPipeline::handleTrackerQueueSizeChanged);
    QObject::connect(tracker.get(), &TrackerWrapper::poseEstimated, this, &SLAMPipeline::handleTrackerPoseEstimated);

	tracker_thread.start();

	emit initTrackerSignal();

	clearMapperQueue();
	data_manager.clearPoseEstimatedFramePackages();
}

void racoon::rvdslam::SLAMPipeline::startTracker()
{
	emit startTrackerSignal();
}

void racoon::rvdslam::SLAMPipeline::pauseTracker()
{
	emit pauseTrackerSignal();
}

void racoon::rvdslam::SLAMPipeline::resumeTracker()
{
	emit resumeTrackerSignal();
}

void racoon::rvdslam::SLAMPipeline::resetTracker()
{
	emit resetTrackerSignal();

	clearMapperQueue();
	data_manager.clearPoseEstimatedFramePackages();
}

void racoon::rvdslam::SLAMPipeline::addFramePackage(std::shared_ptr<FramePackage> frame_package)
{
	emit addTrackerFramePackage(frame_package);
}

void racoon::rvdslam::SLAMPipeline::clearTrackerQueue()
{
	emit clearTrackerQueueSignal();
}

racoon::rvdslam::GenericWrapper::State racoon::rvdslam::SLAMPipeline::getTrackerState()
{
	return tracker_state;
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::SLAMPipeline::getTrackerSettings()
{
	return std::move(tracker->getSettings());
}

void racoon::rvdslam::SLAMPipeline::setTrackerSettings(std::shared_ptr<const racoon::rvdslam::GenericWrapper::Settings> settings)
{
	emit applyTrackerSettings(settings);
}

void racoon::rvdslam::SLAMPipeline::setTrackerProcessAll(bool process_all)
{
	tracker->setProcessAll(process_all);
}

void racoon::rvdslam::SLAMPipeline::handleTrackerQueueSizeChanged(unsigned int queue_size)
{
	view_manager.setTrackerQueueSize(queue_size);
}

void racoon::rvdslam::SLAMPipeline::handleTrackerStateChanged(GenericWrapper::State tracker_state)
{
	this->tracker_state = tracker_state;
	view_manager.setTrackerState(tracker_state);

	if (tracker_state == GenericWrapper::State::RESET)
	{
		data_manager.clearPoseEstimatedFramePackages();
		clearMapperQueue();
	}
}

void racoon::rvdslam::SLAMPipeline::handleTrackerPoseEstimated(std::shared_ptr<FramePackage> frame_package)
{
	view_manager.viewTrackerFrames(frame_package);

	if (frame_package->isGroundtruthSet())
	{
		Eigen::Affine3d groundtruth_offset = data_manager.getGroundtruthOffset();
		Eigen::Affine3d groundtruth = Eigen::AngleAxisd(M_PI, Eigen::Vector3d::UnitZ()) * groundtruth_offset * frame_package->getGroundtruth().getPoseMatrix();
		view_manager.updateGroundtruthPose(groundtruth.cast<float>());
	}

	view_manager.updateCameraPose(frame_package->getEstimatedPose().cast<float>());
	data_manager.addPoseEstimatedFramePackage(frame_package);

	addMapperFramePackage(frame_package);

	std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
	std::chrono::milliseconds timediff = std::chrono::duration_cast<std::chrono::milliseconds>(now - systemtime_tracker_old);

	double fps = 1000.0 / (timediff.count() * 1.0);

	systemtime_tracker_old = now;

	view_manager.setTrackerFps(fps);
}

void racoon::rvdslam::SLAMPipeline::switchMapper(MapperType new_mapper_type)
{
	mapper_thread.quit();
	mapper_thread.wait();
	
	mapper.reset();

	switch(new_mapper_type)
	{
	case MapperType::FASTFUSION:
		mapper = std::unique_ptr<MapperWrapperFastFusion>(new MapperWrapperFastFusion());
		break;
	}

	//mapper = std::make_unique<MapperWrapperCPUTSDF>();

	mapper->moveToThread(&mapper_thread);

	//QObject::connect(&mapper_thread, &QThread::finished, mapper.get(), &QObject::deleteLater);
	QObject::connect(this, &SLAMPipeline::initMapperSignal, mapper.get(), &MapperWrapper::init);
	QObject::connect(this, &SLAMPipeline::startMapperSignal, mapper.get(), &MapperWrapper::start);
	QObject::connect(this, &SLAMPipeline::pauseMapperSignal, mapper.get(), &MapperWrapper::pause);
	QObject::connect(this, &SLAMPipeline::resumeMapperSignal, mapper.get(), &MapperWrapper::resume);
	QObject::connect(this, &SLAMPipeline::resetMapperSignal, mapper.get(), &MapperWrapper::reset);
	QObject::connect(this, &SLAMPipeline::clearMapperQueueSignal, mapper.get(), &MapperWrapper::clearQueue);
	QObject::connect(this, &SLAMPipeline::applyMapperSettings, mapper.get(), &MapperWrapper::applySettings);
	QObject::connect(this, &SLAMPipeline::addMapperFramePackage, mapper.get(), &MapperWrapper::addFramePackage);
	QObject::connect(mapper.get(), &MapperWrapper::stateChanged, this, &SLAMPipeline::handleMapperStateChanged);
	QObject::connect(mapper.get(), &MapperWrapper::queueSizeChanged, this, &SLAMPipeline::handleMapperQueueSizeChanged);
	QObject::connect(mapper.get(), &MapperWrapper::updateDone, this, &SLAMPipeline::handleMapperUpdateDone);
	QObject::connect(mapper.get(), &MapperWrapper::meshReady, this, &SLAMPipeline::handleMapperMesh);
	QObject::connect(mapper.get(), &MapperWrapper::pointCloudReady, this, &SLAMPipeline::handleMapperPointCloud);
	
	mapper_thread.start();

	emit initMapperSignal();
}

void racoon::rvdslam::SLAMPipeline::startMapper()
{
	emit startMapperSignal();
}

void racoon::rvdslam::SLAMPipeline::pauseMapper()
{
	emit pauseMapperSignal();
}

void racoon::rvdslam::SLAMPipeline::resumeMapper()
{
	emit resumeMapperSignal();
}

void racoon::rvdslam::SLAMPipeline::resetMapper()
{
	emit resetMapperSignal();
}

void racoon::rvdslam::SLAMPipeline::clearMapperQueue()
{
	emit clearMapperQueueSignal();
}

racoon::rvdslam::GenericWrapper::State racoon::rvdslam::SLAMPipeline::getMapperState()
{
	return mapper_state;
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::SLAMPipeline::getMapperSettings()
{
	return std::move(mapper->getSettings());
}

void racoon::rvdslam::SLAMPipeline::setMapperSettings(std::shared_ptr<const racoon::rvdslam::GenericWrapper::Settings> settings)
{
	emit applyMapperSettings(settings);
}

void racoon::rvdslam::SLAMPipeline::setMapperProcessAll(bool process_all)
{
	mapper->setProcessAll(process_all);
}

void racoon::rvdslam::SLAMPipeline::handleMapperQueueSizeChanged(unsigned int queue_size)
{
	view_manager.setMapperQueueSize(queue_size);
}

void racoon::rvdslam::SLAMPipeline::handleMapperStateChanged(GenericWrapper::State mapper_state)
{
	this->mapper_state = mapper_state;
	view_manager.setMapperState(mapper_state);
}

void racoon::rvdslam::SLAMPipeline::handleMapperUpdateDone(std::shared_ptr<FramePackage> frame_package)
{
	view_manager.viewMapperFrames(frame_package);

	std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
	std::chrono::milliseconds timediff = std::chrono::duration_cast<std::chrono::milliseconds>(now - systemtime_mapper_old);

	double fps = 1000.0 / (timediff.count() * 1.0);

	systemtime_mapper_old = now;

	view_manager.setMapperFps(fps);
}

void racoon::rvdslam::SLAMPipeline::handleMapperMesh(std::shared_ptr<pcl::PolygonMesh> mesh)
{
	if (mesh->polygons.size() == 0)
		return;

	data_manager.setCurrentMesh(mesh);
	view_manager.updateMesh(mesh);
}

void racoon::rvdslam::SLAMPipeline::handleMapperPointCloud(std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> point_cloud)
{
	if (point_cloud->size() == 0)
		return;

	data_manager.setCurrentPointCloud(point_cloud);
	view_manager.updatePointCloud(point_cloud);
}