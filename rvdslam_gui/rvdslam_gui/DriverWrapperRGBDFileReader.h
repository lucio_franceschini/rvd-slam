#pragma once

#include "DriverWrapper.h"

#include <QObject>

#include <fstream>
#include <string> 

namespace racoon
{
	namespace rvdslam
	{
		class FramePackage;

		/**
	     * Device driver module for the RGBD file reader module. Implements the DriverWrapper interface.
		 * This module reads RGBD datasets from disk linearly, as fast (or slow) as possible. Frame rate depends on program execution speed.
		 */
		class DriverWrapperRGBDFileReader : public DriverWrapper
		{
		public:
			/** Settings struct for RGBD file reader device driver module */
			struct RGBDFileReaderSettings : public Settings
			{
				/**
				* Struct constructor
				*
				* @param path - Path to dataset
				*/
				RGBDFileReaderSettings(std::string path = "");

				std::string path;
			};

			/** Default constructor */
			DriverWrapperRGBDFileReader(RGBDFileReaderSettings settings = RGBDFileReaderSettings());

			/** Default destructor */
			~DriverWrapperRGBDFileReader();

			/** Returns a pointer to the current settings struct */
			std::unique_ptr<Settings> getSettings() const override final;
		private:
			RGBDFileReaderSettings settings;

			unsigned char* cframe;		// color frame raw data
			unsigned short* dframe;		// depth frame raw data

			unsigned int cwidth;		// color frame width in pixels
			unsigned int cheight;		// color frame height in pixels

			unsigned int dwidth;		// depth frame width in pixels
			unsigned int dheight;		// depth frame height in pixels

			double color_timestamp;		// color frame timestamp
			double depth_timestamp;		// depth frame timestamp

			double tx, ty, tz, qx, qy, qz, qw;	// groundtruth pose quaternion
			double fx, fy, cx, cy;				// dataset camera intrinsics
			unsigned int depth_scale;			// scale factor between raw depth values and depth in meters: depth [m] = depth [raw] / depth_scale

			std::ifstream color_file;			// file stream for text file containing ordered mapping between timestamp and relative path to corresponding rgb png file
			std::ifstream depth_file;			// file stream for text file containing ordered mapping between timestamp and relative path to corresponding depth png file
			std::ifstream groundtruth_file;		// file stream for text file containing ordered mapping between timestamp and groundtruth pose

			/** Initializes device driver module. Returns true if initialization successful, false otherwise. */
			bool doInit() override final;

			/** Resets device driver module. Returns true if reset successful, false otherwise. */
			bool doReset() override final;
			
			/** Sets settings and applies them by resetting module */
			void setSettings(const Settings & settings) override final;

			/** Reads the next rgb and depth frames from disk and assembles a frame package. Returns true if successful, false otherwise. */
			bool getFramePackage(std::shared_ptr<FramePackage>& frame_package) override final;

			/** Internal method for reading the next rgb frame from disk */
			bool readNextColorFrame();

			/** Internal method for reading the next depth frame from disk */
			bool readNextDepthFrame();

			/** Internal method for reading the next groundtruth pose from disk */
			bool readNextGroundtruthPose();

			/** Internal method for reading the intrinsic values from disk */
			bool readIntrinsics();

			/** Internal method for reading the depth scale value from disk */
			bool readDepthScale();
		};
	}
}