#pragma once

#include "ui_MainWindow.h"

#include "DriverSavingDatasetProgressDialog.h"
#include "DriverManager.h"
#include "SLAMPipeline.h"

#include <QtWidgets/QMainWindow>
#include <opencv2/core/core.hpp>

namespace racoon
{
	namespace rvdslam
	{
		class ViewManager;

		/**
	     * Main GUI window of the RVDSLAM application.
		 *
		 * Shows controls and state information for modules, queue sizes as well as current color/depth image pairs at module input/output.
		 */
		class MainWindow : public QMainWindow
		{
			Q_OBJECT
		public:
			/** Constructor */
			MainWindow(ViewManager & view_manager, QWidget *parent = 0);

			/** Default destructor */
			~MainWindow();

			/** Updates the driver output color frame pixmap with the image passed as the frame parameter */
			void viewDriverFrameColor(cv::Mat frame) const;

			/** Updates the driver output depth frame pixmap with the image passed as the frame parameter */
			void viewDriverFrameDepth(cv::Mat frame) const;

			/** Updates the tracker input color frame pixmap with the image passed as the frame parameter */
			void viewTrackerFrameColor(cv::Mat frame) const;

			/** Updates the tracker input depth frame pixmap with the image passed as the frame parameter */
			void viewTrackerFrameDepth(cv::Mat frame) const;

			/** Updates the mapper input color frame pixmap with the image passed as the frame parameter */
			void viewMapperFrameColor(cv::Mat frame) const;

			/** Updates the mapper input depth frame pixmap with the image passed as the frame parameter */
			void viewMapperFrameDepth(cv::Mat frame) const;

			/** Sets the tracker queue size label value according to the parameter */
			void setTrackerQueueSize(unsigned int queue_size);

			/** Sets the mapper queue size label value according to the parameter */
			void setMapperQueueSize(unsigned int queue_size);

			/** Sets the driver state label text according to the parameter */
			void setDriverState(GenericWrapper::State driver_state);

			/** Sets the tracker state label text according to the parameter */
			void setTrackerState(GenericWrapper::State tracker_state);

			/** Sets the mapper state label text according to the parameter */
			void setMapperState(GenericWrapper::State mapper_state);

			/** Sets the driver fps display value according to the parameter */
			void setDriverFps(const double fps);

			/** Sets the tracker fps display value according to the parameter */
			void setTrackerFps(const double fps);

			/** Sets the mapper fps display value according to the parameter */
			void setMapperFps(const double fps);

			/** Sets the progress bar value of the dataset saving dialog according to the parameter */
			void setDriverSavingProgress(int percent);
		private:
			Ui::MainWindow ui;
			ViewManager & view_manager;
			DriverSavingDatasetProgressDialog driver_saving_progress_dialog;
		private slots:
			/** Handles the change of driver selection in the GUI driver dropdown and forwards the request to the view manager */
			void driverChanged(int index);

			/** Handles the driver start/pause button press event and forwards the request to the view manager */
			void driverStartPauseButtonPressed();

			/** Handles the driver reset button press event and forwards the request to the view manager */
			void driverResetButtonPressed();

			/** Opens the settings window corresponding to the currently selected driver */
			void driverSettingsButtonPressed();

			/** Lets the user choose a directory, shows a progress dialog and forwards the request to save the current dataset to the view manager */
			void driverSaveDatasetButtonPressed();

			/** Adds/removes the frame package box filter with the values defined in the UI through the view manager */
			void boxfilterStateChanged(int state);

			/** Sets a new frame package box filter with the upadted values defined in the UI through the view manager */
			void boxfilterValueChanged();

			/** Handles the change of tracker selection in the GUI driver dropdown and forwards the request to the view manager */
			void trackerChanged(int index);

			/** Handles the tracker start/pause button press event and forwards the request to the view manager */
			void trackerStartPauseButtonPressed();

			/** Handles the tracker reset button press event and forwards the request to the view manager */
			void trackerResetButtonPressed();

			/** Opens the settings window corresponding to the currently selected tracker */
			void trackerSettingsButtonPressed();

			/** Lets the user choose a filepath and forwards the request to save the current trajectory to the view manager */
			void trackerSaveTrajectoryButtonPressed();

			/** Switches the tracker frame process mode between "all frames" and "most recent frame" through the view manager according to the UI radio button value */
			void trackerProcessAllToggled(bool checked);

			/** Switches the tracker frame process mode between "all frames" and "most recent frame" through the view manager according to the UI radio button value */
			void trackerProcessRecentToggled(bool checked);

			/** Handles the change of mapper selection in the GUI driver dropdown and forwards the request to the view manager */
			void mapperChanged(int index);

			/** Handles the mapper start/pause button press event and forwards the request to the view manager */
			void mapperStartPauseButtonPressed();

			/** Handles the mapper reset button press event and forwards the request to the view manager */
			void mapperResetButtonPressed();

			/** Opens the settings window corresponding to the currently selected mapper */
			void mapperSettingsButtonPressed();

			/** Lets the user choose a filepath and forwards the request to save the current mesh to the view manager */
			void mapperSaveMeshButtonPressed();

			/** Switches the mapper frame process mode between "all frames" and "most recent frame" through the view manager according to the UI radio button value */
			void mapperProcessAllToggled(bool checked);

			/** Switches the mapper frame process mode between "all frames" and "most recent frame" through the view manager according to the UI radio button value */
			void mapperProcessRecentToggled(bool checked);
		};
	}
}
