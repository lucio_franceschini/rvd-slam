#pragma once

#include "DriverManager.h"
#include "DataManager.h"
#include "ViewManager.h"
#include "SLAMPipeline.h"

#include <QtCore/qthread.h>
#include <Eigen/Core>
#include <pcl/PolygonMesh.h>

#include <fstream>
#include <memory>

namespace racoon
{
	namespace rvdslam
	{
		/**
		* Entry point class to instantiate RVDSLAM
		*/
		class RVDSLAM : public QObject
		{
			Q_OBJECT
		public:
			/**
			* Constructor
			*
			* Instantiates ViewManager, DataManager, SLAMPipeline and Driver Manager.
			*/
			RVDSLAM();

			/** Destructor */
			~RVDSLAM();
		private:
			DriverManager driver_manager;
			DataManager data_manager;
			ViewManager view_manager;
			SLAMPipeline slam_pipeline;

			std::ofstream file;
		};
	}
}