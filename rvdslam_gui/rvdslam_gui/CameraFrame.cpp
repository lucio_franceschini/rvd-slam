#include "stdafx.h"

#include "CameraFrame.h"

racoon::rvdslam::CameraFrame::CameraFrame(const double timestamp_secs, const unsigned int width_px, const unsigned int height_px, const EncodingType encoding_type, const FramePackage::Intrinsics intrinsics) : timestamp_secs(timestamp_secs), width_px(width_px), height_px(height_px), encoding_type(encoding_type), intrinsics(intrinsics)
{
}

racoon::rvdslam::CameraFrame::~CameraFrame()
{
}

double racoon::rvdslam::CameraFrame::getTimestampSecs() const
{
	return timestamp_secs;
}

int racoon::rvdslam::CameraFrame::getHeightPx() const
{
	return height_px;
}

int racoon::rvdslam::CameraFrame::getWidthPx() const
{
	return width_px;
}

int racoon::rvdslam::CameraFrame::getNumChannels() const
{
	switch (getEncodingType())
	{
	case EncodingType::ET_8U3_BGR:
	case EncodingType::ET_8U3_RGB:
		return 3;
		break;
	case EncodingType::ET_16U1_GRAY:
		return 1;
		break;
	}
}

racoon::rvdslam::CameraFrame::EncodingType racoon::rvdslam::CameraFrame::getEncodingType() const
{
	return encoding_type;
}

racoon::rvdslam::FramePackage::Intrinsics racoon::rvdslam::CameraFrame::getIntrinsics() const
{
	return intrinsics;
}
