#pragma once

#include <memory>

#include <Eigen/Core>

#include <opencv2/core/core.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace racoon
{
	namespace rvdslam
	{
		/**
	     * This abstract class specifies the interface for a frame package.
		 *
		 * FramePackage serves as the central data type for handling sensor data in RVDSLAM.
		 * Each FramePackage contains data frames from a sensor captured at one specific point in time.
		 * It provides a generic interface to access this data in form of a (possibly pre-filtered) color/depth image pair of identical size.
		 * Any type of sensor can be added to RVDSLAM as long as it is possible to transform the input data in such a way that it fits to this interface.
		 * Additionally, each FramePackage contains a timestamp, intrinsics for reprojecting the color/depth image, groundtruth and estimated pose.
		 */
		class FramePackage
		{
		public:
			/** This class represents the ground truth pose of the sensor */
			class Groundtruth
			{
			public:
				/** Default constructor */
				Groundtruth();

				/** Constructor with translational (vector t) and rotational (quaternion q) parameters */
				Groundtruth(double tx, double ty, double tz, double qx, double qy, double qz, double qw);

				/** Returns the ground truth as a 4x4 affine matrix */
				Eigen::Affine3d getPoseMatrix();
			private:
				Eigen::Quaterniond rotation;
				Eigen::Translation3d translation;
			};

			/** This class represents the intrinsic parameters of the sensor, to reproject the depth & color image pair */
			class Intrinsics
			{
			public:
				/** Default constructor */
				Intrinsics();

				/** Constructor with focal length (fx, fy in pixels) and principal point (cx, cy in pixels) as parameters */
				Intrinsics(float fx, float fy, float cx, float cy);

				float fx, fy;
				float cx, cy;

				/** Returns the intrinsic parameters as a 3x3 intrinsic matrix (fx, 0, cx; 0, fy, cy; 0, 0, 1) */
				Eigen::Matrix3f getIntrinsicMatrix();
			};

			/** Abstract base class for a frame package filter */
			class Filter
			{
			public:
				/** To be implemented by the derived class. Should return the color image with the filter applied (format CV_8UC3, channel order BGR). */
				virtual cv::Mat getColorImageFiltered(const FramePackage & frame_package) const = 0;

				/** To be implemented by the derived class. Should return the depth image with the filter applied (format CV_16U). */
				virtual cv::Mat getDepthImageFiltered(const FramePackage & frame_package) const = 0;
			};

			/** Cube filter implementation. Discards data points which lie outside of a cube specified by min and max values along each spacial dimension. */
			class CubeFilter : public Filter
			{
			public:
				/**
				 * CubeFilter constructor. Parameters specify the min and max values of the cube along each spacial dimension in meters.
				 *
				 * x-axis is pointing to the left along the horizontal axis of the image frame with its origin at the image center (principal point)
				 * y-axis is pointing upwards along the vertical axis of the image frame with its origin at the image center (principal point)
				 * z-axis is pointing out of the image frame with its origin on the image plane
				 */
				CubeFilter(float xmin, float xmax, float ymin, float ymax, float zmin, float zmax);

				/** Returns the color image with the filter applied */
				cv::Mat getColorImageFiltered(const FramePackage & frame_package) const;

				/** Returns the depth image with the filter applied */
				cv::Mat getDepthImageFiltered(const FramePackage & frame_package) const;
			private:
				float xmin, xmax, ymin, ymax, zmin, zmax;
			};

			/** Box filter implementation. Discards data points which lie outside of a box specified by min and max values along the image plane and perpendicular to it. */
			class BoxFilter : public Filter
			{
			public:
				/**
				 * BoxFilter constructor. Parameters specify the min and max values of the box along the image plane (in pixels) and perpendicular to it (in meters).
				 *
				 * u-axis is pointing to the right along the horizontal axis of the image frame with its origin at the top left if the image
				 * v-axis is pointing downwards along the vertical axis of the image frame with its origin at the top left if the image
				 * z-axis is pointing out of the image frame with its origin on the image plane
				 */
				BoxFilter(unsigned int umin, unsigned int umax, unsigned int vmin, unsigned int vmax, float zmin, float zmax);

				/** Returns the color image with the filter applied */
				cv::Mat getColorImageFiltered(const FramePackage & frame_package) const;

				/** Returns the depth image with the filter applied */
				cv::Mat getDepthImageFiltered(const FramePackage & frame_package) const;
			private:
				float umin, umax, vmin, vmax, zmin, zmax;
			};

			/** Default constructor to be called by subclass to initialize class members */
			FramePackage();

			/** Constructor with additional ground truth as parameter */
			FramePackage(const Groundtruth groundtruth);

			/** Virtual default destructor to avoid undefined behaviour when deleting base class pointer to derived class. To be implemented by derived class. */
			virtual ~FramePackage();

			/** Returns the ground truth pose */
			Groundtruth getGroundtruth() const;

			/** Returns whether the ground truth is set */
			bool isGroundtruthSet() const;

			/** Returns the estimated pose as a 4x4 affine matrix */
			Eigen::Affine3d getEstimatedPose() const;

			/** Sets the estimated pose with a 4x4 affine matrix as parameter */
			void setEstimatedPose(Eigen::Affine3d estimated_pose);

			/** Sets the filter to be applied. A newly set filter replaces the previously set filter. */
			void setFilter(std::shared_ptr<Filter> filter);

			/** Returns the (potentially filtered) color image as a cv Mat object (format CV_8UC3, channel order BGR) */
			cv::Mat getColorImage() const;

			/** Returns the (potentially filtered) depth image as a cv Mat object (format CV_16U) */
			cv::Mat getDepthImage() const;

			/** To be implemented by the derived class. Should return the timestamp of when the frame package's data was captured. */
			virtual double getTimestampSecs() const = 0;

			/**
			 * To be implemented by the derived class. Should return the depth image scale factor.
			 *
			 * Dividing a depth value from the depth image by the scale factor yields the distance in meters: depth [m] = depth [raw] / depth_scale
			 */
			virtual unsigned int getDepthImageScale() const = 0;

			/** To be implemented by the derived class. Should return the color and depth image common width in px. */
			virtual unsigned int getImageWidth() const = 0;

			/** To be implemented by the derived class. Should return the color and depth image common height in px. */
			virtual unsigned int getImageHeight() const = 0;

			/** To be implemented by the derived class. Should return the color and depth image common intrinsic parameters. */
			virtual Intrinsics getImageIntrinsics() const = 0;

			/**
			 * Returns a colored 3D point cloud, computed from the (potentially filtered) depth and color image,
			 * with the origin at the center of the image plane, z-axis pointing outwards and y-axis pointing upwards.
			 */
			pcl::PointCloud<pcl::PointXYZRGB> getPointCloud() const;
		private:
			Groundtruth groundtruth;
			bool groundtruth_set;
			Eigen::Affine3d estimated_pose;
			std::shared_ptr<Filter> filter;

			/**
			 * To be implemented by the derived class.
			 * Should return an unfiltered color image as seen from the origin of a common sensor coordinate frame (format CV_8UC3, channel order BGR).
			 * Raw data might have to be remapped (depth / color registration).
			 * Dimensions of both color and depth image need to be identical.
			 */
			virtual cv::Mat getColorImageUnfiltered() const = 0;

			/**
			 * To be implemented by the derived class.
			 * Should return an unfiltered depth image as seen from the origin of a common sensor coordinate frame (format CV_16U).
			 * Raw data might have to be remapped (depth / color registration).
			 * Dimensions of both color and depth image need to be identical.
			 */
			virtual cv::Mat getDepthImageUnfiltered() const = 0;

			/**
			 * Returns a colored 3D point cloud, computed from the unfiltered depth and color image,
			 * with the origin at the center of the image plane, z-axis pointing outwards and y-axis pointing upwards.
			 */
			pcl::PointCloud<pcl::PointXYZRGB> getPointCloudUnfiltered() const;
		};
	}
}