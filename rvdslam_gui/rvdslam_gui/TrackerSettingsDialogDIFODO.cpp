#include "stdafx.h"

#include "TrackerSettingsDialogDIFODO.h"

#include "ViewManager.h"
#include "TrackerWrapperDIFODO.h"

racoon::rvdslam::TrackerSettingsDialogDIFODO::TrackerSettingsDialogDIFODO(ViewManager & view_manager, QWidget *parent) : view_manager(view_manager), QDialog(parent)
{
	ui.setupUi(this);

	auto settings = view_manager.getTrackerSettings();

	auto difodo_settings = dynamic_cast<TrackerWrapperDIFODO::DIFODOSettings*>(settings.get());

	ui.spinbox_downsample->setValue(difodo_settings->downsample);
	ui.spinbox_ctflevels->setValue(difodo_settings->ctf_levels);
	ui.doublespinbox_maxdepth->setValue(difodo_settings->max_depth);
}

racoon::rvdslam::TrackerSettingsDialogDIFODO::~TrackerSettingsDialogDIFODO()
{
}

void racoon::rvdslam::TrackerSettingsDialogDIFODO::accept()
{
	TrackerWrapperDIFODO::DIFODOSettings settings;

	settings.downsample = ui.spinbox_downsample->value();
	settings.ctf_levels = ui.spinbox_ctflevels->value();
	settings.max_depth = ui.doublespinbox_maxdepth->value();

	auto settings_ptr = std::make_shared<TrackerWrapperDIFODO::DIFODOSettings>(settings);

	view_manager.setTrackerSettings(settings_ptr);

	done(QDialog::Accepted);
}