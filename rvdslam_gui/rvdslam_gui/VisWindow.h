#pragma once

#include "ui_VisWindow.h"

#include <QtWidgets/QMainWindow>
#include <pcl/visualization/cloud_viewer.h>

namespace racoon
{
	namespace rvdslam
	{
		class ViewManager;

		/**
	     * Visualization window of the RVDSLAM application.
		 *
		 * Displays camera poses and renders 3D mesh of environment reconstruction.
		 */
		class VisWindow : public QMainWindow
		{
			Q_OBJECT
		public:
			/** Constructor */
			VisWindow(ViewManager & view_manager, QWidget *parent = 0);

			/** Default destructor */
			~VisWindow();

			/** Renders the given point cloud in the display window with the given pose of the origin */
			void updatePointCloud(std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloud, const Eigen::Affine3f & pose = Eigen::Affine3f::Identity());

			/** Renders the given mesh in the display window */
			void updateMesh(std::shared_ptr<pcl::PolygonMesh> mesh);

			/** Removes the mesh from the display window */
			void removeMesh();

			/** Renders a coordinate system for the estimated camera pose in the display window */
			void updateCameraPose(const Eigen::Affine3f & pose);

			/** Renders a coordinate system for the ground truth pose in the display window */
			void updateGroundtruthPose(const Eigen::Affine3f & pose);

			/** Removes both coordinate systems from the display window */
			void removePoses();
		private:
			Ui::VisWindow ui;
			ViewManager & view_manager;
			boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
		};
	}
}
