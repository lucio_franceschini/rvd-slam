#include "stdafx.h"

#include "DataManager.h"

#include "ViewManager.h"
#include "FramePackage.h"

#include <pcl/common/transforms.h>
#include <pcl/io/vtk_lib_io.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <QString>
#include <QDir>

#include <fstream>

racoon::rvdslam::DataManager::DataManager(ViewManager & view_manager) : view_manager(view_manager)
{

}

void racoon::rvdslam::DataManager::setCurrentMesh(std::shared_ptr<pcl::PolygonMesh> mesh)
{
	current_mesh = mesh;
}

void racoon::rvdslam::DataManager::setCurrentPointCloud(std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloud)
{
	current_cloud = cloud;
}

void racoon::rvdslam::DataManager::addCapturedFramePackage(std::shared_ptr<FramePackage> frame_package)
{
	if (captured_frame_packages.size() == 0)
		groundtruth_offset = frame_package->getGroundtruth().getPoseMatrix().inverse();

	captured_frame_packages.push_back(frame_package);
}

void racoon::rvdslam::DataManager::addPoseEstimatedFramePackage(std::shared_ptr<FramePackage> frame_package)
{
	pose_estimated_frame_packages.push_back(frame_package);
}

void racoon::rvdslam::DataManager::clearCapturedFramePackages()
{
	captured_frame_packages.clear();
}

void racoon::rvdslam::DataManager::clearPoseEstimatedFramePackages()
{
	pose_estimated_frame_packages.clear();
}

Eigen::Affine3d racoon::rvdslam::DataManager::getGroundtruthOffset()
{
	return groundtruth_offset;
}

bool racoon::rvdslam::DataManager::saveCapturedDataset(const std::string dirpath) const
{
	QDir rootdir(QString::fromStdString(dirpath));

	std::string rgbpath = dirpath + "/rgb";
	QDir rgbdir(QString::fromStdString(rgbpath));

	std::string depthpath = dirpath + "/depth";
	QDir depthdir(QString::fromStdString(depthpath));

	if (!rootdir.exists())
		rootdir.mkpath(".");

	if (!rgbdir.exists())
		rootdir.mkdir("rgb");

	if (!depthdir.exists())
		rootdir.mkdir("depth");

	std::ofstream rgbfile(dirpath + "/rgb.txt", std::ofstream::trunc);
	std::ofstream depthfile(dirpath + "/depth.txt", std::ofstream::trunc);
	std::ofstream scalefile(dirpath + "/scale.txt", std::ofstream::trunc);
	std::ofstream intrinsicsfile(dirpath + "/intrinsics.txt", std::ofstream::trunc);

	if (!rgbfile.good() ||!depthfile.good() || !scalefile.good() || !intrinsicsfile.good())
		return false;

	intrinsicsfile.setf(ios::fixed);
	intrinsicsfile.precision(1);

	intrinsicsfile << "# rgb and depth image intrinsics" << std::endl
				   << "# fx fy cx cy" << std::endl;

	auto intrinsics = captured_frame_packages[0]->getImageIntrinsics();

	intrinsicsfile << intrinsics.fx << " " << intrinsics.fy << " " << intrinsics.cx << " " << intrinsics.cy << " " << std::endl;

	intrinsicsfile.close();

	scalefile << "# depth image scale factor" << std::endl
			  << captured_frame_packages[0]->getDepthImageScale() << std::endl;

	scalefile.close();

	rgbfile.setf(ios::fixed);
	rgbfile.precision(6);

	rgbfile << "# color images" << std::endl
		    << "# line format: timestamp filename" << std::endl;

	depthfile.setf(ios::fixed);
	depthfile.precision(6);

	depthfile << "# depth images" << std::endl
			  << "# line format: timestamp filename" << std::endl;

	int vector_size = captured_frame_packages.size();
	int index = 1;

	for (auto it = captured_frame_packages.begin(); it != captured_frame_packages.end(); it++, index++)
	{
		auto frame_package = *it;

		std::stringstream rgbimagepath_relative;
		rgbimagepath_relative << "rgb/" << std::fixed << std::setprecision(6) << frame_package->getTimestampSecs() << ".png";

		rgbfile << frame_package->getTimestampSecs() << " " << rgbimagepath_relative.str() << std::endl;

		std::stringstream rgbimagepath_absolute;
		rgbimagepath_absolute << dirpath << "/" << rgbimagepath_relative.str();

		cv::imwrite(rgbimagepath_absolute.str(), frame_package->getColorImage());

		std::stringstream depthimagepath_relative;
		depthimagepath_relative << "depth/" << std::fixed << std::setprecision(6) << frame_package->getTimestampSecs() << ".png";

		depthfile << frame_package->getTimestampSecs() << " " << depthimagepath_relative.str() << std::endl;

		std::stringstream depthimagepath_absolute;
		depthimagepath_absolute << dirpath << "/" << depthimagepath_relative.str();

		cv::imwrite(depthimagepath_absolute.str(), frame_package->getDepthImage());

		view_manager.setDriverSavingProgress(index * 100.0 / vector_size);
	}

	rgbfile.close();
	depthfile.close();

	return true;
}

bool racoon::rvdslam::DataManager::saveEstimatedTrajectory(const std::string filepath) const
{
	std::ofstream trajectory_file(filepath, std::ofstream::trunc);

	if (!trajectory_file.good())
		return false;

	trajectory_file.setf(ios::fixed);
	trajectory_file.precision(6);

	trajectory_file << "# estimated pose trajectory file" << std::endl
					<< "# line format: timestamp tx ty tz qx qy qz qw" << std::endl;

	for (auto it = pose_estimated_frame_packages.begin(); it != pose_estimated_frame_packages.end(); it++)
	{
		auto frame_package = *it;
		auto pose = frame_package->getEstimatedPose();
		auto translation = pose.translation();
		auto rotation = Eigen::Quaterniond(pose.linear());

		trajectory_file << frame_package->getTimestampSecs() << " " << translation.x() << " " << translation.y() << " " << translation.z() << " " 
			<< rotation.x() << " " << rotation.y() << " " << rotation.z() << " " << rotation.w() << std::endl;
	}

	trajectory_file.close();
	
	return true;
}

bool racoon::rvdslam::DataManager::saveMesh(const std::string filepath) const
{
	if (pcl::io::savePolygonFile(filepath, *current_mesh) == 0)
		return true;
	else return false;
}