#include "stdafx.h"

#include "CameraFrameDepth.h"

#include <opencv2/core/eigen.hpp>
#include <opencv2/highgui/highgui.hpp>

racoon::rvdslam::CameraFrameDepth::CameraFrameDepth() : CameraFrame(), depth_scale(1.0), depth_data(NULL)
{
}

racoon::rvdslam::CameraFrameDepth::CameraFrameDepth(const double timestamp_secs, const unsigned int width_px, const unsigned int height_px, const unsigned int depth_scale, const EncodingType encoding_type, const FramePackage::Intrinsics intrinsics, const unsigned short * depth_data) : CameraFrame(timestamp_secs, width_px, height_px, encoding_type, intrinsics), depth_scale(depth_scale)
{
	this->depth_data = (unsigned short*)malloc(getWidthPx() * getHeightPx() * getNumChannels() * sizeof(unsigned short));

	std::copy(depth_data, depth_data + getWidthPx() * getHeightPx() * getNumChannels(), this->depth_data);
}

racoon::rvdslam::CameraFrameDepth::CameraFrameDepth(const CameraFrameDepth & depth_frame) : CameraFrame(depth_frame.timestamp_secs, depth_frame.width_px, depth_frame.height_px, depth_frame.encoding_type, depth_frame.intrinsics), depth_scale(depth_frame.depth_scale)
{
	this->depth_data = (unsigned short*)malloc(getWidthPx() * getHeightPx() * getNumChannels() * sizeof(unsigned short));

	std::copy(depth_frame.depth_data, depth_frame.depth_data + getWidthPx() * getHeightPx() * getNumChannels(), this->depth_data);
}

racoon::rvdslam::CameraFrameDepth & racoon::rvdslam::CameraFrameDepth::operator=(const CameraFrameDepth & depth_frame)
{
	return CameraFrameDepth(depth_frame);
}

racoon::rvdslam::CameraFrameDepth::~CameraFrameDepth()
{
	free(depth_data);
}

unsigned int racoon::rvdslam::CameraFrameDepth::getDepthScale() const
{
	return depth_scale;
}

unsigned short * racoon::rvdslam::CameraFrameDepth::getDataRaw() const
{
	unsigned short* return_data = (unsigned short*)malloc(getWidthPx() * getHeightPx() * getNumChannels() * sizeof(unsigned short));

	std::copy(depth_data, depth_data + getWidthPx() * getHeightPx() * getNumChannels(), return_data);

	return return_data;
}

std::vector<unsigned short> racoon::rvdslam::CameraFrameDepth::getDataVector() const
{
	std::vector<unsigned short> return_vector(depth_data, depth_data + getWidthPx() * getHeightPx() * getNumChannels());

	return return_vector;
}

cv::Mat racoon::rvdslam::CameraFrameDepth::getDataCVMat() const
{
	cv::Mat temp_mat = cv::Mat(getHeightPx(), getWidthPx(), CV_16UC1, depth_data);

	cv::Mat return_mat = temp_mat.clone();

	//return_mat.create(getHeightPx(), getWidthPx(), CV_16UC1);
	//std::copy(depth_data, depth_data + getWidthPx() * getHeightPx() * getNumChannels(), return_mat.data);

	return return_mat;
}

Eigen::MatrixXf racoon::rvdslam::CameraFrameDepth::getDataEigenMatrixXf() const
{
	float * temp_data = (float*)malloc(getWidthPx() * getHeightPx() * getNumChannels() * sizeof(float));
	std::copy(depth_data, depth_data + getWidthPx() * getHeightPx() * getNumChannels(), temp_data);

	Eigen::MatrixXf return_mat = Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(temp_data, getHeightPx(), getWidthPx());

	return_mat *= (1.0f / (depth_scale * 1.0f));
	
	free(temp_data);

	return return_mat;
}
