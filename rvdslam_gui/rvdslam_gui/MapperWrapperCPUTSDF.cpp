#include "stdafx.h"

#include "MapperWrapperCPUTSDF.h"

#include "FramePackage.h"

#include <cpu_tsdf\marching_cubes_tsdf_octree.h>

racoon::rvdslam::MapperWrapperCPUTSDF::MapperWrapperCPUTSDF() : tsdf(new cpu_tsdf::TSDFVolumeOctree)
{
	tsdf->setGridSize(10., 10., 10.); // 10m x 10m x 10m
	tsdf->setResolution(2048, 2048, 2048); // Smallest cell size = 10m / 2048 = about half a centimeter
	tsdf->setIntegrateColor(true); // Set to true if you want the TSDF to store color
	//Eigen::Affine3d tsdf_center; // Optionally offset the center
	//tsdf->setGlobalTransform(tsdf_center);
	tsdf->reset(); // Initialize it to be empty
}

void racoon::rvdslam::MapperWrapperCPUTSDF::updateMap(const FramePackage & frame_package)
{
	Eigen::Affine3d pose = frame_package.getEstimatedPose();

	tsdf->integrateCloud(frame_package.getPointCloud(), pcl::PointCloud<pcl::PointXYZRGB>(), pose); // Integrate the cloud
	// Note, the normals aren't being used in the default settings. Feel free to pass in an empty cloud
}

std::shared_ptr<pcl::PolygonMesh> racoon::rvdslam::MapperWrapperCPUTSDF::getMesh()
{
	std::shared_ptr<pcl::PolygonMesh> mesh = std::make_shared<pcl::PolygonMesh>();

	cpu_tsdf::MarchingCubesTSDFOctree mc;

	mc.setInputTSDF(tsdf);
	mc.setMinWeight(0); // Sets the minimum weight -- i.e. if a voxel sees a point less than 2 times, it will not render  a mesh triangle at that location
	mc.setColorByRGB(true); // If true, tries to use the RGB values of the TSDF for meshing -- required if you want a colored mesh

	mc.reconstruct(*mesh);

	return mesh;
}

std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> racoon::rvdslam::MapperWrapperCPUTSDF::getPointCloud()
{
	//Not working yet

	std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloud = std::make_shared<pcl::PointCloud<pcl::PointXYZRGB>>();

	pcl::PointCloud<pcl::PointNormal>::Ptr raytraced = tsdf->renderView(); // Optionally can render it

	pcl::copyPointCloud(*raytraced, *cloud);

	return cloud;
}
