#pragma once

#include "GenericWrapper.h"

#include <QObject>

#include <memory>

namespace racoon
{
	namespace rvdslam
	{
		class FramePackage;

		/**
	     * This abstract class specifies the interface for a device driver module.
		 * Each device driver module needs to implement the doInit(), doReset() and getFramePackage() methods.
		 */
		class DriverWrapper : public GenericWrapper
		{
			Q_OBJECT
		public:
			/** Default constructor */
			DriverWrapper();

			/** Virtual default destructor to avoid undefined behaviour when deleting base class pointer to derived class. To be implemented by derived class. */
			virtual ~DriverWrapper();
		private:
			/** 
			 * Initializes device driver module. To be implemented by derived class. 
			 * Should return true if initialization successful, false otherwise.
			 */
			virtual bool doInit() = 0;

			/**
			 * Calls getFramePackage() to request a new frame package.
			 * Returns true if successful, emits signal newFramePackageReady() and sets up another run.
			 * Otherwise, returns false.
			 */
			bool doRunning() override final;

			/** 
			 * Resets device driver module. To be implemented by derived class.
			 * Should return true if reset successful, false otherwise.
			 */
			virtual bool doReset() = 0;

			/** 
			 * Main method to be implemented by derived class. Should produce a newly captured frame package every time it is called.
			 * Should return true if frame package capture successful, false otherwise.
			 */
			virtual bool getFramePackage(std::shared_ptr<FramePackage>& frame_package) = 0;
		signals:
			void newFramePackageReady(std::shared_ptr<FramePackage>);
		};
	}
}