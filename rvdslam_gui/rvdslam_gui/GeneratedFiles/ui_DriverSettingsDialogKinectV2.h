/********************************************************************************
** Form generated from reading UI file 'DriverSettingsDialogKinectV2.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DRIVERSETTINGSDIALOGKINECTV2_H
#define UI_DRIVERSETTINGSDIALOGKINECTV2_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QRadioButton>

QT_BEGIN_NAMESPACE

class Ui_DriverSettingsDialogKinectV2
{
public:
    QDialogButtonBox *buttonBox;
    QGroupBox *groupBox;
    QRadioButton *radiobutton_cameramode_color;
    QRadioButton *radiobutton_cameramode_infrared;

    void setupUi(QDialog *DriverSettingsDialogKinectV2)
    {
        if (DriverSettingsDialogKinectV2->objectName().isEmpty())
            DriverSettingsDialogKinectV2->setObjectName(QStringLiteral("DriverSettingsDialogKinectV2"));
        DriverSettingsDialogKinectV2->resize(171, 125);
        buttonBox = new QDialogButtonBox(DriverSettingsDialogKinectV2);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(10, 90, 151, 31));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        groupBox = new QGroupBox(DriverSettingsDialogKinectV2);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 151, 71));
        radiobutton_cameramode_color = new QRadioButton(groupBox);
        radiobutton_cameramode_color->setObjectName(QStringLiteral("radiobutton_cameramode_color"));
        radiobutton_cameramode_color->setGeometry(QRect(10, 20, 82, 17));
        radiobutton_cameramode_color->setChecked(true);
        radiobutton_cameramode_infrared = new QRadioButton(groupBox);
        radiobutton_cameramode_infrared->setObjectName(QStringLiteral("radiobutton_cameramode_infrared"));
        radiobutton_cameramode_infrared->setGeometry(QRect(10, 40, 82, 21));
        radiobutton_cameramode_infrared->setChecked(false);

        retranslateUi(DriverSettingsDialogKinectV2);
        QObject::connect(buttonBox, SIGNAL(accepted()), DriverSettingsDialogKinectV2, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DriverSettingsDialogKinectV2, SLOT(reject()));

        QMetaObject::connectSlotsByName(DriverSettingsDialogKinectV2);
    } // setupUi

    void retranslateUi(QDialog *DriverSettingsDialogKinectV2)
    {
        DriverSettingsDialogKinectV2->setWindowTitle(QApplication::translate("DriverSettingsDialogKinectV2", "Kinect v2 Driver Settings", 0));
        groupBox->setTitle(QApplication::translate("DriverSettingsDialogKinectV2", "Camera mode", 0));
        radiobutton_cameramode_color->setText(QApplication::translate("DriverSettingsDialogKinectV2", "Color", 0));
        radiobutton_cameramode_infrared->setText(QApplication::translate("DriverSettingsDialogKinectV2", "Infrared", 0));
    } // retranslateUi

};

namespace Ui {
    class DriverSettingsDialogKinectV2: public Ui_DriverSettingsDialogKinectV2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DRIVERSETTINGSDIALOGKINECTV2_H
