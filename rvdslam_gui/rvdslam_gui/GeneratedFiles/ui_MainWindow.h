/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QComboBox *combobox_driver_select;
    QComboBox *combobox_tracker_select;
    QComboBox *combobox_mapper_select;
    QLabel *label_driver;
    QLabel *label_tracker;
    QLabel *label_mapper;
    QGroupBox *groupbox_tracker_framesequence;
    QRadioButton *radiobutton_tracker_processall;
    QRadioButton *radiobutton_tracker_processrecent;
    QGroupBox *groupbox_mapper_framesequence;
    QRadioButton *radiobutton_mapper_processall;
    QRadioButton *radiobutton_mapper_processrecent;
    QProgressBar *progressbar_tracker_queue;
    QProgressBar *progressbar_mapper_queue;
    QFrame *line_6;
    QLabel *label_driver_image_depth;
    QFrame *line_3;
    QFrame *line_4;
    QLabel *label_mapper_image_depth;
    QLabel *label_tracker_image_depth;
    QFrame *line;
    QLabel *label_driver_image_rgb;
    QLabel *label_mapper_image_rgb;
    QFrame *line_2;
    QLabel *label_tracker_image_rgb;
    QLabel *label;
    QLCDNumber *lcdnumber_tracker_queue;
    QLCDNumber *lcdnumber_mapper_queue;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLCDNumber *lcdnumber_driver_fps;
    QLabel *label_6;
    QLabel *label_7;
    QLCDNumber *lcdnumber_tracker_fps;
    QLabel *label_8;
    QLCDNumber *lcdnumber_mapper_fps;
    QLabel *label_driver_state;
    QLabel *label_tracker_state;
    QLabel *label_mapper_state;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *button_tracker_start_pause;
    QPushButton *button_tracker_reset;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QPushButton *button_tracker_settings;
    QPushButton *button_tracker_savetrajectory;
    QFrame *line_5;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_3;
    QPushButton *button_mapper_start_pause;
    QPushButton *button_mapper_reset;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayout_4;
    QPushButton *button_mapper_settings;
    QPushButton *button_mapper_savemesh;
    QFrame *line_7;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *verticalLayout_5;
    QPushButton *button_driver_start_pause;
    QPushButton *button_driver_reset;
    QWidget *verticalLayoutWidget_6;
    QVBoxLayout *verticalLayout_6;
    QPushButton *button_driver_settings;
    QPushButton *button_driver_savedataset;
    QFrame *line_8;
    QLabel *label_12;
    QLabel *label_14;
    QDoubleSpinBox *doublespinbox_boxfilter_zmax;
    QLabel *label_9;
    QLabel *label_13;
    QDoubleSpinBox *doublespinbox_boxfilter_zmin;
    QLabel *label_11;
    QCheckBox *checkbox_boxfilter;
    QSpinBox *spinbox_boxfilter_umin;
    QSpinBox *spinbox_boxfilter_vmin;
    QSpinBox *spinbox_boxfilter_umax;
    QSpinBox *spinbox_boxfilter_vmax;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1021, 864);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(1021, 864));
        MainWindow->setMaximumSize(QSize(1021, 864));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        combobox_driver_select = new QComboBox(centralWidget);
        combobox_driver_select->setObjectName(QStringLiteral("combobox_driver_select"));
        combobox_driver_select->setGeometry(QRect(20, 40, 291, 22));
        combobox_tracker_select = new QComboBox(centralWidget);
        combobox_tracker_select->setObjectName(QStringLiteral("combobox_tracker_select"));
        combobox_tracker_select->setGeometry(QRect(370, 40, 281, 22));
        combobox_mapper_select = new QComboBox(centralWidget);
        combobox_mapper_select->setObjectName(QStringLiteral("combobox_mapper_select"));
        combobox_mapper_select->setGeometry(QRect(710, 40, 291, 22));
        label_driver = new QLabel(centralWidget);
        label_driver->setObjectName(QStringLiteral("label_driver"));
        label_driver->setGeometry(QRect(20, 10, 46, 16));
        label_tracker = new QLabel(centralWidget);
        label_tracker->setObjectName(QStringLiteral("label_tracker"));
        label_tracker->setGeometry(QRect(370, 10, 46, 16));
        label_mapper = new QLabel(centralWidget);
        label_mapper->setObjectName(QStringLiteral("label_mapper"));
        label_mapper->setGeometry(QRect(710, 10, 46, 16));
        groupbox_tracker_framesequence = new QGroupBox(centralWidget);
        groupbox_tracker_framesequence->setObjectName(QStringLiteral("groupbox_tracker_framesequence"));
        groupbox_tracker_framesequence->setGeometry(QRect(370, 70, 281, 71));
        radiobutton_tracker_processall = new QRadioButton(groupbox_tracker_framesequence);
        radiobutton_tracker_processall->setObjectName(QStringLiteral("radiobutton_tracker_processall"));
        radiobutton_tracker_processall->setGeometry(QRect(10, 20, 111, 17));
        radiobutton_tracker_processall->setChecked(true);
        radiobutton_tracker_processall->setAutoExclusive(true);
        radiobutton_tracker_processrecent = new QRadioButton(groupbox_tracker_framesequence);
        radiobutton_tracker_processrecent->setObjectName(QStringLiteral("radiobutton_tracker_processrecent"));
        radiobutton_tracker_processrecent->setGeometry(QRect(10, 40, 181, 21));
        groupbox_mapper_framesequence = new QGroupBox(centralWidget);
        groupbox_mapper_framesequence->setObjectName(QStringLiteral("groupbox_mapper_framesequence"));
        groupbox_mapper_framesequence->setGeometry(QRect(710, 70, 291, 71));
        radiobutton_mapper_processall = new QRadioButton(groupbox_mapper_framesequence);
        radiobutton_mapper_processall->setObjectName(QStringLiteral("radiobutton_mapper_processall"));
        radiobutton_mapper_processall->setGeometry(QRect(10, 20, 111, 17));
        radiobutton_mapper_processall->setChecked(true);
        radiobutton_mapper_processrecent = new QRadioButton(groupbox_mapper_framesequence);
        radiobutton_mapper_processrecent->setObjectName(QStringLiteral("radiobutton_mapper_processrecent"));
        radiobutton_mapper_processrecent->setGeometry(QRect(10, 40, 181, 21));
        progressbar_tracker_queue = new QProgressBar(centralWidget);
        progressbar_tracker_queue->setObjectName(QStringLiteral("progressbar_tracker_queue"));
        progressbar_tracker_queue->setGeometry(QRect(330, 40, 21, 201));
        progressbar_tracker_queue->setMaximum(1023);
        progressbar_tracker_queue->setValue(0);
        progressbar_tracker_queue->setOrientation(Qt::Vertical);
        progressbar_mapper_queue = new QProgressBar(centralWidget);
        progressbar_mapper_queue->setObjectName(QStringLiteral("progressbar_mapper_queue"));
        progressbar_mapper_queue->setGeometry(QRect(670, 40, 21, 201));
        progressbar_mapper_queue->setMaximum(1023);
        progressbar_mapper_queue->setValue(0);
        progressbar_mapper_queue->setOrientation(Qt::Vertical);
        line_6 = new QFrame(centralWidget);
        line_6->setObjectName(QStringLiteral("line_6"));
        line_6->setGeometry(QRect(690, 550, 321, 21));
        line_6->setFrameShape(QFrame::HLine);
        line_6->setFrameShadow(QFrame::Sunken);
        label_driver_image_depth = new QLabel(centralWidget);
        label_driver_image_depth->setObjectName(QStringLiteral("label_driver_image_depth"));
        label_driver_image_depth->setGeometry(QRect(10, 570, 320, 240));
        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(10, 550, 321, 21));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        line_4 = new QFrame(centralWidget);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setGeometry(QRect(350, 550, 321, 21));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);
        label_mapper_image_depth = new QLabel(centralWidget);
        label_mapper_image_depth->setObjectName(QStringLiteral("label_mapper_image_depth"));
        label_mapper_image_depth->setGeometry(QRect(690, 570, 320, 240));
        label_tracker_image_depth = new QLabel(centralWidget);
        label_tracker_image_depth->setObjectName(QStringLiteral("label_tracker_image_depth"));
        label_tracker_image_depth->setGeometry(QRect(350, 570, 320, 240));
        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(330, 310, 20, 501));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        label_driver_image_rgb = new QLabel(centralWidget);
        label_driver_image_rgb->setObjectName(QStringLiteral("label_driver_image_rgb"));
        label_driver_image_rgb->setGeometry(QRect(10, 310, 320, 240));
        label_mapper_image_rgb = new QLabel(centralWidget);
        label_mapper_image_rgb->setObjectName(QStringLiteral("label_mapper_image_rgb"));
        label_mapper_image_rgb->setGeometry(QRect(690, 310, 320, 240));
        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(670, 310, 20, 501));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);
        label_tracker_image_rgb = new QLabel(centralWidget);
        label_tracker_image_rgb->setObjectName(QStringLiteral("label_tracker_image_rgb"));
        label_tracker_image_rgb->setGeometry(QRect(350, 310, 320, 240));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(50, 250, 71, 20));
        lcdnumber_tracker_queue = new QLCDNumber(centralWidget);
        lcdnumber_tracker_queue->setObjectName(QStringLiteral("lcdnumber_tracker_queue"));
        lcdnumber_tracker_queue->setGeometry(QRect(320, 270, 41, 21));
        lcdnumber_tracker_queue->setSmallDecimalPoint(false);
        lcdnumber_tracker_queue->setDigitCount(4);
        lcdnumber_tracker_queue->setProperty("intValue", QVariant(0));
        lcdnumber_mapper_queue = new QLCDNumber(centralWidget);
        lcdnumber_mapper_queue->setObjectName(QStringLiteral("lcdnumber_mapper_queue"));
        lcdnumber_mapper_queue->setGeometry(QRect(660, 270, 41, 21));
        lcdnumber_mapper_queue->setDigitCount(4);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(420, 250, 101, 20));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(780, 250, 71, 20));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(180, 250, 61, 21));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(290, 250, 101, 21));
        lcdnumber_driver_fps = new QLCDNumber(centralWidget);
        lcdnumber_driver_fps->setObjectName(QStringLiteral("lcdnumber_driver_fps"));
        lcdnumber_driver_fps->setGeometry(QRect(180, 270, 31, 21));
        lcdnumber_driver_fps->setSmallDecimalPoint(false);
        lcdnumber_driver_fps->setDigitCount(3);
        lcdnumber_driver_fps->setProperty("intValue", QVariant(0));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(540, 250, 71, 20));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(640, 250, 101, 21));
        lcdnumber_tracker_fps = new QLCDNumber(centralWidget);
        lcdnumber_tracker_fps->setObjectName(QStringLiteral("lcdnumber_tracker_fps"));
        lcdnumber_tracker_fps->setGeometry(QRect(540, 270, 31, 21));
        lcdnumber_tracker_fps->setDigitCount(3);
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(900, 250, 71, 20));
        lcdnumber_mapper_fps = new QLCDNumber(centralWidget);
        lcdnumber_mapper_fps->setObjectName(QStringLiteral("lcdnumber_mapper_fps"));
        lcdnumber_mapper_fps->setGeometry(QRect(900, 270, 31, 21));
        lcdnumber_mapper_fps->setSmallDecimalPoint(false);
        lcdnumber_mapper_fps->setDigitCount(3);
        lcdnumber_mapper_fps->setProperty("intValue", QVariant(0));
        label_driver_state = new QLabel(centralWidget);
        label_driver_state->setObjectName(QStringLiteral("label_driver_state"));
        label_driver_state->setGeometry(QRect(50, 270, 71, 16));
        label_tracker_state = new QLabel(centralWidget);
        label_tracker_state->setObjectName(QStringLiteral("label_tracker_state"));
        label_tracker_state->setGeometry(QRect(420, 270, 71, 16));
        label_mapper_state = new QLabel(centralWidget);
        label_mapper_state->setObjectName(QStringLiteral("label_mapper_state"));
        label_mapper_state->setGeometry(QRect(780, 270, 71, 16));
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(370, 150, 133, 92));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        button_tracker_start_pause = new QPushButton(verticalLayoutWidget);
        button_tracker_start_pause->setObjectName(QStringLiteral("button_tracker_start_pause"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(button_tracker_start_pause->sizePolicy().hasHeightForWidth());
        button_tracker_start_pause->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(button_tracker_start_pause);

        button_tracker_reset = new QPushButton(verticalLayoutWidget);
        button_tracker_reset->setObjectName(QStringLiteral("button_tracker_reset"));
        sizePolicy1.setHeightForWidth(button_tracker_reset->sizePolicy().hasHeightForWidth());
        button_tracker_reset->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(button_tracker_reset);

        verticalLayoutWidget_2 = new QWidget(centralWidget);
        verticalLayoutWidget_2->setObjectName(QStringLiteral("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(520, 150, 133, 92));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        button_tracker_settings = new QPushButton(verticalLayoutWidget_2);
        button_tracker_settings->setObjectName(QStringLiteral("button_tracker_settings"));
        sizePolicy1.setHeightForWidth(button_tracker_settings->sizePolicy().hasHeightForWidth());
        button_tracker_settings->setSizePolicy(sizePolicy1);

        verticalLayout_2->addWidget(button_tracker_settings);

        button_tracker_savetrajectory = new QPushButton(verticalLayoutWidget_2);
        button_tracker_savetrajectory->setObjectName(QStringLiteral("button_tracker_savetrajectory"));
        sizePolicy1.setHeightForWidth(button_tracker_savetrajectory->sizePolicy().hasHeightForWidth());
        button_tracker_savetrajectory->setSizePolicy(sizePolicy1);

        verticalLayout_2->addWidget(button_tracker_savetrajectory);

        line_5 = new QFrame(centralWidget);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setGeometry(QRect(500, 150, 20, 91));
        line_5->setFrameShape(QFrame::VLine);
        line_5->setFrameShadow(QFrame::Sunken);
        verticalLayoutWidget_3 = new QWidget(centralWidget);
        verticalLayoutWidget_3->setObjectName(QStringLiteral("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(710, 150, 133, 92));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        button_mapper_start_pause = new QPushButton(verticalLayoutWidget_3);
        button_mapper_start_pause->setObjectName(QStringLiteral("button_mapper_start_pause"));
        sizePolicy1.setHeightForWidth(button_mapper_start_pause->sizePolicy().hasHeightForWidth());
        button_mapper_start_pause->setSizePolicy(sizePolicy1);

        verticalLayout_3->addWidget(button_mapper_start_pause);

        button_mapper_reset = new QPushButton(verticalLayoutWidget_3);
        button_mapper_reset->setObjectName(QStringLiteral("button_mapper_reset"));
        sizePolicy1.setHeightForWidth(button_mapper_reset->sizePolicy().hasHeightForWidth());
        button_mapper_reset->setSizePolicy(sizePolicy1);

        verticalLayout_3->addWidget(button_mapper_reset);

        verticalLayoutWidget_4 = new QWidget(centralWidget);
        verticalLayoutWidget_4->setObjectName(QStringLiteral("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(870, 150, 133, 92));
        verticalLayout_4 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        button_mapper_settings = new QPushButton(verticalLayoutWidget_4);
        button_mapper_settings->setObjectName(QStringLiteral("button_mapper_settings"));
        sizePolicy1.setHeightForWidth(button_mapper_settings->sizePolicy().hasHeightForWidth());
        button_mapper_settings->setSizePolicy(sizePolicy1);

        verticalLayout_4->addWidget(button_mapper_settings);

        button_mapper_savemesh = new QPushButton(verticalLayoutWidget_4);
        button_mapper_savemesh->setObjectName(QStringLiteral("button_mapper_savemesh"));
        sizePolicy1.setHeightForWidth(button_mapper_savemesh->sizePolicy().hasHeightForWidth());
        button_mapper_savemesh->setSizePolicy(sizePolicy1);

        verticalLayout_4->addWidget(button_mapper_savemesh);

        line_7 = new QFrame(centralWidget);
        line_7->setObjectName(QStringLiteral("line_7"));
        line_7->setGeometry(QRect(846, 150, 20, 91));
        line_7->setFrameShape(QFrame::VLine);
        line_7->setFrameShadow(QFrame::Sunken);
        verticalLayoutWidget_5 = new QWidget(centralWidget);
        verticalLayoutWidget_5->setObjectName(QStringLiteral("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(20, 150, 133, 92));
        verticalLayout_5 = new QVBoxLayout(verticalLayoutWidget_5);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        button_driver_start_pause = new QPushButton(verticalLayoutWidget_5);
        button_driver_start_pause->setObjectName(QStringLiteral("button_driver_start_pause"));
        sizePolicy1.setHeightForWidth(button_driver_start_pause->sizePolicy().hasHeightForWidth());
        button_driver_start_pause->setSizePolicy(sizePolicy1);

        verticalLayout_5->addWidget(button_driver_start_pause);

        button_driver_reset = new QPushButton(verticalLayoutWidget_5);
        button_driver_reset->setObjectName(QStringLiteral("button_driver_reset"));
        sizePolicy1.setHeightForWidth(button_driver_reset->sizePolicy().hasHeightForWidth());
        button_driver_reset->setSizePolicy(sizePolicy1);

        verticalLayout_5->addWidget(button_driver_reset);

        verticalLayoutWidget_6 = new QWidget(centralWidget);
        verticalLayoutWidget_6->setObjectName(QStringLiteral("verticalLayoutWidget_6"));
        verticalLayoutWidget_6->setGeometry(QRect(180, 150, 133, 92));
        verticalLayout_6 = new QVBoxLayout(verticalLayoutWidget_6);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        button_driver_settings = new QPushButton(verticalLayoutWidget_6);
        button_driver_settings->setObjectName(QStringLiteral("button_driver_settings"));
        sizePolicy1.setHeightForWidth(button_driver_settings->sizePolicy().hasHeightForWidth());
        button_driver_settings->setSizePolicy(sizePolicy1);

        verticalLayout_6->addWidget(button_driver_settings);

        button_driver_savedataset = new QPushButton(verticalLayoutWidget_6);
        button_driver_savedataset->setObjectName(QStringLiteral("button_driver_savedataset"));
        sizePolicy1.setHeightForWidth(button_driver_savedataset->sizePolicy().hasHeightForWidth());
        button_driver_savedataset->setSizePolicy(sizePolicy1);

        verticalLayout_6->addWidget(button_driver_savedataset);

        line_8 = new QFrame(centralWidget);
        line_8->setObjectName(QStringLiteral("line_8"));
        line_8->setGeometry(QRect(150, 150, 31, 91));
        line_8->setFrameShape(QFrame::VLine);
        line_8->setFrameShadow(QFrame::Sunken);
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(130, 110, 31, 20));
        label_14 = new QLabel(centralWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(260, 70, 41, 16));
        doublespinbox_boxfilter_zmax = new QDoubleSpinBox(centralWidget);
        doublespinbox_boxfilter_zmax->setObjectName(QStringLiteral("doublespinbox_boxfilter_zmax"));
        doublespinbox_boxfilter_zmax->setEnabled(false);
        doublespinbox_boxfilter_zmax->setGeometry(QRect(260, 110, 51, 22));
        doublespinbox_boxfilter_zmax->setSingleStep(0.1);
        doublespinbox_boxfilter_zmax->setValue(10);
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(160, 70, 41, 16));
        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(210, 70, 41, 16));
        doublespinbox_boxfilter_zmin = new QDoubleSpinBox(centralWidget);
        doublespinbox_boxfilter_zmin->setObjectName(QStringLiteral("doublespinbox_boxfilter_zmin"));
        doublespinbox_boxfilter_zmin->setEnabled(false);
        doublespinbox_boxfilter_zmin->setGeometry(QRect(260, 90, 51, 22));
        doublespinbox_boxfilter_zmin->setSingleStep(0.1);
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(130, 90, 31, 20));
        checkbox_boxfilter = new QCheckBox(centralWidget);
        checkbox_boxfilter->setObjectName(QStringLiteral("checkbox_boxfilter"));
        checkbox_boxfilter->setGeometry(QRect(20, 100, 101, 16));
        spinbox_boxfilter_umin = new QSpinBox(centralWidget);
        spinbox_boxfilter_umin->setObjectName(QStringLiteral("spinbox_boxfilter_umin"));
        spinbox_boxfilter_umin->setEnabled(false);
        spinbox_boxfilter_umin->setGeometry(QRect(160, 90, 51, 22));
        spinbox_boxfilter_umin->setMaximum(999);
        spinbox_boxfilter_umin->setDisplayIntegerBase(10);
        spinbox_boxfilter_vmin = new QSpinBox(centralWidget);
        spinbox_boxfilter_vmin->setObjectName(QStringLiteral("spinbox_boxfilter_vmin"));
        spinbox_boxfilter_vmin->setEnabled(false);
        spinbox_boxfilter_vmin->setGeometry(QRect(210, 90, 51, 22));
        spinbox_boxfilter_vmin->setMaximum(999);
        spinbox_boxfilter_vmin->setDisplayIntegerBase(10);
        spinbox_boxfilter_umax = new QSpinBox(centralWidget);
        spinbox_boxfilter_umax->setObjectName(QStringLiteral("spinbox_boxfilter_umax"));
        spinbox_boxfilter_umax->setEnabled(false);
        spinbox_boxfilter_umax->setGeometry(QRect(160, 110, 51, 22));
        spinbox_boxfilter_umax->setMaximum(999);
        spinbox_boxfilter_umax->setValue(640);
        spinbox_boxfilter_umax->setDisplayIntegerBase(10);
        spinbox_boxfilter_vmax = new QSpinBox(centralWidget);
        spinbox_boxfilter_vmax->setObjectName(QStringLiteral("spinbox_boxfilter_vmax"));
        spinbox_boxfilter_vmax->setEnabled(false);
        spinbox_boxfilter_vmax->setGeometry(QRect(210, 110, 51, 22));
        spinbox_boxfilter_vmax->setMaximum(999);
        spinbox_boxfilter_vmax->setValue(480);
        spinbox_boxfilter_vmax->setDisplayIntegerBase(10);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1021, 36));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "RVDSLAM", 0));
        combobox_driver_select->clear();
        combobox_driver_select->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "RGBD File Reader", 0)
         << QApplication::translate("MainWindow", "RGBD Virtual Camera", 0)
         << QApplication::translate("MainWindow", "Kinect v1", 0)
         << QApplication::translate("MainWindow", "Kinect v2", 0)
         << QApplication::translate("MainWindow", "Bumblebee", 0)
         << QApplication::translate("MainWindow", "Zed", 0)
        );
        combobox_tracker_select->clear();
        combobox_tracker_select->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "DIFODO", 0)
        );
        combobox_mapper_select->clear();
        combobox_mapper_select->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "fastfusion", 0)
        );
        label_driver->setText(QApplication::translate("MainWindow", "Driver", 0));
        label_tracker->setText(QApplication::translate("MainWindow", "Tracker", 0));
        label_mapper->setText(QApplication::translate("MainWindow", "Mapper", 0));
        groupbox_tracker_framesequence->setTitle(QApplication::translate("MainWindow", "Frame sequence", 0));
        radiobutton_tracker_processall->setText(QApplication::translate("MainWindow", "Process all frames", 0));
        radiobutton_tracker_processrecent->setText(QApplication::translate("MainWindow", "Process most recent frame", 0));
        groupbox_mapper_framesequence->setTitle(QApplication::translate("MainWindow", "Frame sequence", 0));
        radiobutton_mapper_processall->setText(QApplication::translate("MainWindow", "Process all frames", 0));
        radiobutton_mapper_processrecent->setText(QApplication::translate("MainWindow", "Process most recent frame", 0));
        label_driver_image_depth->setText(QString());
        label_mapper_image_depth->setText(QString());
        label_tracker_image_depth->setText(QString());
        label_driver_image_rgb->setText(QString());
        label_mapper_image_rgb->setText(QString());
        label_tracker_image_rgb->setText(QString());
        label->setText(QApplication::translate("MainWindow", "Driver state:", 0));
        label_2->setText(QApplication::translate("MainWindow", "Tracker state:", 0));
        label_3->setText(QApplication::translate("MainWindow", "Mapper state:", 0));
        label_4->setText(QApplication::translate("MainWindow", "Driver FPS:", 0));
        label_5->setText(QApplication::translate("MainWindow", "Tracker queue size:", 0));
        label_6->setText(QApplication::translate("MainWindow", "Tracker FPS:", 0));
        label_7->setText(QApplication::translate("MainWindow", "Mapper queue size:", 0));
        label_8->setText(QApplication::translate("MainWindow", "Mapper FPS:", 0));
        label_driver_state->setText(QString());
        label_tracker_state->setText(QString());
        label_mapper_state->setText(QString());
        button_tracker_start_pause->setText(QApplication::translate("MainWindow", "Start Tracker", 0));
        button_tracker_reset->setText(QApplication::translate("MainWindow", "Reset Tracker", 0));
        button_tracker_settings->setText(QApplication::translate("MainWindow", "Tracker Settings", 0));
        button_tracker_savetrajectory->setText(QApplication::translate("MainWindow", "Save Trajectory", 0));
        button_mapper_start_pause->setText(QApplication::translate("MainWindow", "Start Mapper", 0));
        button_mapper_reset->setText(QApplication::translate("MainWindow", "Reset Mapper", 0));
        button_mapper_settings->setText(QApplication::translate("MainWindow", "Mapper Settings", 0));
        button_mapper_savemesh->setText(QApplication::translate("MainWindow", "Save Mesh", 0));
        button_driver_start_pause->setText(QApplication::translate("MainWindow", "Start Driver", 0));
        button_driver_reset->setText(QApplication::translate("MainWindow", "Reset Driver", 0));
        button_driver_settings->setText(QApplication::translate("MainWindow", "Driver Settings", 0));
        button_driver_savedataset->setText(QApplication::translate("MainWindow", "Save Dataset", 0));
        label_12->setText(QApplication::translate("MainWindow", "Max:", 0));
        label_14->setText(QApplication::translate("MainWindow", "z (m):", 0));
        label_9->setText(QApplication::translate("MainWindow", "u (pixel):", 0));
        label_13->setText(QApplication::translate("MainWindow", "v (pixel):", 0));
        label_11->setText(QApplication::translate("MainWindow", "Min:", 0));
        checkbox_boxfilter->setText(QApplication::translate("MainWindow", "Apply box filter", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
