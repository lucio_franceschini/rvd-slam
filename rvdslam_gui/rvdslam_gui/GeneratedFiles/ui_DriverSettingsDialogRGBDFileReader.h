/********************************************************************************
** Form generated from reading UI file 'DriverSettingsDialogRGBDFileReader.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DRIVERSETTINGSDIALOGRGBDFILEREADER_H
#define UI_DRIVERSETTINGSDIALOGRGBDFILEREADER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_DriverSettingsDialogRGBDFileReader
{
public:
    QDialogButtonBox *buttonBox;
    QPushButton *button_selectfolder;
    QLabel *label_folder;

    void setupUi(QDialog *DriverSettingsDialogRGBDFileReader)
    {
        if (DriverSettingsDialogRGBDFileReader->objectName().isEmpty())
            DriverSettingsDialogRGBDFileReader->setObjectName(QStringLiteral("DriverSettingsDialogRGBDFileReader"));
        DriverSettingsDialogRGBDFileReader->resize(311, 125);
        buttonBox = new QDialogButtonBox(DriverSettingsDialogRGBDFileReader);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(150, 91, 151, 31));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        button_selectfolder = new QPushButton(DriverSettingsDialogRGBDFileReader);
        button_selectfolder->setObjectName(QStringLiteral("button_selectfolder"));
        button_selectfolder->setEnabled(true);
        button_selectfolder->setGeometry(QRect(10, 10, 291, 41));
        button_selectfolder->setCheckable(false);
        button_selectfolder->setFlat(false);
        label_folder = new QLabel(DriverSettingsDialogRGBDFileReader);
        label_folder->setObjectName(QStringLiteral("label_folder"));
        label_folder->setGeometry(QRect(10, 60, 291, 21));

        retranslateUi(DriverSettingsDialogRGBDFileReader);
        QObject::connect(buttonBox, SIGNAL(accepted()), DriverSettingsDialogRGBDFileReader, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DriverSettingsDialogRGBDFileReader, SLOT(reject()));

        QMetaObject::connectSlotsByName(DriverSettingsDialogRGBDFileReader);
    } // setupUi

    void retranslateUi(QDialog *DriverSettingsDialogRGBDFileReader)
    {
        DriverSettingsDialogRGBDFileReader->setWindowTitle(QApplication::translate("DriverSettingsDialogRGBDFileReader", "RGBD File Reader Driver Settings", 0));
        button_selectfolder->setText(QApplication::translate("DriverSettingsDialogRGBDFileReader", "Select Source Folder", 0));
        label_folder->setText(QApplication::translate("DriverSettingsDialogRGBDFileReader", "Path:", 0));
    } // retranslateUi

};

namespace Ui {
    class DriverSettingsDialogRGBDFileReader: public Ui_DriverSettingsDialogRGBDFileReader {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DRIVERSETTINGSDIALOGRGBDFILEREADER_H
