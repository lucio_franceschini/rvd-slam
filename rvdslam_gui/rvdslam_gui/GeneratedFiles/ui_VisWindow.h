/********************************************************************************
** Form generated from reading UI file 'VisWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VISWINDOW_H
#define UI_VISWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include "QVTKWidget.h"

QT_BEGIN_NAMESPACE

class Ui_VisWindow
{
public:
    QWidget *centralwidget;
    QLabel *label_input_image_depth;
    QVTKWidget *qvtkWidget;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *VisWindow)
    {
        if (VisWindow->objectName().isEmpty())
            VisWindow->setObjectName(QStringLiteral("VisWindow"));
        VisWindow->resize(1024, 768);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(VisWindow->sizePolicy().hasHeightForWidth());
        VisWindow->setSizePolicy(sizePolicy);
        VisWindow->setMinimumSize(QSize(1024, 768));
        VisWindow->setMaximumSize(QSize(1024, 768));
        centralwidget = new QWidget(VisWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        label_input_image_depth = new QLabel(centralwidget);
        label_input_image_depth->setObjectName(QStringLiteral("label_input_image_depth"));
        label_input_image_depth->setGeometry(QRect(10, 270, 320, 240));
        qvtkWidget = new QVTKWidget(centralwidget);
        qvtkWidget->setObjectName(QStringLiteral("qvtkWidget"));
        qvtkWidget->setGeometry(QRect(10, 10, 1005, 715));
        VisWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(VisWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1024, 21));
        VisWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(VisWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        VisWindow->setStatusBar(statusbar);

        retranslateUi(VisWindow);

        QMetaObject::connectSlotsByName(VisWindow);
    } // setupUi

    void retranslateUi(QMainWindow *VisWindow)
    {
        VisWindow->setWindowTitle(QApplication::translate("VisWindow", "Visualization", 0));
        label_input_image_depth->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class VisWindow: public Ui_VisWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VISWINDOW_H
