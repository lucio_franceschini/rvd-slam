/********************************************************************************
** Form generated from reading UI file 'DriverSettingsDialogRGBDVirtualCamera.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DRIVERSETTINGSDIALOGRGBDVIRTUALCAMERA_H
#define UI_DRIVERSETTINGSDIALOGRGBDVIRTUALCAMERA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_DriverSettingsDialogRGBDVirtualCamera
{
public:
    QDialogButtonBox *buttonBox;
    QPushButton *button_selectfolder;
    QLabel *label_folder;

    void setupUi(QDialog *DriverSettingsDialogRGBDVirtualCamera)
    {
        if (DriverSettingsDialogRGBDVirtualCamera->objectName().isEmpty())
            DriverSettingsDialogRGBDVirtualCamera->setObjectName(QStringLiteral("DriverSettingsDialogRGBDVirtualCamera"));
        DriverSettingsDialogRGBDVirtualCamera->resize(311, 125);
        buttonBox = new QDialogButtonBox(DriverSettingsDialogRGBDVirtualCamera);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(150, 91, 151, 31));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        button_selectfolder = new QPushButton(DriverSettingsDialogRGBDVirtualCamera);
        button_selectfolder->setObjectName(QStringLiteral("button_selectfolder"));
        button_selectfolder->setEnabled(true);
        button_selectfolder->setGeometry(QRect(10, 10, 291, 41));
        button_selectfolder->setCheckable(false);
        button_selectfolder->setFlat(false);
        label_folder = new QLabel(DriverSettingsDialogRGBDVirtualCamera);
        label_folder->setObjectName(QStringLiteral("label_folder"));
        label_folder->setGeometry(QRect(10, 60, 291, 21));

        retranslateUi(DriverSettingsDialogRGBDVirtualCamera);
        QObject::connect(buttonBox, SIGNAL(accepted()), DriverSettingsDialogRGBDVirtualCamera, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DriverSettingsDialogRGBDVirtualCamera, SLOT(reject()));

        QMetaObject::connectSlotsByName(DriverSettingsDialogRGBDVirtualCamera);
    } // setupUi

    void retranslateUi(QDialog *DriverSettingsDialogRGBDVirtualCamera)
    {
        DriverSettingsDialogRGBDVirtualCamera->setWindowTitle(QApplication::translate("DriverSettingsDialogRGBDVirtualCamera", "RGBD Virtual Camera Driver Settings", 0));
        button_selectfolder->setText(QApplication::translate("DriverSettingsDialogRGBDVirtualCamera", "Select Source Folder", 0));
        label_folder->setText(QApplication::translate("DriverSettingsDialogRGBDVirtualCamera", "Path:", 0));
    } // retranslateUi

};

namespace Ui {
    class DriverSettingsDialogRGBDVirtualCamera: public Ui_DriverSettingsDialogRGBDVirtualCamera {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DRIVERSETTINGSDIALOGRGBDVIRTUALCAMERA_H
