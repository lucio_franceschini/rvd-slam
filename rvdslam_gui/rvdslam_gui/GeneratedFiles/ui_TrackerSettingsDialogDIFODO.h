/********************************************************************************
** Form generated from reading UI file 'TrackerSettingsDialogDIFODO.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TRACKERSETTINGSDIALOGDIFODO_H
#define UI_TRACKERSETTINGSDIALOGDIFODO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_TrackerSettingsDialogDIFODO
{
public:
    QDialogButtonBox *buttonBox;
    QDoubleSpinBox *doublespinbox_maxdepth;
    QSpinBox *spinbox_downsample;
    QSpinBox *spinbox_ctflevels;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;

    void setupUi(QDialog *TrackerSettingsDialogDIFODO)
    {
        if (TrackerSettingsDialogDIFODO->objectName().isEmpty())
            TrackerSettingsDialogDIFODO->setObjectName(QStringLiteral("TrackerSettingsDialogDIFODO"));
        TrackerSettingsDialogDIFODO->resize(161, 133);
        buttonBox = new QDialogButtonBox(TrackerSettingsDialogDIFODO);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(10, 100, 141, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        doublespinbox_maxdepth = new QDoubleSpinBox(TrackerSettingsDialogDIFODO);
        doublespinbox_maxdepth->setObjectName(QStringLiteral("doublespinbox_maxdepth"));
        doublespinbox_maxdepth->setGeometry(QRect(100, 70, 51, 22));
        doublespinbox_maxdepth->setSingleStep(0.1);
        spinbox_downsample = new QSpinBox(TrackerSettingsDialogDIFODO);
        spinbox_downsample->setObjectName(QStringLiteral("spinbox_downsample"));
        spinbox_downsample->setGeometry(QRect(100, 10, 51, 22));
        spinbox_ctflevels = new QSpinBox(TrackerSettingsDialogDIFODO);
        spinbox_ctflevels->setObjectName(QStringLiteral("spinbox_ctflevels"));
        spinbox_ctflevels->setGeometry(QRect(100, 40, 51, 22));
        label = new QLabel(TrackerSettingsDialogDIFODO);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 71, 21));
        label_2 = new QLabel(TrackerSettingsDialogDIFODO);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 40, 81, 21));
        label_3 = new QLabel(TrackerSettingsDialogDIFODO);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 70, 71, 21));

        retranslateUi(TrackerSettingsDialogDIFODO);
        QObject::connect(buttonBox, SIGNAL(accepted()), TrackerSettingsDialogDIFODO, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), TrackerSettingsDialogDIFODO, SLOT(reject()));

        QMetaObject::connectSlotsByName(TrackerSettingsDialogDIFODO);
    } // setupUi

    void retranslateUi(QDialog *TrackerSettingsDialogDIFODO)
    {
        TrackerSettingsDialogDIFODO->setWindowTitle(QApplication::translate("TrackerSettingsDialogDIFODO", "DIFODO Tracker Settings", 0));
        label->setText(QApplication::translate("TrackerSettingsDialogDIFODO", "Downsample", 0));
        label_2->setText(QApplication::translate("TrackerSettingsDialogDIFODO", "Pyramid levels", 0));
        label_3->setText(QApplication::translate("TrackerSettingsDialogDIFODO", "Max depth (m)", 0));
    } // retranslateUi

};

namespace Ui {
    class TrackerSettingsDialogDIFODO: public Ui_TrackerSettingsDialogDIFODO {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TRACKERSETTINGSDIALOGDIFODO_H
