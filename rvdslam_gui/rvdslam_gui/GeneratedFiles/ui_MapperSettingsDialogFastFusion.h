/********************************************************************************
** Form generated from reading UI file 'MapperSettingsDialogFastFusion.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAPPERSETTINGSDIALOGFASTFUSION_H
#define UI_MAPPERSETTINGSDIALOGFASTFUSION_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_MapperSettingsDialogFastFusion
{
public:
    QDialogButtonBox *buttonBox;
    QDoubleSpinBox *doublespinbox_maxdepth;
    QLabel *label;
    QLabel *label_3;
    QDoubleSpinBox *doublespinbox_voxelscale;

    void setupUi(QDialog *MapperSettingsDialogFastFusion)
    {
        if (MapperSettingsDialogFastFusion->objectName().isEmpty())
            MapperSettingsDialogFastFusion->setObjectName(QStringLiteral("MapperSettingsDialogFastFusion"));
        MapperSettingsDialogFastFusion->resize(171, 101);
        buttonBox = new QDialogButtonBox(MapperSettingsDialogFastFusion);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(10, 61, 151, 41));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        doublespinbox_maxdepth = new QDoubleSpinBox(MapperSettingsDialogFastFusion);
        doublespinbox_maxdepth->setObjectName(QStringLiteral("doublespinbox_maxdepth"));
        doublespinbox_maxdepth->setGeometry(QRect(110, 40, 51, 22));
        doublespinbox_maxdepth->setSingleStep(0.1);
        label = new QLabel(MapperSettingsDialogFastFusion);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 71, 21));
        label_3 = new QLabel(MapperSettingsDialogFastFusion);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 40, 71, 21));
        doublespinbox_voxelscale = new QDoubleSpinBox(MapperSettingsDialogFastFusion);
        doublespinbox_voxelscale->setObjectName(QStringLiteral("doublespinbox_voxelscale"));
        doublespinbox_voxelscale->setGeometry(QRect(110, 10, 51, 22));
        doublespinbox_voxelscale->setDecimals(3);
        doublespinbox_voxelscale->setSingleStep(0.001);

        retranslateUi(MapperSettingsDialogFastFusion);
        QObject::connect(buttonBox, SIGNAL(accepted()), MapperSettingsDialogFastFusion, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), MapperSettingsDialogFastFusion, SLOT(reject()));

        QMetaObject::connectSlotsByName(MapperSettingsDialogFastFusion);
    } // setupUi

    void retranslateUi(QDialog *MapperSettingsDialogFastFusion)
    {
        MapperSettingsDialogFastFusion->setWindowTitle(QApplication::translate("MapperSettingsDialogFastFusion", "fastfusion Mapper Settings", 0));
        label->setText(QApplication::translate("MapperSettingsDialogFastFusion", "Voxel scale (m)", 0));
        label_3->setText(QApplication::translate("MapperSettingsDialogFastFusion", "Max depth (m)", 0));
    } // retranslateUi

};

namespace Ui {
    class MapperSettingsDialogFastFusion: public Ui_MapperSettingsDialogFastFusion {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAPPERSETTINGSDIALOGFASTFUSION_H
