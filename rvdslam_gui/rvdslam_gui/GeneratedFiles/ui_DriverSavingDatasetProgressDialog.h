/********************************************************************************
** Form generated from reading UI file 'DriverSavingDatasetProgressDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DRIVERSAVINGDATASETPROGRESSDIALOG_H
#define UI_DRIVERSAVINGDATASETPROGRESSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QProgressBar>

QT_BEGIN_NAMESPACE

class Ui_DriverSavingDatasetProgressDialog
{
public:
    QProgressBar *progressBar;

    void setupUi(QDialog *DriverSavingDatasetProgressDialog)
    {
        if (DriverSavingDatasetProgressDialog->objectName().isEmpty())
            DriverSavingDatasetProgressDialog->setObjectName(QStringLiteral("DriverSavingDatasetProgressDialog"));
        DriverSavingDatasetProgressDialog->resize(331, 61);
        progressBar = new QProgressBar(DriverSavingDatasetProgressDialog);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(20, 20, 311, 23));
        progressBar->setValue(24);

        retranslateUi(DriverSavingDatasetProgressDialog);

        QMetaObject::connectSlotsByName(DriverSavingDatasetProgressDialog);
    } // setupUi

    void retranslateUi(QDialog *DriverSavingDatasetProgressDialog)
    {
        DriverSavingDatasetProgressDialog->setWindowTitle(QApplication::translate("DriverSavingDatasetProgressDialog", "Saving dataset to folder...", 0));
    } // retranslateUi

};

namespace Ui {
    class DriverSavingDatasetProgressDialog: public Ui_DriverSavingDatasetProgressDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DRIVERSAVINGDATASETPROGRESSDIALOG_H
