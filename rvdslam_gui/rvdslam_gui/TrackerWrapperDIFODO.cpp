#include "stdafx.h"

#include "TrackerWrapperDIFODO.h"

#include "FramePackage.h"

#include <opencv2/core/eigen.hpp>

racoon::rvdslam::TrackerWrapperDIFODO::DIFODOSettings::DIFODOSettings(unsigned int downsample, unsigned int ctf_levels, float max_depth)
	: downsample(downsample), ctf_levels(ctf_levels), max_depth(max_depth)
{
}

racoon::rvdslam::TrackerWrapperDIFODO::TrackerWrapperDIFODO(DIFODOSettings settings) : settings(settings), cols(640), rows(480), timestamp_old(0), difodo(settings.downsample, settings.ctf_levels, settings.max_depth, 640, 480)
{
}

std::unique_ptr<racoon::rvdslam::GenericWrapper::Settings> racoon::rvdslam::TrackerWrapperDIFODO::getSettings() const
{
	auto settings = std::unique_ptr<DIFODOSettings>(new DIFODOSettings(this->settings));

	return std::move(settings);
}

void racoon::rvdslam::TrackerWrapperDIFODO::setSettings(const racoon::rvdslam::GenericWrapper::Settings & settings)
{
	this->settings = dynamic_cast<const DIFODOSettings &>(settings);

	switchState(State::RESET);
}

bool racoon::rvdslam::TrackerWrapperDIFODO::doInit()
{
	return true;
}

bool racoon::rvdslam::TrackerWrapperDIFODO::doReset()
{
	difodo = DIFODO(settings.downsample, settings.ctf_levels, settings.max_depth, cols, rows);

	return true;
}

void racoon::rvdslam::TrackerWrapperDIFODO::computeNextPose(const FramePackage & frame_package)
{
	checkMetadataChanges(frame_package);

	difodo.setInputDepthImage(frame_package.getDepthImage(), frame_package.getDepthImageScale());
	difodo.loadFrame();

	double timestamp_new = frame_package.getTimestampSecs();

	difodo.fps = timestamp_old == 0 ? 30.0 : (float)(1.0 / (timestamp_new - timestamp_old));		//TODO: Check first frame fps assignment

	timestamp_old = timestamp_new;
	
	difodo.odometryCalculation();
}

Eigen::Affine3d racoon::rvdslam::TrackerWrapperDIFODO::getPose() const
{
	Eigen::Affine3d transform;

	mrpt::poses::CPose3D transf;
	transf.setFromValues(0, 0, 0, 0.5*M_PI, -0.5*M_PI, 0);

	mrpt::poses::CPose3D auxpose = difodo.cam_pose - transf;

	mrpt::math::CQuaternionDouble quat;

	auxpose.getAsQuaternion(quat);

	quat.normalize();

	Eigen::Quaterniond quaternion(-quat.r(), quat.y(), quat.z(), -quat.x());
	//Eigen::Quaterniond quaternion(quat.r(), quat.x(), quat.y(), quat.z());
	Eigen::Translation3d translation(Eigen::Vector3d(difodo.cam_pose.x(), difodo.cam_pose.y(), difodo.cam_pose.z()));

	transform = translation * quaternion;

	return transform;
}

void racoon::rvdslam::TrackerWrapperDIFODO::checkMetadataChanges(const FramePackage & frame_package)
{
	unsigned int cols_new = frame_package.getImageWidth();
	unsigned int rows_new = frame_package.getImageHeight();

	if (cols_new != cols || rows_new != rows)
	{
		cols = cols_new;
		rows = rows_new;

		doReset();
	}

	FramePackage::Intrinsics intrinsics = frame_package.getImageIntrinsics();

	float fovh_new = 2.0 * atan2(intrinsics.cx, intrinsics.fx) * 180.0 / M_PI;
	float fovv_new = 2.0 * atan2(intrinsics.cy, intrinsics.fy) * 180.0 / M_PI;

	float fovh, fovv;

	difodo.getFOV(fovh, fovv);

	if (fovh_new != fovh || fovv_new != fovv)
		difodo.setFOV(fovh_new, fovv_new);
}