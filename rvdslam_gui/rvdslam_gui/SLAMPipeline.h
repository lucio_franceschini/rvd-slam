#pragma once

#include "FramePackage.h"
#include "TrackerWrapper.h"
#include "MapperWrapper.h"

#include <QObject>
#include <QThread>
#include <Eigen/Core>
#include <pcl/PolygonMesh.h>

#include <memory>
#include <queue>
#include <chrono>

namespace racoon
{
	namespace rvdslam
	{
		class DataManager;
		class ViewManager;
		class DriverManager;

		/**
		* Class for handling mapper and tracker modules.
		*
		* Starts, stops and configures the modules, fills their input queues and handles module state changes and generated outputs.
		*/
		class SLAMPipeline : public QObject
		{
			Q_OBJECT
		public:
			/** Available tracker modules */
			enum class TrackerType
			{
				DIFODO
			};

			/** Available mapper modules */
			enum class MapperType
			{
				FASTFUSION
			};

			/** Constructor */
			SLAMPipeline(DataManager & data_manager, ViewManager & view_manager, TrackerType tracker_type, MapperType mapper_type);

			/**
			* Destructor
			* 
			* Stops the tracker and mapper threads
			*/
			~SLAMPipeline();

			/** Sets the driver manager pointer */
			void setDriverManager(DriverManager * driver_manager);

			/** Switches the active tracker module to the given one, clears the mapper module queue and clears the processed frame packages */
			void switchTracker(TrackerType new_tracker_type);

			/** Starts the tracker module */
			void startTracker();

			/** Pauses the tracker module */
			void pauseTracker();

			/** Resumes the tracker module */
			void resumeTracker();

			/** Resets the tracker module, clears the mapper module queue and clears the processed frame packages */
			void resetTracker();

			/** Adds a frame package to the tracker module input queue */
			void addFramePackage(std::shared_ptr<FramePackage> frame_package);

			/** Clears the tracker input queue */
			void clearTrackerQueue();

			/** Returns the state of the tracker module state machine */
			GenericWrapper::State getTrackerState();

			/** Returns a pointer to the current tracker's settings struct */
			std::unique_ptr<GenericWrapper::Settings> getTrackerSettings();

			/** Applies the given settings struct to the tracker module */
			void setTrackerSettings(std::shared_ptr<const GenericWrapper::Settings> settings);

			/**
			* Sets the tracker input frame processing mode.
			* If parameter is true, all frame packages in input queue will be processed sequentially.
			* Otherwise, only the most recently added frame package will be processed, with the others being discarded.
			*/
			void setTrackerProcessAll(bool process_all);

			/** Clears the mapper input  queue */
			void clearMapperQueue();

			/** Switches the active mapper module to the given one */
			void switchMapper(MapperType new_mapper_type);

			/** Starts the mapper module */
			void startMapper();

			/** Pauses the mapper module */
			void pauseMapper();

			/** Resumes the mapper module */
			void resumeMapper();

			/** Resets the mapper module */
			void resetMapper();

			/** Returns the state of the mapper module state machine */
			GenericWrapper::State getMapperState();

			/** Returns a pointer to the current mapper's settings struct */
			std::unique_ptr<GenericWrapper::Settings> getMapperSettings();

			/** Applies the given settings struct to the mapper module */
			void setMapperSettings(std::shared_ptr<const GenericWrapper::Settings> settings);

			/**
			* Sets the mapper input frame processing mode.
			* If parameter is true, all frame packages in input queue will be processed sequentially.
			* Otherwise, only the most recently added frame package will be processed, with the others being discarded.
			*/
			void setMapperProcessAll(bool process_all);
		private:
			DataManager & data_manager;
			ViewManager & view_manager;
			DriverManager * driver_manager;

			QThread tracker_thread;
			QThread mapper_thread;

			std::unique_ptr<TrackerWrapper> tracker;
			std::unique_ptr<MapperWrapper> mapper;

			GenericWrapper::State tracker_state;
			GenericWrapper::State mapper_state;

			std::chrono::time_point<std::chrono::high_resolution_clock> systemtime_tracker_old;
			std::chrono::time_point<std::chrono::high_resolution_clock> systemtime_mapper_old;
		private slots:
			/** Updates the GUI tracker state label according to the state of the tracker module state machine */
			void handleTrackerStateChanged(GenericWrapper::State tracker_state);

			/** Updates the GUI tracker queue size label according to the current tracker module input queue size */
			void handleTrackerQueueSizeChanged(unsigned int queue_size);

			/**
			* Gets called after tracker pose estimation was performed on a frame package.
			* 
			* Displays color/depth image pair of frame package in GUI.
			* Updates camera pose display in GUI.
			* Adds frame package to vector of pose estimated frame packages in data manager.
			* Updates tracker fps display in GUI.
			*/
			void handleTrackerPoseEstimated(std::shared_ptr<FramePackage> frame_package);

			/** Updates the GUI mapper state label according to the state of the mapper module state machine */
			void handleMapperStateChanged(GenericWrapper::State tracker_state);

			/** Updates the GUI mapper queue size label according to the current mapper module input queue size */
			void handleMapperQueueSizeChanged(unsigned int queue_size);

			/**
			* Gets called after update of the mapper internal map was performed on a frame package.
			* 
			* Displays color/depth image pair of frame package in GUI.
			* Updates tracker fps display in GUI.
			*/
			void handleMapperUpdateDone(std::shared_ptr<FramePackage> frame_package);

			/**
			* Gets called after a new mesh is produced by the mapper module.
			* 
			* Saves mesh in data manager.
			* Updates mesh display in GUI.
			*/
			void handleMapperMesh(std::shared_ptr<pcl::PolygonMesh> mesh);

			/**
			* Gets called after a new point cloud is produced by the mapper module.
			* 
			* Saves point cloud in data manager.
			* Updates point cloud display in GUI.
			*/
			void handleMapperPointCloud(std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> point_cloud);
		signals:
			void initTrackerSignal();
			void startTrackerSignal();
			void pauseTrackerSignal();
			void resumeTrackerSignal();
			void resetTrackerSignal();
			void clearTrackerQueueSignal();
			void addTrackerFramePackage(std::shared_ptr<FramePackage> frame_package);
			void applyTrackerSettings(std::shared_ptr<const GenericWrapper::Settings> settings);

			void initMapperSignal();
			void startMapperSignal();
			void pauseMapperSignal();
			void resumeMapperSignal();
			void resetMapperSignal();
			void clearMapperQueueSignal();
			void addMapperFramePackage(std::shared_ptr<FramePackage> frame_package);
			void applyMapperSettings(std::shared_ptr<const GenericWrapper::Settings> settings);
		};
	}
}