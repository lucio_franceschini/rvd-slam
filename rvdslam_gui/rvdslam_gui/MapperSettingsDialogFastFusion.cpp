#include "stdafx.h"

#include "MapperSettingsDialogFastFusion.h"

#include "ViewManager.h"
#include "MapperWrapperFastFusion.h"

racoon::rvdslam::MapperSettingsDialogFastFusion::MapperSettingsDialogFastFusion(ViewManager & view_manager, QWidget *parent) : view_manager(view_manager), QDialog(parent)
{
	ui.setupUi(this);

	auto settings = view_manager.getMapperSettings();

	auto fastfusion_settings = dynamic_cast<MapperWrapperFastFusion::FastFusionSettings*>(settings.get());

	ui.doublespinbox_voxelscale->setValue(fastfusion_settings->voxel_scale);
	ui.doublespinbox_maxdepth->setValue(fastfusion_settings->max_depth);
}

racoon::rvdslam::MapperSettingsDialogFastFusion::~MapperSettingsDialogFastFusion()
{
}

void racoon::rvdslam::MapperSettingsDialogFastFusion::accept()
{
	MapperWrapperFastFusion::FastFusionSettings settings;

	settings.voxel_scale = ui.doublespinbox_voxelscale->value();
	settings.max_depth = ui.doublespinbox_maxdepth->value();

	auto settings_ptr = std::make_shared<MapperWrapperFastFusion::FastFusionSettings>(settings);

	view_manager.setMapperSettings(settings_ptr);

	done(QDialog::Accepted);
}