#pragma once

#include <Eigen/Core>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PolygonMesh.h>

#include <memory>
#include <mutex>

namespace racoon
{
	namespace rvdslam
	{
		class ViewManager;
		class FramePackage;

		/**
	     * This class manages data storage and saving of datasets to disk.
		 * It keeps track of captured frame packages and frame packages for which camera pose has already been estimated, as well as the currently reconstructed mesh.
		 * It allows for whole input datasets to be saved to disk as well as estimated camera trajectories and reconstructed 3D meshes.
		 */
		class DataManager
		{
		public:
			/** Default constructor */
			DataManager(ViewManager & view_manager);

			/** Sets the currently reconstructed polygon mesh */
			void setCurrentMesh(std::shared_ptr<pcl::PolygonMesh> mesh);

			/** Sets the currently reconstructed point cloud */
			void setCurrentPointCloud(std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloud);

			/** Adds a pointer to a captured frame package to the storage */
			void addCapturedFramePackage(std::shared_ptr<FramePackage> frame_package);

			/** Adds a pointer to a frame package with estimated poses to the storage */
			void addPoseEstimatedFramePackage(std::shared_ptr<FramePackage> frame_package);

			/** Clears the storage of captured frame packages */
			void clearCapturedFramePackages();

			/** Clears the storage of frame packages with estimated poses */
			void clearPoseEstimatedFramePackages();

			Eigen::Affine3d groundtruth_offset;

			/**
			  * Returns the offset for the groundtruth trajectory.
			  * This is meant to be used for displaying estimated vs groundtruth trajectories where the groundtruth trajectory does not start with a zero pose.
			  */
			Eigen::Affine3d getGroundtruthOffset();

			/**
			  * Saves a dataset of captured frames to disk, to be used again later as an input for the FileReader- and VirtualCamera-Driver.
			  * The dataset includes intrinsics and depth scale as metadata information.
			  * Captured frame packages are saved to the folder dirpath one by one as stored in the member variable captured_frame_packages.
			  * For each frame package, one depth image and one rgb image is saved as a png file in subfolders "depth" and "rgb".
			  */
			bool saveCapturedDataset(const std::string dirpath) const;

			/**
			  * Saves the estimated trajectory for a dataset to a text file on disk at the location given by filepath.
			  * The text file contains one quaternion per line per frame package stored in the member variable pose_estimated_frame_packages.
			  */
			bool saveEstimatedTrajectory(const std::string filepath) const;

			/**
			  * Saves the 3D mesh as stored in the member variable current_mesh to disk at the location given by filepath.
			  * File type is based on extension. Possible file types are: PLY, STL, VTK
			  */
			bool saveMesh(const std::string filepath) const;
		private:
			ViewManager & view_manager;

			std::vector<std::shared_ptr<FramePackage>> captured_frame_packages;
			std::vector<std::shared_ptr<FramePackage>> pose_estimated_frame_packages;

			std::shared_ptr<pcl::PolygonMesh> current_mesh;
			std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> current_cloud;
		};
	}
}