#include "stdafx.h"

#include "CameraFrameColor.h"

#include <opencv2/imgproc.hpp>

racoon::rvdslam::CameraFrameColor::CameraFrameColor() : CameraFrame(), color_data(NULL)
{
}

racoon::rvdslam::CameraFrameColor::CameraFrameColor(const double timestamp_secs, const unsigned int width_px, const unsigned int height_px, const EncodingType encoding_type, const FramePackage::Intrinsics intrinsics, const unsigned char * color_data) : CameraFrame(timestamp_secs, width_px, height_px, encoding_type, intrinsics)
{
	this->color_data = (unsigned char*)malloc(getWidthPx() * getHeightPx() * getNumChannels() * sizeof(unsigned char));

	std::copy(color_data, color_data + getWidthPx() * getHeightPx() * getNumChannels(), this->color_data);
}

racoon::rvdslam::CameraFrameColor::CameraFrameColor(const CameraFrameColor & color_frame) : CameraFrame(color_frame.timestamp_secs, color_frame.width_px, color_frame.height_px, color_frame.encoding_type, color_frame.intrinsics)
{
	this->color_data = (unsigned char*)malloc(getWidthPx() * getHeightPx() * getNumChannels() * sizeof(unsigned char));

	std::copy(color_frame.color_data, color_frame.color_data + getWidthPx() * getHeightPx() * getNumChannels(), this->color_data);
}

racoon::rvdslam::CameraFrameColor & racoon::rvdslam::CameraFrameColor::operator=(const CameraFrameColor & color_frame)
{
	return CameraFrameColor(color_frame);
}

racoon::rvdslam::CameraFrameColor::~CameraFrameColor()
{
	free(color_data);
}

unsigned char * racoon::rvdslam::CameraFrameColor::getDataRaw() const
{
	unsigned char* return_data = (unsigned char*)malloc(getWidthPx() * getHeightPx() * getNumChannels() * sizeof(unsigned char));

	std::copy(color_data, color_data + getWidthPx() * getHeightPx() * getNumChannels(), return_data);

	return return_data;
}

std::vector<unsigned char> racoon::rvdslam::CameraFrameColor::getDataRawVector() const
{
	std::vector<unsigned char> return_vector(color_data, color_data + getWidthPx() * getHeightPx() * getNumChannels());

	return return_vector;
}

std::vector<std::array<unsigned char, 3>> racoon::rvdslam::CameraFrameColor::getDataVector() const
{
	std::vector<std::array<unsigned char, 3>> return_vector;
	return_vector.reserve(getWidthPx() * getHeightPx());

	unsigned char* curr = color_data;
	unsigned char* end = color_data + getWidthPx() * getHeightPx() * getNumChannels();

	std::array<unsigned char, 3> color_array;

	while (curr < end)
	{
		color_array[0] = *curr;
		color_array[1] = *(curr + 1);
		color_array[2] = *(curr + 2);

		return_vector.push_back(color_array);

		curr += 3;
	}

	return return_vector;
}

cv::Mat racoon::rvdslam::CameraFrameColor::getDataCVMat() const
{
	cv::Mat return_mat;

	return_mat.create(getHeightPx(), getWidthPx(), CV_8UC3);
	std::copy(color_data, color_data + getWidthPx() * getHeightPx() * getNumChannels(), return_mat.data);

	if (getEncodingType() == EncodingType::ET_8U3_RGB)
	{
		cv::cvtColor(return_mat, return_mat, CV_RGB2BGR);
	}
	
	return return_mat;
}
