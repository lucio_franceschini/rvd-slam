#pragma once

#include "FramePackageRGBD.h"

#include <fstream>
#include <string> 
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <atomic>

namespace racoon
{
	namespace rvdslam
	{
		class FramePackageRGBD;

		/**
	     * Virtual RGBD camera class
		 *
		 * Reading an RGBD dataset from disk at a frame rate given by the frame timestamps (if possible).
		 */
		class RGBDVirtualCamera
		{
		public:
			/**
			* Constructor
			*
			* @param path - Path to dataset
			*/
			RGBDVirtualCamera(const std::string path);

			/** Destructor */
			~RGBDVirtualCamera();			

			/** 
			* Once the next frame package is read from disk, sets the passed frame package pointer reference accordingly.
			* Returns true if successful, false otherwise.
			*/
			bool getFramePackage(FramePackageRGBD *& frame_package) const;

			/**
			* In a loop, reads the next rgb and depth frame from the dataset on disk and assembles a frame package.
			* Returns true if successful, false otherwise.
			*/
			void run();

			/** Sets the quit_loop variable to true, causing the run loop to abort */
			void quit();
		private:
			/** Internal method for reading the next rgb frame from disk */
			bool readNextColorFrame();

			/** Internal method for reading the next depth frame from disk */
			bool readNextDepthFrame();

			/** Internal method for reading the next groundtruth pose from disk */
			bool readNextGroundtruthPose();

			/** Internal method for reading the intrinsic values from disk */
			bool readIntrinsics();

			/** Internal method for reading the depth scale value from disk */
			bool readDepthScale();

			FramePackageRGBD * frame_package;

			mutable std::atomic<bool> quit_loop;
			mutable std::atomic<bool> error;
			mutable std::atomic<bool> fp_ready;

			mutable std::mutex fp_mutex;

			mutable std::condition_variable fp_cv;

			unsigned char* cframe;
			unsigned short* dframe;

			unsigned int cwidth;
			unsigned int cheight;

			unsigned int dwidth;
			unsigned int dheight;

			double color_timestamp;
			double depth_timestamp;

			std::chrono::time_point<std::chrono::high_resolution_clock> color_systemtime;
			std::chrono::time_point<std::chrono::high_resolution_clock> depth_systemtime;

			double tx, ty, tz, qx, qy, qz, qw;
			double fx, fy, cx, cy;
			unsigned int depth_scale;

			mutable bool color_ready;
			mutable bool depth_ready;
			mutable bool groundtruth_ready;

			const std::string path;

			std::ifstream color_file;
			std::ifstream depth_file;
			std::ifstream groundtruth_file;
		};
	}
}